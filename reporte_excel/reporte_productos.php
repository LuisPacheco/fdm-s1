<?php
//call the autoload
require 'vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call xlsx writer class to make an xlsx file
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/*
//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();
//set the value of cell A1 to "Hello World!"
$sheet->setCellValue('A1', 'Hello World !');
*/

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();

//Importando categorias
require '../modelos/Laboratorio.php';
$lab = new Laboratorio();
//Importando productos
require "../modelos/Articulo.php";
$articulo = new Articulo();

$sql_listar_lab = $lab->listar();

$i = 0;
try{
    while($req = $sql_listar_lab->fetch_object()){
        // Create a new worksheet called "My Data"
        $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $req->nombre );

        //$myWorkSheet= $spreadsheet->getActiveSheet();
        //set the value of cell A1 to "Hello World!"
        $myWorkSheet->setCellValue('A1', "Nro.");
        $myWorkSheet->setCellValue('B1', "Cod Artículo");
        $myWorkSheet->setCellValue('C1', "Nombre");
        $myWorkSheet->setCellValue('D1', "Cantidad");
        $myWorkSheet->setCellValue('E1', "Precio");



        $sql_listar_prods_lab_x = $articulo->selectArtLab($req->idlaboratorio);
        $j = 1;
        while($req_prod = $sql_listar_prods_lab_x->fetch_object()){
            if($req_prod->cod_med != "" && $req_prod->cod_med != null){
                $myWorkSheet->setCellValue('A'. ($j+1), $j);
                $myWorkSheet->setCellValue('B'. ($j+1), utf8_decode(mb_convert_encoding($req_prod->cod_med,"UTF-8","ISO-8859-1")));
                $myWorkSheet->setCellValue('C'. ($j+1), utf8_decode(mb_convert_encoding($req_prod->nombre,"UTF-8","ISO-8859-1")));
                $myWorkSheet->setCellValue('D'. ($j+1), utf8_decode(mb_convert_encoding($req_prod->stock,"UTF-8","ISO-8859-1")));
                $myWorkSheet->setCellValue('E'. ($j+1), utf8_decode(mb_convert_encoding($req_prod->precio_venta,"UTF-8","ISO-8859-1")));

                $j++;
            }
        }

        // Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
        $spreadsheet->addSheet($myWorkSheet, $i);

    }


    //make an xlsx writer object using above spreadsheet
    $writer = new Xlsx($spreadsheet);
    //write the file in current directory
    $writer->save('reporte_articulos.xlsx');
    //redirect to the file
    echo "<meta http-equiv='refresh' content='0;url=reporte_articulos.xlsx'/>";

    //return header('Location: ' . $_SERVER['HTTP_REFERER']);
}catch(Exception $e){
    echo $e->getMessage();
}