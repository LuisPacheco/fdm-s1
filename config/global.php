<?php 
  
    /* CONSTANTES GLOBALES PARA FARMACIA MALLASA */  
    
    //Ip de la pc servidor de base de datos
    define("DB_HOST","localhost");

    //Nombre Base de datos
    define("DB_NAME", "test-s1");

    //Usuario de la base de datos
    define("DB_USERNAME", "root");

    //Contraseña del usuario de la base de datos
    define("DB_PASSWORD", "rootpct");
    
    //definimos la codificación de los caracteres
    define("DB_ENCODE","utf8");
    
    /* CONSTANTES GLOBALES PARA FARMACIA MALLASA */      


    //Definimos una constante como nombre del proyecto
    define("PRO_NOMBRE","farmacia");

   //sincronización de datos
    define("SINC_DATOS","https://pilotosiatservicios.impuestos.gob.bo/v2/FacturacionSincronizacion?wsdl");

    //factura compra venta 
    define("FACTURA_COMPRA_VENTA","https://pilotosiatservicios.impuestos.gob.bo/v2/ServicioFacturacionCompraVenta?wsdl");

    //servicio de operaciones
    define("OPERACIONES","https://pilotosiatservicios.impuestos.gob.bo/v2/FacturacionOperaciones?wsdl");

    //servicio de obtención de códigos
    define("CODIGOS","https://pilotosiatservicios.impuestos.gob.bo/v2/FacturacionCodigos?wsdl");

    //CÓDIGO AMBIENTE
    define("CODIGO_AMBIENTE",2);//2 = PRUEBAS 1=PRODUCCION

    //CÓDIGO SISTEMA
    define('CODIGO_SISTEMA', '720F5D6F851A37CA4DA2146');

    //NIT
    define("NIT",6759048013);

    //CÓDIGO MODALIDAD
    define("CODIGO_MODALIDAD",2); //2 = COMPUTARIZADA

    //CÓDIGO SUCURSAL
    define("CODIGO_SUCURSAL",1);//0 se entiende para la CENTRAL, se debe cambiar en función de la sucursal
    
    //TOKEN
    define("TOKEN","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJCRUFUUklaIiwiY29kaWdvU2lzdGVtYSI6IjcyMEY1RDZGODUxQTM3Q0E0REEyMTQ2Iiwibml0IjoiSDRzSUFBQUFBQUFBQURNek43VTBNTEV3TURRR0FCdkFRd1FLQUFBQSIsImlkIjoyOTI2NjcsImV4cCI6MTcwNjA1NDQwMCwiaWF0IjoxNjc1MjQzMjU4LCJuaXREZWxlZ2FkbyI6Njc1OTA0ODAxMywic3Vic2lzdGVtYSI6IlNGRSJ9.lptG9UNke9MxEjxL7PY0QgVENSQlFeWRX4NUs_WEksV7JRUys2CaHzrMcYzrgWb44_9wXIU3UFJ3bca3LAXsng");
  
?>
