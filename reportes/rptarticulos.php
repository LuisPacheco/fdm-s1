<?php
//Activamos el almacenamiento en el buffer
ob_start();
if (strlen(session_id()) < 1) 
  session_start();

if (!isset($_SESSION["nombre"]))
{
  echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
}
else
{
if ($_SESSION['almacen']==1)
{

//Inlcuímos a la clase PDF_MC_Table
require('PDF_MC_Table.php');
 
//Instanciamos la clase para generar el documento pdf
$pdf=new PDF_MC_Table();
 
//Agregamos la primera página al documento pdf
$pdf->AddPage();
 
//Seteamos el inicio del margen superior en 25 pixeles 
$y_axis_initial = 25;
 
//Seteamos el tipo de letra y creamos el título de la página. No es un encabezado no se repetirá
$pdf->SetFont('Arial','B',12);

$pdf->Cell(40,6,'',0,0,'C');
$pdf->Cell(100,6,'LISTA DE ARTICULOS',1,0,'C'); 
$pdf->Ln(10);
 
//Creamos las celdas para los títulos de cada columna y le asignamos un fondo gris y el tipo de letra
$pdf->SetFillColor(232,232,232); 
$pdf->SetFont('Arial','B',8);
$pdf->Cell(15,6,'Cod.',1,0,'C',1);
$pdf->Cell(25,6,'Nom. Com.',1,0,'C',1); 
$pdf->Cell(17,6,utf8_decode('Categ.'),1,0,'C',1);
$pdf->Cell(31,6,utf8_decode('Nom. Gen.'),1,0,'C',1); 
$pdf->Cell(18,6,utf8_decode('Lab.'),1,0,'C',1);
$pdf->Cell(12,6,'Stock',1,0,'C',1);
$pdf->Cell(12,6,'Min',1,0,'C',1);
$pdf->Cell(35,6,utf8_decode('Descripción'),1,0,'C',1);
$pdf->Cell(15,6,utf8_decode('P.V.'),1,0,'C',1);
 
$pdf->Ln(10);
//Comenzamos a crear las filas de los registros según la consulta mysql
require_once "../modelos/Articulo.php";
$articulo = new Articulo();

$rspta = $articulo->listar();

//Table with rows and columns
$pdf->SetWidths(array(15,25,17,31,18,12,12,35,15));

while($reg= $rspta->fetch_object()){ 
    $cod_med = $reg->cod_med; 
    $nombre = $reg->nombre_comercial;
    $categoria = $reg->categoria;
    $nombre_gen = $reg->nombre_generico;
    $laboratorio = $reg->laboratorio;
    $stock = $reg->stock;
    $stock_min = $reg->stock_minimo;
    $descripcion =$reg->descripcion;
    $precio_venta =$reg->precio_venta;

 	$pdf->SetFont('Arial','',8);
    $pdf->Row(array(utf8_decode($cod_med),utf8_decode($nombre),utf8_decode($categoria),utf8_decode($nombre_gen),utf8_decode($laboratorio),$stock,$stock_min,utf8_decode($descripcion),$precio_venta));
}
 
//Mostramos el documento pdf
$pdf->Output();

?>
<?php
}
else
{
  echo 'No tiene permiso para visualizar el reporte';
}

}
ob_end_flush();
?>