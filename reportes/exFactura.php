<?php

  require_once "../config/Conexion.php";
  function generar_pdf($id,$gen_pdf){
    
    require_once "../phpqrcode/qrlib.php";

    ob_start();

    if (strlen(session_id()) < 1) 
      session_start();

    if (!isset($_SESSION["nombre"]))
      echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
    else{
      if ($_SESSION['ventas'] == 1){

        //Incluímos el archivo Factura.php

        require('Factura.php');

        require_once "../modelos/Informacion.php";

        $reqSql= Informacion::mostrar2();

        require_once "../modelos/Venta.php";

        $venta = new Venta();

        //Recorremos todos los valores obtenidos
        $rsptav = $venta->ventacabecera($id);

        //Establecemos la configuración de la factura

        $pdf = new PDF_Invoice( 'P', 'mm', array(70,3200) );

        $pdf->AddPage();      

        $nombre_razon_social = $reqSql["razonSocialEmisor"];

        //$nombre_farmacia = $reqSql["nombre_empresa"];

        $sucursal = $reqSql["codigoSucursal"];
        $txt_sucursal = "";
        if($sucursal == 0)
          $txt_sucursal = "CASA MATRIZ";
        else
          $txt_sucursal = "SUCURSAL No. " . $sucursal;
        
        $punto_venta  = $rsptav["codigoPuntoVenta"];

        $direccion = $reqSql["direccion"];

        $telefono = $reqSql["telefono"];

        $municipio = $reqSql["municipio"];

        $tipo_factura = "FACTURA\n(CON DERECHO A CRÉDITO FISCAL)"; //SINCRONIZAR CON CATÁLOGO POSTERIORMENTE

        $nit = $reqSql["nit"];

        $cuf  = $rsptav["cuf"];

        $descripcion = $reqSql["descripcion"];

        $nro_cliente = $rsptav["num_documento"];
        $nombre_cliente = $rsptav["cliente"];
        $complemento = $rsptav["complemento"];

        $dia = $rsptav["dia"];
        $mes = $rsptav["mes"];
        $anio = $rsptav["anio"];

        $hora = $rsptav["hora"];
        $minuto = $rsptav["minuto"];
        $segundo = $rsptav["segundo"];

        if($dia < 10) $dia = "0" . $dia;
        if($mes < 10) $mes = "0" . $mes;
        if($hora < 10) $hora = "0" . $hora;
        if($minuto < 10) $minuto = "0" . $minuto;
        if($segundo < 10) $segundo = "0" . $segundo;

        $fecha_hora = "FECHA DE EMISIÓN: $dia/$mes/$anio   $hora:$minuto:$segundo";

        $nro_factura = "";
        if($rsptav["nro_venta"] != "" && $rsptav["nro_venta"] != 0){
          $nro_factura = $rsptav["nro_venta"];
        }else{
          if($rsptav["nro_venta_contingencia"] != "" && $rsptav["nro_venta_contingencia"] != 0){
            $nro_factura = $rsptav["nro_venta_contingencia"];
          }
        }

        $nombre_cliente_edit = "";
        $nombre_cliente_edit = mb_strtoupper($nombre_cliente);
        if(strpos($nombre_cliente_edit,"&QUOT;") != "")
          $nombre_cliente_edit = str_replace("&QUOT;","\"",$nombre_cliente_edit);

        if(strpos($nombre_cliente_edit,"&AMP;") != "")
          $nombre_cliente_edit = str_replace("&AMP;","&",$nombre_cliente_edit);
        
        if(strpos($nombre_cliente_edit,"&#039;") != "")
          $nombre_cliente_edit = str_replace("&#039;","'",$nombre_cliente_edit);
          

       // $y = $pdf->agregarTexto($y-2,5,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');
       // $y = $pdf->agregarTexto(1,5,'B',strtoupper(utf8_decode($nombre_farmacia)),8,66,4,'C');
        $y = $pdf->agregarTexto(1,1,'',strtoupper(utf8_decode($nombre_razon_social)),7,66,4,'C');
        $y = $pdf->agregarTexto($y-1,1,'',utf8_decode($txt_sucursal),7,66,4,'C');
        $y = $pdf->agregarTexto($y-1,1,'',utf8_decode("PUNTO DE VENTA No. " . $punto_venta),7,66,4,'C');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode($direccion)),7,66,4,'C');
        $y = $pdf->agregarTexto($y-1,1,'',strtoupper(utf8_decode("TELÉFONO: ".$telefono)),7,66,4,'C');
        $y = $pdf->agregarTexto($y-1,1,'',strtoupper(utf8_decode($municipio)),7,66,4,'C');
        $y = $pdf->agregarTexto($y-2,1,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode($tipo_factura)),8,66,4,'C');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode("NIT: ".$nit)),8,66,4,'C');
        $y = $pdf->agregarTexto($y,1,'B',strtoupper(utf8_decode("FACTURA NRO.: ". $nro_factura)),7,66,4,'C');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode("COD. AUTORIZACIÓN\n".$cuf)),7,66,4,'C');
        $y = $pdf->agregarTexto($y-2,1,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode($fecha_hora)),7,66,4,'L');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode("NIT/CI: ".$nro_cliente . "             COMPLEMENTO: " . $complemento)),7,66,4,'L');
        $y = $pdf->agregarTexto($y,1,'',iconv('UTF-8', 'windows-1252', "NOMBRE: ".$nombre_cliente_edit),7,66,4,'L');
        $y = $pdf->agregarTexto($y,1,'',strtoupper(utf8_decode("COD. CLIENTE: ".$nro_cliente)),7,66,4,'L');
        $y = $pdf->agregarTexto($y-2,1,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');
        $y = $pdf->agregarTexto($y-2,1,'B',"D E T A L L E",7,66,4,'C');


        $rsptad = $venta->ventadetalle($rsptav["idventa"]);

        $sum_desc = 0;
        
        $y++;
        while ($regd = $rsptad->fetch_object()) {      
          $nombre_art = "";
          $nombre_art = mb_strtoupper($regd->descripcion);
          if(strpos($nombre_art,"&QUOT;") != "")
            $nombre_art = str_replace("&QUOT;","\"",$nombre_art);

          if(strpos($nombre_art,"&AMP;") != "")
            $nombre_art = str_replace("&AMP;","&",$nombre_art);
          
          if(strpos($nombre_art,"&#039;") != "")
            $nombre_art = str_replace("&#039;","'",$nombre_art);

          $cod_med = $regd->cod_med;

          $y = $pdf->agregarTexto($y,1,'B',iconv('UTF-8', 'windows-1252',$cod_med . " - " .$nombre_art . " - " . $regd->nombreUnidadMedida),6.5,66,4,'L');

          $y = $pdf->agregarTexto($y-1,1,'',number_format($regd->cantidad,2),6.5,13,4,'C');

          $y = $pdf->agregarTexto($y-4,14,'',"X",6.5,5,4,'C');

          $y = $pdf->agregarTexto($y-4,19,'', number_format($regd->precio_venta,2),6.5,13,4,'C');

          $y = $pdf->agregarTexto($y-4,32,'',"-",6.5,5,4,'C');

          $y = $pdf->agregarTexto($y-4,37,'', number_format($regd->descuento,2),6.5,14,4,'C');

          $y = $pdf->agregarTexto($y-4,51,'', number_format((($regd->cantidad*$regd->precio_venta)-$regd->descuento),2),6.5,16,4,'R');

        }

        $y = $pdf->agregarTexto($y-2,1,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');

        $total_venta = (float)$rsptav["total_venta"];

        $descuento_global = number_format(0,2);

        $y = $pdf->agregarTexto($y,1,'',utf8_decode("SUBTOTAL: Bs."),7,50,4,'L');

        $y = $pdf->agregarTexto($y-4,51,'',number_format($total_venta,2),7,16,4,'R');

        $y = $pdf->agregarTexto($y,1,'',utf8_decode("DESCUENTO: Bs."),7,50,4,'L');

        $y = $pdf->agregarTexto($y-4,51,'',number_format($descuento_global,2),7,16,4,'R');

        $y = $pdf->agregarTexto($y,1,'',utf8_decode("MONTO A PAGAR: Bs."),7,50,4,'L');

        $y = $pdf->agregarTexto($y-4,51,'',number_format($total_venta,2),7,16,4,'R');

        $y = $pdf->agregarTexto($y,1,'',utf8_decode("IMPORTE BASE CRÉDITO FISCAL: Bs."),7,50,4,'L');

        $y = $pdf->agregarTexto($y-4,51,'',number_format($total_venta,2),7,16,4,'R');

        $y = $pdf->agregarTexto($y-2,1,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');
        
        $y+=2;

        require_once "Letras.php";

        $V = new EnLetras(); 

        $total_venta = $rsptav["total_venta"];

        $con_letra = $V->convertir($total_venta,"Bolivianos");

        $y = $pdf->agregarTexto($y,1,'',$con_letra,8,66,4,'C');

        $y += 5;

        $y = $pdf->agregarTexto($y,1,'',iconv('UTF-8', 'windows-1252',"“Esta factura contribuye al desarrollo del país, el uso ilícito será sancionado penalmente de acuerdo a Ley”."),7,66,4,'C');

        $y += 2;

        $y = $pdf->agregarTexto($y,1,'',utf8_decode($rsptav["leyenda"]),7,66,4,'C');     

        if($rsptav["tipo_emision"] == 1)
          $y = $pdf->agregarTexto($y+2,1,'',iconv('UTF-8', 'windows-1252',"“Este documento es la Representación Gráfica de un Documento Fiscal Digital emitido en una modalidad de facturación en línea”."),7,66,4,'C');     
        else{
          $y = $pdf->agregarTexto($y+2,1,'',iconv('UTF-8', 'windows-1252',"“Este documento es la Representación Gráfica de un Documento Fiscal Digital emitido fuera de línea, verifique su envío con su proveedor o en la página web"),7,66,4,'C');   
          $y = $pdf->agregarTexto($y,1,'U',iconv('UTF-8', 'windows-1252',"www.impuestos.gob.bo”"),7,66,4,'C');             
        }
        //"Este documento es la Representación Gráfica de un Documento Fiscal Digital emitido en una modalidad de facturación en línea" 

        $y = $pdf->agregarTexto($y,1,'',"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",7,66,4,'C');

        $pdf->addSociete2($rsptav["ruta"].$rsptav["nombre_archivo"].".png","png",$y+1);

        $y = $pdf->agregarTexto($y+40,1,'',utf8_decode("Vendedor: " . $rsptav["vendedor"]),7,66,4,'C');

        /*
        $y = $pdf->agregarTexto($y,5,'',utf8_decode("Visite el sitio:"),7,66,4,'C');

        $y = $pdf->agregarTexto($y,5,'B',utf8_decode("https://farmadivmisericordia.online/facturas"),7,66,4,'C');

        $y = $pdf->agregarTexto($y,5,'',utf8_decode("e ingrese su NIT/CI, seguido del código " . $rsptav["nombre_archivo"] . " para obtener la versión digital de su factura."),7,66,4,'C');
        */
        if($gen_pdf == 1){
          $pdf->output("F",$rsptav["ruta"].$rsptav["nombre_archivo"].".pdf");
        }else{
          $pdf->output($rsptav["nombre_archivo"].".pdf","I");
        }
      }else
        echo 'No tiene permiso para visualizar el reporte';
    }

    ob_end_flush();
  }
  

  if(isset($_GET["id"]) && isset($_GET["gen_pdf"])){
     generar_pdf($_GET["id"],$_GET["gen_pdf"]); 
  }