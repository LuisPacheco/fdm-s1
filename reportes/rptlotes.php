<?php
  //Activamos el almacenamiento en el buffer
  ob_start();
  if (strlen(session_id()) < 1) 
    session_start();
  if (!isset($_SESSION["nombre"]))
    echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
  else{
    if ($_SESSION['almacen'] == 1){

      //Inlcuímos a la clase PDF_MC_Table
      require('PDF_MC_Table.php');
       
      //Instanciamos la clase para generar el documento pdf
      $pdf = new PDF_MC_Table();
       
      //Agregamos la primera página al documento pdf
      $pdf->AddPage();
       
      //Seteamos el inicio del margen superior en 25 pixeles 
      $y_axis_initial = 25;
       
      //Seteamos el tipo de letra y creamos el título de la página. No es un encabezado no se repetirá
      $pdf->SetFont('Arial','B',12);

      $pdf->Cell(40,6,'',0,0,'C');
      $pdf->Cell(100,6,'LISTA DE LOTES',1,0,'C'); 
      $pdf->Ln(10);
       
      //Creamos las celdas para los títulos de cada columna y le asignamos un fondo gris y el tipo de letra
      $pdf->SetFillColor(232,232,232); 
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(18,6,'Fecha Reg.',1,0,'C',1); 
      $pdf->Cell(25,6,'Cod.Lote',1,0,'C',1);
      $pdf->Cell(18,6,'Venc.',1,0,'C',1);
      $pdf->Cell(25,6,'Lab.',1,0,'C',1);
      $pdf->Cell(30,6,utf8_decode('Artículo'),1,0,'C',1);
      $pdf->Cell(25,6,'Prov.',1,0,'C',1);
      $pdf->Cell(12,6,'Stock',1,0,'C',1);
      $pdf->Cell(20,6,'Est.',1,0,'C',1);
       
      $pdf->Ln(10);
      //Comenzamos a crear las filas de los registros según la consulta mysql
      require_once "../modelos/Lote.php";
      $lote = new Lote();

      $rspta = $lote->listarLotes();

      //Table with rows and columns
      $pdf->SetWidths(array(18,25,18,25,30,25,12,20));

      while($reg= $rspta->fetch_object()){  
          $fecha = $reg->fecha_registro;
          $cod_lote = $reg->cod_lote;
          if($reg->vencimiento != "0000-00-00") 
            $vencimiento = $reg->vencimiento;
          else
            $vencimiento = "n/a";
          $laboratorio = $reg->laboratorio;
          $medicamento = $reg->medicamento;
          $proveedor = $reg->proveedor;
          $stock = $reg->stock;
          $estado = $reg->estado;
       	
       	  $pdf->SetFont('Arial','',8);
          $pdf->Row(array($fecha,utf8_decode($cod_lote),$vencimiento,utf8_decode($laboratorio),utf8_decode($medicamento),utf8_decode($proveedor),$stock,utf8_decode($estado)));
      }     
      //Mostramos el documento pdf
      $pdf->Output();
    }else
      echo 'No tiene permiso para visualizar el reporte';
  }
  ob_end_flush();