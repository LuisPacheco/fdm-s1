<?php 



class EnLetras { 

  var $Void = ""; 

  var $SP = " "; 

  var $Dot = "."; 

  var $Zero = "0"; 

  var $Neg = "Menos"; 


  /* funciones para convertir los numeros a literales */

   function unidad($numuero){
      switch ($numuero){
         case 9:
         {
            $numu = "NUEVE";
            break;
         }
         case 8:
         {
            $numu = "OCHO";
            break;
         }
         case 7:
         {
            $numu = "SIETE";
            break;
         }
         case 6:
         {
            $numu = "SEIS";
            break;
         }
         case 5:
         {
            $numu = "CINCO";
            break;
         }
         case 4:
         {
            $numu = "CUATRO";
            break;
         }
         case 3:
         {
            $numu = "TRES";
            break;
         }
         case 2:
         {
            $numu = "DOS";
            break;
         }
         case 1:
         {
            $numu = "UNO";
            break;
         }
         case 0:
         {
            $numu = "";
            break;
         }
      }
      return $numu;
   }
 
   function decena($numdero){
      if ($numdero >= 90 && $numdero <= 99){
         $numd = "NOVENTA ";
         if ($numdero > 90)
            $numd = $numd."Y ".($this->unidad($numdero - 90));
      }else if ($numdero >= 80 && $numdero <= 89){
         $numd = "OCHENTA ";
         if ($numdero > 80)
            $numd = $numd."Y ".($this->unidad($numdero - 80));
      }else if ($numdero >= 70 && $numdero <= 79){
         $numd = "SETENTA ";
         if ($numdero > 70)
            $numd = $numd."Y ".($this->unidad($numdero - 70));
      }else if ($numdero >= 60 && $numdero <= 69){
         $numd = "SESENTA ";
         if ($numdero > 60)
            $numd = $numd."Y ".($this->unidad($numdero - 60));
      }else if ($numdero >= 50 && $numdero <= 59){
         $numd = "CINCUENTA ";
         if ($numdero > 50)
            $numd = $numd."Y ".($this->unidad($numdero - 50));
      }else if ($numdero >= 40 && $numdero <= 49){
         $numd = "CUARENTA ";
         if ($numdero > 40)
            $numd = $numd."Y ".($this->unidad($numdero - 40));
      }else if ($numdero >= 30 && $numdero <= 39){
         $numd = "TREINTA ";
         if ($numdero > 30)
            $numd = $numd."Y ".($this->unidad($numdero - 30));
      }else if ($numdero >= 20 && $numdero <= 29){
         if ($numdero == 20)
            $numd = "VEINTE ";
         else
            $numd = "VEINTI".($this->unidad($numdero - 20));
      }else if ($numdero >= 10 && $numdero <= 19){
         switch ($numdero){
         case 10:
         {
            $numd = "DIEZ ";
            break;
         }
         case 11:
         {
            $numd = "ONCE ";
            break;
         }
         case 12:
         {
            $numd = "DOCE ";
            break;
         }
         case 13:
         {
            $numd = "TRECE ";
            break;
         }
         case 14:
         {
            $numd = "CATORCE ";
            break;
         }
         case 15:
         {
            $numd = "QUINCE ";
            break;
         }
         case 16:
         {
            $numd = "DIECISEIS ";
            break;
         }
         case 17:
         {
            $numd = "DIECISIETE ";
            break;
         }
         case 18:
         {
            $numd = "DIECIOCHO ";
            break;
         }
         case 19:
         {
            $numd = "DIECINUEVE ";
            break;
         }
         }
      }
      else
         $numd = $this->unidad($numdero);
      return $numd;
   }
 
	function centena($numc){
		if ($numc >= 100){
			if ($numc >= 900 && $numc <= 999)
			{
				$numce = "NOVECIENTOS ";
				if ($numc > 900)
					$numce = $numce.($this->decena($numc - 900));
			}
			else if ($numc >= 800 && $numc <= 899)
			{
				$numce = "OCHOCIENTOS ";
				if ($numc > 800)
					$numce = $numce.($this->decena($numc - 800));
			}
			else if ($numc >= 700 && $numc <= 799)
			{
				$numce = "SETECIENTOS ";
				if ($numc > 700)
					$numce = $numce.($this->decena($numc - 700));
			}
			else if ($numc >= 600 && $numc <= 699)
			{
				$numce = "SEISCIENTOS ";
				if ($numc > 600)
					$numce = $numce.($this->decena($numc - 600));
			}
			else if ($numc >= 500 && $numc <= 599)
			{
				$numce = "QUINIENTOS ";
				if ($numc > 500)
					$numce = $numce.($this->decena($numc - 500));
			}
			else if ($numc >= 400 && $numc <= 499)
			{
				$numce = "CUATROCIENTOS ";
				if ($numc > 400)
					$numce = $numce.($this->decena($numc - 400));
			}
			else if ($numc >= 300 && $numc <= 399)
			{
				$numce = "TRESCIENTOS ";
				if ($numc > 300)
					$numce = $numce.($this->decena($numc - 300));
			}
			else if ($numc >= 200 && $numc <= 299)
			{
				$numce = "DOSCIENTOS ";
				if ($numc > 200)
					$numce = $numce.($this->decena($numc - 200));
			}
			else if ($numc >= 100 && $numc <= 199)
			{
				if ($numc == 100)
					$numce = "CIEN ";
				else
					$numce = "CIENTO ".($this->decena($numc - 100));
			}
		}
		else
			$numce = $this->decena($numc);
 
		return $numce;
}
 
	function miles($nummero){
		if ($nummero >= 1000 && $nummero < 2000){
			$numm = "UN MIL ".($this->centena($nummero%1000));
		}
		if ($nummero >= 2000 && $nummero <10000){
			$numm = $this->unidad(Floor($nummero/1000))." MIL ".($this->centena($nummero%1000));
		}
		if ($nummero < 1000)
			$numm = $this->centena($nummero);
 
		return $numm;
	}
 
	function decmiles($numdmero){
		if ($numdmero == 10000)
			$numde = "DIEZ MIL";
		if ($numdmero > 10000 && $numdmero <20000){
			$numde = $this->decena(Floor($numdmero/1000))."MIL ".($this->centena($numdmero%1000));
		}
		if ($numdmero >= 20000 && $numdmero <100000){
			$numde = $this->decena(Floor($numdmero/1000))." MIL ".($this->miles($numdmero%1000));
		}
		if ($numdmero < 10000)
			$numde = $this->miles($numdmero);
 
		return $numde;
	}
 
	function cienmiles($numcmero){
		if ($numcmero == 100000)
			$num_letracm = "CIEN MIL";
		if ($numcmero >= 100000 && $numcmero <1000000){
			$num_letracm = $this->centena(Floor($numcmero/1000))." MIL ".($this->centena($numcmero%1000));
		}
		if ($numcmero < 100000)
			$num_letracm = $this->decmiles($numcmero);
		return $num_letracm;
	}
 
	function millon($nummiero){
		if ($nummiero >= 1000000 && $nummiero <2000000){
			$num_letramm = "UN MILLON ".($this->cienmiles($nummiero%1000000));
		}
		if ($nummiero >= 2000000 && $nummiero <10000000){
			$num_letramm = $this->unidad(Floor($nummiero/1000000))." MILLONES ".($this->cienmiles($nummiero%1000000));
		}
		if ($nummiero < 1000000)
			$num_letramm = $this->cienmiles($nummiero);
 
		return $num_letramm;
	}
 
	function decmillon($numerodm){
		if ($numerodm == 10000000)
			$num_letradmm = "DIEZ MILLONES";
		if ($numerodm > 10000000 && $numerodm <20000000){
			$num_letradmm = $this->decena(Floor($numerodm/1000000))."MILLONES ".($this->cienmiles($numerodm%1000000));
		}
		if ($numerodm >= 20000000 && $numerodm <100000000){
			$num_letradmm = $this->decena(Floor($numerodm/1000000))." MILLONES ".($this->millon($numerodm%1000000));
		}
		if ($numerodm < 10000000)
			$num_letradmm = $this->millon($numerodm);
 
		return $num_letradmm;
	}
 
	function cienmillon($numcmeros){
		if ($numcmeros == 100000000)
			$num_letracms = "CIEN MILLONES";
		if ($numcmeros >= 100000000 && $numcmeros <1000000000){
			$num_letracms = $this->centena(Floor($numcmeros/1000000))." MILLONES ".($this->millon($numcmeros%1000000));
		}
		if ($numcmeros < 100000000)
			$num_letracms = $this->decmillon($numcmeros);
		return $num_letracms;
	}
 
	function milmillon($nummierod){
		if ($nummierod >= 1000000000 && $nummierod <2000000000){
			$num_letrammd = "MIL ".($this->cienmillon($nummierod%1000000000));
		}
		if ($nummierod >= 2000000000 && $nummierod <10000000000){
			$num_letrammd = $this->unidad(Floor($nummierod/1000000000))." MIL ".($this->cienmillon($nummierod%1000000000));
		}
		if ($nummierod < 1000000000)
			$num_letrammd = $this->cienmillon($nummierod);
 
		return $num_letrammd;
	}
 
 
function convertir($numero,$moneda){
      $num = str_replace(",","",$numero);
      $num = number_format($num,2,'.','');
      $cents = substr($num,strlen($num)-2,strlen($num)-1);
      $num = (int)$num;

      $numf = $this->milmillon($num);

   return " SON:  ".$numf." ".$cents."/100 " . $moneda;
}

















   

  function ValorEnLetras($x, $Moneda ){ 
   $s=""; 
   $Ent=""; 
   $Frc=""; 
   $Signo=""; 
        
   if(floatVal($x) < 0) 
    $Signo = $this->Neg . " "; 
   else 
    $Signo = ""; 
    
   if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
     $s = number_format($x,2,'.',''); 
   else 
     $s = number_format($x,2,'.',''); 
       
   $Pto = strpos($s, $this->Dot); 
        
   if ($Pto === false){ 
     $Ent = $s; 
     $Frc = $this->Void; 
   }else{ 
     $Ent = substr($s, 0, $Pto ); 
     $Frc =  substr($s, $Pto+1); 
   } 

   if($Ent == $this->Zero || $Ent == $this->Void) 
      $s = "Cero "; 
   elseif( strlen($Ent) > 7){ 
      $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
            "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
   }else{ 
     $s = $this->SubValLetra(intval($Ent)); 
   } 

   if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
      $s = $s . "de "; 

   if($Frc != $this->Void){ 
      $s = $s . " " . $Frc. "/100 " . $Moneda; 
      //$s = $s . " " . $Frc . "/100"; 
   } 
   $letrass=$Signo . $s; 
   return ($Signo . $s); 
} 




function SubValLetra($numero)  { 

    $Ptr=""; 

    $n=0; 

    $i=0; 

    $x =""; 

    $Rtn =""; 

    $Tem =""; 



    $x = trim("$numero"); 

    $n = strlen($x); 



    $Tem = $this->Void; 

    $i = $n; 

     

    while( $i > 0) 

    { 

       $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  

                           str_repeat($this->Zero, $i - 1 ))); 

       If( $Tem != "Cero" ) 

          $Rtn .= $Tem . $this->SP; 

       $i = $i - 1; 

    } 



     

    //--------------------- GoSub FiltroMil ------------------------------ 

    $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 

    while(1) 

    { 

       $Ptr = strpos($Rtn, "Mil ");        

       If(!($Ptr===false)) 

       { 

          If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 

            $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 

          Else 

           break; 

       } 

       else break; 

    } 



    //--------------------- GoSub FiltroCiento ------------------------------ 

    $Ptr = -1; 

    do{ 

       $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 

       if(!($Ptr===false)) 

       { 

          $Tem = substr($Rtn, $Ptr + 5 ,1); 

          if( $Tem == "M" || $Tem == $this->Void) 

             ; 

          else           

             $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 

       } 

    }while(!($Ptr === false)); 



    //--------------------- FiltroEspeciales ------------------------------ 

    $Rtn=str_replace("Diez Un", "Once", $Rtn ); 

    $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 

    $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 

    $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 

    $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 

    $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 

    $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 

    $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 

    $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 

    $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 

    $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 

    $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 

    $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 

    $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 

    $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 

    $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 

    $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 

    $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 



    //--------------------- FiltroUn ------------------------------ 

    If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 

    //--------------------- Adicionar Y ------------------------------ 

    for($i=65; $i<=88; $i++) 

    { 

      If($i != 77) 

         $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 

    } 

    $Rtn=str_replace("*", "a" , $Rtn); 

    return($Rtn); 

} 








function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) 

{ 

  $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 

} 





function Parte($x) 

{ 

    $Rtn=''; 

    $t=''; 

    $i=''; 

    Do 

    { 

      switch($x) 

      { 

         Case 0:  $t = "Cero";break; 

         Case 1:  $t = "Un";break; 

         Case 2:  $t = "Dos";break; 

         Case 3:  $t = "Tres";break; 

         Case 4:  $t = "Cuatro";break; 

         Case 5:  $t = "Cinco";break; 

         Case 6:  $t = "Seis";break; 

         Case 7:  $t = "Siete";break; 

         Case 8:  $t = "Ocho";break; 

         Case 9:  $t = "Nueve";break; 

         Case 10: $t = "Diez";break; 

         Case 20: $t = "Veinte";break; 

         Case 30: $t = "Treinta";break; 

         Case 40: $t = "Cuarenta";break; 

         Case 50: $t = "Cincuenta";break; 

         Case 60: $t = "Sesenta";break; 

         Case 70: $t = "Setenta";break; 

         Case 80: $t = "Ochenta";break; 

         Case 90: $t = "Noventa";break; 

         Case 100: $t = "Cien";break; 

         Case 200: $t = "Doscientos";break; 

         Case 300: $t = "Trescientos";break; 

         Case 400: $t = "Cuatrocientos";break; 

         Case 500: $t = "Quinientos";break; 

         Case 600: $t = "Seiscientos";break; 

         Case 700: $t = "Setecientos";break; 

         Case 800: $t = "Ochocientos";break; 

         Case 900: $t = "Novecientos";break; 

         Case 1000: $t = "Mil";break; 

         Case 1000000: $t = "Millón";break; 

      } 



      If($t == $this->Void) 

      { 

        $i = $i + 1; 

        $x = $x / 1000; 

        If($x== 0) $i = 0; 

      } 

      else 

         break; 

            

    }while($i != 0); 

    

    $Rtn = $t; 

    Switch($i) 

    { 

       Case 0: $t = $this->Void;break; 

       Case 1: $t = " Mil";break; 

       Case 2: $t = " Millones";break; 

       Case 3: $t = " Billones";break; 

    } 

    return($Rtn . $t); 

} 



} 

?>