<?php

  //Activamos el almacenamiento en el buffer

  ob_start();

  if (strlen(session_id()) < 1) 

    session_start();



  if (!isset($_SESSION["nombre"]))

    echo 'Debe ingresar al sistema correctamente para visualizar el reporte';

  else{

    if ($_SESSION['salida'] == 1){

        //Incluímos el archivo Ingreso.php
    
        require('Salida.php');



        require_once "../modelos/Informacion.php";
    
        $informacion = new Informacion();
    
        $res = $informacion->mostrar2(); 
    
    
        //Establecemos los datos de la empresa
    
        $dir = $res["direccion"];
    
        $dir = utf8_decode($dir);
    
        $arr_direccion = str_split($dir);
    
        $i = 0;
    
        $direccion_cad = "";
    
        while($i < count($arr_direccion)){
    
          $direccion_cad .= $arr_direccion[$i] . " ";
    
          $i++;
    
        }
    
    
    
        $zo = $res["zona"];
    
        $zo = utf8_decode($zo);
    
        $arr_zona = str_split($zo);
    
        $i = 0;
    
        $zona_cad = "";
    
        while($i < count($arr_zona)){
    
          $zona_cad .= $arr_zona[$i] . " ";
    
          $i++;
    
        }
    
    
    
        $ciu = $res["ciudad"];
    
        $ciu = utf8_decode($ciu);
    
        $arr_ciudad = str_split($ciu);
    
        $i = 0;
    
        $ciudad_cad = "";
    
        while($i < count($arr_ciudad)){
    
          $ciudad_cad .= $arr_ciudad[$i] . " ";
    
          $i++;
    
        }
    
    
    
        $pais = $res["pais"];
    
        $pais = utf8_decode($pais);
    
        $arr_pais = str_split($pais);
    
        $i = 0;
    
        $pais_cad = "";
    
        while($i < count($arr_pais)){
    
          $pais_cad .= $arr_pais[$i] . " ";
    
          $i++;
    
        }



      //Obtenemos los datos de la cabecera de la venta actual

      require_once "../modelos/Salida.php";

      $salida = new Salida();

      $rsptav = $salida->salidacabecera($_GET["id"]);



      //Recorremos todos los valores obtenidos

      $regv = $rsptav->fetch_object();



      //Establecemos la configuración del comprobante de entrada

        $pdf = new PDF_Invoice( 'P', 'mm', array(76,3200) );

        $pdf->AddPage();



      
        $comprobante = "COMPROBANTE DE SALIDA";
        
        $y = $pdf->agregarTexto(0,5,'B','--- '.strtoupper(utf8_decode($res["nombre_empresa"])). ' ---',8,66,4,'C');
        
        $y = $pdf->agregarTexto($y,5,'',$dir,8,66,4,'C');



        $y = $pdf->agregarTexto($y,5,'','Zona '.$zo,8,66,4,'C');



        $y = $pdf->agregarTexto($y,5,'',$ciu . " - " . $pais,8,66,4,'C');



        $y = $pdf->agregarTexto($y,5,'B',$comprobante,10,66,4,'C');
        
        
        $y = $pdf->agregarTexto($y-2,5,'B',"________________________________",10,66,2,'C');

        $y = $y+2;

        $y = $pdf->agregarTexto($y,5,'',utf8_decode($res["descripcion"]),8,66,3,'C');

        $num_comprobante = $regv->num_comprobante;

        $dia = $regv->dia;

        $mes = $regv->mes;

        $hora = $regv->hora;

        $minuto = $regv->minuto;

        $segundo = $regv->segundo;


        if($dia < 10) $dia = "0" . $dia;

        if($mes < 10) $mes = "0" . $mes;

        if($hora < 10) $hora = "0" . $hora;

        if($minuto < 10) $minuto = "0" . $minuto;

        if($segundo < 10) $segundo = "0" . $segundo;

        $y = $y + 4;
        
        $y = $pdf->agregarTexto($y-2,5,'B',"________________________________",10,66,2,'C');
        
        $y = $pdf->agregarTexto($y+2,5,'',"NRO COMPROBANTE: " . $num_comprobante,8,66,4,'C');

        $y = $pdf->agregarTexto($y-2,5,'B',"________________________________",10,66,2,'C');
        
        $y = $pdf->agregarTexto($y+3,5,'',utf8_decode("Sucursal Destino: ") . $regv->sucursal,8,66,2,'C');
        
        $y = $pdf->agregarTexto($y+1,5,'',"Fecha: ".$dia."/".$mes."/".$regv->anio. " - " .$hora.":".$minuto.":".$segundo ,8,66,4,'C');
        
        $y = $pdf->agregarTexto($y-2,5,'B',"________________________________",10,66,2,'C');

        $y++;
        
        $cols=array("CANT"=>10,

                    utf8_decode("DESCRIPCIÓN")=>25,

                    "LOTE"=>16,

                    "VCTO."=>16);

        $pdf->addCols( $cols,$y);
        
        $cols=array("CANT"=>"L",

                    utf8_decode("DESCRIPCIÓN")=>"L", 

                    "LOTE"=>"L",

                    "VCTO."=>"L");

        $y = $y+7;
        
        $rsptad = $salida->salidadetalle($_GET["id"]);
        
        while ($regd = $rsptad->fetch_object()) {
            $line = array( 

            "CANT"=> round($regd->cantidad,1),

            utf8_decode("DESCRIPCIÓN")=> utf8_decode("$regd->nombre"),

            "LOTE"=> $regd->cod_lote,

            "VCTO."=> $regd->vencimiento

          );

          $size = $pdf->addLine( $y, $line );

          $y   += $size + 2;
        }

      $pdf->Output('Comprobante_Salida','I');





    }else

      echo 'No tiene permiso para visualizar el reporte';

  }

  ob_end_flush();