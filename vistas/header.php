<?php

if (strlen(session_id()) < 1) 

  session_start();

?>

<!DOCTYPE html>

<html>

  <head>

 

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Farmacia Divina Misericordia</title>

    <!-- Tell the browser to be responsive to screen width -->

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    

    <!-- Bootstrap 3.3.5 -->

    <link rel="stylesheet" href="../public/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="../public/css/font-awesome.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins

         folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="../public/css/_all-skins.min.css">

    <link rel="apple-touch-icon" href="../files/logo/logo.png">

    <link rel="shortcut icon" href="../files/logo/logo.png">



    <!-- DATATABLES -->

    <link rel="stylesheet" type="text/css" href="../public/datatables/jquery.dataTables.min.css">    

    <link href="../public/datatables/buttons.dataTables.min.css" rel="stylesheet"/>

    <link href="../public/datatables/responsive.dataTables.min.css" rel="stylesheet"/>



    <link rel="stylesheet" type="text/css" href="../public/css/bootstrap-select.min.css">

    <link rel="stylesheet" type="text/css" href="../public/css/switch.css">

    <style>

      #loader {

        position: fixed;

        left: 0px;

        top: 0px;

        width: 100%;

        height: 100%;

        z-index: 9999;

        background: url('../files/logo/carga.gif') 50% 50% no-repeat rgb(249,249,249);

        opacity: .8;

      }

      #loader2 {
          position: fixed;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          z-index: 9999;
          background: url('../files/logo/carga.gif') 50% 50% no-repeat rgb(249,249,249);
          opacity: .8;
      }

      input[type=number]::-webkit-inner-spin-button, 

      input[type=number]::-webkit-outer-spin-button { 

        -webkit-appearance: none; 

        margin: 0; 

      }



      input[type=number] { -moz-appearance:textfield; }

    </style>
  </head>

  <body class="skin-blue sidebar-mini sidebar-collapse" ><!--onload="mostrarReloj()"-->

 
    <!--
    <div id="loader"></div>

    <div id="loader2"></div>

    -->
    
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->

        <a href="escritorio.php" class="logo">

          <!-- mini logo for sidebar mini 50x50 pixels -->

          <span class="logo-mini"><b>D M</b></span>

          <!-- logo for regular state and mobile devices -->

          <span class="logo-lg"><b>Divina Misericordia</b></span>

        </a>



        <!-- Header Navbar: style can be found in header.less -->

        <nav class="navbar navbar-static-top" role="navigation">

          <!-- Sidebar toggle button-->

          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

            <span class="sr-only">Navegación</span>

          </a>

          <!-- Navbar Right Menu -->

          <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

              <!-- Messages: style can be found in dropdown.less-->       

              <!-- User Account: style can be found in dropdown.less -->
              <li class="nav-item d-none d-sm-inline-block" style="text-align: center;">
                <a href="#" class="nav-link"><strong>MALLASA S-1</strong></a>
              </li>


              <li class="nav-item d-none d-sm-inline-block" style="text-align: center;">
                <a href="#" class="nav-link" id="hora_sistema" onclick="verif_estado_sist();">Conexión sistema <i class="fa fa-connectdevelop" aria-hidden="true"></i></a>
              </li>


              <li class="dropdown user user-menu">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="user-image" alt="User Image">

                  <span class="hidden-xs"><?php echo $_SESSION['nombre']; ?></span>

                </a>

                <ul class="dropdown-menu">

                  <!-- User image -->

                  <li class="user-header">

                    <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="img-circle" alt="User Image">

                    <p>

                      <?php echo $_SESSION['nombre']; ?>

                    </p>

                  </li>

                  

                  <!-- Menu Footer-->

                  <li class="user-footer">

                    

                    <div class="pull-right">

                      <a href="../ajax/usuario.php?op=salir" class="btn btn-default btn-flat">Cerrar</a>

                    </div>

                  </li>

                </ul>

              </li>

              

            </ul>

          </div>

        

        </nav>

      </header>



      <!-- Left side column. contains the logo and sidebar -->

      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar">       

          <!-- sidebar menu: : style can be found in sidebar.less -->

          <ul class="sidebar-menu">

            <li class="header"></li>

            <?php 

              if ($_SESSION['escritorio']==1)

              {

                echo '<li id="mEscritorio">

                <a href="escritorio.php">

                  <i class="fa fa-tasks"></i> <span>Escritorio</span>

                </a>

              </li>';

              }

            ?>

            <?php 

              if ($_SESSION['almacen'] == 1){

                  echo '<li id="mAlmacen" class="treeview">

                  <a href="#">

                    <i class="fa fa-laptop"></i>

                    <span>Almacén</span>

                    <i class="fa fa-angle-left pull-right"></i>

                  </a>

                  <ul class="treeview-menu">

                    <li id="lArticulos"><a href="articulo.php"><i class="fa fa-circle-o"></i> Artículos</a></li>

                    <li id="lCategorias"><a href="categoria.php"><i class="fa fa-circle-o"></i> Categorías</a></li>

                    <li id="lLaboratorios"><a href="laboratorio.php"><i class="fa fa-circle-o"></i> Laboratorios</a></li>

                  </ul>

                </li>';

              }

            ?>

            <?php 

            if ($_SESSION['compras']==1){

              echo '<li id="mCompras" class="treeview">

              <a href="#">

                <i class="fa fa-th"></i>

                <span>Compras</span>

                 <i class="fa fa-angle-left pull-right"></i>

              </a>

              <ul class="treeview-menu">

                <li id="lIngresos"><a href="ingreso.php"><i class="fa fa-circle-o"></i> Ingresos</a></li>

                <li id="lProveedores"><a href="proveedor.php"><i class="fa fa-circle-o"></i> Proveedores</a></li>

              </ul>

            </li>';

            }

            ?>



            <?php 

            if ($_SESSION['ventas']==1)

            {

              echo '<li id="mVentas" class="treeview">

              <a href="#">

                <i class="fa fa-shopping-cart"></i>

                <span>Ventas</span>

                 <i class="fa fa-angle-left pull-right"></i>

              </a>

              <ul class="treeview-menu">

                <li id="lVentas"><a href="venta.php"><i class="fa fa-circle-o"></i> Ventas</a></li>

                <li id="lClientes"><a href="cliente.php"><i class="fa fa-circle-o"></i> Clientes</a></li>

              </ul>

            </li>';

            }

            ?>
                 

            <?php 

            if ($_SESSION['acceso']==1)

            {

              echo '<li id="mAcceso" class="treeview">

              <a href="#">

                <i class="fa fa-folder"></i> <span>Acceso</span>

                <i class="fa fa-angle-left pull-right"></i>

              </a>

              <ul class="treeview-menu">

                <li id="lUsuarios"><a href="usuario.php"><i class="fa fa-circle-o"></i> Usuarios</a></li>

                <li id="lPermisos"><a href="permiso.php"><i class="fa fa-circle-o"></i> Permisos</a></li>

              </ul>

            </li>';

            }

            ?>



            <?php 

            if ($_SESSION['consultac']==1)

            {

              echo '<li id="mConsultaC" class="treeview">

              <a href="#">

                <i class="fa fa-bar-chart"></i> <span>Consulta Compras</span>

                <i class="fa fa-angle-left pull-right"></i>

              </a>

              <ul class="treeview-menu">

                <li id="lConsulasC"><a href="comprasfecha.php"><i class="fa fa-circle-o"></i> Consulta Compras</a></li>                

              </ul>

            </li>';

            }

            ?>



            <?php 

            if ($_SESSION['consultav']==1)

            {

              echo '<li id="mConsultaV" class="treeview">

              <a href="#">

                <i class="fa fa-bar-chart"></i> <span>Consulta Ventas</span>

                <i class="fa fa-angle-left pull-right"></i>

              </a>

              <ul class="treeview-menu">


                <li id="lConsulasVs"><a href="ventasfechaclientes.php"><i class="fa fa-circle-o"></i> Consulta Ventas Clientes</a></li>  

              </ul>

            </li>';

            }
            //<li id="lConsulasV"><a href="ventasfechacliente.php"><i class="fa fa-circle-o"></i> Consulta Ventas Cliente</a></li>                

            ?>



            <?php 

              if ($_SESSION['cajas']==1){

                echo '<li id="mCajas" class="treeview">

                <a href="#">

                  <i class="fa fa-money"></i> <span>Cajas</span>

                  <i class="fa fa-angle-left pull-right"></i>

                </a>

                <ul class="treeview-menu">

                  <li id="lCajas"><a href="registro_cajas.php"><i class="fa fa-circle-o"></i> Registro Cajas</a></li>                 

                </ul>

               </li>';

              }

            ?>



            <?php 

              if ($_SESSION['salida'] == 1 || $_SESSION['lotes'] == 1 || $_SESSION['entrada'] == 1){

                echo '<li id="mES" class="treeview">

                  <a href="#">

                    <i class="fa fa-arrows-h"></i> <span>Entrada/Salida Sucursales</span>

                    <i class="fa fa-angle-left pull-right"></i>

                  </a>';

              

                if($_SESSION['salida'] == 1 && $_SESSION['lotes'] == 1 && $_SESSION['entrada'] == 1){

                  echo '<ul class="treeview-menu"> 

                          <li id="lEntrada"><a href="entrada.php"><i class="fa fa-circle-o"></i> Entrada</a></li>       

                          <li id="lSalida"><a href="salida.php"><i class="fa fa-circle-o"></i> Salida Sucursales</a></li> 

                          <li id="lLote"><a href="lote.php"><i class="fa fa-circle-o"></i> Lotes</a></li>

                        </ul>

                      </li>';

                }else{

                  echo '<ul class="treeview-menu">';

                  if($_SESSION['entrada'] == 1 ){

                    echo '<li id="lEntrada"><a href="entrada.php"><i class="fa fa-circle-o"></i> Entrada</a></li>';

                  }

                  if($_SESSION['salida'] == 1 ){

                    echo '<li id="lSalida"><a href="salida.php"><i class="fa fa-circle-o"></i> Salida Sucursales</a></li>';

                  }

                  if($_SESSION['lotes'] == 1 ){

                    echo '<li id="lLote"><a href="lote.php"><i class="fa fa-circle-o"></i> Lotes</a></li>';

                  }

                  echo '</ul></li>';

                }

              }

            

            ?>



            <?php 

              if ($_SESSION['configuracion'] == 1 || $_SESSION['eventos_significativos'] == 1){

                echo '<li id="mConfiguracion" class="treeview">

                  <a href="#">

                    <i class="fa fa-cog"></i> <span>Configuración</span>

                    <i class="fa fa-angle-left pull-right"></i>

                  </a>

                  <ul class="treeview-menu"> ';
              }
              if($_SESSION['configuracion'] == 1){

                    echo ' <li id="lInformacion"><a href="informacion.php"><i class="fa fa-circle-o"></i> Detalles Sucursal</a></li>       
                                        
                    <li id="lToken"><a href="gestion_token.php"><i class="fa fa-circle-o"></i> Token Delegado</a></li>   

                    <li id="lPuntosVenta"><a href="punto_venta.php"><i class="fa fa-circle-o"></i> Puntos de venta</a></li>  

                    <li id="lCorrelativoVentas"><a href="correlativo_ventas.php"><i class="fa fa-circle-o"></i> Correlativo Ventas</a></li>';
              }
              if($_SESSION['eventos_significativos'] == 1){

                    echo ' <li id="lSincronizacion"><a href="sincronizacion.php"><i class="fa fa-circle-o"></i> Sincronización Catálogos</a></li>   
                    
                    <li id="lEventoSignificativo"><a href="evento_significativo.php"><i class="fa fa-circle-o"></i> Eventos Significativos</a></li>';  


              }
              
              echo '</ul></li>';

            ?>

                        

          </ul>

        </section>

        <!-- /.sidebar -->

      </aside>