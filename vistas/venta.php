<?php
  date_default_timezone_set("America/La_Paz");
  //Activamos el almacenamiento en el buffer

  ob_start();

  session_start();

  if (!isset($_SESSION["nombre"])){
    header("Location: login.html");
  }else{

    require 'header.php';

    if ($_SESSION['ventas']==1){
      
      require_once "../modelos/GestionToken.php";
      $gestion_token = new GestionToken();
      $res = $gestion_token->getTokenInfo();
    
      $id = 0; //estado del token
      $es = 0; //sistema en línea
      $cuis = 0; //verificación de CUIS activo
      $cufd = 0; //verificación de CUFD activo
      $codigo_punto_venta = ""; //verificación de codigo de Punto de Venta
      $estado_punto_venta = 0;

      $fecha_inicio_evento = "";
      $fecha_fin_evento = "";

      $hora_inicio_evento = "";
      $hora_fin_evento = "";

      $cad2_cuis = "<span style='color:red'>VENCIDO O NO HALLADO</span>";
      $cad2_cufd = "<span style='color:red'>VENCIDO O NO HALLADO</span>";

      if($res){ 
        //VERIFICAR SI EXISTE O NO ALGÚN EVENTO SIGNIFICATIVO ACTIVO PARA EL PUNTO DE VENTA
        require "../modelos/EventoSignificativo.php";
        $ev = new EventoSignificativo();
        $res_evento_actual = $ev->obtener_eventos_activos($_SESSION["id_punto_venta"]);
        //var_dump($res_evento_actual);
        if($res_evento_actual == ""){//SI NO EXISTEN EVENTOS SIGNIFICATIVOS ACTIVOS PARA EL PV, RECIÉN SE VERIFICA SI SE TIENE CONEXIÓN A INTERNET O A IMPUESTOS
          if($_SESSION["modo_sistema"] == 0){
            $verif_conexion_internet = GestionToken::verificar_conexion_internet(); //VERIFICAR SI DE INICIO HAY CONEXIÓN A INTERNET
            if($verif_conexion_internet){ //SI HAY CONEXIÓN, SE VERIFICA SI SE PUEDE CONECTAR CON EL SIN
              //$_SESSION["modo_sistema"] = 0;
              $verif_conexion_imp = GestionToken::verificar_comunicacion($res["codigo_token"]);//VERIFICAR SI DE INICIO HAY CONEXIÓN CON EL SIN
              if($verif_conexion_imp){
                //$_SESSION["modo_sistema"] = 0;

                //VERIFICAR SI EL TOKEN AUN SE ENCUENTRA VIGENTE
                $fecha = date("Y-m-d H:i:s");
                if($fecha <= $res["fecha_caducidad_token"]){
                  $token_valido = $gestion_token->validarToken($res["codigo_token"]);
                  if($token_valido)//VERIFICA SI SE CONECTA CON IMPUESTOS POR MEDIO DEL TOKEN
                    $id = $res["id_token"];
                }
                $es =  $_SESSION["modo_sistema"] ; //AQUÍ $_SESSION["modo_sistema"] = 0
              }else{
                $_SESSION["modo_sistema"] = 2;
                $es = $_SESSION["modo_sistema"]; //AQUÍ $_SESSION["modo_sistema"] = 2
              }
            }else{
              $_SESSION["modo_sistema"] = 1;
              $es = $_SESSION["modo_sistema"]; //AQUÍ $_SESSION["modo_sistema"] = 1
            }
          }else{
            $_SESSION["modo_sistema"] = 0;
            $es = 0;
            //$es = $_SESSION["modo_sistema"];//LO QUE SE TENGA EN $_SESSION["modo_sistema"] DISTINTO A 0
          }
        }else{//SI SE TIENE UN EVENTO SIGNIFICATIVO ACTIVO, SE ASIGNA EL MISMO A LA VARIABLE $_SESSION["modo_sistema"]
          $es = $res_evento_actual["codigo_evento"];

          $fecha_inicio_evento = date("Y-m-d",strtotime($res_evento_actual["fecha_inicio_evento"]));
          $fecha_fin_evento = date("Y-m-d",strtotime($res_evento_actual["fecha_fin_evento"]));
          
          $hora_inicio_evento = date("H:i:s",strtotime($res_evento_actual["fecha_inicio_evento"]));
          $hora_fin_evento = date("H:i:s",strtotime($res_evento_actual["fecha_fin_evento"]));
          
          $_SESSION["modo_sistema"] = $es;

        }
      }

      //VERIFICAR SI SE TIENE SINCRONIZACIÓN CREADAS
      require_once "../xml/SincronizacionDatos.php";
      $sinc = new SincronizacionDatos();
      $hay_sincronizacion = 0;
      if($sinc->hay_sincronizacion()->num_rows != 0){
        $hay_sincronizacion = 1;
      }
      
      //VERIFICAR SI CUIS ESTÁ ACTIVO
      require_once "../modelos/PuntoVenta.php";
      $pv = new PuntoVenta();

      if($_SESSION["id_punto_venta"]){
        $res_cuis = $pv->buscar_cuis_activo($_SESSION['id_punto_venta']);
        if($res_cuis){
          $cad = substr($res_cuis["fecha_vigencia_cuis"],0,-6);
          $cad2_cuis = str_replace("T"," ",$cad);
          $fecha_hora_actual = date("Y-m-d H:i:s.v");
          if($fecha_hora_actual < $cad2_cuis)
            $cuis = 1;
          else{
            $cad2_cuis = "<span style='color:red'>VENCIDO O NO HALLADO</span>";
          }
        }
      }

      //VERIFICAR SI CUFD ESTÁ ACTIVO
      if($_SESSION["id_punto_venta"]){
        $res_cufd = $pv->buscar_cufd_activo($_SESSION['id_punto_venta']);
        if($res_cufd){
          $cad = substr($res_cufd["fecha_vigencia_cufd"],0,-6);
          $cad2_cufd = str_replace("T"," ",$cad);
          $fecha_hora_actual = date("Y-m-d H:i:s.v");
          if($fecha_hora_actual < $cad2_cufd){
            $cufd = 1;
            $estado_punto_venta = 1;
          }else{
            $cad2_cufd = "<span style='color:red'>VENCIDO O NO HALLADO</span>";
          }
        }
      }

      //OBTENER CODIGO PV

      $punto_venta = $pv->getPuntoVenta2($_SESSION["id_punto_venta"]);
      //var_dump($punto_venta);
      if($punto_venta)
        $codigo_punto_venta = $punto_venta["codigoPuntoVenta"];

?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                      <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                        <h1 class="box-title">Venta </h1><br>
                        
                        <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true,0)" title="Nueva Venta"><i class="fa fa-plus-circle"></i></button>

                        <a href="../reportes/rptventas.php" target="_blank" id="rpt_ventas">
                          <button class="btn btn-info"><i class="fa fa-clipboard" title="Reporte Ventas"></i></button>
                        </a>

                        <button class="btn btn-success" id="abrir_caja" onclick="abrir_caja()" title="Abrir Caja"><i class="fa fa-money"></i></button>

                        <button class="btn btn-danger" id="cerrar_caja" onclick="cerrar_caja(<?php echo $_SESSION['idusuario']?>)" title="Cerrar Caja"><i class="fa fa-times-circle"></i></button>

                      </div>
                              
                      <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label for="modo_sistema"> Modalidad: </label>
                        
                        <select name="modo_sistema" id="modo_sistema" class="form-control">
                        </select>
                      </div>

                      <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <label> Vigencia CUFD </label>
                        <br>
                        <span><?php echo $cad2_cufd; ?></span>
                      </div>

                      <!--
                      <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-6">
                        <label> Renovar CUFD </label>
                        <br>
                        <button class="form-button"><i class="fa fa-refresh"></i></button>
                      </div>
                      -->

                      <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <label> Vigencia CUIS </label>
                        <br>
                        <span><?php echo $cad2_cuis; ?></span>
                      </div>

                       <!--
                      <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-6">
                        <label> Renovar CUIS </label>
                        <br>
                        <button class="form-button"><i class="fa fa-refresh"></i></button>
                      </div>
                        -->


                      <!--
                      <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <button class="btn btn-primary" title="Verificar conexión" id="verificar_conexion" onclick="verificar_conexion_internet2();">
                          Conexión sistema <i class="fa fa-connectdevelop" aria-hidden="true"></i>
                        </button>
                      </div>
                      -->

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <input type="hidden" id="id_token" value="<?php echo $id; ?>">

                    <input type="hidden" id="hay_sincronizacion" value="<?php echo $hay_sincronizacion; ?>">
              
                    <input type="hidden" id="es" name="es" value="<?php echo $es; ?>">


                    <input type="hidden" id="cuis_activo" name="cuis_activo" value="<?php echo $cuis; ?>">

                    <input type="hidden" id="cufd_activo" name="cufd_activo" value="<?php echo $cufd; ?>">

                    <input type="hidden" id="codigo_punto_venta" name="codigo_punto_venta" value="<?php echo $codigo_punto_venta; ?>">

                    <input type="hidden" id="estado_punto_venta" name="estado_punto_venta" value="<?php echo $estado_punto_venta; ?>">





                    <!--LISTA DE VENTAS-->
                    <div class="panel-body table-responsive" id="listadoregistros">

                      <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                        <thead>

                          <th>Opciones</th>

                          <th>Fecha Registro</th>

                          <th>Fecha Venta</th>

                          <th>Nro. Factura</th>

                          <th>Cliente</th>

                          <th>Nro. Documento</th>

                          <th>Usuario</th>

                          <th>Forma Pago</th>

                          <th>Total Venta</th>

                          <th>CAFC</th>

                          <th>Estado</th>

                        </thead>

                        <tfoot>

                          <th>Opciones</th>

                          <th>Fecha Registro</th>

                          <th>Fecha Venta</th>

                          <th>Nro. Factura</th>

                          <th>Cliente</th>

                          <th>Nro. Documento</th>

                          <th>Usuario</th>

                          <th>Forma Pago</th>

                          <th>Total Venta</th>

                          <th>CAFC</th>

                          <th>Estado</th>

                        </tfoot>

                      </table>

                    </div>
                    <!--LISTA DE VENTAS-->





                    <!--FORMULARIO PRINCIPAL PARA REGISTRAR VENTA Y MOSTRAR-->
                    <div class="panel-body" style="height: 100%;" id="formularioregistros">

                      <form name="formulario" id="formulario" method="POST">

                        <input type="hidden" name="evento_actual" id="evento_actual">

                        <input type="hidden" name="idventa" id="idventa">

                        <!--DEBE IR LA ID DEL EVENTO ACTUAL DEL PV SI HUBIESE-->
                        <input type="hidden" id="id_evento_significativo" name="id_evento_significativo" value="<?php if($res_evento_actual)  echo $res_evento_actual["id_evento_significativo"]; ?>">

                        <!--DEBE IR EL CUFD Y CÓDIGO DE CONTROL DEL EVENTO Si HUBIESE-->
                        <input type="hidden" name="cufd_evento" id="cufd_evento">

                        <input type="hidden" name="cod_control_evento" id="cod_control_evento">

                        <?php if($es == 5 || $es == 6 || $es == 7): ?>

                          <div class="form-group col-lg-4 col-md-4 col-sm-2 col-xs-12">

                            <label for="num_venta_contingencia">Número Factura Contingencia(*):</label>

                            <input type="text" class="form-control" name="num_venta_contingencia" id="num_venta_contingencia" minlength="1" maxlength="10" pattern="[0-9]{1,10}" title="Solo Números" required>

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">

                            <label for="idcliente">Cliente(*):</label>

                            <select id="idcliente" name="idcliente" class="form-control selectpicker" data-live-search="true" required>
                            </select>

                          </div>

                          <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">

                            <label for="fecha">Fecha(*):</label>

                            <input type="date" class="form-control" name="fecha" id="fecha" min="<?php echo $fecha_inicio_evento; ?>" max="<?php echo $fecha_fin_evento; ?>" required="">

                          </div>

                          <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">

                            <label for="hora">Hora(*):</label>

                            <input type="time" class="form-control" name="hora" id="hora" min="<?php echo $hora_inicio_evento; ?>" max="<?php echo $hora_fin_evento; ?>" step="0.001" required="">

                          </div>
                          

                        <?php else:?>

                          <div class="form-group col-lg-4 col-md-4 col-sm-2 col-xs-12">

                            <label for="num_venta">Número Factura(*):</label>

                            <input type="text" class="form-control" name="num_venta" id="num_venta" readonly maxlength="10" value="0">

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">

                            <label for="idcliente">Cliente(*):</label>

                            <select id="idcliente" name="idcliente" class="form-control selectpicker" data-live-search="true" required>
                            </select>

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-10 col-xs-12">

                            <label for="fecha_hora">Fecha(*):</label>

                            <input type="date" class="form-control" name="fecha_hora" id="fecha_hora" required="" readonly>

                          </div>

                        <?php endif; ?>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div_nuevo_cliente">
                          
                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="nombre_cliente">Nombre(*):</label>

                            <input type="text" class="form-control" name="nombre_cliente" pattern="[0-9A-Za-záéíóúÁÉÍÓÚ\x22\u00f1\u00d1“‘][ 0-9A-Za-záéíóúÁÉÍÓÚ(),“”´`‘’\x2D\x26\x22\x27\u00f1\u00d1\.]{1,}[A-Za-záéíóúÁÉÍÓÚ”’\.\x22),]"id="nombre_cliente" placeholder="Nombre Cliente" title="Debe poner un nombre con formato válido" required>
                            <!--onkeyup="this.value = this.value.toUpperCase();"  pattern="[A-Z\u00d1. \x22\x28\x29\x26]{3,} (?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{3,}-->

                          </div>


                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="documento_identidad">Tipo Documento(*):</label>

                            <select id="documento_identidad" name="documento_identidad" class="form-control selectpicker" data-live-search="true" required>
                            </select>

                          </div>


                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="nro_cliente">Nit/Ci(*):</label>

                            <input type="text" class="form-control" name="nro_cliente" id="nro_cliente" pattern="[0-9]{1,15}" minlength="1" maxlength="15" placeholder="Nro Documento" required>

                          </div>


                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="complemento">Complemento:</label>

                            <input type="text" class="form-control" name="complemento" id="complemento" pattern="[ A-Z0-9]{0,5}" placeholder="Opcional">

                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="email">Correo:</label>

                            <input type="email" class="form-control" name="email" id="email" minlength="5" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" maxlength="50" placeholder="Escriba un correo válido">

                          </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="telefono">Tel/Celular:</label>

                            <input type="text" class="form-control" name="telefono" id="telefono" minlength="7" maxlength="10" pattern="[0-9]{7,10}" placeholder="Nro de celular o teléfono">

                          </div>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                          <label for="metodo_pago">Método de Pago(*):</label>

                          <select id="metodo_pago" name="metodo_pago" class="form-control selectpicker" data-live-search="true">
                          </select>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                          <label for="nro_tarjeta">Número de tarjeta:</label>

                          <input type="text" class="form-control" name="nro_tarjeta" readonly id="nro_tarjeta" pattern="[0-9]{16}" maxlength="16" minlength="16" placeholder="Nro de tarjeta">

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div_otro_metodo_pago">
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="otro_metodo_pago">Describa método de pago:</label>

                            <input type="text" class="form-control" name="otro_metodo_pago" id="otro_metodo_pago" pattern="[A-Za-z]{1,16}" maxlength="16" placeholder="Ejm: QR">
                          </div>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                          <label for="codigo_excepcion">Código de excepción:</label>

                          <select id="codigo_excepcion" name="codigo_excepcion" class="form-control selectpicker" data-live-search="true">
                            <option value="1">Si</option>
                            <option value="0" selected>No</option>
                          </select>

                        </div>

                        <?php if($es == 5 || $es == 6 || $es == 7): ?>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="codigo_contingencia">Código de contingencia(*):</label>

                            <input type="text" class="form-control" name="codigo_contingencia" id="codigo_contingencia" readonly maxlength="20" minlength="10" required>
  
                          </div>
                        <?php endif; ?>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        </div>


                        <!--TABLAS QUE SE MUESTRAN AL CREAR UNA VENTA-->
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
  
                          <!--TABLA QUE MUESTRA EL DETALLE DE LA VENTA QUE SE ESTÁ HACIENDO-->
                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 table-responsive">

                            <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">

                              <thead style="background-color:#A9D0F5">

                                    <th>Quitar</th>

                                    <th>Artículo</th>

                                    <th>Cant&nbsp;</th>

                                    <th>Precio Venta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

                                    <th>Desc&nbsp;&nbsp;&nbsp;</th>

                                    <th>Subt</th>

                                </thead>

                                <tfoot>

                                    <th>TOTAL</th>

                                    <th></th>

                                    <th></th>

                                    <th></th>

                                    <th></th>

                                    <th><h4 id="total">Bs. 0.00</h4><input type="hidden" name="total_venta" id="total_venta"></th> 

                                </tfoot>

                                <tbody>

                                  

                                </tbody>

                            </table>

                          </div>
                          <!--TABLA QUE MUESTRA EL DETALLE DE LA VENTA QUE SE ESTÁ HACIENDO-->

                          <!--TABLA QUE MUESTRA LOS ARTÍCULOS DISPONIBLES-->
                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 table-responsive">

                            <table id="tblarticulos" class="table table-striped table-bordered table-condensed table-hover">

                              <thead>

                                  <th>Add</th>

                                  <th>Cod</th>

                                  <th>Comerc.</th>

                                  <th>Gen</th>

                                  <th>Stock</th>

                                  <th>Min</th>

                                  <th>PV</th>

                                  <th>Lab</th>

                                  <th>Cat</th>

                                  <th>Venc.| Lote </th>

                              </thead>

                              <tbody>

                                

                              </tbody>

                              <tfoot>

                                  <th>Add</th>

                                  <th>Cod</th>

                                  <th>Comerc.</th>

                                  <th>Gen</th>

                                  <th>Stock</th>

                                  <th>Min</th>

                                  <th>PV</th>

                                  <th>Lab</th>

                                  <th>Cat</th>

                                  <th>Venc.| Lote </th>

                              </tfoot>

                            </table>

                          </div>
                          <!--TABLA QUE MUESTRA LOS ARTÍCULOS DISPONIBLES-->

                        </div>
                        <!--TABLAS QUE SE MUESTRAN AL CREAR UNA VENTA-->




                        <!--TABLA QUE MUESTRA LOS DETALLES DE UNA VENTA REALIZADA-->
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">

                          <table id="detalles_show" class="table table-striped table-bordered table-condensed table-hover">

                            <thead style="background-color:#A9D0F5">

                                  <th>Nro.</th>

                                  <th>Artículo</th>

                                  <th>Cantidad</th>

                                  <th>Precio Venta</th>

                                  <th>Lote |  Vencimiento</th>

                                  <th>Descuento</th>

                                  <th>Subtotal</th>

                              </thead>


                          </table>

                        </div>
                        <!--TABLA QUE MUESTRA LOS DETALLES DE UNA VENTA REALIZADA-->



                        <br>




                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="monto_cambio">
                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                            <label for="monto">Monto (En Bs.):</label>

                            <input type="text" class="form-control" name="monto" id="monto" pattern="[0-9.]{1,7}" placeholder="Introduzca monto pagado" required>
                          </div>


                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                            <label for="cambio">Cambio (En Bs.):</label>

                            <input type="text" class="form-control" name="cambio" id="cambio" readonly>
                          </div>

                        </div>

                       

                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                          <br>
                        </div>

                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <button class="btn btn-primary btn-block" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <button id="btnCancelar" class="btn btn-danger btn-block" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                          </div>

                        </div>

                      </form>

                    </div>
                    <!--FORMULARIO PRINCIPAL PARA REGISTRAR VENTA Y MOSTRAR-->





                    <!--FORMULARIO PARA EDICIÓN DE VENTAS-->
                    <div class="panel-body" id="formularioeditar" style="height: 100%;">
                      <form name="formulario_editar" id="formulario_editar" method="POST">
                        <input type="hidden" name="evento_editar" id="evento_editar">
                        <input type="hidden" name="idventa_editar" id="idventa_editar">

                        <input type="hidden" id="id_evento_significativo_editar" name="id_evento_significativo_editar">
                        <input type="hidden" name="cufd_editar" id="cufd_editar">
                        <input type="hidden" name="cod_control_editar" id="cod_control_editar">

                        <div class="form-group col-lg-4 col-md-4 col-sm-2 col-xs-12" id="div_num_venta_contingencia_editar">

                          <label for="num_venta_contingencia_editar">Número Factura Contingencia(*):</label>

                          <input type="text" class="form-control" name="num_venta_contingencia_editar" id="num_venta_contingencia_editar" minlength="1" maxlength="10" pattern="[0-9]{1,10}" title="Solo Números" required>

                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-2 col-xs-12" id="div_num_venta_editar">

                          <label for="num_venta_editar">Número Factura(*):</label>

                          <input type="text" class="form-control" name="num_venta_editar" id="num_venta_editar" minlength="1" maxlength="10" pattern="[0-9]{1,10}" title="Solo Números" required>

                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">

                          <label for="fecha_editar">Fecha(*):</label>

                          <input type="date" class="form-control" name="fecha_editar" id="fecha_editar" required="">

                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">

                          <label for="hora_editar">Hora(*):</label>

                          <input type="time" class="form-control" name="hora_editar" id="hora_editar" step="0.001" required="">

                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">

                          <label for="idcliente_editar">Cliente(*):</label>

                          <select id="idcliente_editar" name="idcliente_editar" class="form-control selectpicker" data-live-search="true" required>
                          </select>

                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div_nuevo_cliente">
                          
                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="nombre_cliente_editar">Nombre(*):</label>

                            <input type="text" class="form-control" name="nombre_cliente_editar" pattern="[0-9A-Za-záéíóúÁÉÍÓÚ\x22\u00f1\u00d1“‘][ 0-9A-Za-záéíóúÁÉÍÓÚ(),“”´`‘’\x2D\x26\x22\x27\u00f1\u00d1\.]{1,}[A-Za-záéíóúÁÉÍÓÚ”’\.\x22),]"id="nombre_cliente_editar" onkeyup="this.value = this.value.toUpperCase();" placeholder="Nombre Cliente" required>

                          </div>


                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="documento_identidad_editar">Tipo Documento(*):</label>

                            <select id="documento_identidad_editar" name="documento_identidad_editar" class="form-control selectpicker" data-live-search="true" required>
                            </select>

                          </div>


                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="nro_cliente_editar">Nit/Ci(*):</label>

                            <input type="text" class="form-control" name="nro_cliente_editar" id="nro_cliente_editar" pattern="[0-9]{1,15}" minlength="1" maxlength="15" placeholder="Nro Documento" required>

                          </div>


                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="complemento_editar">Complemento:</label>

                            <input type="text" class="form-control" name="complemento_editar" id="complemento_editar" pattern="[ A-Z0-9]{0,5}" placeholder="Opcional">

                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="email_editar">Correo:</label>

                            <input type="email" class="form-control" name="email_editar" id="email_editar" minlength="5" maxlength="50" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Escriba un correo válido">

                          </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                            <label for="telefono_editar">Tel/Celular:</label>

                            <input type="text" class="form-control" name="telefono_editar" id="telefono_editar" minlength="7" maxlength="10" pattern="[0-9]{7,10}">

                          </div>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                          <label for="metodo_pago_editar">Método de Pago(*):</label>

                          <select id="metodo_pago_editar" class="form-control selectpicker" data-live-search="true">
                          </select>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                          <label for="nro_tarjeta_editar">Número de tarjeta:</label>

                          <input type="text" class="form-control" readonly id="nro_tarjeta_editar" placeholder="Nro de tarjeta">

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div_otro_metodo_pago_editar">
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="otro_metodo_pago_editar">Describa método de pago:</label>

                            <input type="text" class="form-control" id="otro_metodo_pago_editar" placeholder="Ejm: QR">
                          </div>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                          <label for="codigo_excepcion_editar">Código de excepción:</label>

                          <select id="codigo_excepcion_editar" name="codigo_excepcion_editar" class="form-control selectpicker" data-live-search="true">
                            <option value="1">Si</option>
                            <option value="0">No</option>
                          </select>

                        </div>


                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

                          <label for="codigo_contingencia_editar">Código de contingencia(*):</label>

                          <input type="text" class="form-control" name="codigo_contingencia_editar" id="codigo_contingencia_editar" maxlength="20" minlength="10" placeholder="Código CAFC">

                        </div>

                        



                        <!--TABLA QUE MUESTRA LOS DETALLES DE UNA VENTA REALIZADA-->
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">

                          <table id="detalles_editar" class="table table-striped table-bordered table-condensed table-hover">

                            <thead style="background-color:#A9D0F5">

                                  <th>Nro.</th>

                                  <th>Artículo</th>

                                  <th>Cantidad</th>

                                  <th>Precio Venta</th>

                                  <th>Lote |  Vencimiento</th>

                                  <th>Descuento</th>

                                  <th>Subtotal</th>

                              </thead>


                          </table>

                        </div>
                        <!--TABLA QUE MUESTRA LOS DETALLES DE UNA VENTA REALIZADA-->







                        <br>



                        
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="monto_cambio">
                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                            <label for="monto_editar">Monto (En Bs.):</label>

                            <input type="text" class="form-control" id="monto_editar" placeholder="Introduzca monto pagado" required>
                          </div>


                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                            <label for="cambio_editar">Cambio (En Bs.):</label>

                            <input type="text" class="form-control" id="cambio_editar" readonly>
                          </div>

                        </div>



                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                          <br>
                        </div>



                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <button class="btn btn-primary btn-block" type="submit" id="btnGuardarEditar"><i class="fa fa-save"></i> Guardar</button>

                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <button id="btnCancelarEditar" class="btn btn-danger btn-block" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                          </div>

                        </div>


                      </form>
                    </div>
                    <!--FORMULARIO PARA EDICIÓN DE VENTAS-->







                    <!--Fin centro -->

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->

<?php

  }else{
    require 'noacceso.php';
  }

  require 'footer.php';

?>

  <script type="text/javascript" src="scripts/venta.js"></script>

<?php 

  }

  ob_end_flush();

  require 'fin.php';

?>