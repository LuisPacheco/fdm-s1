<?php
  //Activamos el almacenamiento en el buffer
  ob_start();
  session_start();
  if (!isset($_SESSION["nombre"])){
    header("Location: login.html");
  }else{
    require 'header.php';
    if ($_SESSION['configuracion']==1){
      require_once "../modelos/GestionToken.php";
      $res = GestionToken::getTokenInfo();
      $id = 0;
      if($res){
        $fecha = date("Y-m-d H:i:s");
        if($fecha <= $res["fecha_caducidad_token"]){
          $token_valido = GestionToken::validarToken($res["codigo_token"]);
          if($token_valido)
            $id = $res["id_token"];
        }
      }

?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                          <h1 class="box-title">Detalles Sucursal</h1>

                        <div class="box-tools pull-right">

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <div class="panel-body" style="height: 100%;" id="formularioregistros">

                        <form name="formulario" id="formulario" method="POST">

                          <input type="hidden" id="id_token" value="<?php echo $id; ?>">
                      
                          <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <label>Sucursal:</label>
                            <input type="text" class="form-control" id="codigo_sucursal" readonly>
                          </div>


                          <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-10">
                            <label>Razón Social:</label>
                            <input type="text" class="form-control" id="nombre_empresa" readonly>
                          </div>

                         

                          <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12">

                            <label>Nit:</label>

                            <input type="number" class="form-control" id="nit" readonly>

                          </div>



                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <label>Dirección:</label>

                            <input type="text" class="form-control" id="direccion" readonly>

                          </div>


                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <label>Municipio:</label>

                            <input type="text" class="form-control" id="municipio" readonly>

                          </div>


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <label>Descripción:</label>

                            <br>

                            <input type="text" class="form-control" id="descripcion" readonly>

                          </div>


                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <label>Teléfono:</label>

                            <input type="text" class="form-control" name="telefono" id="telefono">

                          </div>

                         

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <label>Correo:</label>

                            <input type="email" class="form-control" name="correo" id="correo">

                          </div>


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                          </div>

                          

                        </form>

                    </div>

                    <!--Fin centro -->

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->



<?php

}

else

{

  require 'noacceso.php';

}



require 'footer.php';

?>

<script type="text/javascript" src="scripts/informacion.js"></script>

<?php 

}

ob_end_flush();

require 'fin.php';

?>