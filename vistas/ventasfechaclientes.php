<?php
date_default_timezone_set("America/La_Paz");
//Activamos el almacenamiento en el buffer

  ob_start();

  session_start();



  if (!isset($_SESSION["nombre"])){

    header("Location: login.html");

  }else{

    require 'header.php';

    if ($_SESSION['consultav']==1){

       require_once "../modelos/Consultas.php";

      $consulta = new Consultas();



?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                          <h1 class="box-title">Consulta de Ventas por fecha y clientes</h1>

                        <div class="box-tools pull-right">

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <div class="panel-body table-responsive" id="listadoregistros">

                        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12">

                          <label>Fecha Inicio</label>

                          <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12">

                          <label>Fecha Fin</label>

                          <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">                    
                            <br>
                          <button class="btn btn-success" onclick="listar()">Mostrar</button>

                        </div>

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Opciones</th>

                            <th>Fecha Registro</th>

                            <th>Fecha Venta</th>

                            <th>Nro. Factura</th>

                            <th>Cliente</th>

                            <th>Nro. Documento</th>

                            <th>Usuario</th>

                            <th>Forma Pago</th>

                            <th>Total Venta</th>

                            <th>CAFC</th>

                            <th>Estado</th>

                          </thead>

                          <tfoot>

                            <th>Opciones</th>

                            <th>Fecha Registro</th>

                            <th>Fecha Venta</th>

                            <th>Nro. Factura</th>

                            <th>Cliente</th>

                            <th>Nro. Documento</th>

                            <th>Usuario</th>

                            <th>Forma Pago</th>

                            <th>Total Venta</th>

                            <th>CAFC</th>

                            <th>Estado</th>

                          </tfoot>

                        </table>

                        <br>

                        <div id="ct">            

                        </div>

                        <div id="st">            

                        </div>

                        <div id="nul">            

                        </div>

                        <div id="dscto">            

                        </div>

                    </div>

                    

                    <!--Fin centro -->



                    <div class="panel-body" style="height: 100%;" id="formularioregistros">

                        <form name="formulario" id="formulario" method="POST">

                          <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">

                            <label>Cliente(*):</label>

                            <input type="hidden" name="idventa" id="idventa">

                            <select id="idcliente" name="idcliente" class="form-control selectpicker" data-live-search="true" required>

                                <option value="2" selected>S/N - 000</option>

                            </select>

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                            <label>Fecha(*):</label>

                            <input type="date" class="form-control" name="fecha_hora" id="fecha_hora" required="">

                          </div>



                          <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">

                            <label>Tipo Comprobante(*):</label>

                            <div class="form-check">

                              <input class="form-check-input" type="radio" name="tipo_comprobante" id="Recibo" value="Recibo" checked>

                              <label class="form-check-label" for="Recibo">

                                Recibo

                              </label>

                            </div>

                            <div class="form-check">

                              <input class="form-check-input" type="radio" name="tipo_comprobante" id="Factura" value="Factura">

                              <label class="form-check-label" for="Factura">

                                Factura

                              </label>

                            </div>

                          </div>



                          <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">

                            <label>Con tarjeta(*):</label>

                            <div class="form-check">

                              <input class="form-check-input" type="radio" name="tiene_tarjeta" id="Si" value="Si">

                              <label class="form-check-label" for="Si">

                                Si

                              </label>

                            </div>

                            <div class="form-check">

                              <input class="form-check-input" type="radio" name="tiene_tarjeta" id="No" value="No" checked>

                              <label class="form-check-label" for="No">

                                No

                              </label>

                            </div>

                          </div>



                          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">

                            <label>Número:</label>

                            <input type="text" class="form-control" name="num_comprobante" id="num_comprobante" maxlength="10" placeholder="Número" value="0">

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">

                            <label>Impuesto:</label>

                            <input type="text" class="form-control" name="impuesto" id="impuesto" value="0" required>

                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <a data-toggle="modal" href="#myModal">           

                              <button id="btnAgregarArt" type="button" class="btn btn-primary"> <span class="fa fa-plus"></span> Agregar Artículos</button>

                            </a>

                          </div>



                          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">

                            <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">

                              <thead style="background-color:#A9D0F5">

                                    <th>Nro.</th>

                                    <th>Artículo</th>

                                    <th>Cantidad</th>

                                    <th>Precio Venta</th>

                                    <th>Descuento</th>

                                    <th>Subtotal</th>

                                </thead>

                                <tfoot>

                                    <th>TOTAL</th>

                                    <th></th>

                                    <th></th>

                                    <th></th>

                                    <th></th>

                                    <th><h4 id="total">Bs. 0.00</h4><input type="hidden" name="total_venta" id="total_venta"></th> 

                                </tfoot>

                                <tbody>

                                  

                                </tbody>

                            </table>

                          </div>

                          <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="monto_cambio">
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                              <label for="monto">Monto (En Bs.):</label>

                              <input type="text" class="form-control" name="monto" id="monto" pattern="[0-9.]{1,7}" placeholder="Introduzca monto pagado" required>
                            </div>


                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                              <label for="cambio">Cambio (En Bs.):</label>

                              <input type="text" class="form-control" name="cambio" id="cambio" readonly>
                            </div>

                          </div>

                       

                          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <br>
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button id="btnCancelar" class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>

                        </form>

                    </div>

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->

<?php

  }else

    require 'noacceso.php';

  require 'footer.php';

?>

<script type="text/javascript" src="scripts/ventasfechaclientes.js"></script>

<?php 

}

ob_end_flush();

require 'fin.php';

?>