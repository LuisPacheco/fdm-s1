<?php

//Activamos el almacenamiento en el buffer

ob_start();

session_start();



if (!isset($_SESSION["nombre"]))

{

  header("Location: login.html");

}

else

{

require 'header.php';



if ($_SESSION['configuracion']==1)

{

?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                          <h1 class="box-title">Token Delegado <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Nuevo Token</button> </h1>

                        <div class="box-tools pull-right">

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <div class="panel-body table-responsive" id="listadoregistros">

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Opciones</th>

                            <th>Token Hasheado</th>

                            <th>Fecha Registro</th>

                            <th>Fecha Caducidad</th>

                            <th>Estado</th>

                          </thead>

                          <tbody>                            

                          </tbody>

                          <tfoot>

                            <th>Opciones</th>

                            <th>Token Hasheado</th>

                            <th>Fecha Registro</th>

                            <th>Fecha Caducidad</th>

                            <th>Estado</th>


                          </tfoot>

                        </table>

                    </div>

                    <div class="panel-body" style="height: 400px;" id="formularioregistros">

                        <form name="formulario" id="formulario" method="POST">

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <label>Token Delegado(*):</label>

                                <input type="text" class="form-control" name="codigo_token" id="codigo_token" maxlength="1000" placeholder="Ingrese el Token generado en el SIAT" required>

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                <label>Fecha y hora de registro(*):</label>

                                <input type="datetime-local" class="form-control" value="2022-01-01T00:00:00" step="1" name="fecha_registro_token" id="fecha_registro_token" required="">

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                <label>Fecha y hora de caducidad(*):</label>

                                <input type="datetime-local" class="form-control" value="2022-12-31T00:00:00" name="fecha_caducidad_token" id="fecha_caducidad_token" required="">

                            </div>

                        

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>



                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                          </div>

                        </form>

                    </div>

                    <!--Fin centro -->

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->

<?php

}

else

{

  require 'noacceso.php';

}



require 'footer.php';

?>

<script type="text/javascript" src="scripts/gestion_token.js"></script>

<?php 

}

ob_end_flush();

require 'fin.php';







