<?php

  //Activamos el almacenamiento en el buffer

  ob_start();

  session_start();

  if (!isset($_SESSION["nombre"]))
    header("Location: login.html");
  else{
    require 'header.php';
    if ($_SESSION['escritorio']==1){
      
      
      require_once "../modelos/Consultas.php";

      $consulta = new Consultas();

      $rsptac = $consulta->totalcomprahoy();
      $regc=$rsptac->fetch_object();
      $totalc=$regc->total_compra;


      /*
      $rsptav = $consulta->totalventahoy();//ventas del dia en gnral
      $regv=$rsptav->fetch_object();
      $totalv=$regv->total_venta;
      */

      //TOTAL VENTAS MAÑANA
      $rspta_maniana = $consulta->totalventahoy2("maniana")["total_venta"];//ventas de la mañana

      //TOTAL VENTAS TARDE
      $rspta_tarde = $consulta->totalventahoy2("tarde")["total_venta"];//ventas de la tarde

      //TOTAL VENTAS TIEMPO COMPLETO
      $rspta_tiempo_completo = $consulta->totalventahoy2("completo")["total_venta"];//ventas de la tarde

      //PAGO EN EFECTIVO
      $rspta_pago_efectivo = $consulta->total_ventas_met_pago("1")["total_venta_mp"];//ventas del dia en efectivo
      
      //PAGO CON TARJETA
      $rspta_pago_tarjeta = $consulta->total_ventas_met_pago("2")["total_venta_mp"];//ventas del dia tarjeta

      //PAGO CON TRANSFERENCIA
      $rspta_pago_transferencia = $consulta->total_ventas_met_pago("7")["total_venta_mp"];//ventas transferencia

      //PAGO MIXTO
      $rspta_pago_mixto = $consulta->total_ventas_met_pago("10")["total_venta_mp"];//ventas mixto tarjeta - efectivo
   
    /*

      $rsptav_ct = $consulta->totalventahoy_ct();//ventas del dia con tarjeta
      $regv_ct=$rsptav_ct->fetch_object();
      $totalv_ct=$regv_ct->total_venta_ct;

      $rsptav_st = $consulta->totalventahoy_st();//ventas del dia sin tarjeta
      $regv_st=$rsptav_st->fetch_object();
      $totalv_st=$regv_st->total_venta_st;
    */


      //Datos para mostrar el gráfico de barras de las compras

      $compras10 = $consulta->comprasultimos_10meses();

      $fechasc='';

      $totalesc='';

      while ($regfechac= $compras10->fetch_object()) {

        $fechasc = $fechasc.'"'.$regfechac->fecha .'",';

        $totalesc = $totalesc.$regfechac->total .','; 

      }


      //Quitamos la última coma

      $fechasc=substr($fechasc, 0, -1);

      $totalesc=substr($totalesc, 0, -1);




      //Datos para mostrar el gráfico de barras de las ventas con tarjeta
      $sql_ventas_tjt = $consulta->ventas_ultimos_sem_mp("2");

      $fechasvct='';

      $totalesvct='';

      while ($regfechav= $sql_ventas_tjt->fetch_object()) {

        $fechasvct=$fechasvct.'"'.$regfechav->fecha .'",';

        $totalesvct=$totalesvct.$regfechav->total .','; 

      }

      //Quitamos la última coma

      $fechasvct=substr($fechasvct, 0, -1);

      $totalesvct=substr($totalesvct, 0, -1);



      //Datos para mostrar el gráfico de barras de las ventas en efectivo
      $sql_ventas_efe = $consulta->ventas_ultimos_sem_mp("1");

      $fechasvst='';

      $totalesvst='';

      while ($regfechav= $sql_ventas_efe->fetch_object()) {

        $fechasvst=$fechasvst.'"'.$regfechav->fecha .'",';

        $totalesvst=$totalesvst.$regfechav->total .','; 

      }

      //Quitamos la última coma

      $fechasvst=substr($fechasvst, 0, -1);

      $totalesvst=substr($totalesvst, 0, -1);



      //Datos para mostrar el gráfico de barras de las ventas por transferencia
      $sql_ventas_tra = $consulta->ventas_ultimos_sem_mp("7");

      $fechasvtrans='';

      $totalesvtrans='';

      while ($regfechav= $sql_ventas_tra->fetch_object()) {

        $fechasvtrans=$fechasvtrans.'"'.$regfechav->fecha .'",';

        $totalesvtrans=$totalesvtrans.$regfechav->total .','; 

      }

      //Quitamos la última coma

      $fechasvtrans=substr($fechasvtrans, 0, -1);

      $totalesvtrans=substr($totalesvtrans, 0, -1);




      //Datos para mostrar el gráfico de barras de las ventas mixtas
      $sql_ventas_mix = $consulta->ventas_ultimos_sem_mp("10");

      $fechasvmix='';

      $totalesvmix='';

      while ($regfechav= $sql_ventas_mix->fetch_object()) {

        $fechasvmix=$fechasvmix.'"'.$regfechav->fecha .'",';

        $totalesvmix=$totalesvmix.$regfechav->total .','; 

      }

      //Quitamos la última coma

      $fechasvmix=substr($fechasvmix, 0, -1);

      $totalesvmix=substr($totalesvmix, 0, -1);


?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                          <h1 class="box-title">Escritorio </h1>

                        <div class="box-tools pull-right">

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <div class="panel-body">

                      <!--COMPRAS DEL DÍA-->
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-aqua">

                          <div class="inner">
                            <h4 style="font-size:17px;"><strong>Bs. <?php echo $totalc; ?></strong></h4>
                            <p>Compras del día</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="ingreso.php" class="small-box-footer">Compras <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>
                    </div>

                    <div class="panel-body">

                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-green">

                          <div class="inner">
                            <h4 style="font-size:17px;"> <strong>Bs. <?php echo $rspta_pago_efectivo; ?></strong></h4>
                            <p>Ventas en efectivo del día</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>
                      
                      

                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-teal">

                          <div class="inner">
                            <h4 style="font-size:17px;">
                              <strong>Bs. <?php echo $rspta_pago_tarjeta; ?></strong>
                            </h4>
                            <p>Ventas con tarjeta del día</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>


                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-blue">

                          <div class="inner">
                            <h4 style="font-size:17px;">
                              <strong>Bs. <?php echo $rspta_pago_transferencia; ?></strong>
                            </h4>
                            <p>Ventas por transferencia del día</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>


                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-red">

                          <div class="inner">
                            <h4 style="font-size:17px;">
                              <strong>Bs. <?php echo $rspta_pago_mixto; ?></strong>
                            </h4>
                            <p>Ventas por otros medios del día</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>

                    </div>

                    <div class="panel-body">
                       
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-green">

                          <div class="inner">
                            <h4 style="font-size:17px;"><strong>Bs <?php echo $rspta_maniana; ?></strong></h4>
                            <p>Monto Ventas Turno Mañana</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>


                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-red">

                          <div class="inner">
                            <h4 style="font-size:17px;"><strong>Bs <?php echo $rspta_tarde; ?></strong></h4>
                            <p>Monto Ventas Turno Tarde</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>


                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="small-box bg-yellow">

                          <div class="inner">
                            <h4 style="font-size:17px;"><strong>Bs <?php echo $rspta_tiempo_completo; ?></strong></h4>
                            <p>Monto Ventas Tiempo Completo</p>
                          </div>

                          <div class="icon">
                            <i class="ion ion-bag"></i>
                          </div>

                          <a href="venta.php" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>

                        </div>
                      </div>

                    </div>

                    <div class="panel-body">

                      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="box box-primary">
                          
                            <div class="box-header with-border">
                              Compras de los últimos 10 meses
                            </div>

                            <div class="box-body">
                              <canvas id="compras" width="400" height="300"></canvas>
                            </div>

                        </div>
                      </div>


                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                          <div class="box box-primary">
                              <div class="box-header with-border">
                                Ventas con tarjeta de los últimos 7 días
                              </div>

                              <div class="box-body">
                                <canvas id="ventas_ct" width="400" height="300"></canvas>
                              </div>

                          </div>
                        </div>




                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                          <div class="box box-primary">

                              <div class="box-header with-border">
                                Ventas en efectivo de los últimos 7 días
                              </div>

                              <div class="box-body">
                                <canvas id="ventas_st" width="400" height="300"></canvas>
                              </div>

                          </div>
                        </div>




                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                          <div class="box box-primary">

                              <div class="box-header with-border">
                                Ventas por transferencia de los últimos 7 días
                              </div>

                              <div class="box-body">
                                <canvas id="ventas_tra" width="400" height="300"></canvas>
                              </div>

                          </div>
                        </div>



                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                          <div class="box box-primary">

                              <div class="box-header with-border">
                                Otros métodos de pago de los últimos 7 días
                              </div>

                              <div class="box-body">
                                <canvas id="ventas_mix" width="400" height="300"></canvas>
                              </div>

                          </div>
                        </div>

                    </div>

                    <!--Fin centro -->

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->

<?php

}else

  require 'noacceso.php';

require 'footer.php';

?>



<script src="../public/js/chart.min.js"></script>

<script src="../public/js/Chart.bundle.min.js"></script> 

<script type="text/javascript">

      var ctx = document.getElementById("compras").getContext('2d');

      var compras = new Chart(ctx, {

          type: 'bar',

          data: {

              labels: [<?php echo $fechasc; ?>],

              datasets: [{

                  label: 'Monto en Bs',

                  data: [<?php echo $totalesc; ?>],

                  backgroundColor: [

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)',

                      'rgba(153, 102, 255, 0.2)',

                      'rgba(255, 159, 64, 0.2)',

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)'

                  ],

                  borderColor: [

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)',

                      'rgba(153, 102, 255, 1)',

                      'rgba(255, 159, 64, 1)',

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)'

                  ],

                  borderWidth: 1

              }]

          },

          options: {

              scales: {

                  yAxes: [{

                      ticks: {

                          beginAtZero:true

                      }

                  }]

              }

          }

      });


      
      var ctx = document.getElementById("ventas_ct").getContext('2d');

      var ventas = new Chart(ctx, {

          type: 'bar',

          data: {

              labels: [<?php echo $fechasvct; ?>],

              datasets: [{

                  label: 'Monto en Bs',

                  data: [<?php echo $totalesvct; ?>],

                  backgroundColor: [

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)',

                      'rgba(153, 102, 255, 0.2)',

                      'rgba(255, 159, 64, 0.2)',

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)'

                  ],

                  borderColor: [

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)',

                      'rgba(153, 102, 255, 1)',

                      'rgba(255, 159, 64, 1)',

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)'

                  ],

                  borderWidth: 1

              }]

          },

          options: {

              scales: {

                  yAxes: [{

                      ticks: {

                          beginAtZero:true

                      }

                  }]

              }

          }

      });
      

      
      var ctx = document.getElementById("ventas_st").getContext('2d');

      var ventas = new Chart(ctx, {

          type: 'bar',

          data: {

              labels: [<?php echo $fechasvst; ?>],

              datasets: [{

                  label: 'Monto en Bs',

                  data: [<?php echo $totalesvst; ?>],

                  backgroundColor: [

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)',

                      'rgba(153, 102, 255, 0.2)',

                      'rgba(255, 159, 64, 0.2)',

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)'

                  ],

                  borderColor: [

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)',

                      'rgba(153, 102, 255, 1)',

                      'rgba(255, 159, 64, 1)',

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)'

                  ],

                  borderWidth: 1

              }]

          },

          options: {

              scales: {

                  yAxes: [{

                      ticks: {

                          beginAtZero:true

                      }

                  }]

              }

          }

      });





      var ctx = document.getElementById("ventas_tra").getContext('2d');

      var ventas = new Chart(ctx, {

          type: 'bar',

          data: {

              labels: [<?php echo $fechasvtrans; ?>],

              datasets: [{

                  label: 'Monto en Bs',

                  data: [<?php echo $totalesvtrans; ?>],

                  backgroundColor: [

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)',

                      'rgba(153, 102, 255, 0.2)',

                      'rgba(255, 159, 64, 0.2)',

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)'

                  ],

                  borderColor: [

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)',

                      'rgba(153, 102, 255, 1)',

                      'rgba(255, 159, 64, 1)',

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)'

                  ],

                  borderWidth: 1

              }]

          },

          options: {

              scales: {

                  yAxes: [{

                      ticks: {

                          beginAtZero:true

                      }

                  }]

              }

          }

      });








      var ctx = document.getElementById("ventas_mix").getContext('2d');

      var ventas = new Chart(ctx, {

          type: 'bar',

          data: {

              labels: [<?php echo $fechasvmix; ?>],

              datasets: [{

                  label: 'Monto en Bs',

                  data: [<?php echo $totalesvmix; ?>],

                  backgroundColor: [

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)',

                      'rgba(153, 102, 255, 0.2)',

                      'rgba(255, 159, 64, 0.2)',

                      'rgba(255, 99, 132, 0.2)',

                      'rgba(54, 162, 235, 0.2)',

                      'rgba(255, 206, 86, 0.2)',

                      'rgba(75, 192, 192, 0.2)'

                  ],

                  borderColor: [

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)',

                      'rgba(153, 102, 255, 1)',

                      'rgba(255, 159, 64, 1)',

                      'rgba(255,99,132,1)',

                      'rgba(54, 162, 235, 1)',

                      'rgba(255, 206, 86, 1)',

                      'rgba(75, 192, 192, 1)'

                  ],

                  borderWidth: 1

              }]

          },

          options: {

              scales: {

                  yAxes: [{

                      ticks: {

                          beginAtZero:true

                      }

                  }]

              }

          }

      });
      

</script>









<?php 

}

ob_end_flush();



?>