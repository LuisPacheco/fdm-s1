<?php

//Activamos el almacenamiento en el buffer

ob_start();

session_start();



if (!isset($_SESSION["nombre"]))

{

  header("Location: login.html");

}

else

{

require 'header.php';



if ($_SESSION['configuracion']==1){

  require_once "../modelos/GestionToken.php";
  $gestion_token = new GestionToken();
  $res = $gestion_token->getTokenInfo();
  $id = 0;
  if($res){
    $fecha = date("Y-m-d H:i:s");
    if($fecha <= $res["fecha_caducidad_token"]){
      $token_valido = $gestion_token->validarToken($res["codigo_token"]);
      if($token_valido)
        $id = $res["id_token"];
    }
  }

  //validar si cuis de PV0 está activo
  require_once "../modelos/PuntoVenta.php";
  $pv = new PuntoVenta();
  $respv0 = $pv->getPuntoVenta(0);
  $hay_cuis = 0;
  if($respv0){
    $id_pv0 = $respv0["id_punto_venta"];
    $id_pv_session_actual = $_SESSION["id_punto_venta"];

    $res_cuis = $pv->buscar_cuis_activo($respv0["id_punto_venta"]);
    if($res_cuis){
      $hay_cuis = 1;
    }
  }
?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                          <h1 class="box-title">Puntos de venta <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button> </h1>

                        <div class="box-tools pull-right">

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <input type="hidden" id="id_token" value="<?php echo $id; ?>">
                    <input type="hidden" id="cuispv0" value="<?php echo $hay_cuis; ?>">

                    <input type="hidden" id="id_pv0" value="<?php echo $id_pv0; ?>">
                    <input type="hidden" id="id_pv_session_actual" value="<?php echo $id_pv_session_actual; ?>">

                    <div class="panel-body table-responsive" id="listadoregistros">

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Opciones</th>

                            <th>Código</th>

                            <th>Nombre</th>

                            <th>Tipo</th>

                            <th>CUIS Vigente</th>

                            <th>CUFD Vigente</th>

                            <th>Estado</th>

                          </thead>

                          <tbody>                            

                          </tbody>

                          <tfoot>

                            <th>Opciones</th>

                            <th>Código</th>

                            <th>Nombre</th>

                            <th>Tipo</th>

                            <th>CUIS Vigente</th>

                            <th>CUFD Vigente</th>

                            <th>Estado</th>

                          </tfoot>

                        </table>

                    </div>

                    <div class="panel-body" style="height: 400px;" id="formularioregistros">

                        <form name="formulario" id="formulario" method="POST">

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                            <label>Nombre Punto de Venta:</label>

                            <input type="text" class="form-control" name="nombrePuntoVenta" id="nombrePuntoVenta" maxlength="50" placeholder="Nombre de Punto de Venta" required>

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                            <label>Descripción Punto de Venta:</label>

                            <input type="text" class="form-control" name="descripcion" id="descripcion" maxlength="256" placeholder="Descripción">

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                            <label>Tipo de Punto de venta:</label>

                           <select name="codigoTipoPuntoVenta" id="codigoTipoPuntoVenta" class="form-control selectpicker" data-live-search="true" required>></select>

                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                          </div>

                        </form>

                    </div>

                    <!--Fin centro -->

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->

<?php

}

else

{

  require 'noacceso.php';

}



require 'footer.php';

?>

<script type="text/javascript" src="scripts/punto_venta.js"></script>

<?php 

}

ob_end_flush();

require 'fin.php';