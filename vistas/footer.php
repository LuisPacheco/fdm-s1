    <footer class="main-footer">

        

        <strong>Farmacia Divina Misericordia - <?php echo date("Y"); ?></strong> 

    </footer> 

    

    <script>

        /*
       window.onload = function(){

           var contenedor = document.getElementById("loader");

           contenedor.style.visibility = "hidden";

           contenedor.style.opacity = '0';




           var contenedor_submit = document.getElementById("loader2");

           contenedor_submit.style.visibility = "hidden";

           contenedor_submit.style.opacity = '0';

       }
       */

    </script>



    <!-- jQuery -->

    <script src="../public/js/jquery-3.1.1.min.js"></script>

    <!-- Bootstrap 3.3.5 -->

    <script src="../public/js/bootstrap.min.js"></script>

    <!-- AdminLTE App -->

    <script src="../public/js/app.min.js"></script>



    <!-- DATATABLES -->

    <script src="../public/datatables/jquery.dataTables.min.js"></script>    

    <script src="../public/datatables/dataTables.buttons.min.js"></script>

    <script src="../public/datatables/buttons.html5.min.js"></script>

    <script src="../public/datatables/buttons.colVis.min.js"></script>

    <script src="../public/datatables/jszip.min.js"></script>

    <script src="../public/datatables/pdfmake.min.js"></script>

    <script src="../public/datatables/vfs_fonts.js"></script> 



    <script src="../public/js/bootbox.min.js"></script> 

    <script src="../public/js/bootstrap-select.min.js"></script>


    <script>
        
        function verif_comunicacion_imp(){
     
            detalles = $.ajax({
                url: "../ajax/venta.php?op=verificar_comunicacion2", 
                dataType: 'text',
                async: false     
            }).responseText;
            
            return detalles;
        }

        function verif_conexion_internet(){

            detalles = $.ajax({
                url: "../ajax/venta.php?op=verificar_conexion_internet2", 
                dataType: 'text',
                async: false     
            }).responseText;
            
            return detalles;
        }

        function verif_estado_sist(){
            var res = "";

            //console.log(r);
            if(verif_conexion_internet()){
                res += "<i class='fa fa-check-circle' aria-hidden='true'></i> Conexión a internet correcta<br>";
                if(verif_comunicacion_imp())
                    res += "<i class='fa fa-check-circle' aria-hidden='true'></i> Conexión a impuestos correcta";
                else
                    res += "<i class='fa fa-times-circle' aria-hidden='true'></i> Error de conexión a impuestos";
                
            }else
                res += "<i class='fa fa-times-circle' aria-hidden='true'></i> Error de conexión a internet<br><i class='fa fa-times-circle' aria-hidden='true'></i> Error de conexión a impuestos";
            bootbox.alert(res);
        }
    </script>
    