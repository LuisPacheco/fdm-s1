<?php

  //Activamos el almacenamiento en el buffer

  ob_start();
  
  session_start();

  if (!isset($_SESSION["nombre"])){
    header("Location: login.html");
  }else{

    require 'header.php';

    if ($_SESSION['eventos_significativos']==1){

      require_once "../modelos/GestionToken.php";
      $gestion_token = new GestionToken();
      $res = $gestion_token->getTokenInfo();
      $id = 0;
      if($res){
        $fecha = date("Y-m-d H:i:s");
        if($fecha <= $res["fecha_caducidad_token"]){
          $token_valido = $gestion_token->validarToken($res["codigo_token"]);
          if($token_valido)
            $id = $res["id_token"];
        }
      }

      //validar si cuis de PV0 está activo
      require_once "../modelos/PuntoVenta.php";
      $pv = new PuntoVenta();
      $hay_cuis = 0;
      $id_pv_session_actual = $_SESSION["id_punto_venta"];

      $res_cuis = $pv->buscar_cuis_activo($id_pv_session_actual);
      if($res_cuis)
        $hay_cuis = 1;      
  ?>

  <!--Contenido-->

        <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">        

          <!-- Main content -->

          <section class="content">

              <div class="row">

                <div class="col-md-12">

                    <div class="box">

                      <div class="box-header with-border">

                            <h1 class="box-title">Eventos Significativos <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button> </h1>

                          <div class="box-tools pull-right">

                          </div>

                      </div>

                      <!-- /.box-header -->

                      <!-- centro -->

                      <input type="hidden" id="id_token" value="<?php echo $id; ?>">
                      <input type="hidden" id="cuispv" value="<?php echo $hay_cuis; ?>">
                      <input type="hidden" id="id_pv_session_actual" value="<?php echo $id_pv_session_actual; ?>">
                      

                      <!-- centro -->

                      <div class="panel-body table-responsive" id="listadoregistros">

                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                          <label>Punto de venta</label>

                          <select name="id_punto_venta" class="form-control" id="id_punto_venta"></select>

                        </div>

                        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12">

                          <label>Fecha Evento</label>

                          <input type="date" class="form-control" name="fecha_evento" id="fecha_evento" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">       
                          <br>             

                          <button class="btn btn-success" onclick="listar()">Mostrar</button>

                        </div>

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Opciones</th>

                            <th>Paquetes</th>

                            <th>Fecha Registro</th>

                            <th>Código Evento</th>

                            <th>Descripción Evento</th>

                            <th>Fecha Inicio Evento</th>

                            <th>Fecha Fin Evento</th>
                          
                            <th>CAFC Asociado</th>
                            
                            <th>Código Recepción Evento</th>

                            <th>Código Recepción Paquete</th>

                          </thead>

                          <tfoot>

                            <th>Opciones</th>

                            <th>Paquetes</th>

                            <th>Fecha Registro</th>

                            <th>Código Evento</th>

                            <th>Descripción Evento</th>

                            <th>Fecha Inicio Evento</th>

                            <th>Fecha Fin Evento</th>
                          
                            <th>CAFC Asociado</th>

                            <th>Código Recepción Evento</th>

                            <th>Código Recepción Paquete</th>

                          </tfoot>

                        </table>

      
                      </div>

                      

                      <div class="panel-body" style="height: 500px;" id="formularioregistros">

                          <form name="formulario" id="formulario" method="POST">

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                              <label>Seleccione Evento Significativo(*)</label>

                              <select name="evento_significativo" class="form-control" id="evento_significativo" required></select>

                              <input type="hidden" name="descripcion_evento" id="descripcion_evento">

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                              <label>Código CAFC(*)</label>

                              <input type="text" class="form-control" name="cafc" id="cafc" pattern="[0-9A-Z]{10,20}" maxlength="20" minlength="10" placeholder="Agregar Cod. Contingencia CAFC" required>

                            </div>
                            

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                              <label>Fecha Inicio Evento(*)</label>

                              <input type="date" class="form-control" name="fecha_inicio_evento" max="<?php echo date("Y-m-d")?>" id="fecha_inicio_evento" required/>

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                              <label>Hora Inicio Evento(*)</label>

                              <input type="time" class="form-control" name="hora_inicio_evento" id="hora_inicio_evento" step="0.001" required/>

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                              <label>Fecha Finalización Evento(*)</label>

                              <input type="date" class="form-control" name="fecha_fin_evento" id="fecha_fin_evento" required/>

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                              <label>Hora Finalización Evento(*)</label>

                              <input type="time" class="form-control" name="hora_fin_evento" id="hora_fin_evento" step="0.0001" required/>

                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                              <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                              <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                            </div>
                            <!--

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                              <label>Descripción Punto de Venta:</label>

                              <input type="text" class="form-control" name="descripcion" id="descripcion" maxlength="256" placeholder="Descripción">

                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                              <label>Tipo de Punto de venta:</label>

                            <select name="codigoTipoPuntoVenta" id="codigoTipoPuntoVenta" class="form-control selectpicker" data-live-search="true" required>></select>

                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                              <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                              <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                            </div>

                            -->

                          </form>

                      </div>

                      


                      <!--Fin centro -->

                    </div><!-- /.box -->

                </div><!-- /.col -->

            </div><!-- /.row -->

        </section><!-- /.content -->



      </div><!-- /.content-wrapper -->

    <!--Fin-Contenido-->

  <?php

  }

  else

  {

    require 'noacceso.php';

  }



  require 'footer.php';

  ?>

  <script type="text/javascript" src="scripts/evento_significativo.js"></script>

  <?php 

  }

  ob_end_flush();

  require 'fin.php';