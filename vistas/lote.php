<?php
  //Activamos el almacenamiento en el buffer
  ob_start();
  session_start();

  if (!isset($_SESSION["nombre"]))
    header("Location: login.html");
  else{
    require 'header.php';
    if ($_SESSION['lotes']==1){
?>

      <!--Contenido-->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">        
          <!-- Main content -->
          <section class="content">
              <div class="row">

                <div class="col-md-12">

                    <div class="box">

                      <div class="box-header with-border">
                            <h1 class="box-title">Lotes  
                              <!--
                              <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button>-->
                               <!--VERIFICAR EL REPORTE DE LOTES, CAMBIARLO!!!-->
                              <a href="../reportes/rptlotes.php" target="_blank">
                                <button class="btn btn-info"><i class="fa fa-clipboard"></i> Reporte</button>
                              </a>
                            </h1>
                          <div class="box-tools pull-right">
                          </div>
                      </div>
                      <!-- /.box-header -->

                      <!-- centro -->
                      <div class="panel-body table-responsive" id="listadoregistros">
                          <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                            <thead>
                              <th>Fecha Reg.</th>
                              <th>Nro. Lote</th>
                              <th>Venc.</th>
                              <th>Lab.</th>
                              <th>Cod.</th>
                              <th>Artículo</th>
                              <th>Stock</th>
                              <th>Estado</th>
                              <th>Visibilidad</th>
                              <th>Opciones</th>
                            </thead>
                            <tbody>                            
                            </tbody>
                            <tfoot>
                              <th>Fecha Reg.</th>
                              <th>Nro. Lote</th>
                              <th>Venc.</th>
                              <th>Lab.</th>
                              <th>Cod.</th>
                              <th>Artículo</th>
                              <th>Stock</th>
                              <th>Estado</th>
                              <th>Visibilidad</th>
                              <th>Opciones</th>
                            </tfoot>

                             
                          </table>
                      </div>

                      <div class="panel-body" style="height: 100%;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          <div class="form-group col-lg-8 col-md-8 col-sm-6 col-xs-12">
                            <label>Código de lote(*):</label>
                            <input type="hidden" name="idlote" id="idlote">
                            <input type="hidden" name="cod_lote_ant" id="cod_lote_ant">
                            <input type="text" class="form-control" name="cod_lote" id="cod_lote" maxlength="150" placeholder="Código" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Fecha de Vencimiento:</label>
                            <input type="date" class="form-control" name="vencimiento" id="vencimiento">
                            <input type="hidden" name="vencimiento_ant" id="vencimiento_ant">
                          </div>
                          
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Laboratorio(*):</label>
                            <select id="idlaboratorio" name="idlaboratorio" class="form-control selectpicker" data-live-search="true" required></select>
                            <input type="hidden" name="idlaboratorio_ant" id="idlaboratorio_ant">
                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Artículo(*):</label>
                            <select id="idarticulo" name="idarticulo" class="form-control selectpicker" data-live-search="true" required></select>
                            <input type="hidden" name="idarticulo_ant" id="idarticulo_ant">
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Stock(*):</label>
                            <input type="number" class="form-control" min=0 step="0.01" name="stock" id="stock" required>
                            <input type="hidden" name="stock_ant" id="stock_ant">
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button id="btnCancelar" class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                      </div>
                      <!--Fin centro -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->
    <!--Fin-Contenido-->

<?php
    }else
      require 'noacceso.php';
    require 'footer.php';
?>
<script type="text/javascript" src="scripts/lote.js"></script>
<?php 
  }
  ob_end_flush();
  require 'fin.php';
?>