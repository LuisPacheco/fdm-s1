<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div_nuevo_cliente">
                          
    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

    <label for="nombre_cliente">Nombre(*):</label>

    <input type="text" class="form-control" name="nombre_cliente" pattern="[A-Z\u00d1. \x22\x28\x29\x26]{3,}"id="nombre_cliente" onkeyup="this.value = this.value.toUpperCase();" placeholder="Nombre Cliente" required>

    </div>


    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

    <label for="documento_identidad">Tipo Documento(*):</label>

    <select id="documento_identidad" name="documento_identidad" class="form-control selectpicker" data-live-search="true" required>
    </select>

    </div>


    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

    <label for="nro_cliente">Nit/Ci(*):</label>

    <input type="text" class="form-control" name="nro_cliente" id="nro_cliente" pattern="[0-9]{1,15}" minlength="1" maxlength="15" placeholder="Nro Documento" required>

    </div>


    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

    <label for="complemento">Complemento:</label>

    <input type="text" class="form-control" name="complemento" id="complemento" pattern="[ A-Z0-9]{0,5}" placeholder="Opcional">

    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>

    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

    <label for="email">Correo:</label>

    <input type="email" class="form-control" name="email" id="email" minlength="5" maxlength="50" placeholder="Escriba un correo válido">

    </div>

    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">

    <label for="telefono">Tel/Celular:</label>

    <input type="text" class="form-control" name="telefono" id="telefono" minlength="7" maxlength="10" pattern="[0-9]{7,10}">

    </div>

</div>