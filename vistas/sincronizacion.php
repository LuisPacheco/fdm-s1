<?php

  //Activamos el almacenamiento en el buffer

  ob_start();

  session_start();



  if (!isset($_SESSION["nombre"])){

    header("Location: login.html");

  }else{

    require 'header.php';



  if ($_SESSION['eventos_significativos']==1){
    require_once "../modelos/GestionToken.php";
    $gestion_token = new GestionToken();
    $res = $gestion_token->getTokenInfo();
    $id = 0;
    if($res){
      $fecha = date("Y-m-d H:i:s");
      if($fecha <= $res["fecha_caducidad_token"]){
        $token_valido = $gestion_token->validarToken($res["codigo_token"]);
        if($token_valido)
          $id = $res["id_token"];
      }
    }

?>

<!--Contenido-->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

                <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                        <h1 class="box-title">Sincronización Catálogos 
                            <br>
                            <form action="POST" id="formulario">
                                <button class="btn btn-success" id="btnagregar"><i class="fa fa-plus-circle"></i> Generar Sincronización</button>  
                            </form>
                        </h1>

                    </div>

                    <!-- /.box-header -->



                    <!-- centro -->

                    <input type="hidden" id="id_token" value="<?php echo $id; ?>">

                    <div class="panel-body table-responsive" id="listadoregistros">

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Opciones</th>

                            <th>Fecha</th>

                            <th>Fecha y hora</th>

                          </thead>

                          <tbody>                            

                          </tbody>

                          <tfoot>

                            <th>Opciones</th>

                            <th>Fecha</th>

                            <th>Fecha y hora</th>

                          </tfoot>

                        </table>

                    </div>

                    <!--Fin centro -->

                  </div><!-- /.box -->

                </div><!-- /.col -->

            </div><!-- /.row -->

        </section><!-- /.content -->



    </div><!-- /.content-wrapper -->
    

    <!--Fin-Contenido-->



<?php

}

else

{

  require 'noacceso.php';

}



require 'footer.php';

?>

<script type="text/javascript" src="scripts/sincronizacion.js"></script>

<?php 

}

ob_end_flush();

require 'fin.php';

?>