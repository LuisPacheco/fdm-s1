<?php
date_default_timezone_set("America/La_Paz");
//Activamos el almacenamiento en el buffer

ob_start();

session_start();



if (!isset($_SESSION["nombre"])){
  header("Location: login.html");
}

else

{

require 'header.php';



if ($_SESSION['ventas']==1)

{

?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

            <div class="row">

              <div class="col-md-12">

                  <div class="box">

                    <div class="box-header with-border">

                          

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                          <h1 class="box-title">Registro de cajas</h1>

                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-8 col-xs-12">

                          <label>Fecha</label>

                          <input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                        <div class="form-inline col-lg-12 col-md-12 col-sm-12 col-xs-12">                    

                          <button class="btn btn-success" onclick="listar()">Mostrar</button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <!-- centro -->

                    <div class="panel-body table-responsive" id="listadoregistros">

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Visualizar</th>

                            <th>Fecha</th>

                            <th>Usuario</th>

                            <th>Hora Apertura</th>

                            <th>Hora Cierre</th>

                            <th>Turno</th>

                            <th>Estado</th>

                          </thead>

                          <tbody>                            

                          </tbody>

                          <tfoot>

                          <th>Visualizar</th>

                            <th>Fecha</th>

                            <th>Usuario</th>

                            <th>Hora Apertura</th>

                            <th>Hora Cierre</th>

                            <th>Turno</th>

                            <th>Estado</th>

                          </tfoot>

                        </table>

                    </div>

                    <!--Fin centro -->

                  </div><!-- /.box -->

              </div><!-- /.col -->

          </div><!-- /.row -->

      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->



  <!-- Modal -->

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto !important;">

    <div class="modal-dialog" style="width: 1100px !important;">

      <div class="modal-content">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

          <h4 class="modal-title">Seleccione un Artículo</h4>

        </div>

        <div class="modal-body table-responsive">

          <table id="tblarticulos" class="table table-striped table-bordered table-condensed table-hover">

            <thead>

                <th>Agregar</th>

                <th>Cod. Med</th>

                <th>Nombre Com.</th>

                <th>Cat.</th>

                <th>Nombre Gen.</th>

                <th>Lab.</th>

                <th>Stock</th>

                <th>Stock Min.</th>

                <th>P.V.</th>

                <th>Lote | Venc.</th>

                <th>Estado</th>

                <th>Img</th>

            </thead>

            <tbody>

              

            </tbody>

            <tfoot>

              <th>Agregar</th>

              <th>Cod. Med</th>

                <th>Nombre Com.</th>

                <th>Cat.</th>

                <th>Nombre Gen.</th>

                <th>Lab.</th>

                <th>Stock</th>

                <th>Stock Min.</th>

                <th>P.V.</th>

                <th>Lote | Venc.</th>

                <th>Estado</th>

                <th>Img</th>

            </tfoot>

          </table>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

        </div>        

      </div>

    </div>

  </div>  

  <!-- Fin modal -->

<?php

}

else

{

  require 'noacceso.php';

}



require 'footer.php';

?>

<script type="text/javascript" src="scripts/registro_cajas.js"></script>

<?php 

}

ob_end_flush();

require 'fin.php';

?>