<?php

  //Activamos el almacenamiento en el buffer

  ob_start();
  
  date_default_timezone_set("America/La_Paz");

  session_start();

  if (!isset($_SESSION["nombre"])){
    header("Location: login.html");
  }else{

    require 'header.php';

    if ($_SESSION['configuracion']==1){
    
  ?>

  <!--Contenido-->

        <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">        

          <!-- Main content -->

          <section class="content">

              <div class="row">

                <div class="col-md-12">

                    <div class="box">

                      <div class="box-header with-border">

                            <h1 class="box-title">Correlativo Ventas <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button> </h1>

                          <div class="box-tools pull-right">

                          </div>

                      </div>

                      <!-- /.box-header -->

                      <!-- centro -->
                      

                      <!-- centro -->

                      <div class="panel-body table-responsive" id="listadoregistros">

                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                          <thead>

                            <th>Opciones</th>

                            <th>Fecha Registro</th>

                            <th>Descripción</th>

                            <th>Fecha Vigencia</th>

                            <th>Nro. Tope Correlativo</th>
                            
                            <th>Nro. Correlativo Actual</th>

                            <th>Estado</th>

                          </thead>

                          <tfoot>

                            <th>Opciones</th>

                            <th>Fecha Registro</th>

                            <th>Descripción</th>

                            <th>Fecha Vigencia</th>

                            <th>Nro. Tope Correlativo</th>
                            
                            <th>Nro. Correlativo Actual</th>

                            <th>Estado</th>

                          </tfoot>

                        </table>

      
                      </div>

                      

                      <div class="panel-body" style="height: 500px;" id="formularioregistros">

                          <form name="formulario" id="formulario" method="POST">

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                              <!--PENDIENTE VERIFICAR QUE LA DESCRIPCIÓN SEA DISTINTA-->

                              <label>Descripción Correlativo(*)</label>

                              <input type="text" max="100" name="descripcion" class="form-control" id="descripcion" placeholder="Añada una descripción para el correlativo" required>

                              <input type="hidden" name="id_correlativo" id="id_correlativo">

                              <input type="hidden" name="actual" id="actual">

                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                              <label>Número tope de correlativo(*)</label>

                              <input type="number" min="1" max="999999" class="form-control" name="max" id="max" step="1" required/>

                            </div>


                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">

                              <label>Fecha Caducidad Correlativo(*)</label>

                              <input type="date" class="form-control" name="fecha_vigencia" min="<?php echo date("Y-m-d")?>" id="fecha_vigencia" required/>

                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                              <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                              <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>

                            </div>
                        

                          </form>

                      </div>

                      


                      <!--Fin centro -->

                    </div><!-- /.box -->

                </div><!-- /.col -->

            </div><!-- /.row -->

        </section><!-- /.content -->



      </div><!-- /.content-wrapper -->

    <!--Fin-Contenido-->

  <?php

  }

  else

  {

    require 'noacceso.php';

  }



  require 'footer.php';

  ?>

  <script type="text/javascript" src="scripts/correlativo_ventas.js"></script>

  <?php 

  }

  ob_end_flush();

  require 'fin.php';