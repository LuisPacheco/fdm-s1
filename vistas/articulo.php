<?php
  //Activamos el almacenamiento en el buffer

  ob_start();

  session_start();



  if (!isset($_SESSION["nombre"]))

    header("Location: login.html");

  else{

    require 'header.php';

    if ($_SESSION['almacen']==1){

      require_once "../modelos/GestionToken.php";
      $gestion_token = new GestionToken();
      $res = $gestion_token->getTokenInfo();
      $id = 0;
      if($res){
        $fecha = date("Y-m-d H:i:s");
        if($fecha <= $res["fecha_caducidad_token"]){
          $token_valido = $gestion_token->validarToken($res["codigo_token"]);
          if($token_valido)
            $id = $res["id_token"];
        }
      }

      
    //$res = conexionImpuestos();

    //var_dump($res);
    
      
?>

<!--Contenido-->

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">        

        <!-- Main content -->

        <section class="content">

          <div class="row">

            <div class="col-md-12">

                <div class="box">

                  <div class="box-header with-border">

                    <h1 class="box-title">Artículo 
                      <a href="../reportes/rptarticulos.php" target="_blank"><button class="btn btn-info"><i class="fa fa-clipboard"></i> Reporte</button></a>
                      <a href="../reporte_excel/reporte_productos.php" target="_blank"><button class="btn btn-success" id="btnReporteExcel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Reporte Art x Laboratorio</button></a>
                    </h1>

                    <div class="box-tools pull-right">

                    </div>

                  </div>

                  <!-- /.box-header -->

                  <!-- centro -->

                  <div class="panel-body table-responsive" id="listadoregistros">

                    <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">

                      <thead>

                        <th>Opciones</th>

                        <th>Código</th>

                        <th>Nombre Comercial</th>

                        <th>Nombre Genérico</th>

                        <th>Categoría</th>

                        <th>Laboratorio</th>

                        <th>Stock</th>

                        <th>Stock Mínimo</th>

                        <th>Precio Venta</th>

                        <th>Imagen</th>

                        <th>Estado</th>

                      </thead>

                      <tbody>                            

                      </tbody>

                      <tfoot>

                        <th>Opciones</th>

                        <th>Código</th>

                        <th>Nombre Comercial</th>

                        <th>Nombre Genérico</th>

                        <th>Categoría</th>

                        <th>Laboratorio</th>

                        <th>Stock</th>

                        <th>Stock Mínimo</th>

                        <th>Precio Venta</th>

                        <th>Imagen</th>

                        <th>Estado</th>

                      </tfoot>

                    </table>

                  </div>

                  

                  <input type="hidden" id="id_token" value="<?php echo $id; ?>">

                  <form name="formulario" id="formulario" method="POST">

                    <div class="panel-body" id="formularioregistros">

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Nombre Comercial(*):</label>
                        <input type="hidden" name="idarticulo" id="idarticulo">
                        <input type="text" class="form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" readonly>
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Nombre Genérico(*):</label>
                        <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Nombre Comercial" readonly>
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Lista de productos /servicios según SIN(*):</label>
                        <select id="codigoProductoSinDisabled" class="form-control selectpicker" data-live-search="true" disabled></select>
                        <input type="hidden" name="codigoProductoSin" id="codigoProductoSin">
                      </div>

                      <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label>Unidad de medida(*):</label>
                        <select id="unidadMedidaDisabled" class="form-control selectpicker" data-live-search="true" disabled></select>
                        <input type="hidden" name="unidadMedida" id="unidadMedida">
                      </div>

                      <input type="hidden" name="nombreUnidadMedida" id="nombreUnidadMedida">

                      <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label>Especifique unidad de medida:</label>
                        <input type="text" class="form-control" name="otraUnidadMedida" id="otraUnidadMedida" maxlength="30" placeholder="Llenar si no seleccionó Unidad de medida" readonly>
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Categoría(*):</label>
                        <select id="idcategoriaDisabled" class="form-control selectpicker" data-live-search="true" disabled></select>
                        <input type="hidden" name="idcategoria" id="idcategoria">
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Descripción:</label>
                        <input type="text" class="form-control" name="descripcion" id="descripcion" maxlength="256" placeholder="Descripción" readonly>
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Laboratorio(*):</label>
                        <select id="idlaboratorioDisabled" class="form-control selectpicker" data-live-search="true" disabled></select>
                        <input type="hidden" name="idlaboratorio" id="idlaboratorio">
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Código Interno(*):</label>
                        <input type="text" class="form-control" name="cod_med" id="cod_med" readonly required maxlength="30" placeholder="Código">
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Stock(*):</label>
                        <input type="number" class="form-control" value="0" name="stock" id="stock" required min=0 pattern="^[0-9]+">
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Stock Mínimo(*):</label>
                        <input type="number" class="form-control" name="stock_minimo" id="stock_minimo" required min=0 pattern="^[0-9]+">
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Precio de Venta(*):</label>
                        <input type="number" class="form-control" min=0 step="0.01" name="precio_venta" id="precio_venta" min=0 pattern="^[0-9]+" readonly>
                      </div>

                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label>Imagen:</label>
                        <input type="file" class="form-control" name="imagen" id="imagen" accept="image/x-png,image/gif,image/jpeg">
                        <input type="hidden" name="imagenactual" id="imagenactual">
                        <img src="" width="150px" height="120px" id="imagenmuestra">
                      </div>



                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>



                        <button class="btn btn-danger" id="btnCancelar" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>



                        <a data-toggle="modal" href="#myModal">           

                          <button id="btnVentaArt" type="button" class="btn btn-success"> <span class="fa fa-search"></span> Historial Ventas Artículo</button>

                        </a>



                        <a data-toggle="modal" href="#myModal2">           

                          <button id="btnEntradaArt" type="button" class="btn btn-warning"> <span class="fa fa-search"></span> Historial Entradas sucursal Artículo</button>

                        </a>



                        <a data-toggle="modal" href="#myModal3">           

                          <button id="btnSalidaArt" type="button" class="btn btn-danger"> <span class="fa fa-search"></span> Historial Salidas sucursal Artículo</button>

                        </a>



                        <a data-toggle="modal" href="#myModal4">           

                          <button id="btnIngresoArt" type="button" class="btn btn-info"> <span class="fa fa-search"></span> Historial Ingresos Artículo</button>

                        </a>



                      </div>



                    </div>

                  </form>

                  <!--Fin centro -->

                </div><!-- /.box -->

            </div><!-- /.col -->

          </div><!-- /.row -->

        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->

  <!--Fin-Contenido-->



  <!-- Modal Ventas -->

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto !important;">

    <div class="modal-dialog" style="width: 850px !important;">

      <div class="modal-content">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

          <h4 class="modal-title" id="titulo_art">Historial de venta del artículo</h4>

        </div>

        <div class="modal-body table-responsive">

          <table id="tblventasart" class="table table-striped table-bordered table-condensed table-hover">

            <thead>

                <th>Vendedor(a)</th>

                <th>Cantidad</th>

                <th>Precio Venta</th>

                <th>Descuento</th>

                <th>Fecha_de_venta</th>

                <th>Lote</th>

                <th>Vencimiento</th>

                <th>Estado</th>

            </thead>

            <tbody>

              

            </tbody>

            

          </table>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

        </div>        

      </div>

    </div>

  </div>

  <!-- Fin modal ventas-->



  <!-- Modal entradas -->

  <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto !important;">

    <div class="modal-dialog" style="width: 850px !important;">

      <div class="modal-content">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

          <h4 class="modal-title" id="titulo_art">Historial de entradas a sucursal del artículo</h4>

        </div>

        <div class="modal-body table-responsive">

          <table id="tblentradasart" class="table table-striped table-bordered table-condensed table-hover">

            <thead>

                <th>Encargado(a)</th>

                <th>Cantidad</th>

                <th>Fecha_de_entrada</th>

                <th>Lote</th>

                <th>Vencimiento</th>

                <th>Sucursal Origen</th>

                <th>Estado</th>

            </thead>

            <tbody>

              

            </tbody>

            

          </table>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

        </div>        

      </div>

    </div>

  </div>

  <!-- Fin modal entradas-->



  <!-- Modal salidas -->

  <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto !important;">

    <div class="modal-dialog" style="width: 850px !important;">

      <div class="modal-content">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

          <h4 class="modal-title" id="titulo_art">Historial de salidas de sucursal del artículo</h4>

        </div>

        <div class="modal-body table-responsive">

          <table id="tblsalidasart" class="table table-striped table-bordered table-condensed table-hover">

            <thead>

                <th>Encargado(a)</th>

                <th>Cantidad</th>

                <th>Fecha_de_salida</th>

                <th>Lote</th>

                <th>Vencimiento</th>

                <th>Sucursal Destino</th>

                <th>Estado</th>

            </thead>

            <tbody>

              

            </tbody>

            

          </table>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

        </div>        

      </div>

    </div>

  </div>

  <!-- Fin modal salidas-->



   <!-- Modal ingresos -->

   <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto !important;">

    <div class="modal-dialog" style="width: 850px !important;">

      <div class="modal-content">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

          <h4 class="modal-title" id="titulo_art">Historial de ingresos del artículo</h4>

        </div>

        <div class="modal-body table-responsive">

          <table id="tblingresosart" class="table table-striped table-bordered table-condensed table-hover">

            <thead>

                <th>Encargado(a)</th>

                <th>Cantidad</th>

                <th>Fecha_de_ingreso</th>

                <th>Lote</th>

                <th>Vencimiento</th>

                <th>Proveedor</th>

                <th>Estado</th>

            </thead>

            <tbody>

              

            </tbody>

            

          </table>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

        </div>        

      </div>

    </div>

  </div>

  <!-- Fin modal ingresos-->

  

<?php

  }else

    require 'noacceso.php';

  require 'footer.php';

?>

<script type="text/javascript" src="scripts/articulo.js"></script>

<?php 

  require 'fin.php';

}

ob_end_flush();

?>