$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();

	

	if(title != "Opciones" && title != "Imagen"){

		$(this).html( '<input type="text" style="width:100%"> ' );

	}else{

		$(this).html( '' );

	}

	

	$( 'input', this ).on( 'keyup change', function () {

		if ( tabla_listado.column(i).search() !== this.value ) {

			tabla_listado

				.column(i)

				.search( this.value )

				.draw();

		}

	} );

} );



var tabla_listado = $('#tbllistado').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});



//Función que se ejecuta al inicio

function init(){

	mostrarform(false);

	listar();



	$("#formulario").on("submit",function(e)

	{

		guardaryeditar(e);	

	});

    $('#mAlmacen').addClass("treeview active");

    $('#lLaboratorios').addClass("active");

}



//Función limpiar

function limpiar()

{

	$("#idlaboratorio").val("");

	$("#nombre").val("");

	$("#descripcion").val("");

}



//Función mostrar formulario

function mostrarform(flag)

{

	limpiar();

	if (flag)

	{

		$("#listadoregistros").hide();

		$("#formularioregistros").show();

		$("#btnagregar").hide();

	}

	else

	{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

	}

}



//Función cancelarform

function cancelarform()

{

	limpiar();

	mostrarform(false);

}



//Función Listar

function listar()

{

	tabla_listado=$('#tbllistado').dataTable(

	{

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/laboratorio.php?op=listar',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}


function mostrar(idlaboratorio)

{

	$.post("../ajax/laboratorio.php?op=mostrar",{idlaboratorio : idlaboratorio}, function(data, status)

	{

		data = JSON.parse(data);		

		mostrarform(true);



		$("#nombre").val(data.nombre);

		$("#descripcion").val(data.descripcion);

 		$("#idlaboratorio").val(data.idlaboratorio);



 	})

}


init();