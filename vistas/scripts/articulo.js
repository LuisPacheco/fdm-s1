$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();
	if(title != "Opciones" && title != "Imagen"){

		$(this).html( '<input type="text" style="width:100%"> ' );

	}else{

		$(this).html( '' );

	}

	

	$( 'input', this ).on( 'keyup change', function () {

		if ( tabla_listado.column(i).search() !== this.value ) {

			tabla_listado

				.column(i)

				.search( this.value )

				.draw();

		}

	} );

} );



var tabla_listado = $('#tbllistado').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});

//Función ListarArticulos
function listarVentasArticulo(){

	tabla=$('#tblarticulos').dataTable(

	{

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [		          

		           

		        ],

		"ajax":

				{

					url: '../ajax/venta.php?op=listarArticulosVenta',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}



//Función que se ejecuta al inicio
function init(){
	mostrarform(false,0);
	listar();
	$("#formulario").on("submit",function(e){
		guardaryeditar(e);	
	});

	//Cargamos los items al select categoria
	$.post("../ajax/articulo.php?op=selectCategoria", function(r){
		$("#idcategoriaDisabled").html(r);
		$('#idcategoriaDisabled').selectpicker('refresh');
	});	

	//carga de laboratorios
	$.post("../ajax/articulo.php?op=selectLaboratorio", function(r){
		$("#idlaboratorioDisabled").html(r);
		$('#idlaboratorioDisabled').selectpicker('refresh');
	});
	
	//carga de lista de productos según SIN
	$.post("../ajax/articulo.php?op=selectListaProductosSin", function(r){
		if(r != false){
			var res = JSON.parse(r);
			var vector_res = res.RespuestaListaProductos.listaCodigos;
			var cad_res = "<option value=''>--Seleccione--</option>";
			for(i = 0; i < vector_res.length; i++){
				if(vector_res[i].codigoProducto == "35270")//otros productos farmaceuticos
					cad_res += "<option value='"+vector_res[i].codigoProducto+"' selected>" + vector_res[i].descripcionProducto + "</option>";
				else{
					if(vector_res[i].codigoProducto == "62273" || vector_res[i].codigoProducto == "62275" || vector_res[i].codigoProducto == "622739" || vector_res[i].codigoProducto == "622759" || vector_res[i].codigoProducto == "35210" || vector_res[i].codigoProducto == "35250")
						cad_res += "<option value='"+vector_res[i].codigoProducto+"'>" + vector_res[i].descripcionProducto + "</option>";
				}
			}
			$("#codigoProductoSinDisabled").html(cad_res);
			$('#codigoProductoSinDisabled').selectpicker('refresh');
		}else{
			alert("Debe realizar la sincronización del día para crear y/o editar productos");
			setTimeout(volver_menu(),3000);
		}	
	});

	//carga de lista de unidades de medida
	$.post("../ajax/articulo.php?op=selectUnidadMedida", function(r){
		if(r != false){
			//console.log(r);
			var res = JSON.parse(r);
			var vector_res = res.RespuestaListaParametricas.listaCodigos;
			var cad_res = "<option value=''>--Seleccione--</option>";
			var vector_um = [2,4,5,6,17,22,25,26,28,33,34,37,40,42,43,57,58,62,70,72,73,76,78,79,80,81,83,84,86,96,97,104,109,110,111,112,115,116,119,121,122];
			for(i = 0; i < vector_res.length; i++){
				if(vector_um.includes(Number(vector_res[i].codigoClasificador))){
					if(vector_res[i].codigoClasificador == "57")//unidades (bienes)
						cad_res += "<option value='"+vector_res[i].codigoClasificador+"' selected>" + vector_res[i].descripcion + "</option>";
					else
						cad_res += "<option value='"+vector_res[i].codigoClasificador+"'>" + vector_res[i].descripcion + "</option>";
				}
				
			}
			$("#unidadMedidaDisabled").html(cad_res);
			$('#unidadMedidaDisabled').selectpicker('refresh');
		}
	});	
	$("#imagenmuestra").hide();
	$('#mAlmacen').addClass("treeview active");
    $('#lArticulos').addClass("active");
}

function volver_menu() {  
	window.location.href = "escritorio.php";
}



//Función limpiar
function limpiar(){

	$("#codigo").val("");

	$("#nombre").val("");

	$("#descripcion").val("");

	$("#stock").val("0");

	$("#stock_minimo").val("");

	$("#imagenmuestra").attr("src","");

	$("#imagen").val("");

	$("#imagenactual").val("");

	$("#print").hide();

	$("#idarticulo").val("");

	$("#idlaboratorio").val("");

	$("#idlaboratorioDisabled").val("");

	$('#idlaboratorioDisabled').selectpicker('refresh');

	$("#idcategoriaDisabled").val("");

	$('#idcategoriaDisabled').selectpicker('refresh');

	$("#idcategoria").val("");

	$("#precio_venta").val("");

	$("#cod_med").val("").prop("readonly",true);

	$("#codigoProductoSinDisabled").val("35270");

	$('#codigoProductoSinDisabled').selectpicker('refresh');

	$("#codigoProductoSin").val("35270");

	$("#unidadMedidaDisabled").val("57");

	$('#unidadMedidaDisabled').selectpicker('refresh');

	$("#unidadMedida").val("57");

	$("#nombreUnidadMedida").val("UNIDAD (BIENES)");

	$("#otraUnidadMedida").prop("readonly",true).removeAttr("required").val("");


}



//Función mostrar formulario

function mostrarform(flag,idarticulo){

	limpiar();

	if (flag)	{

		$("#listadoregistros").hide();

		$("#formularioregistros").show();

		$("#btnGuardar").show();

		$("#btnGuardar").prop("disabled",false);

		$("#btnagregar").hide();

		$("#btnCancelar").show();

		if(idarticulo != 0){	

			$("#btnVentaArt").show();

			$("#btnEntradaArt").show();

			$("#btnSalidaArt").show();

			$("#btnIngresoArt").show();

		}else{

			$("#btnVentaArt").hide();

			$("#btnEntradaArt").hide();

			$("#btnSalidaArt").hide();

			$("#btnIngresoArt").hide();

		}

		
		$("#btnReporteExcel").hide();

	}else{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

		$("#btnGuardar").hide();

		$("#btnCancelar").hide();

		$("#btnVentaArt").hide();

		$("#btnEntradaArt").hide();

		$("#btnSalidaArt").hide();

		$("#btnIngresoArt").hide();

		$("#btnReporteExcel").show();

	}

}



//Función cancelarform

function cancelarform(){

	mostrarform(false,0);

}



//Función Listar

function listar(){

	tabla_listado=$('#tbllistado').dataTable(

	{

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/articulo.php?op=listar',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 3, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}



//listar historial de ventas

function listarHistorialVentas(idarticulo){

	//console.log("llega");

	$('#tblventasart').dataTable({

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [		          

		           

		        ],

		"ajax":

				{

					url: '../ajax/articulo.php?op=listar_venta_articulo&idarticulo=' + idarticulo,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

					

				},

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 4, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}	



//listar historial de entradas de medicamentos

function listarHistorialEntradas(idarticulo){

	//console.log("llega");

	$('#tblentradasart').dataTable({

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [		          

		           

		        ],

		"ajax":

				{

					url: '../ajax/articulo.php?op=listar_entradas_articulo&idarticulo=' + idarticulo,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

					

				},

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 2, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}	



//listar historial de salidas de medicamentos

function listarHistorialSalidas(idarticulo){

	//console.log("llega");

	$('#tblsalidasart').dataTable({

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [		          

		           

		        ],

		"ajax":

				{

					url: '../ajax/articulo.php?op=listar_salidas_articulo&idarticulo=' + idarticulo,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

					

				},

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 2, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}	



//listar historial de ingresos de medicamentos

function listarHistorialIngresos(idarticulo){

	//console.log("llega");

	$('#tblingresosart').dataTable({

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [		          

		           

		        ],

		"ajax":

				{

					url: '../ajax/articulo.php?op=listar_ingresos_articulo&idarticulo=' + idarticulo,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

					

				},

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 2, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}	



//Función para guardar o editar

function guardaryeditar(e){

	e.preventDefault(); //No se activará la acción predeterminada del evento

	bootbox.confirm("¿Está Seguro/a de registrar el artículo?", function(result){
		if(result){
			$("#btnGuardar").prop("disabled",true);

			$("body").css({"padding-right":"0px"});

			var formData = new FormData($("#formulario")[0]);

			$.ajax({

				url: "../ajax/articulo.php?op=guardaryeditar",

				type: "POST",

				data: formData,

				contentType: false,

				processData: false,



				success: function(datos)

				{                    

					bootbox.alert(datos);	          

					mostrarform(false,0);

					tabla_listado.ajax.reload();

				}

			});

			limpiar();
		}else{

			$("body").css({"padding-right":"0px"});

		}
	});

}



function mostrar(idarticulo){
	$.post("../ajax/articulo.php?op=mostrar",{idarticulo : idarticulo}, function(data, status){
		data = JSON.parse(data);		
		
		mostrarform(true,idarticulo);

		$("#idcategoria").val(data.idcategoria);

		$("#idcategoriaDisabled").val(data.idcategoria);

		$('#idcategoriaDisabled').selectpicker('refresh');

		$("#cod_med").val(data.cod_med);

		var codigo = data.codigo;

		codigo = codigo.replace(/&quot;/g,'"');
		codigo = codigo.replace(/&amp;/g,'&');
		codigo = codigo.replace(/&#039;/g,'\'');

		$("#codigo").val(codigo);

		var nombre = data.nombre;

		nombre = nombre.replace(/&quot;/g,'"');
		nombre = nombre.replace(/&amp;/g,'&');
		nombre = nombre.replace(/&#039;/g,'\'');

		$("#nombre").val(nombre);

		$("#idlaboratorio").val(data.idlaboratorio);

		$("#idlaboratorioDisabled").val(data.idlaboratorio);

		$('#idlaboratorioDisabled').selectpicker('refresh');

		$("#unidadMedida").val(data.unidadMedida);

		$("#unidadMedidaDisabled").val(data.unidadMedida);

		$('#unidadMedidaDisabled').selectpicker('refresh');

		$("#nombreUnidadMedida").val(data.nombreUnidadMedida);

		$("#codigoProductoSinDisabled").val(data.codigoProductoSin);

		$('#codigoProductoSinDisabled').selectpicker('refresh');

		$("#codigoProductoSin").val(data.codigoProductoSin);

		$("#otraUnidadMedida").val(data.otraUnidadMedida);

		$("#stock").val(data.stock);

		$("#stock_minimo").val(data.stock_minimo);

		$("#descripcion").val(data.descripcion);

		$("#imagenmuestra").show();

		$("#imagenmuestra").attr("src","../files/articulos/"+data.imagen);

		$("#imagenactual").val(data.imagen);

 		$("#idarticulo").val(data.idarticulo);

 		$("#precio_venta").val(data.precio_venta);

		listarHistorialVentas(idarticulo);

		listarHistorialEntradas(idarticulo);

		listarHistorialSalidas(idarticulo);

		listarHistorialIngresos(idarticulo);

 	});

}



//Función para desactivar registros
function desactivar(idarticulo){

	bootbox.confirm("¿Está Seguro de desactivar el artículo?", function(result){

		if(result)

        {

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/articulo.php?op=desactivar", {idarticulo : idarticulo}, function(e){

        		bootbox.alert(e);

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	});

}

//Función para activar registros
function activar(idarticulo){

	bootbox.confirm("¿Está Seguro de activar el Artículo?", function(result){

		if(result)

        {

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/articulo.php?op=activar", {idarticulo : idarticulo}, function(e){

        		bootbox.alert(e);

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	})

}

init();

$(document).ready(function () {
	/*
	var id = $("#id_token").val();
	if(id == 0){
		alert("Token Inválido o vencido");
		setTimeout(volver_menu(),3000);
	}
	*/
});

