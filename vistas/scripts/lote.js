$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );
$('#tbllistado thead tr:eq(1) th').each( function (i) {
	var title = $(this).text();
	
	if(title != "Opciones" && title != "Imagen"){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}
	
	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla_listado.column(i).search() !== this.value ) {
			tabla_listado
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
} );

var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	$("#btnagregar").show();
	listar();

	$("#formulario").on("submit",function(e){
		guardar(e);	
	});

	$.post("../ajax/articulo.php?op=selectLaboratorio",function(r){
		//console.log(r)
		$("#idlaboratorio").html(r).fadeIn();
		$("#idlaboratorio").selectpicker("refresh");
	});

	$('#mES').addClass("treeview active");
    $('#lLote').addClass("active");

	$(document).ready(function() {
        $("#idlaboratorio").change(function() {
            var form_data = {
                    is_ajax: 1,
                    laboratorio: +$("#idlaboratorio").val()
            };
            $.ajax({
                    type: "POST",
                    url: "../ajax/articulo.php?op=selectArticuloLab",
                    data: form_data,
                    success: function(response)
                    {
                    	//console.log(response)
                        $('#idarticulo').html(response).fadeIn();
                        $("#idarticulo").selectpicker("refresh");
                    }
            });
        });

	    $.post("../ajax/ingreso.php?op=selectProveedor",function(r){
			$("#idproveedor").html(r);
			$("#idproveedor").selectpicker("refresh");
		});
	});
}


//Función Listar TODOS los lotes registrados
function listar(){
	tabla_listado = $('#tbllistado').dataTable({
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/lote.php?op=listar1',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 10,//Paginación
		"order":[[0,"desc"]]
	}).DataTable();
}

//Función limpiar
function limpiar(){
	
	$('#idlaboratorio').val("");
	$("#idlaboratorio_ant").val("");

	$('#idarticulo').val("");
	$("#idarticulo_ant").val("");

	$("#idproveedor").val("");
	$("#idproveedor_ant").val("");

	$("#cod_lote").val("");
	$("#cod_lote_ant").val("");
	
    $('#vencimiento').val("");
    $("#vencimiento_ant").val("");

    $('#idlote').val("");

    $('#stock').val("");
    $("#stock_ant").val("");
}

//Función para ocultar lotes del listado de ventas
function ocultar(idlote){
	bootbox.confirm("¿Está Seguro de ocultar el lote del listado de ventas?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
        	$.post("../ajax/lote.php?op=ocultar", {idlote : idlote}, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});	
        }else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

//Función para visibilizar lotes del listado de ventas
function visibilizar(idlote){
	bootbox.confirm("¿Está Seguro de mostrar el lote del listado de ventas?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
        	$.post("../ajax/lote.php?op=visibilizar", {idlote : idlote}, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});	
        }else{
			$("body").css({"padding-right":"0px"});
		}
	})
}

//cerrar formulario de registro de lote
function cancelarform(){
	limpiar();
	mostrarform(false);
}

//Función mostrar formulario
function mostrarform(flag){
	limpiar();
	if (flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#btnCancelar").show();
		$("#btnGuardar").show();

	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#btnGuardar").hide();
	}
}

//mostrar la información del lote sin poder editar
function mostrar(idlote,idlaboratorio,idarticulo){
	console.log(idlote + " " + idlaboratorio);
	$.post("../ajax/lote.php?op=mostrar",{idlote:idlote}, 
		function(data, status){
			data = JSON.parse(data);
					
			mostrarform(true);
			$("#idproveedor").val(data.idpersona);
			$("#idproveedor").selectpicker('refresh');
			$("#idproveedor").prop('disabled', true);

			$("#idlaboratorio").val(data.idlaboratorio);
			$("#idlaboratorio").selectpicker('refresh');
			$("#idlaboratorio").prop('disabled', true);

			$("#idarticulo").val(data.idarticulo);
			//$("#idarticulo").selectpicker('refresh');
			//$("#idarticulo").prop('disabled', true);


			$("#vencimiento").val(data.vencimiento);
			$("#vencimiento").prop('disabled', true);

			$("#stock").val(data.stock);

			$("#cod_lote").val(data.cod_lote);
			$("#cod_lote").prop('disabled', true);

			$("#idlote").val(data.idlote);

			//Ocultar y mostrar los botones
			$("#btnGuardar").hide();
			$("#btnCancelar").show();
 		}
 	);

	$.post("../ajax/articulo.php?op=selectArticuloLab",{idlaboratorio : idlaboratorio,idarticulo:idarticulo}, function(data){
		$("#idarticulo").html(data);
		//console.log(data);
		$("#idarticulo").selectpicker('refresh');
		$("#idarticulo").prop('disabled', true);
	});

}

function editar(idlote,idlaboratorio,idarticulo){
	$.post("../ajax/lote.php?op=mostrar",{idlote:idlote},  
		function(data, status){
			//console.log(data);
			
			data = JSON.parse(data);
			
			
			mostrarform(true);

			$("#idproveedor").val(data.idpersona);
			$("#idproveedor").selectpicker('refresh');
			$("#idproveedor").prop('disabled', true);
			$("#idproveedor_ant").val(data.idpersona);

			$("#idlaboratorio").val(data.idlaboratorio);
			$("#idlaboratorio").selectpicker('refresh');
			$("#idlaboratorio").prop('disabled', true);
			$("#idlaboratorio_ant").val(data.idlaboratorio);

			//$("#idarticulo").val(data.idarticulo);
			//$("#idarticulo").selectpicker('refresh');
			//$("#idarticulo").prop('disabled', true);
			$("#idarticulo_ant").val(data.idarticulo);


			$("#vencimiento").val(data.vencimiento);
			$("#vencimiento").prop('disabled', false);
			$("#vencimiento_ant").val(data.vencimiento);

			$("#stock").val(data.stock);
			$("#stock_ant").val(data.stock);

			$("#cod_lote").val(data.cod_lote);
			$("#cod_lote").prop('disabled', false);
			$("#cod_lote_ant").val(data.cod_lote);

			$("#idlote").val(data.idlote);
			//Ocultar y mostrar los botones
			$("#btnGuardar").show();
			$("#btnCancelar").show();
			
 		}
 	);

	$.post("../ajax/articulo.php?op=selectArticuloLab",{idlaboratorio : idlaboratorio,idarticulo:idarticulo}, function(data){
		$("#idarticulo").html(data);
		//console.log(data);
		$("#idarticulo").selectpicker('refresh');
		$("#idarticulo").prop('disabled', true);
	});
}

//guarda el lote desde el formulario de lote
function guardar(e){
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	
	var formData = new FormData($("#formulario")[0]);
	$("body").css({"padding-right":"0px"});
	$.ajax({
		url: "../ajax/lote.php?op=guardar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos){                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla_listado.ajax.reload();
	    }

	});
	
	limpiar();
}

//Función para devolver lote, se lo realiza en tiempo real
function devolver(idlote,idarticulo,stock){
	bootbox.confirm("¿Está Seguro de devolver el lote al proveedor?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
        	$.post("../ajax/lote.php?op=devolver", {idlote : idlote, idarticulo : idarticulo, stock:stock}, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});	
        }else{
			$("body").css({"padding-right":"0px"});
        	bootbox.alert("Devolución no efectuada");
		}
	});
}

//Función para devolver lote, se lo realiza en tiempo real
function borrar(idlote,estado,stock,idarticulo){
	console.log(idlote)
	console.log(estado)
	console.log(stock)
	console.log(idarticulo)
	bootbox.confirm("¿Está Seguro de borrar el lote?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
        	$.post("../ajax/lote.php?op=borrar", {idlote : idlote, estado : estado, stock:stock,idarticulo : idarticulo}, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});	
        }else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

init();