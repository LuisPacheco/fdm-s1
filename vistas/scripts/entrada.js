$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();

	

	if(title != "Opciones" && title != "Imagen"){

		$(this).html( '<input type="text" style="width:100%"> ' );

	}else{

		$(this).html( '' );

	}

	

	$( 'input', this ).on( 'keyup change', function () {

		if ( tabla_listado.column(i).search() !== this.value ) {

			tabla_listado

				.column(i)

				.search( this.value )

				.draw();

		}

	} );

} );

var tabla_listado = $('#tbllistado').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});

var tabla;

//Declaración de variables necesarias para trabajar con las compras y sus detalles
var impuesto = 13;

var cont = 0;

var detalles = 0;

//$("#guardar").hide();

$("#btnGuardar").hide();

$("#Factura").change(marcarImpuesto);

$("#Recibo").change(marcarImpuesto);


//Función que se ejecuta al inicio
function init(){

	mostrarform(false);

	listar("ambas");

	$(document).ready(function () {  

		detalles = $.ajax({

			url: "../ajax/entrada.php?op=select_sucursales", 

			dataType: 'text',

			async: false     

		}).responseText;

		$("#sucursal_select").html(detalles);



		$("#sucursal_select").on("change",function () {  

			listar(this.value);

		})

	});



	$("#formulario").on("submit",function(e){

		guardaryeditar(e);	

	});

	$('#mES').addClass("treeview active");

    $('#lEntrada').addClass("active");   

}

//Función limpiar campos con contenido
function limpiar(){

	$(".filas").remove();

	$("#sucursal").val("").selectpicker('refresh').prop("disabled",false);

	$("#num_comprobante").val("0").prop("disabled",false);

	$("#identrada").val("");

	//obtener fecha actual

	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);

	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear() + "-" + (month) + "-" + (day);

	$('#fecha_hora').val(today).prop("disabled",false);

	detalles = 0;

	cont = 0;

}

//Función mostrar formulario de registro de entrada
function mostrarform(flag){

	limpiar();

	if (flag){

		$("#elige_sucursal").hide();

		$("#sucursal_select").hide();

		$("#listadoregistros").hide();

		$("#formularioregistros").show();

		$("#btnagregar").hide();

		listarArticulos();

		$("#btnGuardar").hide();

		$("#btnCancelar").show();

		$("#btnAgregarArt").show();

	}

	else{

		$("#elige_sucursal").show();

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

		$("#sucursal_select").show();

	}



}

//Función cancelarform
function cancelarform(){

	mostrarform(false);

}

//Función Listar TODOS los entradas realizados
function listar(dato){

	tabla_listado = $('#tbllistado').dataTable({

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/entrada.php?op=listar&sucursal='+dato,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función Listar Articulos en la VENTANA MODAL
function listarArticulos(){

	tabla=$('#tblarticulos').dataTable(	{

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla_listado

	    buttons: [],

		"ajax":

				{

					url: '../ajax/entrada.php?op=listarArticulos',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función Listar Lotes en la TABLA MODAL3
function listarLotes(){

	tabla=$('#tblotes').dataTable(	{

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [],

		"ajax":

				{

					url: '../ajax/lote.php?op=selectLotes',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función para guardar o editar
function guardaryeditar(e){

	e.preventDefault(); //No se activará la acción predeterminada del evento

	bootbox.confirm("¿Está Seguro/a de efectuar la entrada?<br>Nota: Una vez efectuada <strong>No podrá ser modificado</strong>", function(result){

		if(result){

			$("body").css({"padding-right":"0px"});

			var formData = new FormData($("#formulario")[0]);

			$.ajax({

				url: "../ajax/entrada.php?op=guardaryeditar",

			    type: "POST",

			    data: formData,

			    contentType: false,

			    processData: false,

			    success: function(datos){                    

			          bootbox.alert(datos);	          

			          mostrarform(false);

					  tabla_listado.ajax.reload();

			    }

			});

			limpiar();

		}else{

			$("body").css({"padding-right":"0px"});

			bootbox.alert("entrada no efectuada");

		}

		

	});		

}

function mostrar(identrada){

	//Se muestra los datos de todos los entradas

	$.post("../ajax/entrada.php?op=mostrar",{identrada : identrada}, function(data, status){

			data = JSON.parse(data);		

			mostrarform(true);

			$("#sucursal").val(data.sucursal).selectpicker('refresh').prop("disabled",true);

			$("#fecha_hora").val(data.fecha).prop("disabled",true);

			$("#num_comprobante").val(data.num_comprobante).prop("disabled",true);

			$("#identrada").val(data.identrada);

			//Ocultar y mostrar los botones

			$("#btnGuardar").hide();

			$("#btnCancelar").show();

			$("#btnAgregarArt").hide();


			//muestra los detalles de un entrada en particular

			$.post("../ajax/entrada.php?op=listarDetalle&id=" + identrada,function(r){

				$("#detalles").html(r);

			});

 	});

}

//Función para anular registros, se lo realiza en tiempo real
function anular(identrada){

	bootbox.confirm("¿Está Seguro de anular la entrada?", function(result){

		if(result){

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/entrada.php?op=anular", {identrada : identrada}, function(e){

        		bootbox.alert(e);

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	});

}

function marcarImpuesto(){

  	var tipo_comprobante = $("input[name='tipo_comprobante']:checked").val();

  	//console.log(tipo_comprobante);

  	if (tipo_comprobante=='Factura')

        $("#impuesto").val(impuesto); 

    else

        $("#impuesto").val("0"); 

}

//Se agrega articulos de la ventana MODAL
function agregarDetalle(idarticulo,articulo,laboratorio,precio_venta,cantidad,precio_compra,fecha_actual){

	venta = "";

	compra = "";

	if (precio_venta != "") 
		venta = precio_venta;
	else 
		venta = 0;

	if (precio_compra != "") 
		compra = precio_compra;
	else 
		compra = 0;

	bootbox.prompt("Introduzca la cantidad", function(cant){ 
		if (cant != null) {
			if (!isNaN(cant)) {
				cant = parseInt(cant);
				if (cant > 0) {
				    if (idarticulo != ""){

						//SELECT DE LOS LOTES DISPONIBLES
				    	var dato = $.ajax({
							url: "../ajax/entrada.php?op=buscar_lotes&idarticulo="+idarticulo+"&cont="+cont, //indicamos la ruta
							dataType: 'text',//indicamos que es de tipo texto plano
							async: false     //ponemos el parámetro asyn a falso
						}).responseText;

				    	var fila ='<tr class="filas" id="fila'+cont+'">'+
				    	'<td><button type="button" class="btn btn-danger" name="boton" id="boton'+cont+'" onclick="eliminarDetalle('+cont+')">X</button></td>'+
				    	'<td><input type="hidden" name="idarticulo[]" id="idartidulo'+cont+'" value="'+idarticulo+'">'+articulo+'</td>'+
						'<td><input class="form-control" type="number" min=1 pattern="^[0-9]+" name="cantidad[]" id="cantidad'+cont+'" value="'+cant+'" required></td>'+
						'<td>'+dato+'</td>'+	
				    	'<td><input class="form-control" type="text" name="cod_lote[]" value="" id="cod_lote'+cont+'" required></td>'+
				    	'<td><input class="form-control" type="date" name="vencimiento[]" min="'+fecha_actual+'" value="" id="vencimiento'+cont+'"></td>'+
				    	'</tr>';

						$('#detalles').append(fila);

				    	cont++;

				    	detalles++;

						evaluar();

				    }else
				    	bootbox.alert("<h4><i class='fa fa-times-circle-o' aria-hidden='true'></i>Error al ingresar el detalle, revisar los datos del artículo</h4>");

				}else
					bootbox.alert("<h4><i class='fa fa-times-circle-o' aria-hidden='true'></i>Debe ingresar un valor entero positivo</h4>");

			}else
				bootbox.alert("<h4><i class='fa fa-times-circle-o' aria-hidden='true'></i>Debe ingresar un valor entero positivo</h4>");
		}
	});

}

function evaluar(){
  	if (detalles > 0)
      	$("#btnGuardar").show();
    else{
      	$("#btnGuardar").hide(); 
      	cont = 0;
    }
}

function eliminarDetalle(indice){

	$("#fila" + indice).remove();

	detalles--;
	cont--;

	var filas = document.getElementsByClassName("filas");
	var boton = document.getElementsByName("boton");
	var idarticulo = document.getElementsByName("idarticulo[]");
	var cantidad = document.getElementsByName("cantidad[]");
	var idlote = document.getElementsByName("idlote[]");
	var cod_lote = document.getElementsByName("cod_lote[]");
	var vencimiento = document.getElementsByName("vencimiento[]");
	
	for(i = 0; i < idarticulo.length; i++){
		var fila = filas[i];
		var fila_boton = boton[i];
		var fila_idarticulo = idarticulo[i];
		var fila_cantidad = cantidad[i];
		var fila_idlote = idlote[i];
		var fila_cod_lote = cod_lote[i];
		var fila_vencimiento = vencimiento[i];

		fila_idarticulo.setAttribute("id","idarticulo"+i);
		fila_boton.setAttribute("id","boton"+i);
		fila.setAttribute("id","fila"+i);
		fila_cantidad.setAttribute("id","cantidad"+i);
		fila_idlote.setAttribute("id","idlote"+i);
		fila_cod_lote.setAttribute("id","cod_lote"+i);
		fila_vencimiento.setAttribute("id","vencimiento"+i);

		fila_boton.setAttribute("onclick","eliminarDetalle("+i+")");
		fila_idlote.setAttribute("onclick","verif_lote("+i+")");
		
	}
  	evaluar();

}

function verif_lote(cont){
    var idlote = document.getElementsByName("idlote[]");
    var cod_lote = document.getElementsByName("cod_lote[]");
    var vencimiento = document.getElementsByName("vencimiento[]");
    
    cod_lote[cont].value = "";

    vencimiento[cont].value = "";

    if(idlote[cont].value == "nuevo"){
        cod_lote[cont].readOnly = false;
        cod_lote[cont].required= true;

        vencimiento[cont].readOnly = false;
        
    }else{
        cod_lote[cont].readOnly = true;
        cod_lote[cont].required = false;

        vencimiento[cont].readOnly = true;
    }
}

init();