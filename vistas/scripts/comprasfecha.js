$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );
$('#tbllistado thead tr:eq(1) th').each( function (i) {
	var title = $(this).text();
	
	if(title != "Opciones" && title != "Imagen"){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}
	
	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla_listado.column(i).search() !== this.value ) {
			tabla_listado
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
} );

var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

//Función que se ejecuta al inicio
function init(){
	listar();
	monto_compra();
	monto_anul();
	$("#fecha_inicio").change(listar);
	$("#fecha_fin").change(listar);
	$('#mConsultaC').addClass("treeview active");
    $('#lConsulasC').addClass("active");
}


//Función Listar
function listar()
{
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_fin = $("#fecha_fin").val();

	tabla_listado=$('#tbllistado').dataTable(
	{
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/consultas.php?op=comprasfecha',
					data:{fecha_inicio: fecha_inicio,fecha_fin: fecha_fin},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
	monto_compra();
	monto_anul();

}

function monto_compra(){
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_fin = $("#fecha_fin").val();

	//alert(fecha_inicio + " " + fecha_fin)
	$.post("../ajax/consultas.php?op=monto_comprasfecha",{fecha_inicio : fecha_inicio, fecha_fin: fecha_fin}, function(data, status)
	{
		
		data = JSON.parse(data);
		//alert(data.aaData);
		cad = "Monto de compras: "	
		if(data.aaData == "")
			$("#com").html("<strong>" + cad + " Bs. 0 </strong>");
		else
			$("#com").html("<strong>" + cad + " Bs. " +data.aaData +"</strong>");

	})
}

function monto_anul(){
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_fin = $("#fecha_fin").val();

	//alert(fecha_inicio + " " + fecha_fin)
	$.post("../ajax/consultas.php?op=anul_comprasfecha",{fecha_inicio : fecha_inicio, fecha_fin: fecha_fin}, function(data, status)
	{
		
		data = JSON.parse(data);
		//alert(data.aaData);
		cad = "Monto de compras anuladas: "	
		if(data.aaData == "")
			$("#nul").html("<strong>" + cad + " Bs. 0 </strong>");
		else
			$("#nul").html("<strong>" + cad + " Bs. " +data.aaData +"</strong>");

	})
}

init();