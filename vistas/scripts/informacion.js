var tabla;
var tabla_detalles = $("#detalles").DataTable();
var tabla_combos = $("#detallesCombo").DataTable();

//Función que se ejecuta al inicio
function init(){
	mostrarform();
	$("#formulario").on("submit",function(e){
		guardar(e);	
	});
	$('#mConfiguracion').addClass("treeview active");
    $('#lInformacion').addClass("active");  
}

//Función mostrar formulario
function mostrarform(){
	$.post("../ajax/informacion.php?op=mostrar", function(data, status){
		console.log(data);
		data = JSON.parse(data);
		$("#codigo_sucursal").val(data.codigoSucursal);
		$("#nombre_empresa").val(data.nombre_empresa + " - " + data.razonSocialEmisor);
		$("#nit").val(data.nit);
		$("#direccion").val(data.direccion);
		$("#municipio").val(data.municipio);
		$("#descripcion").val(data.descripcion);
		$("#telefono").val(data.telefono);
		$("#correo").val(data.correo);
	});
}



//Función para guardar la preventa

function guardar(e){
	e.preventDefault(); //No se activará la acción predeterminada del evento
	bootbox.confirm("¿Está Seguro/a de editar la información?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
			var formData = new FormData($("#formulario")[0]);
			$.ajax({
				url: "../ajax/informacion.php?op=editar",
			    type: "POST",
			    data: formData,
			    contentType: false,
			    processData: false,
			    success: function(datos){                    
			        bootbox.alert(datos);	          
			    	mostrarform();
			    }
			});
		}
	});
}

function volver_menu() {  
	window.location.href = "escritorio.php";
}

init();

$(document).ready(function () {
	/*
	var id = $("#id_token").val();
	if(id == 0){
		alert("Token Inválido o vencido");
		setTimeout(volver_menu(),3000);
	}
	*/

	//carga de lista de productos según SIN
	$.post("../ajax/sinc_datos.php?op=hora_fecha", function(r){
		if(r == false){
			alert("Debe realizar la sincronización del día para cargar la lista de puntos de venta");
			setTimeout(volver_menu(),3000);
		}
	});
});