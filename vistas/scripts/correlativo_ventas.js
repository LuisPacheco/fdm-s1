var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();
	$("#formulario").on("submit",function(e){
		guardaryeditar(e);	
	});
    $('#mConfiguracion').addClass("treeview active");
    $('#lCorrelativoVentas').addClass("active");

}

//Función limpiar
function limpiar(){
	$("#id_correlativo").val("");
	$("#actual").val("");
	$("#descripcion").val("");
	$("#max").val("");
	$("#fecha_vigencia").val("");
}

//Función mostrar formulario
function mostrarform(flag){
	
	limpiar();
	if (flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform(){
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(){
	tabla_listado=$('#tbllistado').dataTable({

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla

	    buttons: [		          

			'copyHtml5',

			'excelHtml5',

			'csvHtml5',

			'pdf'

		],

		"ajax":{
				url: '../ajax/correlativo_ventas.php?op=listar',
				type : "get",
				dataType : "json",						
				error: function(e){
					console.log(e.responseText);	
				}

		},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();
}

//Función para guardar o editar
function guardaryeditar(e){
	e.preventDefault(); //No se activará la acción predeterminada del evento
	var id_correlativo = $("#id_correlativo").val();
	var mensaje = "";
	if(id_correlativo == "")
		mensaje = "¿Está Seguro/a de registrar el correlativo?";
	else
		mensaje = "¿Está Seguro/a de actualizar el correlativo?";

	bootbox.confirm(mensaje, function(result){
		if(result){
			$("#btnGuardar").prop("disabled",true);
			var formData = new FormData($("#formulario")[0]);
			$("body").css({"padding-right":"0px"});
			$.ajax({
				url: "../ajax/correlativo_ventas.php?op=guardaryeditar",
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				success: function(datos){                    
					bootbox.alert(datos);	          
					mostrarform(false);
					tabla_listado.ajax.reload();
				}
			});
			limpiar();
		}
		$("body").css({"padding-right":"0px"});
		
	});
	
}

//activar un correlativo
function activar(id_corr){
	bootbox.confirm("¿Está Seguro/a de activar el correlativo?<br><strong>Solo puede activar un correlativo a la vez</strong>", function(result){
		if(result){
			$.ajax({
				url: "../ajax/correlativo_ventas.php?op=activar&id_correlativo="+id_corr,
				type: "GET",
				contentType: false,
				processData: false,
				success: function(datos){                    
					bootbox.alert(datos);	          
					mostrarform(false);
					tabla_listado.ajax.reload();
				}
			});
			limpiar();
		}
		$("body").css({"padding-right":"0px"});
	});
}

//mostrar correlativo
function mostrar(id_corr){
	$.post("../ajax/correlativo_ventas.php?op=mostrar",{id_correlativo : id_corr}, function(data, status){

		data = JSON.parse(data);		
		
		mostrarform(true);

		$("#id_correlativo").val(data.id_correlativo);

		$("#actual").val(data.actual);

		$("#descripcion").val(data.descripcion);

		$("#max").val(data.max);

		$("#fecha_vigencia").val(data.fecha_vigencia);

 	});
}

init();