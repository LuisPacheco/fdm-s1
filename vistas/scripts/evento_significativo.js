var tabla = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();
	$("#formulario").on("submit",function(e){
		guardar(e);	
	});
    $('#mConfiguracion').addClass("treeview active");
    $('#lEventoSignificativo').addClass("active");

}

//Función limpiar
function limpiar(){
	$("#evento_significativo").val("");
	$("#cafc").val("");
	$("#fecha_inicio_evento").val("");
	$("#hora_inicio_evento").val("").prop("readonly",true);
	$('#fecha_fin_evento').val('').prop("readonly",true);
	$("#hora_fin_evento").val("").prop("readonly",true);
}

//Función mostrar formulario
function mostrarform(flag){
	
	limpiar();
	if (flag){
		//obtener punto de venta actual y verificar si no tiene otro evento significativo activo
		var id_pv_session_actual = $("#id_pv_session_actual").val();
		$.ajax({
			type: "get",
			url: "../ajax/evento_significativo.php?op=verif_es_activo_pv&id_pv="+id_pv_session_actual,
			success: function (response) {
				if(response)
					bootbox.alert("Ya se tiene eventos activos para el punto de venta, intente más tarde");
				else{
					$("#listadoregistros").hide();
					$("#formularioregistros").show();
					$("#btnGuardar").prop("disabled",false);
					$("#btnagregar").hide();
					mostrar_eventos();
				}
			}
		});
		
	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		mostrar_puntos_venta();
	}
}

function mostrar_puntos_venta(){
	$.ajax({
		type: "GET",
		url: "../ajax/punto_venta.php?op=select_puntos_venta2",
		dataType: "text",
		success: function (response) {
			$("#id_punto_venta").html(response);
		}
	});
}

function mostrar_eventos(){
	$.ajax({
		type: "GET",
		url: "../ajax/evento_significativo.php?op=obtenerEventosSignificativos",
		dataType: "text",
		success: function (response) {
			$("#evento_significativo").html(response);
		},
		error:function(res){
			console.log(res);
		}
	});
}
//Función cancelarform
function cancelarform(){
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(){

	var punto_venta = $("#id_punto_venta").val();
	var fecha_evento = $("#fecha_evento").val();

	tabla=$('#tbllistado').dataTable({

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla

	    buttons: [		          

			'copyHtml5',

			'excelHtml5',

			'csvHtml5',

			'pdf'

		],

		"ajax":{
				url: '../ajax/evento_significativo.php?op=listar',
				data:{punto_venta: punto_venta,fecha_evento: fecha_evento},
				type : "get",
				dataType : "json",						
				error: function(e){
					console.log(e.responseText);	
				}

		},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 2, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();
}

//Función para guardar o editar
function guardar(e){
	e.preventDefault(); //No se activará la acción predeterminada del evento
	bootbox.confirm("¿Está Seguro/a de registrar el evento significativo?", function(result){
		if(result){
			$("#btnGuardar").prop("disabled",true);
			var formData = new FormData($("#formulario")[0]);
			$("body").css({"padding-right":"0px"});
			$.ajax({
				url: "../ajax/evento_significativo.php?op=guardar",
				type: "POST",
				data: formData,
				dataType: "json",
				contentType: false,
				processData: false,
				success: function(datos){       
					//bootbox.alert(datos);   
					if(datos.estado == "1"){         
						bootbox.alert("Modalidad del sistema fuera de línea<br><strong>Evento:</strong> " + datos.mensaje +"<br><strong>Fecha y Hora de registro de evento:</strong> "+datos.hora_registro);	          
					}else{
						bootbox.alert(datos.mensaje);	          
					}
					mostrarform(false);
				},
				error:function(datos){
					bootbox.alert(datos);	
				}
			});
			limpiar();
		}else{
			$("body").css({"padding-right":"0px"});
		}
	});
	
}

function enviar_paquetes(id_evento_significativo){
	
	bootbox.confirm("¿Está Seguro/a de enviar el paquete y registrar el evento al SIN?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/evento_significativo.php?op=enviar_paquetes", {id_evento_significativo : id_evento_significativo}, function(e){
				bootbox.alert(e);
				return;
				e = JSON.parse(e);
				bootbox.alert(e.mensaje);
				if(e.estado == 1){
					tabla.ajax.reload();
				}
				
        		//bootbox.alert(e);

				//mostrarform(false);
				
        	});	
			limpiar();
		}else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

function validar_paquetes(id_evento_significativo){
	$("body").css({"padding-right":"0px"});

	$.post("../ajax/evento_significativo.php?op=validar_paquetes", {id_evento_significativo : id_evento_significativo}, function(e){
		//bootbox.alert(e);		
		
		e = JSON.parse(e);
		bootbox.alert(e.mensaje);
		
		if(e.estado == 1){
			tabla.ajax.reload();
		}
		
	});	

	limpiar();
}

function reenviar_paquetes(id_es){
	bootbox.confirm("¿Está Seguro/a de reenviar el paquete para su revisión del SIN?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/evento_significativo.php?op=reenviar_paquetes", {id_evento_significativo : id_es}, function(e){
					
				//bootbox.alert(e);
				
				e = JSON.parse(e);
				bootbox.alert(e.mensaje);
				if(e.estado == 1){
					tabla.ajax.reload();
				}
				
			
        	});	
			limpiar();
		}else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

function volver_menu() {  
	window.location.href = "escritorio.php";
}


init();

$(document).ready(function () {
	var pv = $("#id_pv_session_actual").val();
	if(pv != ""){
		/*
		var id = $("#id_token").val();
		if(id == 0){
			alert("Token Inválido o vencido");
			setTimeout(volver_menu(),3000);
		}
		*/


		var cuis_activo = $("#cuispv").val();
		if(cuis_activo == 0){
			//var id_pv = $("#id_pv_session_actual").val();
			//if( id_pv == ""){
				alert("Cuis de PV inactivo");
				setTimeout(volver_menu(),3000);
			//}
		}
	}else{
		alert("No tiene un Punto de Venta asignado, intente más tarde");
		setTimeout(volver_menu(),3000);
	}
});

//obtener nombre de la opcion utilizada en el select de eventos
$("#evento_significativo").change(function () { 
	var texto = $( "#evento_significativo option:selected" ).text();
	$("#descripcion_evento").val(texto);
});

$("#fecha_inicio_evento").change(function(){
	var valor = $(this).val();
	if(valor != ""){
		$("#hora_inicio_evento").removeAttr("readonly");
		$("#fecha_fin_evento").prop("min",valor).prop("max",new Date().toJSON().slice(0, 10));
	}else{
		$("#hora_inicio_evento").prop("readonly",true).val("");
		$("#fecha_fin_evento").prop("readonly",true).val("").prop("min","").prop("max","");
		$("#hora_fin_evento").prop("readonly",true).val("").prop("min","").prop("max","");
	}
});

$("#hora_inicio_evento").change(function(){
	var valor = $(this).val();
	if(valor != ""){
		$("#fecha_fin_evento").removeAttr("readonly");		
	}else{
		$("#fecha_fin_evento").prop("readonly",true).val("");
		//.prop("min","").prop("max","");
		$("#hora_fin_evento").prop("readonly",true).val("");
		//.prop("min","").prop("max","");
	}
});


$("#fecha_fin_evento").change(function(){
	var valor = $(this).val();
	$("#hora_fin_evento").prop("readonly",true).val("").prop("min","").prop("max","");
	if(valor != ""){
		$("#hora_fin_evento").removeAttr("readonly");
		var fecha_inicio_ev = $("#fecha_inicio_evento").val();
		//var fecha_fin_ev = $("#fecha_fin_evento").val();
		if(valor == fecha_inicio_ev){
			var hora_inicio_ev = $("#hora_inicio_evento").val();
			$("#hora_fin_evento").prop("min",hora_inicio_ev).prop("max","23:59:59.999");
		}
	}else{
		$("#hora_fin_evento").prop("readonly",true).val("");
		//.prop("min","").prop("max","");
	}
});