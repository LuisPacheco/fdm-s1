var tabla;



var tabla_detalles = $('#detalles').DataTable();



//Función que se ejecuta al inicio

function init(){

	listar();

	mostrarform(false);

	//Cargamos los items al select cliente

	montos_ct();

	montos_st();

	montos_nul();

	sumar_desc();

	$.post("../ajax/venta.php?op=selectCliente", function(r){

	            $("#idcliente").html(r);

	            $('#idcliente').selectpicker('refresh');

	});

	$('#mConsultaV').addClass("treeview active");

    $('#lConsulasVs').addClass("active");

}



//Función mostrar formulario

function mostrarform(flag){

	limpiar();

	if (flag){

		$("#listadoregistros").hide();

		$("#formularioregistros").show();	

		$("#btnCancelar").show();

	

	}else{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

	}

}



//Función limpiar

function limpiar(){

	//Get the text using the value of select

	var text = $("select[name=idcliente] option[value='2']").text();

	//We need to show the text inside the span that the plugin show

	$('.bootstrap-select .filter-option').text(text);

	//Check the selected attribute for the real select

	$('#idcliente').val(2);





	//$("#idcliente").val("S/N");

	$("#cliente").val("");

	$("#No").prop("checked",true);

	$("#Recibo").prop("checked",true);

	$("#impuesto").val("0");



	$("#total_venta").val("");

	$(".filas").remove();

	$("#total").html("0");

	$("#num_comprobante").val("0");

	$("#idventa").val("");

	//Obtenemos la fecha actual

	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);

	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $('#fecha_hora').val(today);

	$("#monto").val("");

	$("#cambio").val("");

}



//Función cancelarform

function cancelarform(){

	limpiar();

	mostrarform(false);

}



function mostrar(idventa){

	$.post("../ajax/venta.php?op=mostrar",{idventa : idventa}, function(data, status){

		data = JSON.parse(data);

		//console.log(data);		

		mostrarform(true);



		$("#idcliente").val(data.idcliente);

		$("#idcliente").selectpicker('refresh');



		$("#idcliente").prop('disabled',true);

		

		if (data.tipo_comprobante == "Factura") 

			$("#Factura").prop("checked",true);

		else

			$("#Recibo").prop("checked",true);

		

		$("#Factura").prop("disabled",true);

		$("#Recibo").prop("disabled",true);

		

		if (data.tiene_tarjeta == "Si") 

			$("#Si").prop("checked",true);

		else

			$("#No").prop("checked",true);

		

		$("#Si").prop("disabled",true);

		$("#No").prop("disabled",true);



		$("#num_comprobante").val(data.num_comprobante);

		$("#num_comprobante").prop("disabled",true);



		$("#fecha_hora").val(data.fecha);

		$("#impuesto").val(data.impuesto);



		$("#fecha_hora").prop("disabled",true);

		$("#impuesto").prop("disabled",true);



		$("#idventa").val(data.idventa);



		//Ocultar y mostrar los botones

		$("#btnCancelar").show();

		$("#btnAgregarArt").hide();



		$("#total").html("Bs. " + data.total_venta);

		//mostrar cambio y monto
		var monto = data.monto;
		monto = Number(monto).toFixed(2);
		var cambio = data.cambio;
		cambio = Number(cambio).toFixed(2);

		$("#monto").val(monto).prop("readonly",true);
		$("#cambio").val(cambio);

		listarDetalle(idventa);

 	});



 	



}



function listarDetalle(idventa){

	tabla_detalles = $('#detalles').dataTable(

		{

			"aProcessing": true,							//Activamos el procesamiento del datatables

			"aServerSide": true,							//Paginación y filtrado realizados por el servidor

  			"pagingType": "simple",

			"ajax":

				{

					url: '../ajax/venta.php?op=listarDetalleConsultas&id='+idventa,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

			"bDestroy": true,	

		}

	).DataTable();

	$("#detalles").show();



	

}



//Función Listar

function listar(){

	var fecha_inicio = $("#fecha_inicio").val();

	var fecha_fin = $("#fecha_fin").val();



	tabla=$('#tbllistado').dataTable(

	{

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/consultas.php?op=ventasfechaclientes',

					data:{fecha_inicio: fecha_inicio,fecha_fin: fecha_fin},

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

	montos_ct();

	montos_st();

	montos_nul();

	sumar_desc();

}



function montos_ct(){

	var fecha_inicio = $("#fecha_inicio").val();

	var fecha_fin = $("#fecha_fin").val();



	//alert(fecha_inicio + " " + fecha_fin)

	$.post("../ajax/consultas.php?op=ventasfechaclientes_ct_acp",{fecha_inicio : fecha_inicio, fecha_fin: fecha_fin}, function(data, status)

	{

		

		data = JSON.parse(data);

		//alert(data.aaData);

		cad = "Monto de ventas con tarjeta: "	

		if(data.aaData == "")

			$("#ct").html("<strong>" + cad + " Bs. 0 </strong>");

		else

			$("#ct").html("<strong>" + cad + " Bs. " +data.aaData +"</strong>");



	})

}



function montos_st(){

	var fecha_inicio = $("#fecha_inicio").val();

	var fecha_fin = $("#fecha_fin").val();



	//alert(fecha_inicio + " " + fecha_fin)

	$.post("../ajax/consultas.php?op=ventasfechaclientes_st_acp",{fecha_inicio : fecha_inicio, fecha_fin: fecha_fin}, function(data, status)

	{

		

		data = JSON.parse(data);

		//alert(data.aaData);

		cad = "Monto de ventas sin tarjeta: "	

		if(data.aaData == "")

			$("#st").html("<strong>" + cad + " Bs. 0 </strong>");

		else

			$("#st").html("<strong>" + cad + " Bs. " +data.aaData +"</strong>");



	})

}



function montos_nul(){

	var fecha_inicio = $("#fecha_inicio").val();

	var fecha_fin = $("#fecha_fin").val();



	//alert(fecha_inicio + " " + fecha_fin)

	$.post("../ajax/consultas.php?op=ventasfechaclientes_nul",{fecha_inicio : fecha_inicio, fecha_fin: fecha_fin}, function(data, status)

	{

		

		data = JSON.parse(data);

		//alert(data.aaData);

		cad = "Monto de ventas anuladas: "	

		if(data.aaData == "")

			$("#nul").html("<strong>" + cad + " Bs. 0 </strong>");

		else

			$("#nul").html("<strong>" + cad + " Bs. " +data.aaData +"</strong>");



	})

}



function sumar_desc(){

	var fecha_inicio = $("#fecha_inicio").val();

	var fecha_fin = $("#fecha_fin").val();



	$.post("../ajax/consultas.php?op=sumar_descuentos",{fecha_inicio : fecha_inicio, fecha_fin: fecha_fin}, function(data, status)

	{

		

		data = JSON.parse(data);

		console.log(data);

		//alert(data.aaData);

		cad = "Suma de descuentos realizados: "	

		if(data.aaData == "")

			$("#dscto").html("<strong>" + cad + " Bs. 0 </strong>");

		else

			$("#dscto").html("<strong>" + cad + " Bs. " +data.aaData +"</strong>");



	})

}

//Función para anular registros
function anular(nombre_archivo){

	var np = nombre_archivo;
	bootbox.dialog({
		message: '<style>.datepicker{z-index: 99999 !important}</style><form class="form-horizontal" role="form" id="anular_factura"><input type="hidden" name="nombre_archivo" value="'+np+'"><div class="form-group"><label class="col-sm-3 control-label no-padding-right" for="motivo_anulacion"> Especifique Motivo </label><div class="col-sm-9"><select class="form-control" name="motivo_anulacion" id="motivo_anulacion" onchange="select_motivo_anulacion()"></select></div></div></form>',
		title: "Anulación Factura",
	
		buttons:{
			"success" :{
				"label" : "<i class='fa fa-times'></i> Anular Factura",
				"className" : "btn-sm btn-danger aceptar_anulacion",
				"callback": function() {
					var formData = new FormData($("#anular_factura")[0]);
					$.ajax({
						url: "../ajax/venta.php?op=anular",
						type: "POST",
						data: formData,
						contentType: false,
						processData: false,
						success: function(datos){                    
							bootbox.alert(datos);								
							mostrarform(false,0);
							tabla.ajax.reload();
						}
					});
				}		
			},
			"cancel" :{
				"label" : "<i class='icon-ok'></i> Cancelar",
				"className" : "btn-sm btn-secondary",
				"callback": function() {
				}
			}
		}
	});
	cargar_motivos_anulacion();
	$(".aceptar_anulacion").prop("disabled","true");
}

//motivos anulación
function cargar_motivos_anulacion(){
	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=obtenerMotivosAnulacion",
		dataType: "text",
		success: function (response) {
			$("#motivo_anulacion").html(response);
		},
		error:function(response){
			console.log(response);
		}
	});
}

//select motivos anulación
function select_motivo_anulacion() {  
	var x = document.getElementById("motivo_anulacion").value;
	if(x != ""){
		$(".aceptar_anulacion").removeAttr("disabled");
	}else{
		$(".aceptar_anulacion").prop("disabled","true");
	}
}

//ENVIAR POR CORREO EL DOCUMENTO EN PDF Y EL XML DE LA FACTURA SI ES QUE ESTA SE ENCUENTRA ACEPTADA
function enviar_correo(correo,idventa){
	$.ajax({
		type: "get",
		url: "../ajax/venta.php?op=enviar_correo&id_venta="+idventa+"&correo="+correo,
		dataType: "text",
		success: function (response) {
			bootbox.alert(response);
		},
		error:function(response){
			console.log(response);
		}
	});
}

//VERIFICAR ESTADO FACTURA
function verificar_estado(cuf){
	$.ajax({
		type: "get",
		url: "../ajax/venta.php?op=verificar_estado_factura&cuf="+cuf,
		dataType: "text",
		success: function (response) {
			bootbox.alert(response);
		},
		error:function(response){
			console.log(response);
		}
	});
}

init();