$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );
$('#tbllistado thead tr:eq(1) th').each( function (i) {
	var title = $(this).text();
	
	if(title != "Opciones" && title != "P.U." && title != "Img" && title != "Precios" && title != "Est."){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}
	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla.column(i).search() !== this.value ) {
			tabla
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
} );

var tabla = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

//Función que se ejecuta al inicio
function init(){
	listar();
	//Cargamos los items al select cliente

	$('#mConsultaV').addClass("treeview active");
    $('#lConsulasPr').addClass("active");

}

//Función Listar
function listar(){
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_fin = $("#fecha_fin").val();

	tabla=$('#tbllistado').dataTable(
	{
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
		dom: 'Blptrip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/consultas.php?op=productos_vendidos',
					data:{fecha_inicio: fecha_inicio,fecha_fin: fecha_fin},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 10,//Paginación
	    "order": [[ 7, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();


}
init();