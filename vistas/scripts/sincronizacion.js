$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {
	var title = $(this).text();
	if(title != "Opciones" && title != "Imagen"){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}

	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla_listado.column(i).search() !== this.value ) {
			tabla_listado
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
});

var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

var tabla;

var tablaDetalle = $("#detalles_show").DataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
});

//Función que se ejecuta al inicio

function init(){
	listar();
	$("#formulario").on("submit",function(e){
		guardar(e);	
	});
	$('#mConfiguracion').addClass("treeview active");
    $('#lSincronizacion').addClass("active");   
}


//Función Listar TODOS los ingresos realizados

function listar(){
	tabla_listado = $('#tbllistado').dataTable({
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/sinc_datos.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},

		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 10,//Paginación
	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();

}

//Función para guardar o editar
function guardar(e){
	e.preventDefault(); //No se activará la acción predeterminada del evento
	bootbox.confirm("¿Está Seguro/a de generar la sincronización de datos de la fecha?", function(result){
		if(result){
			$("#btnagregar").prop("disabled",true);
			$("body").css({"padding-right":"0px"});
			var formData = new FormData($("#formulario")[0]);
			$.ajax({
				url: "../ajax/sinc_datos.php?op=guardar",
			    type: "POST",
			    data: formData,
			    contentType: false,
			    processData: false,
			    success: function(datos){                    
			          bootbox.alert(datos);	        
					  tabla_listado.ajax.reload();
				}
			});
			refresh_pag();
		}
	});		
}

function hay_sincronizacion() {  
	$.ajax({
		type: "GET",
		url: "../ajax/sinc_datos.php",
		data: {op:'hay_sincronizacion'},
		dataType: "text",
		success: function (response) {
			if(response == "si")
				$("#btnagregar").prop("disabled",true);
			else
				$("#btnagregar").removeAttr("disabled");
		}
	});
}

function refresh_pag(){
	var fecha = new Date();
	var anio = fecha.getFullYear();
	var mes = "";
	var dia = "";
	if(fecha.getMonth() < 9)
		mes = "0"+(fecha.getMonth() + 1);
	else
		mes = (fecha.getMonth() + 1);

	if(fecha.getDate() < 10)
		dia = "0"+fecha.getDate();
	else
		dia = fecha.getDate();
	var fecha_actual = anio+"-"+mes+"-"+dia;
	//hay_sincronizacion(fecha_actual);
	$("#btnagregar").removeAttr("disabled");
}

function volver_menu() {  
	window.location.href = "escritorio.php";
}

function sincronizar_hora(){
	bootbox.confirm("¿Está Seguro/a de sincronizar la hora actual?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
			var formData = new FormData($("#formulario")[0]);
			$.ajax({
				url: "../ajax/sinc_datos.php?op=sincronizar_hora",
			    type: "POST",
			    data: formData,
			    contentType: false,
			    processData: false,
			    success: function(datos){                    
			          bootbox.alert(datos);	        
					  tabla_listado.ajax.reload();
				}
			});
			refresh_pag();
		}
	});		
}

function actualizar_sincronizacion(){
	bootbox.confirm("¿Está Seguro/a de actualizar la sincronización del día?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
			var formData = new FormData($("#formulario")[0]);
			$.ajax({
				url: "../ajax/sinc_datos.php?op=actualizar_sincronizacion",
			    type: "POST",
			    data: formData,
			    contentType: false,
			    processData: false,
			    success: function(datos){                    
			          bootbox.alert(datos);	        
					  tabla_listado.ajax.reload();
				}
			});
			refresh_pag();
		}
	});		
}

$(document).ready(function () {
	init();
	refresh_pag();

	/*
	var id = $("#id_token").val();
	if(id == 0){
		alert("Token Inválido o vencido");
		setTimeout(volver_menu(),3000);
	}*/
});
