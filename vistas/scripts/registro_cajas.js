var tabla;



//Función que se ejecuta al inicio
function init(){

	mostrarform();

	listar();



	$('#mCajas').addClass("treeview active");

    $('#lCajas').addClass("active");   

}

//Función limpiar
function limpiar(){

	//Get the text using the value of select

	var text = $("select[name=idcliente] option[value='2']").text();

	//We need to show the text inside the span that the plugin show

	$('.bootstrap-select .filter-option').text(text);

	//Check the selected attribute for the real select

	$('#idcliente').val(2);

	$("#idcliente").prop("disabled",false);



	$("#No").prop("checked",true).prop("disabled",false);

	$("#Si").prop("disabled",false);



	$("#Recibo").prop("checked",true).prop("disabled",false);

	$("#Factura").prop("disabled",false);



	$("#impuesto").val("0").prop("disabled",false);



	$("#total_venta").val("");

	$(".filas").remove();

	$("#total").html("0");

	$("#num_comprobante").val("0").prop("disabled",false);

	$("#idventa").val("");



	//Obtenemos la fecha actual

	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);

	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $('#fecha_hora').val(today).prop("disabled",false);

}

//Función mostrar formulario
function mostrarform(){

	$("#listadoregistros").show();

}

//Función cancelarform
function cancelarform(){

	limpiar();

	mostrarform();

}

//Función Listar
function listar(){

	var fecha = $("#fecha").val();



	tabla=$('#tbllistado').dataTable(

	{

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/venta.php?op=listar_cajas',

					data:{fecha: fecha},

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

	

}

function mostrar_detalles_caja(id,idusuario,fecha){

	$.post("../ajax/venta.php?op=mostrar_detalles_caja&id="+id+"&fecha="+fecha, function(data, status){
		data = JSON.parse(data);
		monto_inicial = Number(data.monto_inicial);


		if(monto_inicial == null || monto_inicial == "")
			monto_inicial  = 0.00;
		else
			monto_inicial = Number(monto_inicial);

        //PAGO EN EFECTIVO
		total_efectivo = $.ajax({
			url: "../ajax/venta.php?op=obtener_efectivo&idusuario="+idusuario+"&fecha="+fecha, 
			dataType: 'number',
			async: false     
		}).responseText;

		if(total_efectivo == null || total_efectivo == "")
			total_efectivo  = 0.00;
		else
			total_efectivo = Number(total_efectivo);

	    //PAGO EN TARJETA
		total_tarjeta = $.ajax({
			url: "../ajax/venta.php?op=obtener_pagos_tarjeta&idusuario="+idusuario+"&fecha="+fecha, 
			dataType: 'number',
			async: false     
		}).responseText;

		if(total_tarjeta == null || total_tarjeta == "")
			total_tarjeta  = 0.00;
		else
			total_tarjeta = Number(total_tarjeta);
			
		//PAGOS POR TRANSFERENCIA
    	total_transferencia = $.ajax({
    		url: "../ajax/venta.php?op=obtener_pagos_transferencia&idusuario="+idusuario+"&fecha="+fecha, 
    		dataType: 'number',
    		async: false     
    	}).responseText;
    
    	if(total_transferencia == null || total_transferencia == "")
    		total_transferencia  = 0.00;
    	else
    		total_transferencia = Number(total_transferencia);

	    //ANULADO EN EFECTIVO
		efectivo_anulado = $.ajax({
			url: "../ajax/venta.php?op=obtener_efectivo_anulado&idusuario="+idusuario+"&fecha="+fecha, 
			dataType: 'number',
			async: false     
		}).responseText;

		if(efectivo_anulado == null ||efectivo_anulado == "")
			efectivo_anulado  = 0.00;
		else
			efectivo_anulado = Number(efectivo_anulado);

	
        //ANULADO TARJETA
		tarjeta_anulado = $.ajax({
			url: "../ajax/venta.php?op=obtener_tarjeta_anulado&idusuario="+idusuario+"&fecha="+fecha, 
			dataType: 'number',
			async: false     
		}).responseText;

		if(tarjeta_anulado == null ||tarjeta_anulado == "")
			tarjeta_anulado  = 0.00;
		else
			tarjeta_anulado = Number(tarjeta_anulado);

		//ANULADO POR TRANSFERENCIA
    	transferencia_anulado = $.ajax({
    		url: "../ajax/venta.php?op=obtener_transferencia_anulado&idusuario="+idusuario+"&fecha="+fecha, 
    		dataType: 'number',
    		async: false     
    	}).responseText;
    
    	if(transferencia_anulado == null ||transferencia_anulado == "")
    		transferencia_anulado  = 0.00;
    	else
    		transferencia_anulado = Number(transferencia_anulado);

	    //DESCUENTOS EN TOTAL
		descuentos = $.ajax({
			url: "../ajax/venta.php?op=obtener_descuentos&idusuario="+idusuario+"&fecha="+fecha, 
			dataType: 'number',
			async: false     
		}).responseText;

		if(descuentos == null ||descuentos == "")
			descuentos  = 0.00;
		else
			descuentos = Number(descuentos);
		

		estado = "";
		if(data.estado == "Sin Abrir")
			estado = '<span class="label bg-blue">Sin Abrir</span>';
		if(data.estado == "Caja Abierta")
			estado = '<span class="label bg-green">Abierta</span>';
		if(data.estado == "Caja Cerrada")
			estado = '<span class="label bg-red">Cerrada</span>';

		obs_fin = "";
		if(data.observaciones_fin == "" || data.observaciones_fin == null)
			obs_fin = "Ninguna";
		else
			obs_fin = data.observaciones_fin;

		hora_cierre = "";
		if(data.hora_cierre == "" || data.hora_cierre == null || data.hora_cierre == "00:00:00"){
			hora_cierre = "--";
			obs_fin = "--";
		}else
			hora_cierre = data.hora_cierre;

		obs_inicio = "";

		if(data.observaciones_inicio == "" || data.observaciones_inicio == null)
			obs_inicio = "Ninguna";

		else
			obs_inicio = data.observaciones_inicio;

		bootbox.dialog({

			message: '<style>.datepicker{z-index: 99999 !important}</style>	<form class="form-horizontal" role="form" id="resumen_caja">		<h4><span>Nombre Usuario: '+ data.nombre + ' - ' + data.login + '</span></h4>		<h4><span>Estado: '+ estado + '</span></h4>		<h4><span>Turno: '+ data.turno + '</span></h4>		<hr>		<h4><span>Hora Apertura: '+ data.hora_apertura + '</span></h4><h4><span>Observaciones: '+ obs_inicio + '</span></h4><hr><h4><span>Hora Cierre: '+ hora_cierre + '</span></h4><h4><span>Observaciones: '+ obs_fin + '</span></h4><hr><h4><span>Monto inicial: Bs. '+ monto_inicial+'</span></h4><h4><span>Total de ventas en Efectivo: Bs. '+ total_efectivo+'</span></h4><h4><span>Total de ventas con Tarjeta : Bs. '+ total_tarjeta+'</span></h4><h4><span>Total de ventas por Transferencia : Bs. '+ total_transferencia+'</span></h4><h4><span>Total de ventas en Efectivo anuladas: Bs. '+ efectivo_anulado+'</span></h4><h4><span>Total de ventas con Tarjeta anuladas: Bs. '+ tarjeta_anulado+'</span></h4><h4><span>Total de ventas por Transferencia anuladas: Bs. '+ transferencia_anulado+'</span></h4><h4><span>Total de descuentos: Bs. '+ descuentos+'</span></h4><hr><h3><span>Monto total en caja: Bs. '+ (total_efectivo + monto_inicial)+'</span></h3><h3><span>Monto total en ventas (efectivo + tarjeta + transf): Bs. '+ (total_efectivo + total_tarjeta + total_transferencia) +'</span></h3><h3><span>Monto total ventas anuladas: Bs. '+ (efectivo_anulado + tarjeta_anulado + transferencia_anulado)+'</span></h3><hr></form>',

			title: "Detalles Caja",

			buttons:             

			{

				"cancel":

				{

					"label" : "Cancelar",

					"className" : "btn-sm btn-sendondary",

				}

			}

		});

 	});

}





init();