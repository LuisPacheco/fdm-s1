$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {
	var title = $(this).text();
	if(title != "Opciones" && title != "Imagen"){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}

	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla_listado.column(i).search() !== this.value ) {
			tabla_listado
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
} );

var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

var tabla;

$('#tblarticulos thead tr').clone(true).appendTo( '#tblarticulos thead' );

$('#tblarticulos thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();

	if(title != "Add" && title != "Imagen" && title != "Stock" && title != "Min" && title != "PV"){

		$(this).html( '<input type="text" style="width:100%"> ' );

	}else{

		$(this).html( '' );

	}

	$( 'input', this ).on( 'keyup change', function () {

		if ( tabla.column(i).search() !== this.value ) {

			tabla

				.column(i)

				.search( this.value )

				.draw();

		}

	} );

} );

var tabla = $('#tblarticulos').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});

var tablaDetalle = $("#detalles_show").DataTable({"bPaginate": false,

	"bLengthChange": false,

	"bFilter": true,

	"bInfo": false,

	"bAutoWidth": false
});

var tablaDetalle = $("#detalles_editar").DataTable({"bPaginate": false,

	"bLengthChange": false,

	"bFilter": true,

	"bInfo": false,

	"bAutoWidth": false
});

//Función que se ejecuta al inicio
function init(){
	mostrarform(false,0);
	listar();
	listarArticulos();

	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=obtenerModalidad",
		dataType: "text",
		success: function (r) {
			$("#modo_sistema").html(r);
			$('#modo_sistema').selectpicker('refresh');
		}
	});
	

	$('#mVentas').addClass("treeview active");
    $('#lVentas').addClass("active");  
}

//Función limpiar
function limpiar(){
	//Get the text using the value of select
	//var text = $("select[name=idcliente] option[value='2']").text();
	//We need to show the text inside the span that the plugin show
	//$('.bootstrap-select .filter-option').text(text);
	//Check the selected attribute for the real select
	$('#idcliente').val("");
	$('#idcliente').selectpicker('refresh');
	$("#idcliente").prop("disabled",false);

	$("#total_venta").val("");

	$(".filas").remove();

	$("#total").html("0");

	$("#idventa").val("");
	
	$("#metodo_pago").val("1").removeAttr("disabled");
	$('#metodo_pago').selectpicker('refresh');

	$("#nro_tarjeta").val("").prop("readonly",true);

	$("#codigo_excepcion").val("0").removeAttr("disabled");
	$('#codigo_excepcion').selectpicker('refresh');

	$("#codigo_contingencia").prop("readonly",true);

	$("#num_venta_contingencia").val("").prop("readonly",false);

	$("#motivo_anulacion").val("");

	//Obtenemos la fecha actual

	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);

	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $('#fecha_hora').val(today).prop("disabled",false);

	$("#fecha").val("");

	$("#hora").val("");

	//ocultar div_nuevo_cliente y limpiar campos
	//ocultar_div_nuevo_cliente();

	//LIMPIAR MONTO Y CAMBIO
	$("#monto").val("").removeAttr("readonly");
	$("#cambio").val("");	

	$("#div_otro_metodo_pago").hide();

	$("#otro_metodo_pago").val("").removeAttr("required").removeAttr("readonly");

	$(".alert").remove();

	//para datos del cliente
	$("#nombre_cliente").val("").removeAttr("readonly");
	$("#nombre_cliente_editar").val("").removeAttr("readonly");

	$("#documento_identidad").removeAttr("disabled").val("").selectpicker("refresh");
	$("#documento_identidad_editar").removeAttr("disabled").val("").selectpicker("refresh");

	$("#nro_cliente").val("").removeAttr("readonly");
	$("#nro_cliente_editar").val("").removeAttr("readonly");

	$("#nro_cliente").prop("pattern","[0-9]{1,15}").prop("maxlength","15");
	$("#nro_cliente_editar").prop("pattern","[0-9]{1,15}").prop("maxlength","15");

	$("#email").val("").removeAttr("readonly").removeAttr("disabled");
	$("#email_editar").val("").removeAttr("readonly").removeAttr("disabled");

	$("#telefono").val("").removeAttr("readonly").removeAttr("disabled");
	$("#telefono_editar").val("").removeAttr("readonly").removeAttr("disabled");
	
	$("#complemento").val("").removeAttr("readonly").removeAttr("disabled");
	$("#complemento_editar").val("").removeAttr("readonly").removeAttr("disabled");

	//para venta editable
	$("#id_evento_significativo_editar").val("");
	$("#cufd_editar").val("");
	$("#cod_control_editar").val("");
	$("#evento_editar").val("");

	$("#num_venta_contingencia_editar").val("");
	$("#num_venta_editar").val("");

	$("#fecha_editar").val("");
	$("#hora_editar").val("");

	$("#idcliente_editar").val("");
	$('#idcliente_editar').selectpicker('refresh');
	$("#idcliente_editar").prop("disabled",false);

	$("#metodo_pago_editar").val("");
	$('#metodo_pago_editar').selectpicker('refresh');
	$("#metodo_pago_editar").prop("disabled",false);

	$("#nro_tarjeta_editar").val("");

	$("#div_otro_metodo_pago_editar").hide();

	$("#otro_metodo_pago_editar").val("");

	$("#codigo_excepcion_editar").val("0");
	$('#codigo_excepcion_editar').selectpicker('refresh');
	$("#codigo_excepcion_editar").prop("disabled",false);

	$("#codigo_contingencia_editar").val("").prop("readonly",true).removeAttr("required");
}

//Función mostrar formulario
function mostrarform(flag,idventa){
	limpiar();
	if (flag){

		$("#listadoregistros").hide();

		$("#formularioregistros").show();


		$("#detalles").show();

		$("#detalles_show").hide();

		$('#detalles_show').parents('div.dataTables_wrapper').first().hide();

		$("#detalles_editar").hide();

		$('#detalles_editar').parents('div.dataTables_wrapper').first().hide();

		$("#btnGuardar").prop("disabled",false);

		$("#btnGuardarEditar").prop("disabled",false);

		$("#btnagregar").hide();

		$("#rpt_ventas").hide();


		$("#abrir_caja").hide();

		$("#cerrar_caja").hide();

		if(idventa == 0){//venta nueva
			
			//obtener correlativos si no se tiene eventos 5, 6 o 7
			var es = $("#es").val();
			if(es != "5" && es != "6" && es != "7"){
				//obtención correlativo
				var datos_corr = $.ajax({
					url: "../ajax/venta.php?op=obtener_correlativo_actual", //indicamos la ruta
					dataType: 'text',//indicamos que es de tipo texto plano
					async: false     //ponemos el parámetro asyn a falso
				}).responseText;
	
				let yourDate = new Date()
				//console.log(yourDate)
				var mes = yourDate.getMonth()+1;
				var dia = yourDate.getDate();

				if(mes < 10)
					mes = "0"+mes;
				
				if(dia < 10)
					dia = "0"+dia;

				var fecha = yourDate.getFullYear()+"-"+mes+"-"+dia;

				if(datos_corr != "null"){
					datos_corr = JSON.parse(datos_corr);
					
					if(Number(datos_corr.actual) > Number(datos_corr.max) || datos_corr.fecha_vigencia < fecha){
						bootbox.alert("Debe registrar un correlativo vigente o que no haya alcanzado al límite");
						setTimeout(volver_menu,3000);
					}else
						$("#num_venta").val(datos_corr.actual);
					
				}else{
					bootbox.alert("No cuenta con un correlativo vigente activo, debe realizar su registro para poder vender");
					setTimeout(volver_menu,3000);
				}
			}
			
			//validar si tiene cuis, cufd válido
			var cuis_activo = $("#cuis_activo").val();
			var cufd_activo = $("#cufd_activo").val();
			
			if(cuis_activo == 1 && cufd_activo == 1){

				$("#tblarticulos").show();

				$('#tblarticulos').parents('div.dataTables_wrapper').first().show();

				$("#evento_actual").val($("#modo_sistema").val());
			}else{
				bootbox.alert("Debe generar el CUIS o el CUFD antes de realizar ventas");
				setTimeout(volver_menu,3000);
			}

			//lista de clientes
			$.post("../ajax/venta.php?op=selectCliente", function(r){
				
				r = JSON.parse(r);
				var cad = "<option value=''>--Seleccione--</option><option value='nuevo' selected>NUEVO CLIENTE</option>";
				for(var i = 0; i < r.datos.length; i++){
				    if(r.datos[i][0] == 225 || r.datos[i][0] == 226 || (r.datos[i][0] >= 228 && r.datos[i][0] <= 239) || (r.datos[i][0] >= 241 && r.datos[i][0] <= 246) || r.datos[i][0] == 249 || (r.datos[i][0] >= 251 && r.datos[i][0] <= 253) || (r.datos[i][0] >= 255 && r.datos[i][0] <= 258) || (r.datos[i][0] >= 260 && r.datos[i][0] <= 261) || (r.datos[i][0] >= 263 && r.datos[i][0] <= 268) || r.datos[i][0] == 270){
					    cad += "<option value='"+r.datos[i][0]+"' disabled>"+r.datos[i][1]+" - "+r.datos[i][2]+" - "+r.datos[i][3]+"</option>";
					}else
					    cad += "<option value='"+r.datos[i][0]+"'>"+r.datos[i][1]+" - "+r.datos[i][2]+" - "+r.datos[i][3]+"</option>";
				}
				$("#idcliente").html(cad);
				$('#idcliente').selectpicker('refresh');


				//obtener metodos de pago
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=obtenerMetodosPago",
					dataType: "text",
					success: function (r) {
						
						r = JSON.parse(r);
						//console.log(r);

						var cad = "<option value=''>--Seleccione--</option>";
						for(var i = 0; i < r.datos.length; i++){
							if(i == 0)
								cad += "<option value='"+r.datos[i]["cod"]+"' selected>"+r.datos[i]["desc"]+"</option>";
							else
								cad += "<option value='"+r.datos[i]["cod"]+"'>"+r.datos[i]["desc"]+"</option>";
						}
						$("#metodo_pago").html(cad);
						$('#metodo_pago').selectpicker('refresh');
					}
				});
			});
			
		}else{//mostrar detalles venta

			$("#tblarticulos").hide();

			$('#tblarticulos').parents('div.dataTables_wrapper').first().hide();

		}

		//cargar lista de docs de identidad
		$.ajax({
			type: "GET",
			url: "../ajax/venta.php?op=obtenerDocumentoIdentidad",
			dataType: "text",
			success: function (r) {
				$("#documento_identidad").html(r);
				$('#documento_identidad').selectpicker('refresh');
			}
		});

		$("#btnGuardar").show().prop("disabled",true);

		$("#btnCancelar").show();

		detalles=0;
		cont = 0;

	}else{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#formularioeditar").hide();

		$("#btnagregar").show();

		$("#rpt_ventas").show();


		$("#abrir_caja").show();

		$("#cerrar_caja").show();

	}
}

//Función cancelarform
function cancelarform(){

	mostrarform(false,0);

}

//Función Listar
function listar(){

	tabla_listado=$('#tbllistado').dataTable({

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/venta.php?op=listar',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();



	$("body").css({"padding-right":"0px"});



	detalles = $.ajax({

		url: "../ajax/venta.php?op=verificar_usuario_apertura_caja", 

		dataType: 'text',

		async: false     

	}).responseText;

	if(detalles == "Caja Cerrada"){

		bootbox.alert("La caja ya se encuentra cerrada para el día de hoy");

	}

}

function abrir_caja(){
	//SE DEBE CARGAR LOS PV QUE SE ENCUENTREN DISPONIBLES DE SER USADOS
	var cad_pv  = $.ajax({
		url: "../ajax/punto_venta.php?op=select_puntos_venta3", //indicamos la ruta
		dataType: 'text',//indicamos que es de tipo texto numérico
		async: false     //ponemos el parámetro asyn a falso
	}).responseText;

	bootbox.dialog({
		message: '<style>.datepicker{z-index: 99999 !important}</style><form class="form-horizontal" role="form" id="apertura_caja"><div class="form-group"><label class="col-sm-3 control-label no-padding-right" for="monto_inicial"> Monto inicial </label><div class="col-sm-9"><input type="number" id="monto_inicial" name="monto_inicial" class="form-control" required></div></div><div class="form-group"><label class="col-sm-3 control-label no-padding-right" for="observaciones_inicio"> Observaciones </label><div class="col-sm-9"><textarea class="form-control" name="observaciones_inicio" placeholder="Observaciones"></textarea></div></div><div class="form-group"><label class="col-sm-3 control-label no-padding-right" for="turno" required> Turno </label><div class="col-sm-9"><select id="turno" name="turno" class="form-control" required><option value="" selected>--Seleccione--</option><option value="maniana">Mañana</option><option value="tarde">Tarde</option><option value="completo">Tiempo completo</option></select></div></div><div class="form-group"><label class="col-sm-3 control-label no-padding-right" for="id_punto_venta_usr"> Punto de venta </label><div class="col-sm-9">'+cad_pv+'</div></div></form>',
		title: "Apertura de caja",
		buttons:{
			"success" :{
				"label" : "<i class='fa fa-check'></i> Abrir caja",
				"className" : "btn-sm btn-success hola",
				"callback": function() {
					var formData = new FormData($("#apertura_caja")[0]);
					$.ajax({
						
						url: "../ajax/venta.php?op=aperturar_caja",
						type: "POST",
						data: formData,
						contentType: false,
						processData: false,
						success: function(datos){       
							//var pv = $("#codigo_punto_venta").val();
							//generar_cufd_caja(pv);   
							datos = JSON.parse(datos);          
							if(datos.mensaje == "si"){
								$cad = "<i class='fa fa-check-circle' aria-hidden='true'></i> Apertura de caja realizada correctamente<br>"+
										"<i class='fa fa-check-circle' aria-hidden='true'></i> Punto de venta asignado a usuario<br>"+
										"<i class='fa fa-check-circle' aria-hidden='true'></i> CUFD obtenido correctamente<br>"+
										"<i class='fa fa-check-circle' aria-hidden='true'></i> CUFD registrado para PV<br>";
								bootbox.alert($cad);
								$("#btnagregar").prop("disabled",false);
							}else{
								switch(datos.mensaje){
									case "no1":
										$cad = "<i class='fa fa-times-circle' aria-hidden='true'></i> No se pudo realizar apertura de caja, turno no elegido o punto de venta no asignado";
									break;

									case "no2":
										$cad = "<i class='fa fa-times-circle' aria-hidden='true'></i> Punto de venta no asignado";
									break;

									case "no3":
										$cad = "<i class='fa fa-times-circle' aria-hidden='true'></i> Punto de venta no registrado";
									break;

									case "no4":
										$cad = "<i class='fa fa-times-circle' aria-hidden='true'></i> CUFD no fue generado";
									break;

									case "no5":
										$cad = "<i class='fa fa-times-circle' aria-hidden='true'></i> CUFD no fue generado";
									break;

									default:
										$cad = "<i class='fa fa-times-circle' aria-hidden='true'></i> Hubo un error al abrir caja";
									break;
								}
								bootbox.alert($cad);
								$("#btnagregar").prop("disabled",true);
							}
							mostrarform(false,0);
							tabla_listado.ajax.reload();
							setInterval("location.reload()",2000);
						}
					});
				}		
			},

			"cancel" :{
				"label" : "<i class='icon-ok'></i> Cancelar",
				"className" : "btn-sm btn-secondary",
				"callback": function() {
					console.log("entra");
					$("#btnagregar").prop("disabled",true);
					$("#abrir_caja").prop("disabled",false);
					$("#cerrar_caja").prop("disabled",true);
					$.ajax({
						success: function(datos){       
							setTimeout(volver_menu,1000);				
						}
					});
				}
			}
		}
	});
}

function cerrar_caja(idusuario){
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
	
	//Monto inicial (por lo general no ponen ningún valor o sea Bs. 0)
	monto_inicial = $.ajax({
		url: "../ajax/venta.php?op=obtener_monto_inicial&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'text',
		async: false     
	}).responseText;

	if(monto_inicial == null || monto_inicial == "" || monto_inicial == undefined)
		monto_inicial  = 0.00;
	else
		monto_inicial = Number(monto_inicial);

	//PAGOS EN EFECTIVO
	total_efectivo = $.ajax({
		url: "../ajax/venta.php?op=obtener_efectivo&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(total_efectivo == null || total_efectivo == "" || total_efectivo == undefined)
		total_efectivo  = 0.00;
	else
		total_efectivo = Number(total_efectivo);

	//PAGOS EN TARJETA
	total_tarjeta = $.ajax({
		url: "../ajax/venta.php?op=obtener_pagos_tarjeta&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(total_tarjeta == null || total_tarjeta == "")
		total_tarjeta  = 0.00;
	else
		total_tarjeta = Number(total_tarjeta);

	//PAGOS POR TRANSFERENCIA
	total_transferencia = $.ajax({
		url: "../ajax/venta.php?op=obtener_pagos_transferencia&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(total_transferencia == null || total_transferencia == "")
		total_transferencia  = 0.00;
	else
		total_transferencia = Number(total_transferencia);

	//ANULADO EN EFECTIVO
	efectivo_anulado = $.ajax({
		url: "../ajax/venta.php?op=obtener_efectivo_anulado&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(efectivo_anulado == null ||efectivo_anulado == "")
		efectivo_anulado  = 0.00;
	else
		efectivo_anulado = Number(efectivo_anulado);

	//ANULADO POR TARJETA
	tarjeta_anulado = $.ajax({
		url: "../ajax/venta.php?op=obtener_tarjeta_anulado&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(tarjeta_anulado == null ||tarjeta_anulado == "")
		tarjeta_anulado  = 0.00;
	else
		tarjeta_anulado = Number(tarjeta_anulado);

	//ANULADO POR TRANSFERENCIA
	transferencia_anulado = $.ajax({
		url: "../ajax/venta.php?op=obtener_transferencia_anulado&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(transferencia_anulado == null ||transferencia_anulado == "")
		transferencia_anulado  = 0.00;
	else
		transferencia_anulado = Number(transferencia_anulado);
	
	//DESCUENTOS EN TOTAL
	descuentos = $.ajax({
		url: "../ajax/venta.php?op=obtener_descuentos&idusuario="+idusuario+"&fecha="+today, 
		dataType: 'number',
		async: false     
	}).responseText;

	if(descuentos == null ||descuentos == "")
		descuentos  = 0.00;
	else
		descuentos = Number(descuentos);

	bootbox.dialog({
		message: '<style>.datepicker{z-index: 99999 !important}</style><form class="form-horizontal" role="form" id="apertura_caja"><h4><span>Monto inicial: Bs. '+ monto_inicial+'</span></h4><h4><span>Total de ventas en Efectivo: Bs. '+ total_efectivo+'</span></h4><h4><span>Total de ventas con Tarjeta : Bs. '+ total_tarjeta+'</span></h4><h4><span>Total de ventas por Transferencia : Bs. '+ total_transferencia+'</span></h4><h4><span>Total de ventas en Efectivo anuladas: Bs. '+ efectivo_anulado+'</span></h4><h4><span>Total de ventas con Tarjeta anuladas: Bs. '+ tarjeta_anulado+'</span></h4><h4><span>Total de ventas por Transferencia anuladas: Bs. '+ transferencia_anulado+'</span></h4><h4><span>Total de descuentos: Bs. '+ descuentos+'</span></h4><hr><h3><span>Monto total en caja: Bs. '+ (total_efectivo + monto_inicial)+'</span></h3><h3><span>Monto total en ventas (efectivo + tarjeta + transf): Bs. '+ (total_efectivo + total_tarjeta + total_transferencia)+'</span></h3><h3><span>Monto total ventas anuladas: Bs. '+ (efectivo_anulado + tarjeta_anulado + transferencia_anulado)+'</span></h3><hr><div class="form-group">	<label class="col-sm-3 control-label no-padding-right" for="observaciones_fin"> Observaciones </label>	<div class="col-sm-9"><textarea class="form-control" name="observaciones_fin" placeholder="Observaciones"></textarea></div></div></form>',
		title: "Cierre de caja",
		buttons:             
		{
			"success" :
			{
				"label" : "<i class='icon-ok'></i> Cerrar caja",
				"className" : "btn-sm btn-danger",
				"callback": function() {
					var formData = new FormData($("#apertura_caja")[0]);
					$.ajax({
						url: "../ajax/venta.php?op=cerrar_caja",
						type: "POST",
						data: formData,
						contentType: false,
						processData: false,
						success: function(datos){  
							$("body").css({"padding-right":"0px"});                  
							bootbox.alert(datos);
							if(datos == "Cierre de caja realizado satisfactoriamente"){
								$("#cerrar_caja").prop("disabled",true);
								$("#btnagregar").prop("disabled",true);
							}else{
								$("#cerrar_caja").prop("disabled",false);
								$("#btnagregar").prop("disabled",false);
							}	
							$("#abrir_caja").prop("disabled",true);			
						}
					});
				}
			},
			"cancel":
			{
				"label" : "Cancelar",
				"className" : "btn-sm btn-sendondary",
			}
		}
	});
				
}

//Función ListarArticulos
function listarArticulos(){

	tabla=$('#tblarticulos').dataTable({

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [],

		"ajax":

				{

					url: '../ajax/venta.php?op=listarArticulosVenta2',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 9, "asc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función para guardar o editar
function guardaryeditar(e,tipo){

	e.preventDefault(); //No se activará la acción predeterminada del evento

	if(tipo == "crear"){
		bootbox.confirm("¿Está Seguro/a de efectuar la venta?", function(result){

			if(result){
			    $('.btn').prop("disabled",true);

				$("#btnGuardar").prop("disabled",true);

				$("body").css({"padding-right":"0px"});

				var formData = new FormData($("#formulario")[0]);

				$.ajax({

					url: "../ajax/venta.php?op=guardaryeditar",

					type: "POST",

					data: formData,

					contentType: false,

					processData: false,

					success: function(datos){   

						//bootbox.alert(datos);
							
						
						datos = JSON.parse(datos);
				
						bootbox.alert(datos.respuesta);	                 
						if(datos.estado == "0"){
							$("#btnGuardar").removeAttr("disabled");
							$("#btnagregar").removeAttr("disabled");
							$("#cerrar_caja").removeAttr("disabled");
							$("#rpt_ventas").removeAttr("disabled");
						}else{
							//respuesta correcta de factura generada ya sea online u offline
							mostrarform(false,0);
							$("#btnagregar").removeAttr("disabled");
							$("#cerrar_caja").removeAttr("disabled");
							$("#rpt_ventas").removeAttr("disabled");

							tabla.ajax.reload();
		
							tabla_listado.ajax.reload();

							limpiar();

							$(".btn").removeAttr("disabled");

							$("#abrir_caja").prop("disabled",true);
							
						} 
								
					},
					error:function(datos){
						console.log(datos);
						alert("error");
					}
				});
			}else{
				$("body").css({"padding-right":"0px"});
				bootbox.alert("Venta no efectuada");
				$('.btn').removeAttr("disabled");
			}
		});
	}else{
		bootbox.confirm("¿Está Seguro/a de editar la venta?", function(result){

			if(result){
			    
			    $('.btn').prop("disabled",true);
				
				$("#btnGuardarEditar").prop("disabled",true);

				$("body").css({"padding-right":"0px"});

				var formData = new FormData($("#formulario_editar")[0]);

				$.ajax({

					url: "../ajax/venta.php?op=editar_venta",

					type: "POST",

					data: formData,

					contentType: false,

					processData: false,

					success: function(datos){   

						//bootbox.alert(datos);
						
						datos = JSON.parse(datos);
				
						bootbox.alert(datos.mensaje);	                 
						if(datos.estado == "0")
							$("#btnGuardarEditar").removeAttr("disabled");
						else{
							//respuesta correcta de factura generada ya sea online u offline
							mostrarform(false,0);
			
							tabla.ajax.reload();
		
							tabla_listado.ajax.reload();

							limpiar();

							$('.btn').removeAttr("disabled");

							$("#abrir_caja").prop("disabled",true);

							
						} 
						
						
					},
					error:function(datos){
						console.log(datos);
						alert("error al editar");
					}
				});
			}else{
				$("body").css({"padding-right":"0px"});
				bootbox.alert("Venta no actualizada");
				$('.btn').removeAttr("disabled");
			}
		});
	}
}

function mostrar(idventa,editar){
	if(editar == "0"){
		$("#formularioeditar").hide();
		$.post("../ajax/venta.php?op=mostrar",{idventa : idventa}, function(data, status){

			data = JSON.parse(data);

			mostrarform(true,idventa);

			$("#idventa").val(data.venta.idventa);

			//nro de venta
			if(data.venta.nro_venta != 0 && data.venta.nro_venta != ""){
				$("#num_venta").val(data.venta.nro_venta);
			}else{
				if(data.venta.nro_venta_contingencia != 0 && data.venta.nro_venta_contingencia != ""){
					$("#num_venta").val(data.venta.nro_venta_contingencia);
				}
			}

			//console.log(data);
			//FECHA DE VENTA
			$("#fecha_hora").val(data.venta.fecha).prop("disabled",true);

			//lista de clientes
			$.post("../ajax/venta.php?op=selectCliente", function(r){
					
				r = JSON.parse(r);
				var cad = "<option value=''>--Seleccione--</option><option value='nuevo'>NUEVO CLIENTE</option>";
				for(var i = 0; i < r.datos.length; i++){
					if(r.datos[i][0] == data.venta.idcliente){
						cad += "<option value='"+r.datos[i][0]+"' selected>"+r.datos[i][1]+" - "+r.datos[i][2]+" - "+r.datos[i][3]+"</option>";
						break;
					}
				}
				$("#idcliente").html(cad);
				$('#idcliente').selectpicker('refresh').prop("disabled",true);


				//LLAMAR A LOS DATOS DE LA PERSONA EN BASE AL ID
				mostrar_cliente(data.venta.idcliente);

				$("#nombre_cliente").prop("readonly",true);
				$("#nro_cliente").prop("readonly",true);
				$("#complemento").prop("readonly",true);
				$("#email").prop("readonly",true);
				$("#telefono").prop("readonly",true);
				$("#documento_identidad").prop("disabled",true);

				//obtener metodos de pago
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=obtenerMetodosPago",
					dataType: "text",
					success: function (r) {
						
						r = JSON.parse(r);
						//console.log(r);

						var cad = "<option value=''>--Seleccione--</option>";
						for(var i = 0; i < r.datos.length; i++){
							if(data.venta.metodo_pago == r.datos[i]["cod"]){
								cad += "<option value='"+r.datos[i]["cod"]+"' selected>"+r.datos[i]["desc"]+"</option>";
								break;
							}
						}
					
						$("#metodo_pago").html(cad).prop("disabled",true);
						$('#metodo_pago').selectpicker('refresh');
					}
				});
			});

			//NRO TARJETA OFUSCADO
			$("#nro_tarjeta").val(data.venta.nro_tarjeta_ofus);

			//OTRA FORMA DE PAGO
			if(data.venta.otro_metodo_pago != "" && data.venta.otro_metodo_pago != null){
				$("#div_otro_metodo_pago").show();
				$("#otro_metodo_pago").val(data.venta.otro_metodo_pago).prop("readonly",true);
			}

			//CÓDIGO EXCEPCION
			if(data.venta.codigo_excepcion == "0"){
				$("#codigo_excepcion").val("0").selectpicker("refresh").prop("disabled",true);
			}else
				$("#codigo_excepcion").val("1").selectpicker("refresh").prop("disabled",true);

			//CAFC
			if(data.venta.cafc != "" && data.venta.cafc != null){
				$("#codigo_contingencia").val(data.venta.cafc).prop("readonly",true);
			}

			//DETALLES ARTÍCULOS
			listarDetalle(idventa,"show");


			//Ocultar y mostrar los botones

			$("#btnGuardar").hide();

			$("#btnCancelar").show();

			//mostrar cambio y monto
			var monto = data.venta.monto;
			monto = Number(monto).toFixed(2);
			var cambio = data.venta.cambio;
			cambio = Number(cambio).toFixed(2);

			$("#monto").val(monto).prop("readonly",true);
			$("#cambio").val(cambio);

		});
	}else{
		$.post("../ajax/venta.php?op=mostrar",{idventa : idventa,editar:editar}, function(data, status){
			mostrarform(true,idventa);

			$("#formularioregistros").hide();

			$("#formularioeditar").show();
			
			data = JSON.parse(data);
			console.log(data);

			$("#idventa_editar").val(data.venta.idventa);

			//verificar si se tiene información del evento de la venta
			
			$("#id_evento_significativo_editar").val(data.evento.id_evento_significativo);
			$("#cufd_editar").val(data.evento.codigo_cufd);
			$("#cod_control_editar").val(data.evento.codigo_control_cufd);
			$("#evento_editar").val(data.evento.codigo_evento);
		

			if(data.evento.codigo_evento == "5" || data.evento.codigo_evento == "6" || data.evento.codigo_evento == "7"){
				$("#div_num_venta_contingencia_editar").show();
				$("#div_num_venta_editar").hide();
				$("#num_venta_contingencia_editar").val(data.venta.nro_venta_contingencia).prop("required");
				$("#num_venta_editar").removeAttr("required");
			}else{
				$("#div_num_venta_contingencia_editar").hide();
				$("#div_num_venta_editar").show();
				$("#num_venta_editar").val(data.venta.nro_venta).prop("readonly",true).prop("required");
				$("#num_venta_contingencia_editar").removeAttr("required");
			}

			var array_fechas = data.venta.fecha_hora.split(" ");
			var array_fecha_inicio = data.evento.fecha_inicio_evento.split("T");
			var array_fecha_fin = data.evento.fecha_fin_evento.split("T");

			$("#fecha_editar").val(array_fechas[0]).prop("min",array_fecha_inicio[0]).prop("max",array_fecha_fin[0]);
			$("#hora_editar").val(array_fechas[1]).prop("min",array_fecha_inicio[1]).prop("max",array_fecha_fin[1]);
			

			
			$.post("../ajax/venta.php?op=selectCliente", function(r){
				r = JSON.parse(r);
				var cad = "<option value=''>--Seleccione--</option><option value='nuevo'>NUEVO CLIENTE</option>";
				for(var i = 0; i < r.datos.length; i++){
					if(r.datos[i][0] == data.venta.idcliente)
						cad += "<option value='"+r.datos[i][0]+"' selected>"+r.datos[i][1]+" - "+r.datos[i][2]+" - "+r.datos[i][3]+"</option>";
					else
						cad += "<option value='"+r.datos[i][0]+"'>"+r.datos[i][1]+" - "+r.datos[i][2]+" - "+r.datos[i][3]+"</option>";
				}
				$("#idcliente_editar").html(cad);
				$('#idcliente_editar').selectpicker('refresh');

				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=obtenerDocumentoIdentidad",
					dataType: "text",
					success: function (r) {
						$("#documento_identidad_editar").html(r);
						$('#documento_identidad_editar').selectpicker('refresh');
					}
				});
			
				//LLAMAR A LOS DATOS DE LA PERSONA EN BASE AL ID
				mostrar_cliente(data.venta.idcliente);

				//DATOS DE MÉTODO DE PAGO DISABLED
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=obtenerMetodosPago",
					dataType: "text",
					success: function (r) {
						
						r = JSON.parse(r);
						//console.log(r);

						var cad = "<option value=''>--Seleccione--</option>";
						for(var i = 0; i < r.datos.length; i++){
							if(data.venta.metodo_pago == r.datos[i]["cod"]){
								cad += "<option value='"+r.datos[i]["cod"]+"' selected>"+r.datos[i]["desc"]+"</option>";
								break;
							}
						}
					
						$("#metodo_pago_editar").html(cad);
						$('#metodo_pago_editar').selectpicker('refresh').prop("disabled",true);
					}
				});
			});

			//NRO TARJETA OFUSCADO DISABLED
			$("#nro_tarjeta_editar").val(data.venta.nro_tarjeta_ofus).prop("disabled",true);

			//OTRA FORMA DE PAGO DISABLED
			if(data.venta.otro_metodo_pago != "" && data.venta.otro_metodo_pago != null){
				$("#div_otro_metodo_pago_editar").show();
				$("#otro_metodo_pago_editar").val(data.venta.otro_metodo_pago).prop("disabled",true);
			}

			//CÓDIGO EXCEPCION
			if(data.venta.codigo_excepcion == "0"){
				$("#codigo_excepcion_editar").val("0").selectpicker("refresh");
			}else
				$("#codigo_excepcion_editar").val("1").selectpicker("refresh");

			//CÓDIGO CAFC
			if(data.evento.codigo_evento == "5" || data.evento.codigo_evento == "6" || data.evento.codigo_evento == "7"){
				$("#codigo_contingencia_editar").val(data.venta.cafc).prop("required",true).removeAttr("readonly");
			}

			//DETALLES ARTÍCULOS
			listarDetalle(idventa,"editar");



			//mostrar cambio y monto
			var monto = data.venta.monto;
			monto = Number(monto).toFixed(2);
			var cambio = data.venta.cambio;
			cambio = Number(cambio).toFixed(2);

			$("#monto_editar").val(monto).prop("readonly",true);
			$("#cambio_editar").val(cambio);

			//Mostrar los botones

			$("#btnGuardarEditar").show();

			$("#btnCancelarEditar").show();
			
		});

	}

}

function listarDetalle(idventa,forma){

	$("#detalles").hide();

	$("#detalles_"+forma).show();

	tablaDetalle = $('#detalles_'+forma).DataTable({

		"aProcessing": true,							//Activamos el procesamiento del datatables

		"aServerSide": true,							//Paginación y filtrado realizados por el servidor

		"pagingType": "simple",

		"ajax":

			{

				url: '../ajax/venta.php?op=listarDetalle&id='+idventa,

				type : "get",

				dataType : "json",						

				error: function(e){

					console.log(e.responseText);	

				}

			},

		"bDestroy": true,	

	});

}

//Función para anular registros
function anular(nombre_archivo){

	var np = nombre_archivo;
	bootbox.dialog({
		message: '<style>.datepicker{z-index: 99999 !important}</style><form class="form-horizontal" role="form" id="anular_factura"><input type="hidden" name="nombre_archivo" value="'+np+'"><div class="form-group"><label class="col-sm-3 control-label no-padding-right" for="motivo_anulacion"> Especifique Motivo </label><div class="col-sm-9"><select class="form-control" name="motivo_anulacion" id="motivo_anulacion" onchange="select_motivo_anulacion()"></select></div></div></form>',
		title: "Anulación Factura",
	
		buttons:{
			"success" :{
				"label" : "<i class='fa fa-times'></i> Anular Factura",
				"className" : "btn-sm btn-danger aceptar_anulacion",
				"callback": function() {
					var formData = new FormData($("#anular_factura")[0]);
					$.ajax({
						url: "../ajax/venta.php?op=anular",
						type: "POST",
						data: formData,
						contentType: false,
						processData: false,
						success: function(datos){                    
							bootbox.alert(datos);								
							mostrarform(false,0);
							tabla_listado.ajax.reload();
						}
					});
				}		
			},
			"cancel" :{
				"label" : "<i class='icon-ok'></i> Cancelar",
				"className" : "btn-sm btn-secondary",
				"callback": function() {
				}
			}
		}
	});
	cargar_motivos_anulacion();
	$(".aceptar_anulacion").prop("disabled","true");
}

function cargar_motivos_anulacion(){
	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=obtenerMotivosAnulacion",
		dataType: "text",
		success: function (response) {
			$("#motivo_anulacion").html(response);
		},
		error:function(response){
			console.log(response);
		}
	});
}

//Declaración de variables necesarias para trabajar con las compras y

//sus detalles

var impuesto = 13;

var cont = 0;

var detalles=0;

//$("#guardar").hide();

$("#btnGuardar").hide();

function agregarDetalle(idarticulo,articulo,precio_venta,stock,idlaboratorio,cod_lote,idlote,i,cod_med,codigoActividad,codigoProductoSin,unidadMedida,nombreUnidadMedida){
	
	var descuento=0;

	var subtotal = parseFloat(1 * precio_venta).toFixed(2);

	var fila = '<tr class="filas" id="fila'+cont+'">'+

	'<td><button type="button" name="boton" id="boton'+cont+'" class="btn btn-danger" onclick="eliminarDetalle('+cont+','+i+')">X</button></td>'+

	'<td><input type="hidden" name="idarticulo[]" id="idarticulo'+cont+'" value="'+idarticulo+'" required><strong>'+articulo+'</strong><br><small>Código: '+cod_med+'</small><br><small>Unidad Medida: ' + nombreUnidadMedida + '</small></td>'+

	'<td><input onkeyup="modificarSubototales()" type="number" class="form-control" min=1 max="'+ stock +'" pattern="^[0-9]+" name="cantidad[]" id="cantidad'+cont+'" value="" required style="width:100%;"></td>'+

	'<td><input onkeyup="modificarSubototales()" type="number" class="form-control" min=0 step="0.01" name="precio_venta[]" id="precio_venta'+cont+'" value="'+precio_venta+'"required style="width:100%; -webkit-appearance: none; margin: 0; -moz-appearance: textfield;" readonly></td>'+

	'<td><input onkeyup="modificarSubototales()" type="number" class="form-control" min=0 step="0.01" name="descuento[]" id="descuento'+cont+'" value="'+descuento+'" required style="width:100%; -webkit-appearance: none; margin: 0; -moz-appearance: textfield;"></td>'+

	'<td><span name="subtotal" id="subtotal'+cont+'">'+subtotal+'</span></td>'+

	'<td><button type="button" onclick="modificarSubototales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+

	'<input type="hidden" name="idlaboratorio[]" id="idlaboratorio'+cont+'" value="'+idlaboratorio+'">'+

	'<input type="hidden" name="idlote[]" id="idlote'+cont+'" required value="'+idlote+'">'+

	'<input type="hidden" name="cod_med[]" id="cod_med'+cont+'" required value="'+cod_med+'">'+ 

	'<input type="hidden" name="cod_actividad[]" id="cod_actividad'+cont+'" required value="'+codigoActividad+'">'+

	'<input type="hidden" name="cod_producto_sin[]" id="cod_producto_sin'+cont+'" required value="'+codigoProductoSin+'">'+

	'<input type="hidden" name="unidad_medida[]" id="unidad_medida'+cont+'" required value="'+unidadMedida+'">'+

	'<input type="hidden" name="descripcion[]" id="descripcion'+cont+'" required value=\''+articulo+'\'>'+

	'<input type="hidden" name="nombreUnidadMedida[]" id="nombreUnidadMedida'+cont+'" required value=\''+nombreUnidadMedida+'\'>'+

	'<input type="hidden" name="lote_corr" id="lote_corr'+cont+'" value="'+i+'">'+

	'</tr>';

	cont = cont + 1;

	detalles = detalles + 1;

	$('#detalles').append(fila);

	$("#btnAdd"+i).prop("disabled",true);

	modificarSubototales();	 

}

function modificarSubototales() {

	var cant = document.getElementsByName("cantidad[]");

	var prec = document.getElementsByName("precio_venta[]");

	var desc = document.getElementsByName("descuento[]");

	var sub = document.getElementsByName("subtotal");



	for (var i = 0; i <cant.length; i++) {

		var inpC=cant[i];

		var inpP=prec[i];

		var inpD=desc[i];

		var inpS=sub[i];



		inpS.value=(inpC.value * inpP.value)-inpD.value;

		document.getElementsByName("subtotal")[i].innerHTML = parseFloat(inpS.value).toFixed(2);

	}

	calcularTotales();

}

function calcularTotales(){

	var sub = document.getElementsByName("subtotal");

	var total = 0.0;

	for (var i = 0; i <sub.length; i++) 
		total += document.getElementsByName("subtotal")[i].value;

	total = (Math.round(total*100)/100).toFixed(2);

	$("#total").html("Bs. " + (total));

	$("#total_venta").val(total);

	var monto = Number($("#monto").val());
	if(isNaN(monto))
		monto = 0.0;
	else
		monto = Number(monto);

	var cambio =parseFloat(monto - total).toFixed(2);

	$("#cambio").val(cambio);

	evaluar();

}

function evaluar(){

	var total_venta = $("#total_venta").val();
	var cambio = $("#cambio").val();
	var monto = $("#monto").val();
	total_venta = Number(total_venta);
	cambio = Number(cambio);

	if (detalles > 0 && total_venta >= 0 && cambio >= 0 && !isNaN(monto))

  		$("#btnGuardar").removeAttr("disabled");

	else{
 		 $("#btnGuardar").prop("disabled",true); 
	}

}

function eliminarDetalle(indice,i){

	$("#fila" + indice).remove();
	$("#btnAdd"+i).prop("disabled",false);
	detalles--;
	cont--;

	var filas = document.getElementsByClassName("filas");
	var idarticulo = document.getElementsByName("idarticulo[]");
	var cantidad = document.getElementsByName("cantidad[]");
	var precio_venta = document.getElementsByName("precio_venta[]");
	var descuento = document.getElementsByName("descuento[]");
	var subtotal = document.getElementsByName("subtotal");
	var idlaboratorio = document.getElementsByName("idlaboratorio[]");
	var idlote = document.getElementsByName("idlote[]");
	var lote_corr = document.getElementsByName("lote_corr");
	var boton = document.getElementsByName("boton");

	//DESPUÉS CONTINUAR CON LOS DEMÁS ITEMS PARA SIN
	for(j = 0; j < idarticulo.length; j++){
		fila = filas[j];
		fila_idarticulo = idarticulo[j];
		fila_cantidad = cantidad[j];
		fila_precio_venta = precio_venta[j];
		fila_descuento = descuento[j];
		fila_subtotal = subtotal[j];
		fila_idlaboratorio = idlaboratorio[j];
		fila_idlote = idlote[j];
		fila_lote_corr = lote_corr[j];
		fila_boton = boton[j];

		fila_boton.removeAttribute("onclick");

		fila.setAttribute("id","fila"+j);
		fila_idarticulo.setAttribute("id","idarticulo"+j);
		fila_cantidad.setAttribute("id","cantidad"+j);
		fila_precio_venta.setAttribute("id","precio_venta"+j);
		fila_descuento.setAttribute("id","descuento"+j);
		fila_subtotal.setAttribute("id","subtotal"+j);
		fila_idlaboratorio.setAttribute("id","idlaboratorio"+j);
		fila_idlote.setAttribute("id","idlote"+j);
		fila_boton.setAttribute("onclick","eliminarDetalle("+j+","+lote_corr[j].value+")");
		fila_boton.setAttribute("id","boton"+j);
		fila_lote_corr.setAttribute("id","lote_corr"+j);
	}


	calcularTotales();

}

function mostrar_div_nuevo_cliente(){
	$("#nombre_cliente").prop("required",true);
	$("#nombre_cliente").val("");
	$("#nombre_cliente_editar").prop("required",true);
	$("#nombre_cliente_editar").val("");
	
	$("#documento_identidad").prop("required",true);
	$("#documento_identidad").val("");
	$('#documento_identidad').selectpicker('refresh');
	$("#documento_identidad_editar").prop("required",true);
	$("#documento_identidad_editar").val("");
	$('#documento_identidad_editar').selectpicker('refresh');
	
	$("#nro_cliente").prop("pattern","[0-9]{1,15}").prop("maxlength","20");
	$("#nro_cliente_editar").prop("pattern","[0-9]{1,15}").prop("maxlength","20");

	$("#nro_cliente").prop("required",true);
	$("#nro_cliente").val("");
	$("#nro_cliente_editar").prop("required",true);
	$("#nro_cliente_editar").val("");

	$("#complemento").val("").prop("readonly",true);
	$("#complemento_editar").val("").prop("readonly",true);

	$("#email").val("").removeAttr("readonly");
	$("#email_editar").val("").removeAttr("readonly");

	$("#telefono").val("").removeAttr("readonly");
	$("#telefono_editar").val("").removeAttr("readonly");
	
}

function mostrar_cliente(idcliente){
	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=mostrar_cliente&cliente="+idcliente,
		dataType: "text",
		success: function (r) {
			r = JSON.parse(r);

			var nombre = r.nombre;
			nombre = nombre.replace(/&quot;/g,'"');//"
			nombre = nombre.replace(/&amp;/g,'&');//&
			nombre = nombre.replace(/&#039;/g,'\'');//'

			$("#nombre_cliente").val(nombre);
			$("#nombre_cliente_editar").val(nombre);
			

			$("#documento_identidad").val(r.tipo_documento)
			$("#documento_identidad").selectpicker('refresh');
			$("#documento_identidad_editar").val(r.tipo_documento)
			$("#documento_identidad_editar").selectpicker('refresh');

			$("#nro_cliente").val(r.num_documento);
			$("#nro_cliente_editar").val(r.num_documento);

			$("#complemento").val(r.complemento);
			$("#complemento_editar").val(r.complemento);

			$("#email").val(r.email);
			$("#email_editar").val(r.email);

			$("#telefono").val(r.telefono);
			$("#telefono_editar").val(r.telefono);

			if(r.tipo_documento == 1 || r.tipo_documento == 5){
				$("#nro_cliente").prop("pattern","[0-9]{1,15}").prop("maxlength","15");
				$("#nro_cliente_editar").prop("pattern","[0-9]{1,15}").prop("maxlength","15");
			}else{
				$("#nro_cliente_editar").prop("pattern","[0-9A-Z\x2d]{1,20}").prop("maxlength","20");
				$("#nro_cliente_editar").prop("pattern","[0-9A-Z\x2d]{1,20}").prop("maxlength","20");
			}

			if(r.tipo_documento != 1){
				$("#complemento").prop("readonly",true);
				$("#complemento_editar").prop("readonly",true);
			}else{
				$("#complemento").removeAttr("readonly");
				$("#complemento_editar").removeAttr("readonly");
			}

			//VALIDAR ESTADO NIT DEL CLIENTE (SI ES QUE SU TIPO DE DOCUMENTO ES NIT)
			verificar_estado_nit(r.num_documento,r.tipo_documento);

		}
	});
}

function obtener_metodos_pago(){
	//se debe invocar al catálogo de tipos de documentos de identidad
	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=obtenerMetodosPago",
		dataType: "text",
		success: function (r) {
			
			r = JSON.parse(r);
			console.log(r);
			$("#metodo_pago").html(r);
			$('#metodo_pago').selectpicker('refresh');
		}
	});
}

function volver_menu() {  
	console.log("go");
	window.location.href = "escritorio.php";
}

function select_motivo_anulacion() {  
	var x = document.getElementById("motivo_anulacion").value;
	if(x != ""){
		$(".aceptar_anulacion").removeAttr("disabled");
	}else{
		$(".aceptar_anulacion").prop("disabled","true");
	}
}

function verificar_comunicacion(){
	var es = $("#es").val();
	if(es == 0 || es == ""){//si no se tienen eventos activos entonces se validará si hay conexión a impuestos

		$.get("../ajax/venta.php?op=verificar_comunicacion")

		.done(function(data) {
			if(data == false){
				//bootbox.alert("Error de conexión con impuestos");
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=cambiar_modo_offline&modo=2&reg_es=1",
					dataType: "json",
					success: function (r) {
						if(r.estado == "1"){
							//significa que el registro se hizo correctamente
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#id_evento_significativo").val(r.id_evento_creado);
							
							bootbox.alert("Modalidad del sistema fuera de línea<br><strong>Evento:</strong> " + r.mensaje +"<br><strong>Fecha y Hora de registro de evento:</strong> "+r.hora_registro);
							$("#cufd_evento").val(r.cufd);
							$("#cod_control_evento").val(r.codigo_control);
							$("#codigo_contingencia").val(r.cafc);
						}else{
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#codigo_contingencia").val(r.cafc);
							bootbox.alert(r.mensaje);
						}
			
						$("#evento_actual").val("2");
						$("#es").val("2");
			
						setInterval("location.reload()",2000);
					},
					error:function(r){
						console.log(r);
					}
				});
			}
				//alert("Error de conexión con impuestos");
			//alert("Conexión con impuestos exitosa");
			
		})

		.fail(function(data) {
			if(data == false)
				bootbox.alert("Error de conexión con impuestos");
				//alert("Error de conexión con impuestos");
		});
	}
}

function verificar_comunicacion2(){//HACIENDO CLICK EN EL BOTÓN DE VERIFICAR CONEXIÓN EN VENTAS
	var es = $("#es").val();
	if(es == 0 || es == ""){//si no se tienen eventos activos entonces se validará si hay conexión a impuestos

		$.get("../ajax/venta.php?op=verificar_comunicacion")

		.done(function(data) {
			if(data == false){
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=cambiar_modo_offline&modo=2&reg_es=0",
					dataType: "json",
					success: function (r) {
						if(r.estado == "1"){
							//significa que el registro se hizo correctamente
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#id_evento_significativo").val(r.id_evento_creado);
							
							bootbox.alert("Modalidad del sistema fuera de línea<br><strong>Evento:</strong> " + r.mensaje +"<br><strong>Fecha y Hora de registro de evento:</strong> "+r.hora_registro);
							$("#cufd_evento").val(r.cufd);
							$("#cod_control_evento").val(r.codigo_control);
							$("#codigo_contingencia").val(r.cafc);
						}else{
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#codigo_contingencia").val(r.cafc);
							bootbox.alert(r.mensaje);
						}
			
						$("#evento_actual").val("2");
						$("#es").val("2");
						$("#verificar_conexion").prop("disabled",true);
			
						setInterval("location.reload()",2000);
					},
					error:function(r){
						console.log(r);
					}
				});
			}else{//SIGNIFICA QUE SI EXISTE CONEXIÓN CON IMPUESTOS
				res = "<i class='fa fa-check-circle' aria-hidden='true'></i> Conexión a internet correcta<br>";
				res += "<i class='fa fa-check-circle' aria-hidden='true'></i> Conexión a impuestos correcta";
				bootbox.alert(res);
			}			
		})

		.fail(function(data) {
			if(data == false)
				bootbox.alert("Error de conexión con impuestos");
		});
	}
}

function verificar_conexion_internet(){
	var es = $("#es").val();
	if(es == 0 || es == ""){//si no se tienen eventos activos entonces se validará si existe conexión a internet o si hay conexión a impuestos
	
		$.get("../ajax/venta.php?op=verificar_conexion_internet")

		.done(function(data) {
			if(data == false){
				//bootbox.alert("Error de conexión de internet");
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=cambiar_modo_offline&modo=1&reg_es=1",
					dataType: "json",
					success: function (r) {
						if(r.estado == "1"){
							//significa que el registro se hizo correctamente
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#id_evento_significativo").val(r.id_evento_creado);
							
							bootbox.alert("Modalidad del sistema fuera de línea<br><strong>Evento:</strong> " + r.mensaje +"<br><strong>Fecha y Hora de registro de evento:</strong> "+r.hora_registro);
							
							$("#cufd_evento").val(r.cufd);

							$("#cod_control_evento").val(r.codigo_control);
							$("#codigo_contingencia").val(r.cafc);
						}else{
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#codigo_contingencia").val(r.cafc);
							bootbox.alert(r.mensaje);
						}
			
						$("#evento_actual").val("1");
						$("#es").val("1");
			
						setInterval("location.reload()",2000);
					},
					error:function(r){
						console.log(r);
					}
				});
			}else
				setTimeout(verificar_comunicacion,15000);
			
		})

		.fail(function(data) {
			if(data == false)
				bootbox.alert("Error de conexión de internet");
		});
	}
}

function verificar_conexion_internet2(){//HACIENDO CLICK EN EL BOTÓN DE VERIFICAR CONEXIÓN EN VENTAS
	var es = $("#es").val();
	if(es == 0 || es == ""){//si no se tienen eventos activos entonces se validará si existe conexión a internet o si hay conexión a impuestos
		$.get("../ajax/venta.php?op=verificar_conexion_internet")

		.done(function(data) {
			//SI NO HAY CONEXIÓN DE INTERNET SE CREA EL EVENTO CORRESPONDIENTE
			if(data == false){
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=cambiar_modo_offline&modo=1&reg_es=0",
					dataType: "json",
					success: function (r) {
						if(r.estado == "1"){
							//significa que el registro se hizo correctamente
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#id_evento_significativo").val(r.id_evento_creado);
							
							bootbox.alert("Modalidad del sistema fuera de línea<br><strong>Evento:</strong> " + r.mensaje +"<br><strong>Fecha y Hora de registro de evento:</strong> "+r.hora_registro);
							
							$("#cufd_evento").val(r.cufd);

							$("#cod_control_evento").val(r.codigo_control);
							$("#codigo_contingencia").val(r.cafc);
						}else{
							$("#modo_sistema").html(r.select);
							$('#modo_sistema').selectpicker('refresh');
							$("#codigo_contingencia").val(r.cafc);
							bootbox.alert(r.mensaje);
						}
			
						$("#evento_actual").val("1");
						$("#es").val("1");
						$("#verificar_conexion").prop("disabled",true);
			
						setInterval("location.reload()",2000);
					},
					error:function(r){
						console.log(r);
					}
				});
			}else //SI HAY CONEXIÓN A INTERNET, SE VERIFICA SI HAY CONEXIÓN A IMPUESTOS
				verificar_comunicacion2();
			
		})

		.fail(function(data) {
			if(data == false)
				bootbox.alert("Error de conexión de internet");
		});
	}
	//SI SE TIENE ALGÚN EVENTO ACTIVO EL BOTÓN TENDRÍA QUE ESTAR INHABILITADO
}

function validar_cuis(codigo_pv,cuis,cufd){
	if(cuis == 0){
		bootbox.dialog({
			message: '<style>.datepicker{z-index: 99999 !important}</style><form class="form-horizontal" role="form" id="modal_cuis"><input type="hidden" name="nuevo_cuis" value="si"></form>',
			title: "CUIS Inactivo",
			buttons:{
				"success" :{
					"label" : "<i class='fa fa-check'></i> Obtener nuevo CUIS",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var formData = new FormData($("#modal_cuis")[0]);
						$.ajax({
							url: "../ajax/punto_venta.php?op=generar_cuis&cod_pv="+codigo_pv,
							type: "POST",
							data: formData,
							contentType: false,
							processData: false,
							success: function(datos){
								if(datos == "1"){
									bootbox.alert("Registro de CUIS realizado correctamente");
									$("#cuis_activo").val("1");
								}else{
									bootbox.alert("ERROR EN REGISTRAR CUIS EN BB.DD.");   
									$("#cuis_activo").val("0");   
								}
								setInterval("location.reload()",2000);
							}
						});
					}		
				},
				"cancel" :{
					"label" : "<i class='icon-ok'></i> Volver Inicio",
					"className" : "btn-sm btn-secondary",
					"callback": function() {
						volver_menu();
					}
				}
			}
		});
		
	}else
		validar_cufd(codigo_pv,cufd);
}

function validar_cufd(codigo_pv,cufd){
	var es = $("#es").val();
	if(cufd == 0){
		//MIENTRAS HAYA UN EVENTO SIGNIFICATIVO NO PEDIR UN NUEVO CUFD EN CASO QUE ESTE SE HAYA VENCIDO (VERIFICAR)
		if(es == "0" || es == ""){
			bootbox.dialog({
				message: '<style>.datepicker{z-index: 99999 !important}</style><form class="form-horizontal" role="form" id="modal_cufd"><input type="hidden" name="nuevo_cufd" value="si"></form>',
				title: "CUFD Inactivo",
				buttons:{
					"success" :{
						"label" : "<i class='fa fa-check'></i> Obtener nuevo CUFD",
						"className" : "btn-sm btn-success",
						"callback": function() {
							var formData = new FormData($("#modal_cufd")[0]);
							$.ajax({
								url: "../ajax/punto_venta.php?op=generar_cufd2&cod_pv="+codigo_pv+"&estado_pv=1",
								type: "POST",
								data: formData,
								contentType: false,
								processData: false,
								success: function(datos){
									if(datos == "1"){
										bootbox.alert("Registro de CUFD realizado correctamente");
										$("#cufd_activo").val("1");
									}else{
										bootbox.alert("ERROR EN REGISTRAR CUFD EN BB.DD.");   
										$("#cufd_activo").val("0");   
									}
									setInterval("location.reload()",2000);
								}
							});
						}		
					},
					"cancel" :{
						"label" : "<i class='icon-ok'></i> Volver Inicio",
						"className" : "btn-sm btn-secondary",
						"callback": function() {
							volver_menu();
						}
					}
				}
			});
		}
	}else
		estado_cajas();
}

function estado_cajas(){
	//Si el código de PV es distinto a nulo, significa que el punto de venta se encuentra en uso por un usuario

	detalles = $.ajax({
		url: "../ajax/venta.php?op=verificar_usuario_apertura_caja", 
		dataType: 'text',
		async: false     
	}).responseText;

	if(detalles == "Caja Cerrada"){
		$("#btnagregar").prop("disabled",true);
		$("#abrir_caja").prop("disabled",true);
		$("#cerrar_caja").prop("disabled",true);
	}else{
		if(detalles == "Sin Abrir"){
			bootbox.confirm("La caja aún no se encuentra abierta<br><strong>¿Desea abrir la caja ahora?</strong>", function(result){ 
				if(result){
					abrir_caja();
				}else{
					$("#btnagregar").prop("disabled",true);
					$("#abrir_caja").removeAttr("disabled");
					$("#cerrar_caja").prop("disabled",true);
				}
			});
		}else{
			$("#btnagregar").removeAttr("disabled");
			$("#abrir_caja").prop("disabled",true);
			$("#cerrar_caja").removeAttr("disabled");
		}	
	}
}

//ENVIAR POR CORREO EL DOCUMENTO EN PDF Y EL XML DE LA FACTURA SI ES QUE ESTA SE ENCUENTRA ACEPTADA
function enviar_correo(correo,idventa){
	$.ajax({
		type: "get",
		url: "../ajax/venta.php?op=enviar_correo&id_venta="+idventa+"&correo="+correo,
		dataType: "text",
		success: function (response) {
			bootbox.alert(response);
		},
		error:function(response){
			console.log(response);
		}
	});
}

//VERIFICAR ESTADO FACTURA
function verificar_estado(cuf){
	$.ajax({
		type: "get",
		url: "../ajax/venta.php?op=verificar_estado_factura&cuf="+cuf,
		dataType: "text",
		success: function (response) {
			bootbox.alert(response);
		},
		error:function(response){
			console.log(response);
		}
	});
}

//VERIFICAR SI NIT ES ACTIVO, INACTIVO O INEXISTENTE
function verificar_estado_nit(nit,tipo_doc){
	$(".alert").remove();
	if(tipo_doc == "5" && nit != ""){
		$.ajax({
			type: "get",
			url: "../ajax/venta.php?op=verificar_nit&dato_nit="+nit,
			dataType: "text",
			success: function (response) {
				response = JSON.parse(response);
				if(response.estado == "0"){
					$("#nro_cliente").after('<div class="alert alert-warning text-white">' + response.mensaje + '</div>');
					$("#nro_cliente_editar").after('<div class="alert alert-warning text-white">' + response.mensaje + '</div>');

					$("#codigo_excepcion").val("1");
					$("#codigo_excepcion").selectpicker("refresh");

					$("#codigo_excepcion_editar").val("1");
					$("#codigo_excepcion_editar").selectpicker("refresh");
				}else{
					$("#codigo_excepcion").val("0");
					$("#codigo_excepcion").selectpicker("refresh");

					$("#codigo_excepcion_editar").val("0");
					$("#codigo_excepcion_editar").selectpicker("refresh");
				}
			},
			error:function(response){
				console.log(response);
			}
		});
	}
}

//GENERAR CUFD AL ABRIR CAJA
function generar_cufd_caja(codigo_pv){
	$.ajax({
		url: "../ajax/punto_venta.php?op=generar_cufd2&cod_pv="+codigo_pv+"&estado_pv=1",
		type: "POST",
		contentType: false,
		processData: false,
		success: function(datos){
			if(datos == "1"){
				/*
				bootbox.alert("Registro de CUFD realizado correctamente");
				*/
				$("#cufd_activo").val("1");
			}else{
				bootbox.alert("ERROR EN REGISTRAR CUFD EN BB.DD. VUELVA A RECARGAR LA PÁGINA");   
				$("#cufd_activo").val("0");   
			}
		}
	});
}

init();



$("#nro_cliente").change(function(){
	var dato = $(this).val();
	var documento_identidad = $("#documento_identidad").val();
	verificar_estado_nit(dato,documento_identidad);
});

$("#nro_cliente_editar").change(function(){
	var dato = $(this).val();
	var documento_identidad = $("#documento_identidad_editar").val();
	verificar_estado_nit(dato,documento_identidad);
});



$("#idcliente").change(function () { 
	$(".alert").remove();
	$("#codigo_excepcion").val("0").selectpicker('refresh');
	var opcion = $(this).val();
	if(opcion == "nuevo" || opcion == ""){
		$("#nombre_cliente").removeAttr("readonly");
	    $("#nro_cliente").removeAttr("readonly");
	    $("#complemento").removeAttr("readonly");
	    $("#email").removeAttr("readonly");
	    $("#telefono").removeAttr("readonly");
	    //limpiar datos de cliente
		mostrar_div_nuevo_cliente();
	}else{
		//mostrar datos de cliente, y verificar que el nit sea válido o no, si no fuera válido se debe marcar código de excepción
		mostrar_cliente(opcion);
		if(opcion == "2" || opcion == "110" || opcion == "111"){
		    $("#nombre_cliente").prop("readonly",true);
		    $("#nro_cliente").prop("readonly",true);
		    $("#complemento").prop("readonly",true);
		    $("#email").prop("readonly",true);
		    $("#telefono").prop("readonly",true);
	    }else{
	        $("#nombre_cliente").removeAttr("readonly");
		    $("#nro_cliente").removeAttr("readonly");
		    $("#complemento").removeAttr("readonly");
		    $("#email").removeAttr("readonly");
		    $("#telefono").removeAttr("readonly");
	    }
    }

});


$("#idcliente_editar").change(function () {
	$(".alert").remove();
	$("#codigo_excepcion_editar").val("0").selectpicker('refresh');
	var opcion = $(this).val();
	if(opcion == "nuevo" || opcion == ""){
	    $("#nombre_cliente_editar").removeAttr("readonly");
	    $("#nro_cliente_editar").removeAttr("readonly");
	    $("#complemento_editar").removeAttr("readonly");
	    $("#email_editar").removeAttr("readonly");
	    $("#telefono_editar").removeAttr("readonly");
	    //limpiar datos de cliente
		mostrar_div_nuevo_cliente();
	}else{
		mostrar_cliente(opcion);
		if(opcion == "2" || opcion == "110" || opcion == "111"){
		    $("#nombre_cliente_editar").prop("readonly",true);
		    $("#nro_cliente_editar").prop("readonly",true);
		    $("#complemento_editar").prop("readonly",true);
		    $("#email_editar").prop("readonly",true);
		    $("#telefono_editar").prop("readonly",true);
	    }else{
	        $("#nombre_cliente_editar").removeAttr("readonly");
		    $("#nro_cliente_editar").removeAttr("readonly");
		    $("#complemento_editar").removeAttr("readonly");
		    $("#email_editar").removeAttr("readonly");
		    $("#telefono_editar").removeAttr("readonly");
	    }
	}
});

$("#metodo_pago").change(function () {  
	var opcion = $(this).val();
	if(opcion == "" || opcion == 1 || opcion == 7){
		$("#nro_tarjeta").prop("readonly",true);
		$("#div_otro_metodo_pago").hide();
		$("#otro_metodo_pago").val("").removeAttr("required");
	}else{
		if(opcion == 10 || opcion == 2){
			$("#nro_tarjeta").removeAttr("readonly");
			$("#div_otro_metodo_pago").hide();
			$("#otro_metodo_pago").val("").removeAttr("required");
		}else{//opcion = 5
			$("#nro_tarjeta").prop("readonly",true);
			$("#div_otro_metodo_pago").show();
			$("#otro_metodo_pago").prop("required",true).val("");
		}
	}
	$("#nro_tarjeta").val("");
});

$("#monto").keyup(function(){
	var valor = $(this).val();
	var total_venta = $("#total_venta").val();
	var cambio = Number(valor) - Number(total_venta);
	cambio = cambio.toFixed(2);
	$("#cambio").val(cambio);
	evaluar();
});

$("#modo_sistema").change(function () { //si se selecciona algún evento de la lista
	var modo = $(this).val();
	//se debe invocar el modo que se tiene
	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=cambiar_modo_offline&modo="+modo+"&reg_es=1",
		dataType: "json",
		success: function (r) {
			//bootbox.alert(JSON.parse(r));
			
			if(r.estado == "1"){
				//significa que el registro se hizo correctamente
				$("#modo_sistema").html(r.select);
				$('#modo_sistema').selectpicker('refresh');
				$("#id_evento_significativo").val(r.id_evento_creado);
				console.log(modo);
				if(modo != 0)
					bootbox.alert("Modalidad del sistema fuera de línea<br><strong>Evento:</strong> " + r.mensaje +"<br><strong>Fecha y Hora de registro de evento:</strong> "+r.hora_registro);
				else
					bootbox.alert(r.mensaje);

				$("#cufd_evento").val(r.cufd);
				$("#cod_control_evento").val(r.codigo_control);
			}else{
				$("#modo_sistema").html(r.select);
				$('#modo_sistema').selectpicker('refresh');
				bootbox.alert(r.mensaje);
			}

			$("#codigo_contingencia").val(r.cafc);

			$("#evento_actual").val(modo);
			$("#es").val(modo);

			setInterval("location.reload()",2000);
			
		},
		error:function(r){
			console.log(r);
		}
	});
});



$("#documento_identidad").change(function(){
	var dato = $(this).val();
	var nro_cliente = $("#nro_cliente").val();//si es que existe
	console.log(nro_cliente);
	if(dato == 1)
		$("#complemento").removeAttr("readonly").val("");
	else
		$("#complemento").prop("readonly",true).val("");

	if(dato != 1 && dato != 5)
		$("#nro_cliente").prop("pattern","[0-9A-Z\x2d]{1,20}").prop("maxlength","20");
	else
		$("#nro_cliente").prop("pattern","[0-9]{1,15}").prop("maxlength","15");

	$("#nro_cliente").removeAttr("readonly");
	$(".alert").remove();

	$("#codigo_excepcion").val("0").selectpicker('refresh');
	
	if(nro_cliente != "" && dato == 5)
	    verificar_estado_nit(nro_cliente,dato);
	
	
});

$("#documento_identidad_editar").change(function(){
	var dato = $(this).val();
	var nro_cliente = $("#nro_cliente_editar").val();//si es que existe

	if(dato == 1)
		$("#complemento_editar").removeAttr("readonly").val("");
	else
		$("#complemento_editar").prop("readonly",true).val("");

	if(dato != 1 && dato != 5)
		$("#nro_cliente_editar").prop("pattern","[0-9A-Z\x2d]{1,20}").prop("maxlength","20");
	else
		$("#nro_cliente_editar").prop("pattern","[0-9]{1,15}").prop("maxlength","15");

	$("#nro_cliente_editar").removeAttr("readonly");
	$(".alert").remove();

	$("#codigo_excepcion_editar").val("0").selectpicker('refresh');
	
	if(nro_cliente != "" && dato == 5)
	    verificar_estado_nit(nro_cliente,dato);
	

});


$("#nro_tarjeta").keyup(function(){
	var dato = $(this).val();
	var aux_dato = "";
	for(i = 0; i < dato.length; i++){
		if(i >= 4 && i <= 11){
			if(dato[i]!="0")
				aux_dato += "0";
			else
				aux_dato += dato[i];
		}else
			aux_dato += dato[i];
		
	}
	$("#nro_tarjeta").val(aux_dato);
});

$("#nro_tarjeta_editar").keyup(function(){
	var dato = $(this).val();
	var aux_dato = "";
	for(i = 0; i < dato.length; i++){
		if(i >= 4 && i <= 11){
			if(dato[i]!="0")
				aux_dato += "0";
			else
				aux_dato += dato[i];
		}else
			aux_dato += dato[i];
		
	}
	$("#nro_tarjeta_editar").val(aux_dato);
});

setInterval(verificar_conexion_internet,60000);

$("#formulario").submit(function (e) {
	guardaryeditar(e,"crear");	
});

$("#formulario_editar").submit(function (e) { 
	guardaryeditar(e,"editar");	
});

$(document).ready(function () {
	//CONTAR CAJAS DISPONIBLES
	//PAGOS EN EFECTIVO
	var cajas_disponibles = $.ajax({
		url: "../ajax/punto_venta.php?op=contar_pv_disponibles", 
		dataType: 'number',
		async: false     
	}).responseText;


	if(cajas_disponibles == null || cajas_disponibles == "")
		cajas_disponibles  = 0;
	else
		cajas_disponibles = Number(cajas_disponibles);

	if(cajas_disponibles > -1){
		var codigo_punto_venta = $("#codigo_punto_venta").val();
		//var estado_punto_venta = $("#estado_punto_venta").val();
		if(codigo_punto_venta != ""){//VERIFICAR SI EL VENDEDOR TIENE PUNTO DE VENTA ASIGNADO AL MOMENTO DE VENDER
			var es = $("#es").val();//OBTENER EL EVENTO SIGNIFICATIVO ACTUAL SI EXISTE
			if(es != 0 && es != ""){//SI HUBIESE EVENTOS SIGNIFICATIVOS ABIERTOS
				$.ajax({
					type: "GET",
					url: "../ajax/venta.php?op=cambiar_modo_offline&modo="+es+"&reg_es=0",
					dataType: "json",
					success: function (r) {
						$("#modo_sistema").html(r.select);
						$('#modo_sistema').selectpicker('refresh');
						$("#id_evento_significativo").val(r.id_evento_creado);			
						$("#cufd_evento").val(r.cufd);		
						$("#cod_control_evento").val(r.codigo_control);	
						$("#codigo_contingencia").val(r.cafc);			
					},
					error:function (e) {
						console.log(e);
						}
				});
				$("#verificar_conexion").prop("disabled",true);
			}else{//SI NO HUBIESE EVENTOS SIGNIFICATIVOS ABIERTOS
				/*
				var id = $("#id_token").val();
				if(id == 0){
					bootbox.alert("Token Inválido o vencido");
					setTimeout(volver_menu,3000);
				}
				*/
			
				var hay_sincronizacion = $("#hay_sincronizacion").val();
				if(hay_sincronizacion == 0){
					bootbox.alert("Debe realizar la sincronización de catálogos para poder realizar ventas");
					setTimeout(volver_menu,3000);
				}

				$("#verificar_conexion").removeAttr("disabled");

			}

			$("#evento_actual").val(es);

			var cuis_activo = $("#cuis_activo").val();

			var cufd_activo = $("#cufd_activo").val();
			validar_cuis(codigo_punto_venta,cuis_activo,cufd_activo);
			
		}else{
			estado_cajas();
			/*
			bootbox.alert("No tiene punto de venta asignado, intente más tarde");
			setTimeout(volver_menu,3000);
			*/
		}
	}else{
		bootbox.alert("No se tiene cajas disponibles en este momento, intente más tarde");
		setTimeout(volver_menu,3000);
	}
});