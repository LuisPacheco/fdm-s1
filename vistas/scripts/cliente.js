$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();

	

	if(title != "Opciones" && title != "Imagen"){

		$(this).html( '<input type="text" style="width:100%"> ' );

	}else{

		$(this).html( '' );

	}

	

	$( 'input', this ).on( 'keyup change', function () {

		if ( tabla_listado.column(i).search() !== this.value ) {

			tabla_listado

				.column(i)

				.search( this.value )

				.draw();

		}

	} );

} );



var tabla_listado = $('#tbllistado').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});



//Función que se ejecuta al inicio

function init(){

	mostrarform(false);

	listar();



	$("#formulario").on("submit",function(e)

	{

		guardaryeditar(e);	

	});

	//se debe invocar al catálogo de tipos de documentos de identidad
	$.ajax({
		type: "GET",
		url: "../ajax/venta.php?op=obtenerDocumentoIdentidad",
		dataType: "text",
		success: function (r) {
			$("#tipo_documento").html(r);
			$('#tipo_documento').selectpicker('refresh');
		}
	});

	$("#tipo_documento").prop("required",true);
	$("#tipo_documento").val("");
	$('#tipo_documento').selectpicker('refresh');

	$('#mVentas').addClass("treeview active");

    $('#lClientes').addClass("active");

}



//Función limpiar

function limpiar()

{

	$("#nombre").val("");

	$("#num_documento").val("");

	$("#direccion").val("");

	$("#telefono").val("");

	$("#email").val("");

	$("#idpersona").val("");

	$("#tipo_documento").val("");

	$('#tipo_documento').selectpicker('refresh');

	$("#complemento").val("");

}



//Función mostrar formulario

function mostrarform(flag)

{

	limpiar();

	if (flag)

	{

		$("#listadoregistros").hide();

		$("#formularioregistros").show();

		$("#btnGuardar").prop("disabled",false);

		$("#btnagregar").hide();

	}

	else

	{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

	}

}



//Función cancelarform

function cancelarform()

{

	limpiar();

	mostrarform(false);

}



//Función Listar

function listar()

{

	tabla_listado=$('#tbllistado').dataTable(

	{

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/persona.php?op=listarc',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función para guardar o editar



function guardaryeditar(e)

{

	e.preventDefault(); //No se activará la acción predeterminada del evento

	$("#btnGuardar").prop("disabled",true);

	var formData = new FormData($("#formulario")[0]);

	$("body").css({"padding-right":"0px"});

	$.ajax({

		url: "../ajax/persona.php?op=guardaryeditar",

	    type: "POST",

	    data: formData,

	    contentType: false,

	    processData: false,



	    success: function(datos)

	    {                    

	          bootbox.alert(datos);	          

	          mostrarform(false);

	          tabla_listado.ajax.reload();

	    }



	});

	

	limpiar();

}



function mostrar(idpersona)

{

	$.post("../ajax/persona.php?op=mostrar",{idpersona : idpersona}, function(data, status)

	{

		data = JSON.parse(data);		

		mostrarform(true);



		var nombre = data.nombre;
		nombre = nombre.replace(/&quot;/g,'"');//"
		nombre = nombre.replace(/&amp;/g,'&');//&
		nombre = nombre.replace(/&#039;/g,'\'');//'



		$("#nombre").val(nombre);


		
		$("#tipo_documento").val(data.tipo_documento);
		$('#tipo_documento').selectpicker('refresh');

		$("#num_documento").val(data.num_documento);

		$("#direccion").val(data.direccion);

		$("#telefono").val(data.telefono);

		$("#email").val(data.email);

 		$("#idpersona").val(data.idpersona);

		$("#complemento").val(data.complemento);

 	})

}



//Función para eliminar registros

function eliminar(idpersona)

{

	bootbox.confirm("¿Está Seguro de eliminar el cliente?", function(result){

		if(result)

        {

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/persona.php?op=eliminar", {idpersona : idpersona}, function(e){

        		bootbox.alert(e);

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	})

}



init();