
var tabla_listado = $('#tbllistado').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});



//Función que se ejecuta al inicio

function init(){

	mostrarform(false);

	listar();



	$("#formulario").on("submit",function(e){
		guardar(e);	
	});

    $('#mConfiguracion').addClass("treeview active");

    $('#lToken').addClass("active");

}



//Función limpiar

function limpiar()

{

	$("#id_token").val("");

	$("#codigo_token").val("");

	$("#fecha_registro_token").val("");

	$("#fecha_caducidad_token").val("");

	$("#btnGuardar").prop("disabled",true);

	
}



//Función mostrar formulario

function mostrarform(flag)

{

	limpiar();

	if (flag)

	{

		$("#listadoregistros").hide();

		$("#formularioregistros").show();

		//$("#btnGuardar").prop("disabled",false);

		$("#btnagregar").hide();

	}

	else

	{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

	}

}



//Función cancelarform

function cancelarform()

{

	limpiar();

	mostrarform(false);

}



//Función Listar

function listar()

{

	tabla_listado=$('#tbllistado').dataTable(

	{

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/gestion_token.php?op=listar',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función para guardar o editar



function guardar(e){

	e.preventDefault(); //No se activará la acción predeterminada del evento

	bootbox.confirm("¿Está Seguro/a de guardar el token?<br><strong>Nota: Una vez guardado, no podrá realizarce edición posterior, además se anularán los tokens anteriores</strong>", function(result){

		if(result){
			$("#btnGuardar").prop("disabled",true);

			var formData = new FormData($("#formulario")[0]);

			$("body").css({"padding-right":"0px"});

			$.ajax({

				url: "../ajax/gestion_token.php?op=guardar",

				type: "POST",

				data: formData,

				contentType: false,

				processData: false,
				success: function(datos)
				{                    

					bootbox.alert(datos);	          

					mostrarform(false);

					tabla_listado.ajax.reload();

				}



			});

			

			limpiar();
		}
	});
	
}



function mostrar(idcategoria)

{

	$.post("../ajax/categoria.php?op=mostrar",{idcategoria : idcategoria}, function(data, status)

	{

		

		data = JSON.parse(data);		

		mostrarform(true);



		$("#nombre").val(data.nombre);

		$("#descripcion").val(data.descripcion);

 		$("#idcategoria").val(data.idcategoria);



 	})

}



//Función para desactivar registros

function desactivar(id_token){
	bootbox.confirm("¿Está Seguro/a de desactivar el Token? <strong>Nota: Esta acción no puede ser revertida</strong>", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
        	$.post("../ajax/gestion_token.php?op=desactivar", {id_token : id_token}, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});	
        }else
			$("body").css({"padding-right":"0px"});
	});
}



//Función para activar registros

function activar(idcategoria)

{

	bootbox.confirm("¿Está Seguro de activar la Categoría?", function(result){

		if(result)

        {

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/categoria.php?op=activar", {idcategoria : idcategoria}, function(e){

        		bootbox.alert(e);

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	})

}





init();

$("#codigo_token").keyup(function () {
	var token = $(this).val(); 
	$.ajax({
		type: "POST",
		url: "../ajax/gestion_token.php?op=validar_token",
		data: {token_prueba:token},
		dataType: "text",
		success: function (response) {
			alert(response);
			if(response == "Token Correcto")
				$("#btnGuardar").removeAttr("disabled");
			else
				$("#btnGuardar").prop("disabled",true);
		}
	});
});

$("#fecha_registro_token").change(function(){
	var fecha_reg = $(this).val();
	$("#fecha_caducidad_token").prop("min",fecha_reg);
});