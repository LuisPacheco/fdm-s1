$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();

	

	if(title != "Opciones" && title != "Imagen"){

		$(this).html( '<input type="text" style="width:100%"> ' );

	}else{

		$(this).html( '' );

	}

	

	$( 'input', this ).on( 'keyup change', function () {

		if ( tabla_listado.column(i).search() !== this.value ) {

			tabla_listado

				.column(i)

				.search( this.value )

				.draw();

		}

	} );

} );

var tabla_listado = $('#tbllistado').DataTable({

	orderCellsTop: true,

	fixedHeader: true

});

var tabla;

var tablaDetalle = $("#detalles_show").DataTable({
    
    "bPaginate": false,

    "bLengthChange": false,
    
    "bFilter": true,
    
    "bInfo": false,
    
    "bAutoWidth": false
    
});

//Declaración de variables necesarias para trabajar con las compras y sus detalles

var impuesto = 13;

var cont = 0;

var detalles = 0;

//$("#guardar").hide();

$("#btnGuardar").hide();

$("#Factura").change(marcarImpuesto);

$("#Recibo").change(marcarImpuesto);



//Función que se ejecuta al inicio

function init(){

	mostrarform(false,0);

	listar();



	$("#formulario").on("submit",function(e){

		guardaryeditar(e);	

	});



	$.post("../ajax/ingreso.php?op=selectProveedor",function(r){

		$("#idproveedor").html(r);

		$("#idproveedor").selectpicker("refresh");

	});

	$('#mCompras').addClass("treeview active");

    $('#lIngresos').addClass("active");   

}

//Función limpiar campos con contenido

function limpiar(){

	$("#idproveedor").val("");

	$("#idproveedor").selectpicker("refresh");

	$("#proveedor").val("");

	$("#num_comprobante").val("0");

	$("#impuesto").val("0");

	$("#Recibo").prop("checked",true);

	$("#fecha_hora").val("");

	$("#total_compra").val(""); 

	$(".filas").remove();

	$("#total").html("Bs. 0.00");

	$("#idingreso").val("");

	//obtener fecha actual

	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);

	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear() + "-" + (month) + "-" + (day);

	$("#fecha_hora").val(today);

	detalles = 0;

	cont = 0;

}


//Función mostrar formulario de registro de ingreso

function mostrarform(flag,idingreso){

	limpiar();
	
	if (flag){

		$("#listadoregistros").hide();

		$("#formularioregistros").show();
		
		
		
		$("#detalles").show();

		$("#detalles_show").hide();

		$('#detalles_show').parents('div.dataTables_wrapper').first().hide();

		
		//$("#btnGuardar").prop("disabled",false);

		$("#btnagregar").hide();

		listarArticulos();

		//listarLotes();



		$("#btnGuardar").hide();

		$("#btnCancelar").show();

		detalles=0;
		
		cont = 0;

		$("#btnAgregarArt").show();

	}

	else{

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

	}



}


//Función cancelarform

function cancelarform(){

	limpiar();

	mostrarform(false,0);

}


//Función Listar TODOS los ingresos realizados

function listar(){

	tabla_listado = $('#tbllistado').dataTable({

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/ingreso.php?op=listar',

					type : "get",

					dataType : "json",						

					error: function(e){
					    

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}


//Función Listar Articulos en la VENTANA MODAL

function listarArticulos(){

	tabla=$('#tblarticulos').dataTable(	{

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [],

		"ajax":

				{

					url: '../ajax/ingreso.php?op=listarArticulos',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}



//Función Listar Lotes en la TABLA MODAL3

function listarLotes(){

	tabla=$('#tblotes').dataTable(	{

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [],

		"ajax":

				{

					url: '../ajax/lote.php?op=selectLotes',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función para guardar o editar
function guardaryeditar(e){

	e.preventDefault(); //No se activará la acción predeterminada del evento

	bootbox.confirm("¿Está Seguro/a de efectuar el ingreso?<br>Nota: Una vez efectuado <strong>No podrá ser modificado</strong>", function(result){

		//console.log(result);

		if(result){

			$("body").css({"padding-right":"0px"});

			var formData = new FormData($("#formulario")[0]);

			$.ajax({

				url: "../ajax/ingreso.php?op=guardaryeditar",

			    type: "POST",

			    data: formData,

			    contentType: false,

			    processData: false,

			    success: function(datos){                    

			          bootbox.alert(datos);	          

			          mostrarform(false,0);

					  tabla_listado.ajax.reload();

				}

			});

			limpiar();

		}else{

			$("body").css({"padding-right":"0px"});

			bootbox.alert("Ingreso no efectuado");

		}

		

	});		

}

function mostrar(idingreso){

	//Se muestra los datos de todos los ingresos

	$.post("../ajax/ingreso.php?op=mostrar",{idingreso : idingreso}, function(data, status){

			data = JSON.parse(data);

			//console.log(data);		

			mostrarform(true,idingreso);



			$("#idproveedor").val(data.idproveedor);

			$("#idproveedor").selectpicker('refresh');



			if (data.tipo_comprobante == "Factura") 

				$("#Factura").prop("checked",true);

			else

				$("#Recibo").prop("checked",true);



			$("#num_comprobante").val(data.num_comprobante);

			$("#fecha_hora").val(data.fecha);

			$("#impuesto").val(data.impuesto);

			$("#idingreso").val(data.idingreso);



			//Ocultar y mostrar los botones

			$("#btnGuardar").hide();

			$("#btnCancelar").show();

			$("#btnAgregarArt").hide();
			
			console.log(data.total_compra);
			
			$("#total").html("Bs. " + data.total_compra);
			
			//muestra los detalles de un ingreso en particular
            listarDetalle(idingreso);
 	});

 	

}

function listarDetalle(idingreso){
    
    $("#detalles").hide();

	$("#detalles_show").show();

	tabla_detalles = $('#detalles_show').dataTable(

		{

			"aProcessing": true,							//Activamos el procesamiento del datatables

			"aServerSide": true,							//Paginación y filtrado realizados por el servidor

  			"pagingType": "simple",

			"ajax":

				{

					url: "../ajax/ingreso.php?op=listarDetalle&id=" + idingreso,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

			"bDestroy": true,	

		}

	).DataTable();

}

//Función para anular registros, se lo realiza en tiempo real
function anular(idingreso){

	bootbox.confirm("¿Está Seguro de anular el ingreso?", function(result){

		if(result){

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/ingreso.php?op=anular", {idingreso : idingreso}, function(e){

        		bootbox.alert(e);

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	});

}

function marcarImpuesto(){

  	var tipo_comprobante = $("input[name='tipo_comprobante']:checked").val();

  	//console.log(tipo_comprobante);

  	if (tipo_comprobante=='Factura')

        $("#impuesto").val(impuesto); 

    else

        $("#impuesto").val("0"); 

}

//Se agrega articulos de la ventana MODAL
function agregarDetalle(idarticulo,articulo,laboratorio,precio_venta,cantidad,precio_compra,fecha_actual){

	venta = "";

	compra = "";

	if (precio_venta != "") 
		venta = precio_venta;
	else 
		venta = 0;

	if (precio_compra != "") 
		compra = precio_compra;
	else 
		compra = 0;

	bootbox.prompt("Introduzca la cantidad", function(cant){ 
		if (cant != null) {
			if (!isNaN(cant)) {
				cant = parseInt(cant);
				if (cant > 0) {
				    if (idarticulo != ""){

						//SELECT DE LOS LOTES DISPONIBLES
						var dato = $.ajax({
							url: "../ajax/ingreso.php?op=buscar_lotes&idarticulo="+idarticulo+"&cont="+cont, //indicamos la ruta
							dataType: 'text',//indicamos que es de tipo texto plano
							async: false     //ponemos el parámetro asyn a falso
						}).responseText;


						//NÚMERO DE LOTES DISPONIBLES
						var nro_lotes = $.ajax({
							url: "../ajax/ingreso.php?op=contar_lotes&idarticulo="+idarticulo, //indicamos la ruta
							dataType: 'number',//indicamos que es de tipo texto plano
							async: false     //ponemos el parámetro asyn a falso
						}).responseText;

						nro_lotes = parseInt(nro_lotes);

						nuevo_lote = "";

						/*
						if(nro_lotes != 0)
							nuevo_lote = '<input class="form-control" type="text" name="cod_lote[]" value="" id="cod_lote[]" required>';
						else
							nuevo_lote = '<input class="form-control" type="text" name="cod_lote[]" value="" id="cod_lote[]" required>';
						*/

				    	var subtotal = parseFloat(cantidad * precio_compra).toFixed(2);

				    	var fila ='<tr class="filas" id="fila'+cont+'">'+
				    	'<td><button type="button" class="btn btn-danger" name="boton" id="boton'+cont +'" onclick="eliminarDetalle('+cont+')">X</button></td>'+
				    	'<td><input type="hidden" name="idarticulo[]" id="idartidulo'+cont+'" value="'+idarticulo+'">'+articulo+'</td>'+
				    	'<td><input class="form-control" type="number" min=1 pattern="^[0-9]+" name="cantidad[]" id="cantidad'+cont+'" value="'+cant+'" required onkeyup="modificar('+cont+')"></td>'+
				    	'<td><input class="form-control" type="number" min=0 step="0.01" name="precio_compra[]" id="precio_compra'+cont+'" value="'+compra+'" required onkeyup="modificar('+cont+')"></td>'+
				    	'<td><input class="form-control" type="number" min=0 step="0.01" name="precio_venta[]" id="precio_venta'+cont+'" value="'+venta+'" required></td>'+ 
						'<td>'+dato+'</td>'+	
				    	'<td><input class="form-control" type="text" name="cod_lote[]" value="" id="cod_lote'+cont+'" required></td>'+
				    	'<td><input class="form-control" type="date" name="vencimiento[]" min="'+fecha_actual+'" value="" id="vencimiento'+cont+'"></td>'+
				    	'<td><span name="subtotal" id="subtotal'+cont+'">'+subtotal+'</span></td>'+
				    	'</tr>';

						
				    	$('#detalles').append(fila);
				    	detalles++

						modificar(cont);

				    	cont++;



				    }else
				    	bootbox.alert("<h4><i class='fa fa-times-circle-o' aria-hidden='true'></i>Error al ingresar el detalle, revisar los datos del artículo</h4>");

				}else
					bootbox.alert("<h4><i class='fa fa-times-circle-o' aria-hidden='true'></i>Debe ingresar un valor entero positivo</h4>");

			}else
				bootbox.alert("<h4><i class='fa fa-times-circle-o' aria-hidden='true'></i>Debe ingresar un valor entero positivo</h4>");

		}
	});

}

function calcularTotales(){

  	var sub = document.getElementsByName("subtotal");

	console.log(sub);

  	var total = 0.0;

  	for (var i = 0; i < sub.length; i++) 
		total += Number(parseFloat(sub[i].value).toFixed(2));

	if(isNaN(total))
		total = 0;
	
	total = Number(total).toFixed(2);

	$("#total").html("Bs. " + total);

    $("#total_compra").val(total);

    evaluar();

}

function evaluar(){
    
    console.log(detalles);
    console.log(cont);

  	if (detalles > 0)

      $("#btnGuardar").show();

    else{

      $("#btnGuardar").hide(); 

      cont = 0;

    }

}

function eliminarDetalle(indice){

  	$("#fila" + indice).remove();

	detalles--;
	cont--;

	var filas = document.getElementsByClassName("filas");
	var boton = document.getElementsByName("boton");
	var idarticulo = document.getElementsByName("idarticulo[]");
	var cantidad = document.getElementsByName("cantidad[]");
	var precio_venta = document.getElementsByName("precio_venta[]");
	var precio_compra = document.getElementsByName("precio_compra[]");
	var idlote = document.getElementsByName("idlote[]");
	var cod_lote = document.getElementsByName("cod_lote[]");
	var vencimiento = document.getElementsByName("vencimiento[]");
	var sub = document.getElementsByName("subtotal");
	
	for(i = 0; i < idarticulo.length; i++){
		var fila = filas[i];
		var fila_boton = boton[i];
		var fila_idarticulo = idarticulo[i];
		var fila_cantidad = cantidad[i];
		var fila_precio_venta = precio_venta[i];
		var fila_precio_compra = precio_compra[i];
		var fila_idlote = idlote[i];
		var fila_cod_lote = cod_lote[i];
		var fila_vencimiento = vencimiento[i];
		var fila_subtotal = sub[i];

		fila_idarticulo.setAttribute("id","idarticulo"+i);
		fila_boton.setAttribute("id","boton"+i);
		fila.setAttribute("id","fila"+i);
		fila_cantidad.setAttribute("id","cantidad"+i);
		fila_precio_venta.setAttribute("id","precio_venta"+i);
		fila_precio_compra.setAttribute("id","precio_compra"+i);
		fila_idlote.setAttribute("id","idlote"+i);
		fila_cod_lote.setAttribute("id","cod_lote"+i);
		fila_vencimiento.setAttribute("id","vencimiento"+i);
		fila_subtotal.setAttribute("id","subtotal"+i);

		fila_boton.setAttribute("onclick","eliminarDetalle("+i+")");
		fila_idlote.setAttribute("onclick","verif_lote("+i+")");
		

		fila_cantidad.setAttribute("onkeyup","modificar("+i+")");
		fila_precio_compra.setAttribute("onkeyup","modificar("+i+")");
		
	}

	calcularTotales();

}

function verif_lote(cont){
    var idlote = document.getElementsByName("idlote[]");
    var cod_lote = document.getElementsByName("cod_lote[]");
    var vencimiento = document.getElementsByName("vencimiento[]");
    
    cod_lote[cont].value = "";

    vencimiento[cont].value = "";

    if(idlote[cont].value == "nuevo"){
        cod_lote[cont].readOnly = false;
        cod_lote[cont].required= true;

        vencimiento[cont].readOnly = false;
        
    }else{
        cod_lote[cont].readOnly = true;
        cod_lote[cont].required = false;

        vencimiento[cont].readOnly = true;
    }
}

function modificar(det){
	var cantidad = document.getElementById("cantidad"+det);
	var precio_compra = document.getElementById("precio_compra"+det);
	var subtotal = document.getElementById("subtotal"+det);

	var valor_cantidad = cantidad.value;
	var valor_precio = precio_compra.value;

	var resultado = parseFloat(valor_cantidad) * parseFloat(valor_precio);

	if(isNaN(resultado))
		resultado = 0;

	resultado = resultado.toFixed(2);

	subtotal.innerHTML = resultado;

	subtotal.value = resultado;
	
	
	calcularTotales();
}

init();