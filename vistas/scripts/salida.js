$('#tbllistado thead tr').clone(true).appendTo( '#tbllistado thead' );

$('#tbllistado thead tr:eq(1) th').each( function (i) {
	var title = $(this).text();

	if(title != "Opciones" && title != "Imagen"){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}

	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla_listado.column(i).search() !== this.value ) {
			tabla_listado
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
});

var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

var tabla;

$('#tblarticulos thead tr').clone(true).appendTo( '#tblarticulos thead' );

$('#tblarticulos thead tr:eq(1) th').each( function (i) {

	var title = $(this).text();

	if(title != "Add" && title != "Imagen" && title != "Stock" && title != "Min" && title != "PV"){
		$(this).html( '<input type="text" style="width:100%"> ' );
	}else{
		$(this).html( '' );
	}

	

	$( 'input', this ).on( 'keyup change', function () {
		if ( tabla.column(i).search() !== this.value ) {
			tabla
				.column(i)
				.search( this.value )
				.draw();
		}
	} );
} );

var tabla = $('#tblarticulos').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

var tablaDetalle = $("#detalles_show").DataTable({"bPaginate": false,

"bLengthChange": false,

"bFilter": true,

"bInfo": false,

"bAutoWidth": false});


//Declaración de variables necesarias para trabajar con las compras y sus detalles
var cont=0;
var detalles=0;
$("#btnGuardar").hide();


//Función que se ejecuta al inicio
function init(){

	mostrarform(false,0);

	listar("ambas");

	listarArticulos();

	$(document).ready(function () {  

		detalles = $.ajax({

			url: "../ajax/salida.php?op=select_sucursales", 

			dataType: 'text',

			async: false     

		}).responseText;

		$("#sucursal_select").html(detalles);

		$("#sucursal_select").on("change",function () {  

			listar(this.value);

		})

	});

	$("#formulario").on("submit",function(e){
		guardaryeditar(e);	
	});

	//Cargamos los items al select cliente
	$('#mES').addClass("treeview active");

    $('#lSalida').addClass("active");  

}

//Función limpiar
function limpiar(){

	$(".filas").remove();

	$("#sucursal").val("").selectpicker('refresh').prop("disabled",false);

	$("#num_comprobante").val("0").prop("disabled",false);

	$("#idsalida").val("");

	//Obtenemos la fecha actual

	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);

	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $('#fecha_hora').val(today).prop("disabled",false);

	detalles = 0;

	cont = 0;

}

//Función mostrar formulario
function mostrarform(flag,idsalida){

	limpiar();

	if (flag){

		$("#elige_sucursal").hide();

		$("#sucursal_select").hide();

		$("#listadoregistros").hide();

		$("#formularioregistros").show();

		$("#detalles").show();

		$("#detalles_show").hide();

		$('#detalles_show').parents('div.dataTables_wrapper').first().hide();

		$("#btnGuardar").prop("disabled",false);

		$("#btnagregar").hide();

		if(idsalida == 0){

			$("#tblarticulos").show();

			$('#tblarticulos').parents('div.dataTables_wrapper').first().show();

		}else{

			$("#tblarticulos").hide();

			$('#tblarticulos').parents('div.dataTables_wrapper').first().hide();

		}

		$("#btnGuardar").hide();

		$("#btnCancelar").show();

		$("#btnAgregarArt").show();

	}else{

		$("#elige_sucursal").show();

		$("#sucursal_select").show();

		$("#listadoregistros").show();

		$("#formularioregistros").hide();

		$("#btnagregar").show();

	}

}

//Función cancelarform
function cancelarform(){

	mostrarform(false,0);

}

//Función Listar
function listar(dato){

	tabla_listado=$('#tbllistado').dataTable({

		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado

	    buttons: [		          

		            'copyHtml5',

		            'excelHtml5',

		            'csvHtml5',

		            'pdf'

		        ],

		"ajax":

				{

					url: '../ajax/salida.php?op=listar&sucursal='+dato,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"language": {

            "lengthMenu": "Mostrar : _MENU_ registros",

            "buttons": {

            "copyTitle": "Tabla Copiada",

            "copySuccess": {

                    _: '%d líneas copiadas',

                    1: '1 línea copiada'

                }

            }

        },

		"bDestroy": true,

		"iDisplayLength": 10,//Paginación

	    "order": [[ 1, "desc" ]]//Ordenar (columna,orden)

	}).DataTable();



	$("body").css({"padding-right":"0px"});

}

//Función ListarArticulos
function listarArticulos(){

	tabla=$('#tblarticulos').dataTable({

		"aProcessing": true,//Activamos el procesamiento del datatables

	    "aServerSide": true,//Paginación y filtrado realizados por el servidor

	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    buttons: [		          

		           

		        ],

		"ajax":

				{

					url: '../ajax/salida.php?op=listarArticulosSalida2',

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

		"bDestroy": true,

		"iDisplayLength": 5,//Paginación

	    "order": [[ 9, "asc" ]]//Ordenar (columna,orden)

	}).DataTable();

}

//Función para guardar o editar
function guardaryeditar(e){

	e.preventDefault(); //No se activará la acción predeterminada del evento

	bootbox.confirm("¿Está Seguro/a de efectuar la salida?<br>Nota: Una vez efectuada <strong>No podrá ser modificada</strong>", function(result){

		if(result){

			$("#btnGuardar").prop("disabled",true);

			$("body").css({"padding-right":"0px"});

			var formData = new FormData($("#formulario")[0]);

			$.ajax({

				url: "../ajax/salida.php?op=guardaryeditar",

			    	type: "POST",

			    	data: formData,

			    	contentType: false,

			    	processData: false,

			    	success: function(datos){                    

			          	bootbox.alert(datos);	          

			          	mostrarform(false,0);

						//listarArticulos();

						tabla.ajax.reload();

			          	tabla_listado.ajax.reload();

			    	}

			});

			limpiar();

		}else{

			$("body").css({"padding-right":"0px"});

			bootbox.alert("salida no efectuada");

		}

	});

}

function mostrar(idsalida){

	$.post("../ajax/salida.php?op=mostrar",{idsalida : idsalida}, function(data, status){

		data = JSON.parse(data);

		//console.log(data);		

		mostrarform(true,idsalida);



		$("#sucursal").val(data.sucursal).selectpicker('refresh').prop("disabled",true);

		

		



		$("#fecha_hora").val(data.fecha).prop("disabled",true);



		$("#num_comprobante").val(data.num_comprobante).prop("disabled",true);



		$("#idsalida").val(data.idsalida);



		//tabla

		listarDetalle(idsalida);

		



		//Ocultar y mostrar los botones

		$("#btnGuardar").hide();

		$("#btnCancelar").show();

		$("#btnAgregarArt").hide();

 	});



}

function listarDetalle(idsalida){

	$("#detalles").hide();

	$("#detalles_show").show();

	//$('#detalles_show').dataTable({searching: false, paging: false, info: false});

	

	tablaDetalle = $('#detalles_show').DataTable(

		{

			"aProcessing": true,							//Activamos el procesamiento del datatables

			"aServerSide": true,							//Paginación y filtrado realizados por el servidor

  			"pagingType": "simple",

			"ajax":

				{

					url: '../ajax/salida.php?op=listarDetalle&id='+idsalida,

					type : "get",

					dataType : "json",						

					error: function(e){

						console.log(e.responseText);	

					}

				},

			"bDestroy": true,	

		}

	);

	



}

//Función para anular registros
function anular(idsalida){

	bootbox.confirm("¿Está Seguro de anular el detalle de salida?", function(result){

		if(result){

			$("body").css({"padding-right":"0px"});

        	$.post("../ajax/salida.php?op=anular", {idsalida : idsalida}, function(e){

        		bootbox.alert(e);

				//listarArticulos();

				tabla.ajax.reload();

	            tabla_listado.ajax.reload();

        	});	

        }else{

			$("body").css({"padding-right":"0px"});

		}

	});

}

function agregarDetalle(idarticulo,articulo,stock,cod_lote,idlote,vencimiento,i){

	var fila = '<tr class="filas" id="fila'+cont+'">'+
	'<td><button type="button" class="btn btn-danger" name="boton" id="boton'+cont+'" onclick="eliminarDetalle('+cont+','+i+')">X</button></td>'+
	'<td><input type="hidden" name="idarticulo[]" id="idartidulo'+cont+'" value="'+idarticulo+'" required>'+articulo+'</td>'+
	'<td><input class="form-control" type="number" min=1 max="'+ stock +'" pattern="^[0-9]+" name="cantidad[]" id="cantidad'+cont+'" value="" required style="width:100%  -webkit-appearance: none; margin: 0; -moz-appearance: textfield;"></td>'+
	'<td><span>'+ vencimiento +'</span></td>'+
	'<input type="hidden" name="idlote[]" id="idlote'+cont+'" value="'+idlote+'">'+
	'<input type="hidden" name="lote_corr" id="lote_corr'+cont+'" value="'+i+'">'+
	'</tr>';

	$('#detalles').append(fila);

	cont++;

	detalles++;

	$("#btnAdd"+i).prop("disabled",true); 

	evaluar();

}

function evaluar(){

	if (detalles>0)

  		$("#btnGuardar").show();

	else{

 		 $("#btnGuardar").hide(); 

  	cont=0;

	}

}

function eliminarDetalle(indice,i){

	$("#fila" + indice).remove();

	$("#btnAdd"+i).prop("disabled",false);

	detalles--;
	cont--;

	var filas = document.getElementsByClassName("filas");
	var boton = document.getElementsByName("boton");
	var idarticulo = document.getElementsByName("idarticulo[]");
	var cantidad = document.getElementsByName("cantidad[]");
	var idlote = document.getElementsByName("idlote[]");
	var lote_corr = document.getElementsByName("lote_corr");

	
	for(j = 0; j < idarticulo.length; j++){
		var fila = filas[j];
		var fila_boton = boton[j];
		var fila_idarticulo = idarticulo[j];
		var fila_cantidad = cantidad[j];
		var fila_idlote = idlote[j];
		fila_lote_corr = lote_corr[j];

		fila_idarticulo.setAttribute("id","idarticulo"+j);
		fila_boton.setAttribute("id","boton"+j);
		fila.setAttribute("id","fila"+j);
		fila_cantidad.setAttribute("id","cantidad"+j);
		fila_idlote.setAttribute("id","idlote"+j);
		fila_boton.setAttribute("onclick","eliminarDetalle("+j+","+lote_corr[j].value+")");	
		fila_lote_corr.setAttribute("id","lote_corr"+j);
	}

	evaluar();

}

init();