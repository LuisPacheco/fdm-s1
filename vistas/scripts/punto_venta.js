var tabla_listado = $('#tbllistado').DataTable({
	orderCellsTop: true,
	fixedHeader: true
});

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();
	$("#formulario").on("submit",function(e){
		guardar(e);	
	});
    $('#mConfiguracion').addClass("treeview active");
    $('#lPuntosVenta').addClass("active");

	//carga la lista de tipos de 
	$.post("../ajax/punto_venta.php?op=listarTipoPuntosVenta", function(r){
		$("#codigoTipoPuntoVenta").html(r);
		$('#codigoTipoPuntoVenta').selectpicker('refresh');
	});
}

//Función limpiar
function limpiar(){
	$("#nombrePuntoVenta").val("");
	$("#descripcion").val("");
	$("#codigoTipoPuntoVenta").val("5");
	$('#codigoTipoPuntoVenta').selectpicker('refresh');
}

//Función mostrar formulario
function mostrarform(flag){
	limpiar();
	if (flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform(){
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(){
	tabla_listado=$('#tbllistado').dataTable({
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla_listado
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":{
					url: '../ajax/punto_venta.php?op=listar',
					type : "get",
					dataType : "json",	
							
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 1, "asc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

//Función para guardar o editar
function guardar(e){
	e.preventDefault(); //No se activará la acción predeterminada del evento
	bootbox.confirm("¿Está Seguro/a de registrar el punto de venta? Nota: <strong>Una vez registrado el punto de venta NO podrá ser editado posteriormente</strong>", function(result){
		if(result){
			$("#btnGuardar").prop("disabled",true);
			var formData = new FormData($("#formulario")[0]);
			$("body").css({"padding-right":"0px"});
			$.ajax({
				url: "../ajax/punto_venta.php?op=guardar",
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				success: function(datos){                    
					bootbox.alert(datos);	          
					mostrarform(false);
					tabla_listado.ajax.reload();
				}
			});
			limpiar();
		}else{
			$("body").css({"padding-right":"0px"});
		}
	});
	
}

function generar_cuis(cod_pv){
    //console.log(cod_pv);
	bootbox.confirm("¿Está Seguro/a de generar el CUIS para el punto de venta?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
			$.post("../ajax/punto_venta.php?op=generar_cuis&cod_pv="+cod_pv, function(e){
			    //console.log(e)
			    
				if(e == "1")
        			bootbox.alert("Registro de CUIS realizado correctamente");
				else
					bootbox.alert("Registro de CUIS no fuer realizado");
	            tabla_listado.ajax.reload();
	            
        	});
        }else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

function generar_cufd(cod_pv){
	bootbox.confirm("¿Está Seguro/a de generar el CUFD para el punto de venta?", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
			$.post("../ajax/punto_venta.php?op=generar_cufd&cod_pv="+cod_pv, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});
        }else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

function cierre_operaciones(cod_pv){
	bootbox.confirm("¿Está Seguro/a de de realizar el cierre de operaciones del punto de venta? <br><strong>El CUIS y CUFD actuales se desactivarán</strong>", function(result){
		if(result){
			$("body").css({"padding-right":"0px"});
			$.post("../ajax/punto_venta.php?op=cierre_operaciones&cod_pv="+cod_pv, function(e){
        		bootbox.alert(e);
	            tabla_listado.ajax.reload();
        	});
        }else{
			$("body").css({"padding-right":"0px"});
		}
	});
}

function volver_menu() {  
	window.location.href = "escritorio.php";
}


init();

$(document).ready(function () {
	/*
	var id = $("#id_token").val();
	if(id == 0){
		alert("Token Inválido o vencido");
		setTimeout(volver_menu(),3000);
	}
	*/

	var cuis_activo = $("#cuispv0").val();
	if(cuis_activo == 0){
		var id_pv0 = $("#id_pv0").val();
		var id_pv_session_actual = $("#id_pv_session_actual").val();
		if(id_pv0 != id_pv_session_actual || id_pv0 == "" || id_pv_session_actual == ""){
			alert("Cuis de PV0 inactivo");
			setTimeout(volver_menu(),3000);
		}
	}

	//carga de lista de productos según SIN
	$.post("../ajax/sinc_datos.php?op=hora_fecha", function(r){
		if(r == false){
			alert("Debe realizar la sincronización del día para cargar la lista de puntos de venta");
			setTimeout(volver_menu(),3000);
		}
	});
});

//validar nombre de PV que no sea repetido
$("#nombrePuntoVenta").keyup(function (e) { 
	$(".alert").remove();
	$("#btnGuardar").prop("disabled",false);
	var dato = $(this).val();
	$.ajax({
		type: "GET",
		url: "../ajax/punto_venta.php?op=verificar_nombre_pv&nombre_pv="+dato,
		cache:false,
		contentType: false,
		processData: false,
		success: function (datos) {
			datos = Number(datos);
			console.log(datos);
			if(datos >= 1){
				$("#nombrePuntoVenta").after('<div class="alert alert-warning text-white">El nombre para el PV ya se encuentra registrado</div>');
				$("#btnGuardar").prop("disabled",true);
			}
		}
	});
});