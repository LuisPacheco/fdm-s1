<?php 

	ob_start();

	if (strlen(session_id()) < 1)

		session_start();//Validamos si existe o no la sesión

	if (!isset($_SESSION["nombre"]))

	  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

	else{

		//Validamos el acceso solo al usuario logueado y autorizado.

		if ($_SESSION['entrada'] == 1){

			require_once "../modelos/Entrada.php";



			$entrada = new Entrada();

			date_default_timezone_set("America/La_Paz");

			$identrada=isset($_POST["identrada"])? limpiarCadena($_POST["identrada"]):"";

			$idusuario=$_SESSION["idusuario"];

			$sucursal = isset($_POST["sucursal"])? limpiarCadena($_POST["sucursal"]):"";

			$num_comprobante=isset($_POST["num_comprobante"])? limpiarCadena($_POST["num_comprobante"]):"";

			$fecha_hora=isset($_POST["fecha_hora"])? date("Y") . "-" . date("m") . "-" . date("d") . " " . date("H") . ":" . date("i") . ":" . date("s"):"";

			

			switch ($_GET["op"]){

				case 'guardaryeditar':

					//var_dump($_POST);

					

					if (empty($identrada)){

						//var_dump($_POST);

						if(isset($_POST["idarticulo"]) && isset($_POST["cantidad"]) && isset($_POST["cod_lote"]) && isset($_POST["idlote"]) && isset($_POST["vencimiento"])){
						    
							if(count($_POST["idarticulo"]) > 0 && count($_POST["cantidad"]) > 0 && count($_POST["cod_lote"]) > 0 && count($_POST["idlote"]) > 0 && count($_POST["vencimiento"]) > 0){
							    
								$idarticulo = $_POST["idarticulo"];

								$cantidad = $_POST["cantidad"];

								$cod_lote = $_POST["cod_lote"];

								$vencimiento = $_POST["vencimiento"];

								$idlote = $_POST["idlote"];

								$rspta = $entrada->insertar($idusuario,$num_comprobante,$sucursal,$fecha_hora,$idarticulo,$cantidad,$cod_lote,$vencimiento,$idlote);

								if ($rspta) 

									echo "Entrada y Lote registrados correctamentes";

								else

									echo "No se pudieron registrar todos los datos de la entrada";

							}else{

								$errores = "";

								if(count($_POST["idarticulo"]) <= 0)

									$errores .= "Artículo vacío<br>";

								if(count($_POST["cantidad"]) <= 0)

									$errores .= "Cantidad vacía<br>";

								if(count($_POST["cod_lote"]) <= 0)

									$errores .= "Cod Lote vacío<br>";

								if(count($_POST["vencimiento"]) <= 0)

									$errores .= "Vencimiento vacío<br>";

								if(count($_POST["idlote"]) <= 0)

									$errores .= "Lote vacío<br>";

								$errores .= "<b>No se efectuará la entrada</b>";

									echo $errores;

							}

						}else{

							$errores = "";

							if(!isset($_POST["idarticulo"]))

								$errores .= "Error en registrar artículo<br>";

							if(!isset($_POST["cantidad"]))

							if(!isset($_POST["cod_lote"]))

								$errores .= "Error en registrar cod lote<br>";

							if(!isset($_POST["vencimiento"]))

								$errores .= "Error en registrar vencimiento<br>";

							if(!isset($_POST["idlote"]))

								$errores .= "Error en registrar lote<br>";

							$errores .= "<b>No se efectuará la entrada</b>";

								echo $errores;

						}	

					}else

						echo "Error al registrar entrada";

				break;



				case 'anular':

					$rspta=$entrada->anular($identrada);

					$rspta2 = $entrada->listarDetalle($identrada);

					require_once "../modelos/Lote.php";

					$lote = new Lote();

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();

					$sw = true;

					while ($reg = $rspta2->fetch_object()){

						$sql_stock_lote = $lote->getStock($reg->idlote);

						$stock_lote = $sql_stock_lote->fetch_object(); //stock total lote



						$sql_estado_lote = $lote->mostrarEstado($reg->idlote);

						$estado_lote = $sql_estado_lote->fetch_object();//estado del lote



						$dev_stock_articulo = $articulo->devolverStock($reg->idarticulo);

						$stock_art = $dev_stock_articulo->fetch_object();//stock total del articulo



						if($estado_lote->estado != "Devuelto" && $estado_lote->estado != "Agotado"){

							$dif1 = $stock_lote->stock - $reg->cantidad;

							$dif2 = $stock_art->stock - $reg->cantidad;

							$res_estado = "";

							$res_stock_lote = "";

							if($dif1 <= 0){

								$res_estado = $lote->modificarEstado("Agotado",$reg->idlote);

							}else{

								$res_estado = true;

							}

							$res_stock_lote = $lote->setStock($reg->idlote,$dif1);

							$res_stock = $articulo->modificarStock($reg->idarticulo,$dif2);

							

							if (!$res_estado || !$res_stock || !$res_stock_lote){

								$sw .= "false".$i;

							} 

								

						}

						$i++;

					}

					if($rspta2)

						$rspta2 = "true";

					else

						$rspta2 = "false";

					//echo $rspta . " " . $rspta2 . " " . $sw;

					if($rspta == "true" && $rspta2 == "true" && $sw == "true")

						echo "Entrada Anulada";

					else

						echo "Entrada no se puede anular";

				break;



				case 'mostrar':

					$rspta=$entrada->mostrar($identrada);

		 			//Codificar el resultado utilizando json

		 			echo json_encode($rspta);

				break;



				case 'listarDetalle':

					//Recibimos el identrada

					$id = $_GET['id'];



					$rspta = $entrada->listarDetalle($id);

					$total = 0;

					

					echo '<thead style="background-color:#A9D0F5">

		    	            <th>Opciones</th>

		                	<th>Artículo</th>

		                	<th>Cantidad</th>

		                	<th>Lote</th>

		                	<th>Vencimiento</th>

		              	</thead>';

	

		            //var_dump($rspta);

					while ($reg = $rspta->fetch_object()){

						if($reg->vencimiento == "" || $reg->vencimiento == "0000-00-00" || $reg->vencimiento == NULL || $reg->vencimiento == "0/0")

							echo '<tr class="filas"><td></td><td>'.$reg->nombre.'</td><td>'.$reg->cantidad.'</td><td>'.$reg->cod_lote.'</td><td>n/a</td></tr>';

						else

							echo '<tr class="filas"><td></td><td>'.$reg->nombre.'</td><td>'.$reg->cantidad.'</td><td>'.$reg->cod_lote.'</td><td>'.$reg->vencimiento.'</td></tr>';

						$total = $total+($reg->precio_compra*$reg->cantidad);

					}

					echo '<tfoot>

			                <th></th>

		                	<th></th>

		                	<th></th>

		                	<th></th>

		               	</tfoot>';

				break;



				case 'listar':

				$sucursal = $_GET["sucursal"];

					$rspta = $entrada->listar($sucursal);

		 			//Vamos a declarar un array

		 			$data = Array();

		 			while ($reg = $rspta->fetch_object()){

			 			$data[] = array(

							"0" => '<button class="btn btn-warning" onclick="mostrar('.$reg->identrada.')"><i class="fa fa-eye"></i></button>'.

							'<a target="_blank" href="../reportes/comprobante_entrada.php?id='.$reg->identrada.'"> <button class="btn btn-info"><i class="fa fa-file"></i></button></a>',

							 /*

		 					"0"=>(($reg->estado == 'Aceptado')?'<button class="btn btn-warning" onclick="mostrar('.$reg->identrada.')"><i class="fa fa-eye"></i></button>'.

			 					' <button class="btn btn-danger" onclick="anular('.$reg->identrada.')"><i class="fa fa-close"></i></button>':

		 						'<button class="btn btn-warning" onclick="mostrar('.$reg->identrada.')"><i class="fa fa-eye"></i></button>').

		 						'<a target="_blank" href="../reportes/comprobante_entrada.php?id='.$reg->identrada.'"> <button class="btn btn-info"><i class="fa fa-file"></i></button></a>',*/

		 					"1"=>$reg->fecha,

		 					

		 					"2"=>$reg->usuario,

		 					

		 					"3"=>$reg->num_comprobante,

		 					"4"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':

		 					'<span class="label bg-red">Anulado</span>'

		 				);

		 			}

		 			$results = array(

			 			"sEcho"=>1, //Información para el datatables

		 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

		 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

		 				"aaData"=>$data);

		 			echo json_encode($results);

				break;



				case 'selectProveedor':

					require_once "../modelos/Persona.php";

					$persona = new Persona();



					$rspta = $persona->listarP();



					echo '<option value='.'>-- Seleccione --</option>';

					while ($reg = $rspta->fetch_object())

						echo '<option value=' . $reg->idpersona . '>' . $reg->nombre . '</option>';	

				break;



				case 'selectArticulo':

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();



					$rspta = $articulo->listarArt();



					while ($reg = $rspta->fetch_object())

						echo '<option value=' . $reg->idarticulo . '>' . $reg->nombre_comercial . ' | ' . $reg->nombre_generico . ' | '. $reg->laboratorio .  '</option>';	

				break;



				case 'listarArticulos':

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();



					$rspta = $articulo->listarActivos();

		 			//Vamos a declarar un array

		 			$data = Array();

		 			$i = 0;

		 			while ($reg = $rspta->fetch_object()){

		 				$dato = "";

		 				//var_dump($reg);

		 				if ($reg->stock <= $reg->stock_minimo)
		 					$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";
		 				else 
			 				$dato = $reg->stock;

						$fecha_actual = date("Y-m-d");

		 				$data[] = array(

		 					"0"=>'<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre.'\',\''.$reg->idlaboratorio.'\',\''.$reg->precio_venta.'\',\''.$reg->cantidad.'\',\''.$reg->precio_compra.'\',\''.$fecha_actual.'\')"><span class="fa fa-plus"></span></button>',

		 					"1"=>$reg->cod_med,

		 					"2"=>$reg->nombre,

		 					"3"=>$reg->codigo,

		 					"4"=>$reg->categoria,

		 					"5"=>$reg->laboratorio,

		 					"6"=>$dato,

		 					"7"=>$reg->stock_minimo,

		 					"8"=>$reg->precio_venta,

		 					"9"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >"

		 				);

		 				$i++;

		 			}

		 			$results = array(

		 				"sEcho"=>1, //Información para el datatables

		 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

		 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

		 				"aaData"=>$data);

		 			echo json_encode($results);

				break;

				case "buscar_lotes":

					require_once "../modelos/Lote.php";

					$lote = new Lote();		

					$idarticulo = $_GET["idarticulo"];

					$cont = $_GET["cont"];

					$lotes = "<select class='form-control' name='idlote[]' id='idlote$cont' onclick='verif_lote(".$cont.")'><option value='nuevo' selected>--nuevo lote--</option>";

					$sql_listar_lotes = $lote->listar_lotes($idarticulo);

					$num_lotes = 0;

					while($rec = $sql_listar_lotes->fetch_object()){

						if($rec->estado == "Vencido" || $rec->estado == "Devuelto")

							$lotes .="<option value='".$rec->idlote."' disabled>".$rec->cod_lote." - " . $rec->vencimiento . " - " . $rec->estado . "</option>";

						else{

							if($rec->estado == "Agotado")

								$lotes .="<option value='".$rec->idlote."' style='font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";

							else{

								if($rec->vencimiento != "" && $rec->vencimiento != NULL && $rec->vencimiento != "0000-00-00"){

									date_default_timezone_set("America/La_Paz");

									$fecha_vencimiento = new DateTime($rec->vencimiento);

									$actual = date("Y-m-d");

									$fecha = new DateTime($actual);

									$res = $fecha->diff($fecha_vencimiento);

									$dias = $res->days;

									if($dias >= 180 && $res->invert == 0)

										$lotes .="<option value='".$rec->idlote."' style='color:green;font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";

									else{

										if ($dias > 90 && $dias < 180 && $res->invert == 0) 

											$lotes .="<option value='".$rec->idlote."' style='color:orange;font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";

										else{

											if ($dias <= 90 && $res->invert == 0) 

												$lotes .="<option value='".$rec->idlote."' style='color:red;font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";

											else

												$lotes .="<option value='".$rec->idlote."' disabled>".$rec->cod_lote." - " . $rec->vencimiento . " - " . $rec->estado . "</option>";

										}

									}

								}else{

									$lotes .="<option value='".$rec->idlote."' style='color:blue;font-weight:900'>".$rec->cod_lote." - " . $rec->vencimiento . " - " . $rec->estado . "</option>";

								}

							}



							

						}

						$num_lotes++;

					}

					$lotes .= "</select>";

					echo $lotes;

				break;



				case "contar_lotes":

					require_once "../modelos/Lote.php";

					$lote = new Lote();		

					$idarticulo = $_GET["idarticulo"];

					$sql_listar_lotes = $lote->listar_lotes($idarticulo);

					echo $sql_listar_lotes->num_rows;

				break;



				case 'select_sucursales':

					echo "<select id='sucursal_select'>

							<option value='ambas' selected>Todas</option>

							<option value='irpavi2'>Irpavi Nueva</option>

							<option value='irpavi'>Irpavi Central</option>

							<option value='seguencoma'>Seguencoma</option>

						</select>";

				break;



			}

		//Fin de las validaciones de acceso

		}

		else

	  		require 'noacceso.php';

	}

	ob_end_flush();