<?php 
	ob_start();
	if (strlen(session_id()) < 1)
		session_start();//Validamos si existe o no la sesión
	if (!isset($_SESSION["nombre"]))
  		header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
	else{
		//Validamos el acceso solo al usuario logueado y autorizado.
		if ($_SESSION['lotes'] == 1){
			require_once "../modelos/Lote.php";
			$lote = new Lote();
			date_default_timezone_set("America/La_Paz");
			$idlote=isset($_POST["idlote"])? limpiarCadena($_POST["idlote"]):"";
			$cod_lote=isset($_POST["cod_lote"])? limpiarCadena($_POST["cod_lote"]):"";
			$vencimiento=isset($_POST["vencimiento"])? limpiarCadena($_POST["vencimiento"]):"";
			$idusuario=$_SESSION["idusuario"];
			$descripcion=isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";
			$idlaboratorio=isset($_POST["idlaboratorio"])? limpiarCadena($_POST["idlaboratorio"]):"";
			$idarticulo=isset($_POST["idarticulo"])? limpiarCadena($_POST["idarticulo"]):"";
			$stock=isset($_POST["stock"])? limpiarCadena($_POST["stock"]):"";
			$stock_ant=isset($_POST["stock_ant"])? limpiarCadena($_POST["stock_ant"]):"";
			$idproveedor = isset($_POST["idproveedor"])? limpiarCadena($_POST["idproveedor"]):"";
			$estado = isset($_POST["estado"]) ?limpiarCadena($_POST["estado"]):"";
			//$laboratorio_lote
			switch ($_GET["op"]){
				
				case 'listar1':
					$rspta = $lote->listarLotes();
					//var_dump($rspta);
					$data = Array();
					$cont = 0;
					$fecha_actual = date("Y-m-d");
					$actual = new DateTime($fecha_actual);
			 		while ($reg=$rspta->fetch_object()){

			 			$estado = "";
	 	
	 					$vencimiento = new DateTime($reg->vencimiento);
	 					
	 					if($reg->stock <= 0)
							$estado = "Agotado";
						else{
							if ($reg->estado != "Devuelto") {
							//	echo($reg->vencimiento);
		 						if ($reg->vencimiento != "0000-00-00" && $reg->vencimiento != NULL && $reg->vencimiento != "" && $reg->vencimiento != "0/0") {
		 							$diferencia = $actual->diff($vencimiento);	
		 							if($diferencia->invert == 0 && $diferencia->days > 0){
										if($diferencia->days >= 180)
											$estado = "Activo (6+)";
										else{
											if ($diferencia->days > 90 && $diferencia->days < 180) 
												$estado = "Activo (3+)";
											else
												$estado = "Por vencerse (3-)";
										}
									}else
										$estado = "Vencido";	
								}else
									$estado = "n/a";	
		 					}else
		 						$estado = "Devuelto";
						}

						$fondo = "";
	 					$opciones = "";
	 					//determinar el tipo de estado del lote
	 					switch ($estado) {
	 						case 'n/a':
	 							$fondo = '<span class="label bg-light text-black">N/A</span>'; 
	 						break;
	 						
	 						case 'Agotado':
	 							$fondo = '<span class="label bg-black text-white">Stock lote Agotado</span>'; 
	 						break;

	 						case 'Activo (6+)':
	 							$fondo = '<span class="label bg-green text-white">Vencimiento lote mayor a 6 meses</span>'; 
	 						break;

	 						case 'Activo (3+)':
	 							$fondo = '<span class="label bg-yellow text-white">Vencimiento lote mayor a 3 meses</span>'; 
	 						break;

	 						case 'Por vencerse (3-)':
	 							$fondo = '<span class="label bg-red text-white">Vencimiento lote menor a 3 meses</span>'; 
	 						break;

	 						case 'Vencido':
	 							$fondo = '<span class="label bg-red text-white">Lote Vencido</span>'; 
	 						break;

	 						case 'Devuelto':
	 							$fondo = '<span class="label bg-blue text-white">Lote devuelto a proveedor</span>'; 
	 						break;
	 					}

	 					$btn_visibilidad = "";
	 					if ($reg->visibilidad == "0") 
	 						$btn_visibilidad = '<button class="btn btn-primary" title="Visibilizar lote en ventas" onclick="visibilizar('.$reg->idlote.')"><i class="fa fa-check"></i></button>';
	 					else
	 						$btn_visibilidad = '<button class="btn btn-danger" title="Ocultar lote de ventas" onclick="ocultar('.$reg->idlote.')"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>';
	 					
	 					//determinar si va el botón de devolución o no
	 					if($estado != 'Devuelto' && $estado != 'Agotado'){
			 				$opciones = '<button class="btn btn-danger" title="Devolver lote a proveedor" onclick="devolver('.$reg->idlote.',\''.$reg->idarticulo.'\',\''.$reg->stock.'\')"><i class="fa fa-minus-circle" aria-hidden="true"></i></button><br>';
			 			}

			 			if ($estado != 'Devuelto' && $estado != "Vencido") {
				 			$opciones .= '<button class="btn btn-warning" title="Editar" onclick="editar('.$reg->idlote.',\''.$reg->idlaboratorio.'\',\''.$reg->idarticulo.'\')"><i class="fa fa-pencil" aria-hidden="true"></i></button>';
				 		}

						if($estado == "Devuelto" || $estado == "Vencido" || $estado == 'Agotado')
						 	$opciones .= '<button class="btn btn-danger" title="Borrar lote" onclick="borrar('.$reg->idlote.',\''.$reg->estado.'\',\''.$reg->stock.'\',\''.$reg->idarticulo.'\')"><i class="fa fa-trash" aria-hidden="true"></i></button><br>';

			 			$opciones .= '<button class="btn btn-info" title="Mostrar Detalles" onclick="mostrar('.$reg->idlote.',\''.$reg->idlaboratorio.'\',\''.$reg->idarticulo.'\')"><i class="fa fa-eye"></i></button>' . '<br>' . $btn_visibilidad;
			 			
			 			//$opciones = "";
			 			//determinar si existe mes y año
			 			$fecha = "";
			 			if ($reg->mes == 0 && $reg->anio == 0) 
			 				$fecha = "n/a";
			 			else
			 				$fecha = $reg->mes . "/" . $reg->anio;

			 			$visibilidad = "";
			 			if($reg->visibilidad == '1')
			 				$visibilidad = '<span class="label bg-green">Visible</span>';
			 			else
			 				$visibilidad = '<span class="label bg-red">Invisible</span>';
			 			
			 			$data[]=array(
			 				"0"=>$reg->fecha_registro,
		 					"1"=>$reg->cod_lote,
		 					"2"=>$fecha,
		 					"3"=>$reg->laboratorio,
		 					"4"=>$reg->cod_med,
		 					"5"=>$reg->medicamento,
		 					"6"=>$reg->stock,
		 					"7"=>$fondo,
		 					"8"=>$visibilidad,
		 					"9"=>$opciones
			 				);
			 			
			 			if($estado != $reg->estado)
			 				$lote->modificarEstado($estado,$reg->idlote);
			 		}
			 		$results = array(
			 			"sEcho"=>1, //Información para el datatables
			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
			 			"aaData"=>$data);
			 		echo json_encode($results);
				break;

				case 'ocultar':
					$rspta=$lote->ocultar($idlote);
			 		echo $rspta ? "Lote oculto de <strong>ventas</strong>" : "Lote no se puede ocultar de <strong>ventas</strong>";
				break;

				case 'visibilizar':
					$rspta=$lote->visibilizar($idlote);
			 		echo $rspta ? "Lote visible en <strong>ventas</strong>" : "Lote no se puede mostrar en <strong>ventas</strong>";
				break;

				case 'devolver':
					$devol = $lote->devolver($idlote);

					$ocul = $lote->ocultar($idlote);

					require_once "../modelos/Articulo.php";
					
					$articulo = new Articulo();

					$res = $articulo->devolverStock($idarticulo);

					$reg = $res->fetch_object();

					$dif = $reg->stock - $stock;

					$rspta = $articulo->modificarStock($idarticulo,$dif);

			 		echo $rspta && $devol && $ocul? "Lote devuleto a proveedor" : "No se pudo devolver el lote";
				break;

				case 'borrar':
					
					if($estado == "Vencido"){
						require_once "../modelos/Articulo.php";

						$articulo = new Articulo();

						$res = $articulo->devolverStock($idarticulo);

						$reg = $res->fetch_object();

						$dif = $reg->stock - $stock;

						$rspta = $articulo->modificarStock($idarticulo,$dif);

						$borr = $lote->borrar($idlote);

						$vis = $lote->ocultar($idlote);

						echo $borr && $rspta && $vis? "Lote vencido borrado correctamente": "No se pudo borrar el lote vencido";
					}else{
						$vis = $lote->ocultar($idlote);

						$borr = $lote->borrar($idlote);

						echo $borr && $vis ? "Lote borrado correctamente": "No se pudo borrar el lote";
					}
			 		
				break;

				case 'mostrar':
					$rspta1 = $lote->mostrar_ingreso($idlote);	
					$rspta2 = $lote->mostrar_entrada($idlote);
					$rspta1_res = $rspta1->fetch_object();
					$rspta2_res = $rspta2->fetch_object();	
					if($rspta1_res->idarticulo != null && $rspta1_res->idlaboratorio != null)
						echo json_encode($rspta1_res);
					else
						echo json_encode($rspta2_res);
					
				break;

				case 'guardar':
					
					if (!empty($idlote)){
						//var_dump($_POST);
						
						require_once "../modelos/Articulo.php";
						$articulo = new Articulo();
						$dev_stock_articulo = $articulo->devolverStock($_POST["idarticulo_ant"]);
						$stock_art = $dev_stock_articulo->fetch_object();
						$total_art = $stock_art->stock;
						$ant = $_POST["stock_ant"];
						$act = $_POST["stock"];

						$res = $total_art - $ant + $act;

						$rspta = $articulo->modificarStock($_POST["idarticulo_ant"],$res);

						if($vencimiento == "" || $vencimiento == NULL || $vencimiento == "0000-00-00"){
							$rspta2 = $lote->modificar($idlote,$cod_lote,NULL,$act);
						}else{
							if($vencimiento == $_POST["vencimiento_ant"])
								$rspta2 = $lote->modificar($idlote,$cod_lote,$_POST["vencimiento_ant"],$act);
							else
								$rspta2 = $lote->modificar($idlote,$cod_lote,$vencimiento,$act);
						}
						echo $rspta && $rspta2 ? "Modificación de lote realizada satisfactoriamente": "No se pudo modificar el stock del lote";
						
					}else
						echo "Error al modificar el lote";
					
				break;
			}
			//Fin de las validaciones de acceso
		}
		else
  			require 'noacceso.php';
	}
	ob_end_flush();