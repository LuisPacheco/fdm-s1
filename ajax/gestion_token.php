<?php 

ob_start();

if (strlen(session_id()) < 1){

	session_start();//Validamos si existe o no la sesión

}

if (!isset($_SESSION["nombre"]))

{

  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

}

else

{

//Validamos el acceso solo al usuario logueado y autorizado.

if ($_SESSION['configuracion']==1){

require_once "../modelos/GestionToken.php";



$gestion_token = new GestionToken();

$id_token = isset($_POST["id_token"])? limpiarCadena($_POST["id_token"]):"";

$token_prueba = isset($_POST["token_prueba"])? limpiarCadena($_POST["token_prueba"]):"";

$token = isset($_POST["codigo_token"])? limpiarCadena($_POST["codigo_token"]):"";

$fecha_registro_token = isset($_POST["fecha_registro_token"])? limpiarCadena($_POST["fecha_registro_token"]):"";

$fecha_caducidad_token = isset($_POST["fecha_caducidad_token"])? limpiarCadena($_POST["fecha_caducidad_token"]):"";

switch ($_GET["op"]){

	case 'listar':
		$rspta=$gestion_token->listar();
 		//Vamos a declarar un array
 		$data= Array();
 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
				"0"=>($reg->estado_token)?'<button class="btn btn-danger" onclick="desactivar('.$reg->id_token.')"><i class="fa fa-close"></i></button>':"",
				"1"=>$reg->codigo_token_hash,
				"2"=>$reg->fecha_registro_token,
				"3"=>$reg->fecha_caducidad_token,
				"4"=>($reg->estado_token)?'<span class="label bg-green">Activo</span>':
				'<span class="label bg-red">Inactivo</span>'
			);	
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);



	break;

	case 'validar_token':
		$rspta = $gestion_token->validarToken($token_prueba);
		if($rspta){
			$cod = $rspta->return->mensajesList->codigo;
			if($cod == "926")
				echo "Token Correcto";
			else
				"Error, vuelva a intentar";
		}else{
			echo "Error, token inválido";
		}
	break;

	case 'guardar':
		$rspta = $gestion_token->validarToken($token);
		if($rspta){
			$token_hash=hash("SHA256",$token);

			$agregar = $gestion_token->insertar($token,$token_hash,$fecha_registro_token,$fecha_caducidad_token);

			if($agregar) {
				$desactivar_ant = $gestion_token->desactivarTokens($fecha_caducidad_token);
				echo $desactivar_ant?"Token agregado correctamente":"No se pudo agregar el token, vuelva a intentar";
			}else
				echo "No se pudo agregar el token, vuelva a intentar";
		}else{
			echo "Error, token inválido";
		}
	break;

	case 'desactivar':

		$rspta=$gestion_token->desactivar($id_token);

 		echo $rspta ? "Token Desactivado" : "Token no se puede desactivar";

	break;

}
//Fin de las validaciones de acceso
}

else{
  require 'noacceso.php';
}

}

ob_end_flush();