<?php
	date_default_timezone_set("America/La_Paz");
	ob_start();

	if (strlen(session_id()) < 1){
		session_start();//Validamos si existe o no la sesión
	}

	if (!isset($_SESSION["nombre"])){
		header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
	}else{
		//Validamos el acceso solo al usuario logueado y autorizado.
		if ($_SESSION['eventos_significativos']==1){

			require_once "../modelos/EventoSignificativo.php";

			$es = new EventoSignificativo();

			$id_punto_venta = $_SESSION["id_punto_venta"];

			$evento_significativo = isset($_POST["evento_significativo"])? limpiarCadena($_POST["evento_significativo"]):"";

			$descripcion_evento = isset($_POST["descripcion_evento"])? limpiarCadena($_POST["descripcion_evento"]):"";

			$cafc = isset($_POST["cafc"]) ? limpiarCadena($_POST["cafc"]):"";

			$fecha_inicio_evento = isset($_POST["fecha_inicio_evento"])? limpiarCadena($_POST["fecha_inicio_evento"]):"";

			$hora_inicio_evento = isset($_POST["hora_inicio_evento"])? limpiarCadena($_POST["hora_inicio_evento"]):"";

			$fecha_fin_evento = isset($_POST["fecha_fin_evento"])? limpiarCadena($_POST["fecha_fin_evento"]):"";

			$hora_fin_evento = isset($_POST["hora_fin_evento"])? limpiarCadena($_POST["hora_fin_evento"]):"";

			$id_evento_significativo = isset($_POST["id_evento_significativo"]) ? limpiarCadena($_POST["id_evento_significativo"]):"";
			/*
			$descripcion=isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";

			$nombrePuntoVenta=isset($_POST["nombrePuntoVenta"])? limpiarCadena($_POST["nombrePuntoVenta"]):"";

			$codigoTipoPuntoVenta=isset($_POST["codigoTipoPuntoVenta"])? limpiarCadena($_POST["codigoTipoPuntoVenta"]):"5";
			*/

			switch ($_GET["op"]){

				case 'listar':
					date_default_timezone_set("America/La_Paz");

					$punto_venta=$_REQUEST["punto_venta"];

					$fecha_evento=$_REQUEST["fecha_evento"];

					if($punto_venta != "" && $fecha_evento != ""){

						$rspta=$es->listar($punto_venta,$fecha_evento);

						//Vamos a declarar un array

						$data= Array();

						while ($reg=$rspta->fetch_object()){

							$botones = "";

							if($reg->actual != "" && $reg->actual > 0){
								if($reg->estado_evento == 2){
									$botones = '<button class="btn btn-success" onclick="enviar_paquetes(\''.$reg->id_evento_significativo.'\')" title="Enviar evento y paquete"><i class="fa fa-share-square-o"></i></button>';

									//' <button class="btn btn-warning" onclick="reanudar_evento(\''."$reg->id_evento_significativo".'\')" title="Reanudar Evento"><i class="fa fa-retweet"></i></button>';
								}else{
									if($reg->estado_evento == 3 || $reg->estado_evento == 4){
										$botones = '<button class="btn btn-primary" onclick="validar_paquetes(\''.$reg->id_evento_significativo.'\')" title="Validar paquete"><i class="fa fa-search"></i></button>';
										//Si evente es 4, debe incluir un botón de reenvío de paquete de las facturas que requieren corrección
										if($reg->estado_evento == 4)
											$botones .= '<button class="btn btn-warning" onclick="reenviar_paquetes(\''.$reg->id_evento_significativo.'\')" title="Reenviar paquete"><i class="fa fa-share-square-o"></i></button>';
										
									}
									
								}
							}

							//paquete(s)
							$paquetes = "";
							$res_paquetes = $es->obtener_paquetes_es($reg->id_evento_significativo);
							$j = 1;
							while($pqt = $res_paquetes->fetch_object()){
								$paquetes .= '<button class="btn btn-primary" title="Paquete '.$j.'"><i class="fa fa-archive"></i><sub> ('.$pqt->actual.')</sub></button><br>';
								$j++;
							}

							$data[]=array(

								"0" => $botones,

								"1" =>$paquetes,

								"2"=>$reg->fecha_registro_evento,

								"3"=>$reg->codigo_evento,

								"4"=>$reg->descripcion_evento,

								"5"=>$reg->fecha_inicio_evento,

								"6"=>$reg->fecha_fin_evento,

								"7"=>$reg->cafc_evento != "" ? $reg->cafc_evento: "N/A",

								"8"=>$reg->codigo_recepcion_evento_significativo ,

								"9"=>$reg->codigo_recepcion_paquete

							);

						}

						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>count($data), //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

							"aaData"=>$data);

						//LLAMAR AL MÉTODO DE IMPUESTOS PARA PODER VERIFICAR SI EL CUFD ESTÁ VIGENTE

						
					}else{
						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>0, //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>0, //enviamos el total registros a visualizar

							"aaData"=>Array());
					}

					echo json_encode($results);
				break;

				case 'guardar':
					date_default_timezone_set("America/La_Paz");
					//obtener el CUFD que estuvo vigente al inicio de la contingencia para el punto de venta
					require_once "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$cufd = $pv->mostrar_cufd_fecha($fecha_inicio_evento . " " . $hora_inicio_evento,$_SESSION["id_punto_venta"]);
					
					//si se encuentra el CUFD, registrar el evento
					$estado = "0";
					if($cufd){
						$res = $es->insertar($evento_significativo,$descripcion_evento,$fecha_inicio_evento."T".$hora_inicio_evento,$fecha_fin_evento."T".$hora_fin_evento,date("Y-m-d H:i:s.v"),$id_punto_venta,$cufd["id_cufd"],$cafc);

						if($res){
							
							$res_evento_actual = $es->obtener_eventos_activos($_SESSION["id_punto_venta"]);
							
							if($res_evento_actual){//se obtiene el id del evento significativo recientemente creado, y luego se obtiene el CUFD del evento
								$id_evento_actual = $res_evento_actual["id_evento_significativo"];
														
								//crear registro de paquete para evento significativo
								$caracteres_quitar = array("T",".",":","-");
								$nueva_hora_fecha = str_replace($caracteres_quitar, "", $res_evento_actual["fecha_inicio_evento"]);
								try{
									$nombre_carpeta = $nueva_hora_fecha."_".$_SESSION["id_punto_venta"]."_".$res_evento_actual["codigo_evento"]."_1";
									$ruta_carpeta = "../xml_qr/paquetes_contingencia/".$nombre_carpeta;
							
									if (!file_exists($ruta_carpeta)){
										mkdir($ruta_carpeta, 0777, true);
										$crear_pqt = $es->crear_paquete($nombre_carpeta,$res_evento_actual["id_evento_significativo"]);
										
										if($crear_pqt)
											$estado = "1";
									}//CORREGIR EN CASO DE QUE YA HUBIESE LA CARPETA QUE HACER	

									
								}catch(Exception $e){

								}
								
							}


							$_SESSION["modo_sistema"] = $evento_significativo;


							if($estado == "1")
								$resp = [ "mensaje" => $descripcion_evento, "hora_registro" => $res_evento_actual["fecha_inicio_evento"], "id_evento_creado" => $id_evento_actual, "estado" => $estado];
							else
								$resp = ["mensaje" => "No se pudo registrar el evento, intente más tarde","estado" => $estado];

							echo json_encode($resp);
						}else{
							$resp = ["mensaje" => "Hubo un problema en el registro del evento, intente más tarde","estado" => $estado];
							echo json_encode($resp);					
						}
						
					}else{
						$resp = ["mensaje" => "No se encontró un CUFD para el rango de fechas establecidos, intente más tarde","estado" => $estado];
						echo json_encode($resp);					
					}


				break;

				case 'obtenerEventosSignificativos':
					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();
					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaEventosSignificativos");
					$rspta = json_decode($res["sincronizarParametricaEventosSignificativos"])->RespuestaListaParametricas;
					$cad = "<option value='' selected>--Seleccione--</option>";
					if($rspta->transaccion){
						$listaCodigos = $rspta->listaCodigos;
						for($i = 0; $i < count($listaCodigos); $i++){
							if($i >= 4)
								$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'">' . $listaCodigos[$i]->descripcion . '</option>';
						}
					}
					echo $cad;
				break;

				case 'verif_es_activo_pv':
					$id_pv = $_GET["id_pv"];
					$resp = $es->obtener_eventos_activos($id_pv);
					if($resp != "")
						echo true;
				break;

				case 'enviar_paquetes':
					//OBTENER DATOS EVENTO SIGNIFICATIVO
					require_once "../modelos/EventoSignificativo.php";
					$es = new EventoSignificativo();
					$es_actual = $es->obtener_ultimo_evento2($id_evento_significativo);
					$mensaje = "";
					$estado = 0;
					if($es_actual){
						//OBTENER PUNTO DE VENTA
						require_once "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();
						$pv_actual = $pv->getPuntoVenta2($es_actual["id_punto_venta"]);
						if($pv_actual){
							//OBTENER PAQUETE
							$paquete = $es->obtener_ultimo_paquete_es($id_evento_significativo);
							if($paquete){
								//OBTENER CARPETA DONDE SE ENCUENTRAN LOS XML's
								$nombre_carpeta = $paquete["nombre_paquete"];
								$ruta = "../xml_qr/paquetes_contingencia/".$nombre_carpeta;
								//GUARDAR EN UN ARRAY EL NOMBRE DE LOS ARCHIVOS XML VERIFICANDO PRIMERO SI LA RUTA ES VÁLIDA
								$array_nombres_xml = [];
								try{
									// Abrir la carpeta que se pasa como parámetro
									if(!is_dir($ruta))
										$mensaje = "No se pudo encontrar el paquete";
									else{
										$dir = opendir($ruta);
										
										// Leer todos los ficheros de la carpeta
										while ($elemento = readdir($dir)){
											// Tratamos los elementos . y .. que tiene la carpeta
											if( $elemento != "." && $elemento != ".."){
												// Si NO es una carpeta
												if( !is_dir($ruta.$elemento) ){
													// obtener el nombre del archivo si tiene extensión .xml
													if(strpos($elemento,".xml"))
														array_push($array_nombres_xml,$elemento);
												} 
											}
										}
										
										
										//ORDENA ARRAY DE NOMBRES EN BASE AL ORDEN DE CREACIÓN (NOMBRE)
                                        sort($array_nombres_xml);

										//OBTENER EL CUFD Y CÓDIGO DE CONTROL DEL EVENTO
										$obj_cufd_evento = $pv->mostrar_cufd($es_actual["id_cufd"]);
										if($obj_cufd_evento){
											$cufd_evento = $obj_cufd_evento["codigo_cufd"];
											$cod_control_evento = $obj_cufd_evento["codigo_control_cufd"];

											//OBTENER INFORMACIÓN GENERAL DEL SISTEMA PARA ENVIAR  
											require_once "../modelos/Informacion.php";
											$cuis_actual = $pv->buscar_cuis_activo($pv_actual["id_punto_venta"]);
											$token = TOKEN;

											$cod_ambiente = Informacion::mostrar2()["codigoAmbiente"];
											$cod_modalidad = Informacion::mostrar2()["codigoModalidad"];
											$cod_sistema = Informacion::mostrar2()["codigoSistema"];
											$cod_sucursal = Informacion::mostrar2()["codigoSucursal"];
											$nit = Informacion::mostrar2()["nit"];
											$cuis = $cuis_actual["codigo_cuis"];
											$cod_punto_venta = $pv_actual["codigoPuntoVenta"];

											$cod_doc_sector = 1;//FACTURA COMPRA VENTA
											$cod_emision = 2;//OFFLINE
											$tipo_factura = 1;//FACTURA CON DERECHO A CREDITO FISCAL
											$cafc_evento = $es_actual["cafc_evento"];
											$cant_facturas = count($array_nombres_xml);//cantidad de facturas
											$cod_evento = $es_actual["codigo_evento"];

								
											//LLAMAR A SERVICIO PARA GENERAR NUEVO CUFD
											require_once "../xml/Codigos.php";
											$codigos = new Codigos();
											//GENERAR CUFD
											$obj_nuevo_cufd = $codigos->generarCufd2($cod_punto_venta,$cod_ambiente,$cod_modalidad,$cod_sistema,$cod_sucursal,$cuis,$nit,$token);
											if(is_object($obj_nuevo_cufd)){
												//SI LA TRANSACCION ES CORRECTA, SE ALMACENA EN LA BB.DD, Y SE UTILIZA EL CUFD NUEVO PARA LAS SIGUIENTES ACCIONES
												if($obj_nuevo_cufd->transaccion){
													$cufd_actual = $obj_nuevo_cufd->codigo;
													$cod_control_actual = $obj_nuevo_cufd->codigoControl;
													$direccion_actual = $obj_nuevo_cufd->direccion;
													$fecha_vigencia_actual = $obj_nuevo_cufd->fechaVigencia;
													$res_cufd_nuevo = $pv->registrar_cufd($cufd_actual,$cod_control_actual,$direccion_actual,$fecha_vigencia_actual,$cod_punto_venta);
													if($res_cufd_nuevo){
														//REGISTRAR EVENTO EN IMPUESTOS
														$descripcion_evento = $es_actual["descripcion_evento"];
														$fecha_inicio_evento = $es_actual["fecha_inicio_evento"];
														$fecha_fin_evento = $es_actual["fecha_fin_evento"];

														require_once "../xml/Operaciones.php";
														$ope = new Operaciones();
														$res_registro_evento = $ope->registrar_evento_significativo($token,$cod_ambiente,$cod_evento,$cod_punto_venta,$cod_sistema,$cod_sucursal,$cufd_actual,$cufd_evento,$cuis,$descripcion_evento,$fecha_inicio_evento,$fecha_fin_evento,$nit);
														if(is_object($res_registro_evento)){
															if($res_registro_evento->transaccion){
																$cod_recepcion_es = $res_registro_evento->codigoRecepcionEventoSignificativo;
																//UNA VEZ SE TENGA EL CÓDIGO DE RECEPCIÓN SE INVOCA AL MÉTODO PARA ENVIAR EL PAQUETE
																$vector_datos = [
																	"codigoAmbiente"=>$cod_ambiente,
																	"codigoDocumentoSector"=>$cod_doc_sector,
																	"codigoEmision"=>$cod_emision,
																	"codigoModalidad"=>$cod_modalidad,
																	"codigoPuntoVenta"=>$cod_punto_venta,
																	"codigoSistema"=>$cod_sistema,
																	"codigoSucursal"=>$cod_sucursal,
																	"cufd"=>$cufd_actual,
																	"cuis"=>$cuis,
																	"nit"=>$nit,
																	"tipoFacturaDocumento"=>$tipo_factura,
																	"cafc"=>$cafc_evento,
																	"cantidadFacturas"=>$cant_facturas,
																	"codigoEvento"=>$cod_evento,
																	"codigoRecepcion"=>$cod_recepcion_es
																];
															
																//FECHA Y HORA ENVÍO PAQUETE
																$fecha_hora = date("Y-m-d")."T".date("H:i:s.v");
																require "../xml/crear_xml/CrearXML.php";
																$recep_pqt = new CrearXML();
																$res_recep_pqt = $recep_pqt->recepcion_paquete_facturas($token,$vector_datos,$array_nombres_xml,$ruta,$nombre_carpeta,$fecha_hora);
																if(is_object($res_recep_pqt)){
																	$res = $res_recep_pqt->RespuestaServicioFacturacion;
																	if($res->transaccion){
																		if(count($array_nombres_xml) != 0){
																			//EL CÓDIGO DE RECEPCIÓN DEL PAQUETE Y DEL EVENTO DEBEN GUARDARSE EN LA BASE DE DATOS
																			$cod_recepcion = $res->codigoRecepcion;
																			$res_actl_pqt = $es->setColumnaPaquete("codigo_recepcion_paquete",$cod_recepcion,$paquete["id_paquete"]);
																			$res_actl_pqt_est = $es->setColumnaPaquete("estado_paquete",2,$paquete["id_paquete"]);//PENDIENTE
																			$res_actl_pqt_intento = $es->setColumnaPaquete("intento",1,$paquete["id_paquete"]); //SE ACTUALIZA EL INTENTO EN EL CUAL SE ENVÍA TODO EL PAQUETE
																			$res_actl_es = $es->setColumna("codigo_recepcion_evento_significativo",$cod_recepcion_es,$es_actual["id_evento_significativo"]);
																			$res_actl_es_est = $es->setColumna("estado_evento",3,$es_actual["id_evento_significativo"]);//PARA VALIDACIÓN
																			if($res_actl_pqt && $res_actl_es && $res_actl_pqt_est && $res_actl_es_est){
																				if($res->codigoEstado == "901"){
																					$mensaje = "(".$res->codigoDescripcion.") Paquete de facturas pendiente de revisión en SIN";
																					$estado = 1;
																				}else{ //NO TENDRIÁ QU INGRESAR AQUÍ
																					$mensaje = "-- ENVIÓ DE PAQUETE --<br>Hubo un problema con el paquete o evento registrado";
																				}
																			}else
																				$mensaje = "No se pudo guardar alguno de los códigos de recepción en la BASE DE DATOS";
																		}else
																			$mensaje = "Error";
																	}else{
																		$mensaje = "-- ENVIÓ DE PAQUETE --<br>";
																		if(is_array($res->mensajesList)){
																			for($j = 0; $j < count($res->mensajesList); $j++)
																				$mensaje .= ($j+1). ". (Error ".$res->mensajesList[$j]->codigo .") ".$res->mensajesList[$j]->descripcion . "<br>"; 
																		}else
																			$mensaje .= "(Error ".$res->mensajesList->codigo .") ".$res->mensajesList->descripcion;
																	}
																}else
																	$mensaje = "Error en enviar paquete a SIN";															
															}else{
																$mensaje = "-- REGISTRO EVENTO --<br>";
																if(is_array($res_registro_evento->mensajesList)){
																	for($j = 0; $j < count($res_registro_evento->mensajesList); $j++)
																		$mensaje .= ($j+1). ". (Error ".$res_registro_evento->mensajesList[$j]->codigo .") ".$res_registro_evento->mensajesList[$j]->descripcion . "<br>"; 
																}else
																	$mensaje .= "(Error ".$res_registro_evento->mensajesList->codigo .") ".$res_registro_evento->mensajesList->descripcion;
															}
														}else
															$mensaje = "No se pudo registrar el evento en SIN";
													}else
														$mensaje = "No se pudo almacenar el CUFD nuevo en la BB.DD.";
												}else
													$mensaje = "Hubo un problema para obtener un nuevo CUFD";
											}else
												$mensaje = "No se pudo obtener un nuevo CUFD";
										}else
											$mensaje = "Cufd de evento no encontrado";
									}
								}catch(Exception $e){
									$mensaje = "Error al obtener archivos XML";
								}
							}else
								$mensaje = "Paquete no encontrado";
						}else
							$mensaje = "Punto de venta no encontrado";	
					}else
						$mensaje = "Evento significativo no encontrado";
		
					$resp = ["mensaje"=>$mensaje,"estado"=>$estado];
					echo json_encode($resp);
					
				break;

				case 'reenviar_paquetes':
					$mensaje = "";
					$estado = 0;
					if(isset($id_evento_significativo)){
						//SE DEBE RECUPERAR EL EVENTO QUE SE QUIERE REENVIAR
						$id_es = $id_evento_significativo;
						require_once "../modelos/EventoSignificativo.php";
						$es = new EventoSignificativo();
						$es_actual = $es->obtener_ultimo_evento2($id_es);
						
						if($es_actual){
							//OBTENER PUNTO DE VENTA
							require_once "../modelos/PuntoVenta.php";
							$pv = new PuntoVenta();
							$pv_actual = $pv->getPuntoVenta2($es_actual["id_punto_venta"]);
							if($pv_actual){
								//OBTENER PAQUETE
								$paquete = $es->obtener_ultimo_paquete_es($id_es);
								if($paquete){
									//OBTENER CARPETA DONDE SE ENCUENTRAN LOS XML's
									$nombre_carpeta = $paquete["nombre_paquete"];
								
									$ruta = "../xml_qr/paquetes_contingencia/".$nombre_carpeta;
									$intento = $paquete["intento"];//intento actual para envío de paquetes
									//GUARDAR EN UN ARRAY EL NOMBRE DE LOS ARCHIVOS XML VERIFICANDO PRIMERO SI LA RUTA ES VÁLIDA
									$array_nombres_xml = [];
									try{
										// Abrir la carpeta que se pasa como parámetro
										if(!is_dir($ruta))
											$mensaje = "No se pudo encontrar el paquete";
										else{
											$dir = opendir($ruta);
											
											// Almacenar solo los archivos que se encuentren observados (se entiende que al menos debe haber un archivo observado para realizar esta acción)
											//se tiene que recuperar la venta en base al nombre de archivo que se tiene, excluyendo su extensión
											//si luego de ver que la venta tiene observaciones, entonces se la incluye, caso contrario no
											require_once "../modelos/Venta.php";
											$venta = new Venta();
											
											

											while ($elemento = readdir($dir)){
												// Tratamos los elementos . y .. que tiene la carpeta
												
												if( $elemento != "." && $elemento != ".."){
													// Si NO es una carpeta
													if( !is_dir($ruta.$elemento) ){
														// obtener el nombre del archivo si tiene extensión .xml
														if(strpos($elemento,".xml")){
															$caracteres_quitar = array(".xml");
															$aux_elemento = str_replace($caracteres_quitar, "", $elemento);
															$venta_obs = $venta->mostrar2($aux_elemento);
															if($venta_obs["estado"] == "Pendiente Correccion" || $venta_obs["estado"] == "Pendiente")
																array_push($array_nombres_xml,$elemento);
														}
													} 
												}
											}

											//ORDENA ARRAY DE NOMBRES EN BASE AL ORDEN DE CREACIÓN (NOMBRE)
											sort($array_nombres_xml);
											
											//OBTENER INFORMACIÓN GENERAL DEL SISTEMA PARA ENVIAR  
											require_once "../modelos/Informacion.php";
											$cuis_actual = $pv->buscar_cuis_activo($pv_actual["id_punto_venta"]);
											$token = TOKEN;

											$cod_ambiente = Informacion::mostrar2()["codigoAmbiente"];
											$cod_modalidad = Informacion::mostrar2()["codigoModalidad"];
											$cod_sistema = Informacion::mostrar2()["codigoSistema"];
											$cod_sucursal = Informacion::mostrar2()["codigoSucursal"];
											$nit = Informacion::mostrar2()["nit"];
											$cuis = $cuis_actual["codigo_cuis"];
											$cod_punto_venta = $pv_actual["codigoPuntoVenta"];

											$cod_doc_sector = 1;//FACTURA COMPRA VENTA
											$cod_emision = 2;//OFFLINE
											$tipo_factura = 1;//FACTURA CON DERECHO A CREDITO FISCAL
											$cafc_evento = $es_actual["cafc_evento"];
											$cant_facturas = count($array_nombres_xml);//cantidad de facturas
											$cod_evento = $es_actual["codigo_evento"];



											require_once "../xml/Codigos.php";
											$codigos = new Codigos();
											//GENERAR CUFD
											$obj_nuevo_cufd = $codigos->generarCufd2($cod_punto_venta,$cod_ambiente,$cod_modalidad,$cod_sistema,$cod_sucursal,$cuis,$nit,$token);
											if(is_object($obj_nuevo_cufd)){
												//SI LA TRANSACCION ES CORRECTA, SE ALMACENA EN LA BB.DD, Y SE UTILIZA EL CUFD NUEVO PARA LAS SIGUIENTES ACCIONES
												if($obj_nuevo_cufd->transaccion){
													$cufd_actual = $obj_nuevo_cufd->codigo;
													$cod_control_actual = $obj_nuevo_cufd->codigoControl;
													$direccion_actual = $obj_nuevo_cufd->direccion;
													$fecha_vigencia_actual = $obj_nuevo_cufd->fechaVigencia;
													$res_cufd_nuevo = $pv->registrar_cufd($cufd_actual,$cod_control_actual,$direccion_actual,$fecha_vigencia_actual,$cod_punto_venta);
													if($res_cufd_nuevo){
														$cod_recepcion_es = $es_actual["codigo_recepcion_evento_significativo"];
														$vector_datos = [
															"codigoAmbiente"=>$cod_ambiente,
															"codigoDocumentoSector"=>$cod_doc_sector,
															"codigoEmision"=>$cod_emision,
															"codigoModalidad"=>$cod_modalidad,
															"codigoPuntoVenta"=>$cod_punto_venta,
															"codigoSistema"=>$cod_sistema,
															"codigoSucursal"=>$cod_sucursal,
															"cufd"=>$cufd_actual,
															"cuis"=>$cuis,
															"nit"=>$nit,
															"tipoFacturaDocumento"=>$tipo_factura,
															"cafc"=>$cafc_evento,
															"cantidadFacturas"=>$cant_facturas,
															"codigoEvento"=>$cod_evento,
															"codigoRecepcion"=>$cod_recepcion_es
														];
													
														//FECHA Y HORA ENVÍO PAQUETE
														$fecha_hora = date("Y-m-d")."T".date("H:i:s.v");
														require "../xml/crear_xml/CrearXML.php";
														$recep_pqt = new CrearXML();
														$res_recep_pqt = $recep_pqt->recepcion_paquete_facturas($token,$vector_datos,$array_nombres_xml,$ruta,$nombre_carpeta,$fecha_hora,$intento);
														if(is_object($res_recep_pqt)){
															$res = $res_recep_pqt->RespuestaServicioFacturacion;
															if($res->transaccion){
																if(count($array_nombres_xml) != 0){
																	//EL CÓDIGO DE RECEPCIÓN DEL PAQUETE Y DEL EVENTO DEBEN GUARDARSE EN LA BASE DE DATOS
																	$intento++;
																	$cod_recepcion = $res->codigoRecepcion;
																	$res_actl_pqt = $es->setColumnaPaquete("codigo_recepcion_paquete",$cod_recepcion,$paquete["id_paquete"]);
																	$res_actl_pqt_est = $es->setColumnaPaquete("estado_paquete",2,$paquete["id_paquete"]);//PENDIENTE
																	$res_actl_pqt_intento = $es->setColumnaPaquete("intento",$intento,$paquete["id_paquete"]); //SE ACTUALIZA EL INTENTO EN EL CUAL SE ENVÍA TODO EL PAQUETE
																	$res_actl_es = $es->setColumna("codigo_recepcion_evento_significativo",$cod_recepcion_es,$es_actual["id_evento_significativo"]);
																	$res_actl_es_est = $es->setColumna("estado_evento",3,$es_actual["id_evento_significativo"]);//PARA VALIDACIÓN
																	if($res_actl_pqt && $res_actl_es && $res_actl_pqt_est && $res_actl_es_est){
																		if($res->codigoEstado == "901"){
																			$mensaje = "(".$res->codigoDescripcion.") Paquete de facturas pendiente de revisión en SIN";
																			$estado = 1;
																		}else{ //NO TENDRIÁ QU INGRESAR AQUÍ
																			$mensaje = "-- ENVIÓ DE PAQUETE --<br>Hubo un problema con el paquete o evento registrado";
																		}
																	}else
																		$mensaje = "No se pudo guardar alguno de los códigos de recepción en la BASE DE DATOS";
																}else
																	$mensaje = "Error";
															}else{
																$mensaje = "-- ENVIÓ DE PAQUETE --<br>";
																if(is_array($res->mensajesList)){
																	for($j = 0; $j < count($res->mensajesList); $j++)
																		$mensaje .= ($j+1). ". (Error ".$res->mensajesList[$j]->codigo .") ".$res->mensajesList[$j]->descripcion . "<br>"; 
																}else
																	$mensaje .= "(Error ".$res->mensajesList->codigo .") ".$res->mensajesList->descripcion;
															}
														}else
															$mensaje = "Error en enviar paquete a SIN";															
															
													}else
														$mensaje = "No se pudo almacenar el CUFD nuevo en la BB.DD.";
												}else
													$mensaje = "Hubo un problema para obtener un nuevo CUFD";
											}else
												$mensaje = "No se pudo obtener un nuevo CUFD";
											
										}
									}catch(Exception $e){
										$mensaje = "Error al obtener archivos XML";
									}

								}else
									$mensaje = "No se encontró un paquete válido";
							}else
								$mensaje = "No se encontró un evento significativo válido";
						}
					}else
						$mensaje = "No se encontró un id de evento significativo válido";

					echo json_encode(["mensaje" => $mensaje, "estado" => $estado]);
				break;

				case 'validar_paquetes':
					//OBTENER DATOS EVENTO SIGNIFICATIVO
					require_once "../modelos/EventoSignificativo.php";
					$es = new EventoSignificativo();
					$es_actual = $es->obtener_ultimo_evento2($id_evento_significativo);
					$mensaje = "";
					$estado = 0;
					if($es_actual){
						//OBTENER PUNTO DE VENTA
						require_once "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();
						$pv_actual = $pv->getPuntoVenta2($es_actual["id_punto_venta"]);

						if($pv_actual){
							//OBTENER PAQUETE
							$paquete = $es->obtener_ultimo_paquete_es($id_evento_significativo);
							if($paquete){
								//OBTENER EL CÓDIGO DE RECEPCIÓN DEL PAQUETE
								$codigo_recepcion_paquete = $paquete["codigo_recepcion_paquete"];
								//OBTENER EL NOMBRE DEL PAQUETE
								$nombre_carpeta = $paquete["nombre_paquete"];
								$intento = $paquete["intento"];//intento actual para envío de paquetes
									
								//OBTENER INFORMACIÓN GENERAL DEL SISTEMA PARA ENVIAR  
								require_once "../modelos/Informacion.php";
								$cuis_actual = $pv->buscar_cuis_activo($pv_actual["id_punto_venta"]);
								$cufd_actual = $pv->buscar_cufd_activo($pv_actual["id_punto_venta"]);

								$token = TOKEN;

								$cod_ambiente = Informacion::mostrar2()["codigoAmbiente"];
								$cod_modalidad = Informacion::mostrar2()["codigoModalidad"];
								$cod_sistema = Informacion::mostrar2()["codigoSistema"];
								$cod_sucursal = Informacion::mostrar2()["codigoSucursal"];
								$nit = Informacion::mostrar2()["nit"];
								$cuis = $cuis_actual["codigo_cuis"];
								$cufd = $cufd_actual["codigo_cufd"];
								$cod_punto_venta = $pv_actual["codigoPuntoVenta"];

								$cod_doc_sector = 1;//FACTURA COMPRA VENTA
								$cod_emision = 2;//OFFLINE
								$tipo_factura = 1;//FACTURA CON DERECHO A CREDITO FISCAL
								//$cod_evento = $es_actual["codigo_evento"];

								//UNA VEZ SE TENGA EL CÓDIGO DE RECEPCIÓN SE INVOCA AL MÉTODO PARA VALIDAR EL PAQUETE
								$vector_datos = [
									"codigoAmbiente"=>$cod_ambiente,
									"codigoDocumentoSector"=>$cod_doc_sector,
									"codigoEmision"=>$cod_emision,
									"codigoModalidad"=>$cod_modalidad,
									"codigoPuntoVenta"=>$cod_punto_venta,
									"codigoSistema"=>$cod_sistema,
									"codigoSucursal"=>$cod_sucursal,
									"cufd"=>$cufd,
									"cuis"=>$cuis,
									"nit"=>$nit,
									"tipoFacturaDocumento"=>$tipo_factura,
									"codigoRecepcion"=>$codigo_recepcion_paquete
								];
							
								//FECHA Y HORA ENVÍO PAQUETE
								$fecha_hora = date("Y-m-d")."T".date("H:i:s.v");

								require "../xml/crear_xml/CrearXML.php";
								$recep_pqt = new CrearXML();
								$res_val_pqt = $recep_pqt->validar_recepcion_paquete_facturas($token,$vector_datos,$fecha_hora);

								if(is_object($res_val_pqt)){
									//var_dump($res_val_pqt);
									//return;
									$res = $res_val_pqt->RespuestaServicioFacturacion;
									if($res->transaccion){
										require_once "../modelos/Venta.php";
										$venta = new Venta();
										//OBTENER TODAS LAS VENTAS EN BASE AL NOMBRE DEL PAQUETE
										$ventas_paquete = $venta->mostrar3($nombre_carpeta);
										
										/*
											CÓDIGOS PARA EVENTO
												0 = GENERADO
												1 = FINALIZADO
												2 = PEDIENTE REGISTRO EVENTO Y ENVÍO PAQUETE
												3 = PENDIENTE VALIDACIÓN
												4 = PENDIENTE REENVÍO PAQUETE
										
											CÓDIGOS PAQUETE
												0 = CREADO
												1 = FINALIZADO
												2 = PENDIENTE
										
											SI EL CÓDIGO DE ESTADO ES 908... 
												1. TODAS LAS VENTAS SE PONEN COMO ESTADO ACEPTADO Y SE PONE EL CÓDIGO DE RECEPCIÓN EN LA VENTA
												2. EL ESTADO DEL PAQUETE Y DEL EVENTO SE CAMBIAN A 1 EN AMBOS CASOS
												3. SE ENVIAN POR CORREO (SI ES QUE TIENEN) A TODOS LOS CLIENTES A LOS QUE SE LES HAYA VENDIDO
										
											SI EL CÓDIGO DE ESTADO ES 904 (OBSERVADO)...
												1. SE VERIFICA EN BASE A LA LISTA DE OBSERVACIONES DE CADA ARCHIVO Y SE MANTIENE COMO PENDIENTE DE ARREGLO
												2. LOS QUE NO TIENEN OBSERVACIONES SE CAMBIAN A ACEPTADO Y SE PONE EL CÓDIGO DE RECEPCIÓN EN LA VENTA
												3. ES ESTADO DEL PAQUETE SE MANTIENE EN 2, Y EL ESTADO DEL EVENTO CAMBIA A 4 (REENVIAR)
												4. SOLO SE ENVÍA POR CORREO (SI ES QUE TIENEN) A AQUELLOS CLIENTES CUYA VENTA SE ENCUENTRE CORRECTAMENTE VALIDADA
										*/
										if($res->codigoEstado == 908){
											$sw = true;
											while($vq = $ventas_paquete->fetch_object()){
												$act_cr = $venta->setColumnaVenta("codigo_recepcion",$res->codigoRecepcion,$vq->idventa);
												$act_estado = $venta->setColumnaVenta("estado","Aceptado",$vq->idventa);
												//OBTENER EL CORREO DEL CLIENTE (SI TUVIESE)

												$num_venta = "";
												if($vq->nro_venta != "" && $vq->nro_venta != "0")
													$num_venta = $vq->nro_venta;
												else{
													if($vq->nro_venta_contingencia != "" && $vq->nro_venta_contingencia != "0")
														$num_venta = $vq->nro_venta_contingencia;
												}

												if($vq->email != ""){
													//OBTENER ÚLTIMA VENTA													
													$resp_correo = $venta->enviar_correo($vq->idventa,$vq->email);
													if($resp_correo)
														$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Venta Nro. $num_venta: Correo enviado correctamente<br>";
													else
														$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Venta Nro. $num_venta: No se pudo enviar el correo<br>";
												}

												if(!$act_cr || !$act_estado){
													$sw = false;
													$mensaje .= "Venta " . $vq->idventa . " no se pudo actualizar<br>";
												}
											}
											if($sw){
												$act_estado_es = $es->setColumna("estado_evento",1,$es_actual["id_evento_significativo"]);
												$act_estado_pqt = $es->setColumnaPaquete("estado_paquete",1,$paquete["id_paquete"]);
												if(!$act_estado_es || !$act_estado_pqt)
													$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Estado de evento y/o paquete no se actualizaron correctamente<br>";
												else{
													$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Validación de paquete realizada correctamente";
													$estado = 1;
												}
											}
										}else{
											if($res->codigoEstado == 904){
												$mensaje = "*** Hubo errores en algunas de las facturas validadas del paquete que debe corregir ***<br>";
												
												$array_errores = $res->mensajesList;
												
											
												$sw = true;
												$i = 0;
												$sw1 = true; //para ver estado del paquete
												//RECORRER TODAS LAS VENTAS DEL PAQUETE
												while($vq = $ventas_paquete->fetch_object()){
													$sw = true;//si sw se mantiene true quiere decir que la venta es correcta

													//Verificar si es venta normal o de contingencia
													$nro_venta = "";
													if($vq->nro_venta != "" && $vq->nro_venta != 0 && $vq->nro_venta != null)
														$nro_venta = $vq->nro_venta;
													else
														$nro_venta = $vq->nro_venta_contingencia;
													
													$mensaje_venta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Errores factura Nro. " . $nro_venta . ": <br>";//mensaje a concatenar en caso de hallar error(es) para la venta
													//se recorre el array de errores por cada venta para verificar si tiene errores
													$k = 1; //nro de errores por factura
													//si se tiene más de una observación se lo considera como array
													if(is_array($array_errores)){
														for($j = 0; $j < count($array_errores); $j++){
															//si existe coincidencia con el numeroArchivo
															if($array_errores[$j]->numeroArchivo == $i){
																$sw = false;
																$mensaje_venta .= $k . ". (Error " .  $array_errores[$j]->codigo . ") " . $array_errores[$j]->descripcion . "<br>";
																$k++;
															}
														}
													}else{
														if($array_errores->numeroArchivo == $i){
															$mensaje_venta .= "(Error " .  $array_errores->codigo . ") " . $array_errores->descripcion . "<br>";
															$sw = false;
														}
													}

													if(!$sw){
														$mensaje .= $mensaje_venta;
														//actualizar venta poniendo código de recepción y cambiando estado a "Pendiente Correccion"
														$act_cr = $venta->setColumnaVenta("codigo_recepcion",$res->codigoRecepcion,$vq->idventa);
														$act_estado = $venta->setColumnaVenta("estado","Pendiente Correccion",$vq->idventa);
														if(!$act_cr || !$act_estado){
															$sw1 = false;
															$mensaje .= "Venta " . $nro_venta . " no se pudo actualizar<br>";
														}
													}else{
														//si la venta no tiene errores, igual se actualiza poniendo código de recepción y cambiando esta a "Aceptado"
														$act_cr = $venta->setColumnaVenta("codigo_recepcion",$res->codigoRecepcion,$vq->idventa);
														$act_estado = $venta->setColumnaVenta("estado","Aceptado",$vq->idventa);
														if(!$act_cr || !$act_estado){
															$sw1 = false;
															$mensaje .= "Venta " . $nro_venta . " no se pudo actualizar<br>";
														}
													}
													$mensaje_venta = "";

													$i++;
												}

												if($sw1){
													$act_estado_es = $es->setColumna("estado_evento",4,$es_actual["id_evento_significativo"]);
													$act_estado_pqt = $es->setColumnaPaquete("estado_paquete",2,$paquete["id_paquete"]);
													if(!$act_estado_es || !$act_estado_pqt)
														$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Estado de evento y/o paquete no se actualizaron correctamente<br>";
													
												}
												
											}else
												$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Hubo un problema en la verificación de SIN";
										}
							
									}else{
										
										$mensaje = "-- VALIDACIÓN DE PAQUETE --<br>";
										if(is_array($res->mensajesList)){
											for($j = 0; $j < count($res->mensajesList); $j++)
												$mensaje .= ($j+1). ". (Error ".$res->mensajesList[$j]->codigo .") ".$res->mensajesList[$j]->descripcion . "<br>"; 
										}else
											$mensaje .= "(Error ".$res->mensajesList->codigo .") ".$res->mensajesList->descripcion;
									}
									
								}else
									$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error en validar paquete a SIN";															
							}else
								$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Paquete no encontrado";
						}else
							$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Punto de venta no encontrado";	
					}else
						$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Evento significativo no encontrado";
		
					$resp = ["mensaje"=>$mensaje,"estado"=>$estado];
					echo json_encode($resp);
				break;
				
			}
			//Fin de las validaciones de acceso
		}else
			require 'noacceso.php';
	}
	ob_end_flush();