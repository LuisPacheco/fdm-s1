<?php 

ob_start();

if (strlen(session_id()) < 1){

	session_start();//Validamos si existe o no la sesión

}

if (!isset($_SESSION["nombre"])){
  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
}

else{

	//Validamos el acceso solo al usuario logueado y autorizado.

	if ($_SESSION['eventos_significativos']==1){ 

		$id_punto_venta = $_SESSION["id_punto_venta"];

		require_once "../xml/SincronizacionDatos.php";

		$sinc_datos = new SincronizacionDatos();

		switch ($_GET["op"]){

			case "guardar":
				$rspta = $sinc_datos->generar_sinc(1);
				if($rspta)
					echo "Sincronización generada exitosamente";
				else
					echo "No se pudo generar la sincronización";
			break;

			case 'hora_fecha':
				$rspta=$sinc_datos->obtenerCatalogoX("sincronizarParametricaTipoPuntoVenta");
				echo $rspta;
			break;

			case 'listar':
				$rspta = $sinc_datos->listar();

				//Vamos a declarar un array

				$data = Array();

				$i = 0;

				while ($reg = $rspta->fetch_object()){

					$boton = "";
					if($i == 0)
						$boton = '<button class="btn btn-primary" type="button" title="sincronizar hora" onclick="sincronizar_hora()"><i class="fa fa-clock-o" aria-hidden="true"></i></button> <button class="btn btn-danger" type="button" title="actualizar catálogo" onclick="actualizar_sincronizacion()"><i class="fa fa-refresh" aria-hidden="true"></i></button>';
					
					$data[] = array(

						"0"=>$boton,
				
						"1"=>$reg->fecha_sincronizacion,

						"2"=>$reg->fecha_hora_sincronizacion

					);

					$i++;

				}

				$results = array(

					"sEcho"=>1, //Información para el datatables

					"iTotalRecords"=>count($data), //enviamos el total registros al datatable

					"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

					"aaData"=>$data);

				echo json_encode($results);

			break;

			case 'hay_sincronizacion':
				$rspta = $sinc_datos->hay_sincronizacion();
				if($rspta->num_rows != 0)
					echo "si";
				else
					echo "no";
			break;

			case 'sincronizar_hora':
				$rspta = $sinc_datos->sincronizar_hora($id_punto_venta);
				if($rspta)
					echo "Sincronización de hora exitosamente";
				else
					echo "No se pudo realizar la sincronización";
			break;

			case 'actualizar_sincronizacion':
				$rspta = $sinc_datos->actualizar_sincronizacion($id_punto_venta);
				if($rspta)
					echo "Sincronización actualizada exitosamente";
				else
					echo "No se pudo actualizar la sincronización";
			break;
		}

	//Fin de las validaciones de acceso

	}

	else
		require 'noacceso.php';
	

}

ob_end_flush();