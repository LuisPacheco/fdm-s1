<?php 

ob_start();

if (strlen(session_id()) < 1){

	session_start();//Validamos si existe o no la sesión

}

if (!isset($_SESSION["nombre"]))

{

  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

}

else

{

//Validamos el acceso solo al usuario logueado y autorizado.

if ($_SESSION['almacen']==1)

{

require_once "../modelos/Laboratorio.php";



$laboratorio=new Laboratorio();



$idlaboratorio=isset($_POST["idlaboratorio"])? limpiarCadena($_POST["idlaboratorio"]):"";

$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";

$descripcion=isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";



switch ($_GET["op"]){

	case 'mostrar':

		$rspta=$laboratorio->mostrar($idlaboratorio);

 		//Codificar el resultado utilizando json

 		echo json_encode($rspta);

	break;



	case 'listar':

		$rspta=$laboratorio->listar();

 		//Vamos a declarar un array

 		$data= Array();



 		while ($reg=$rspta->fetch_object()){

 			$data[]=array(

 				"0"=>'<button class="btn btn-info" onclick="mostrar('.$reg->idlaboratorio.')"><i class="fa fa-eye"></i></button>',

 				"1"=>$reg->nombre,

 				"2"=>$reg->descripcion,

 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':

 				'<span class="label bg-red">Desactivado</span>'

 				);

 		}

 		$results = array(

 			"sEcho"=>1, //Información para el datatables

 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 			"aaData"=>$data);

 		echo json_encode($results);



	break;

}

//Fin de las validaciones de acceso

}

else

{

  require 'noacceso.php';

}

}

ob_end_flush();

?>