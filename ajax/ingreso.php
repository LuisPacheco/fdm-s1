<?php 

	ob_start();

	if (strlen(session_id()) < 1)

		session_start();//Validamos si existe o no la sesión

	if (!isset($_SESSION["nombre"]))

	  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

	else{

		//Validamos el acceso solo al usuario logueado y autorizado.

		if ($_SESSION['compras'] == 1){

			require_once "../modelos/Ingreso.php";



			$ingreso = new Ingreso();

			date_default_timezone_set("America/La_Paz");

			$idingreso=isset($_POST["idingreso"])? limpiarCadena($_POST["idingreso"]):"";

			$idproveedor=isset($_POST["idproveedor"])? limpiarCadena($_POST["idproveedor"]):"";

			$idusuario=$_SESSION["idusuario"];

			$tipo_comprobante=isset($_POST["tipo_comprobante"])? limpiarCadena($_POST["tipo_comprobante"]):"";

			$num_comprobante=isset($_POST["num_comprobante"])? limpiarCadena($_POST["num_comprobante"]):"";

			$fecha_hora=isset($_POST["fecha_hora"])? date("Y") . "-" . date("m") . "-" . date("d") . " " . date("H") . ":" . date("i") . ":" . date("s"):"";

			$impuesto=isset($_POST["impuesto"])? limpiarCadena($_POST["impuesto"]):"";

			$total_compra=isset($_POST["total_compra"])? limpiarCadena($_POST["total_compra"]):"";

			switch ($_GET["op"]){

				case 'guardaryeditar':
					if (empty($idingreso)){
						if(isset($_POST["idarticulo"]) && isset($_POST["cantidad"]) && isset($_POST["precio_compra"]) && isset($_POST["precio_venta"]) && isset($_POST["cod_lote"]) && isset($_POST["idlote"]) && isset($_POST["vencimiento"])){
							if(count($_POST["idarticulo"]) > 0 && count($_POST["cantidad"]) > 0 && count($_POST["precio_compra"]) > 0 && count($_POST["precio_venta"]) > 0 && count($_POST["cod_lote"]) > 0 && count($_POST["idlote"]) > 0 && count($_POST["vencimiento"]) > 0){

								$idarticulo = $_POST["idarticulo"];

								$cantidad = $_POST["cantidad"];

								$precio_compra = $_POST["precio_compra"];

								$precio_venta = $_POST["precio_venta"];

								$cod_lote = $_POST["cod_lote"];

								$vencimiento = $_POST["vencimiento"];

								$idlote = $_POST["idlote"];

								$rspta = $ingreso->insertar($idproveedor,$idusuario,$tipo_comprobante,$num_comprobante,$fecha_hora,$impuesto,$total_compra,$idarticulo,$cantidad,$precio_compra,$precio_venta,$cod_lote,$vencimiento,$idlote);

								if ($rspta) 

									echo "Ingreso y Lote registrados correctamentes";

								else

									echo "No se pudieron registrar todos los datos del ingreso";



							}else{

								$errores = "";

								if(count($_POST["idarticulo"]) <= 0)

									$errores .= "Artículo vacío<br>";

								if(count($_POST["cantidad"]) <= 0)

									$errores .= "Cantidad vacía<br>";

								if(count($_POST["precio_compra"]) <= 0)

									$errores .= "Precio de compra vacío<br>";

								if(count($_POST["precio_venta"]) <= 0)

									$errores .= "Precio de venta vacío<br>";

								if(count($_POST["cod_lote"]) <= 0)

									$errores .= "Cod Lote vacío<br>";

								if(count($_POST["vencimiento"]) <= 0)

									$errores .= "Vencimiento vacío<br>";

								if(count($_POST["idlote"]) <= 0)

									$errores .= "Lote vacío<br>";

								$errores .= "<b>No se efectuará el ingreso</b>";

									echo $errores;

							}

						}else{

							$errores = "";

							if(!isset($_POST["idarticulo"]))

								$errores .= "Error en registrar artículo<br>";

							if(!isset($_POST["cantidad"]))

								$errores .= "Error en registrar cantidad<br>";

							if(!isset($_POST["precio_compra"]))

								$errores .= "Error en registrar precio de compra<br>";

							if(!isset($_POST_["precio_venta"]))

								$errores .= "Error en registrar precio de venta<br>";

							if(!isset($_POST["cod_lote"]))

								$errores .= "Error en registrar cod lote<br>";

							if(!isset($_POST["vencimiento"]))

								$errores .= "Error en registrar vencimiento<br>";

							if(!isset($_POST["idlote"]))

								$errores .= "Error en registrar lote";

							$errores .= "<b>No se efectuará el ingreso</b>";

								echo $errores;

						}

						//var_dump($_POST);

						

						



						

							

					}else

						echo "Error al registrar ingreso";

				break;



				case 'anular':

					$rspta=$ingreso->anular($idingreso);

					$rspta2 = $ingreso->listarDetalle($idingreso);

					require_once "../modelos/Lote.php";

					$lote = new Lote();

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();

					$sw = true;

					while ($reg = $rspta2->fetch_object()){

						$sql_stock_lote = $lote->getStock($reg->idlote);

						$stock_lote = $sql_stock_lote->fetch_object();



						$sql_estado_lote = $lote->mostrarEstado($reg->idlote);

						$estado_lote = $sql_estado_lote->fetch_object();



						$dev_stock_articulo = $articulo->devolverStock($reg->idarticulo);

						$stock_art = $dev_stock_articulo->fetch_object();



						if($estado_lote->estado != "Devuelto" && $estado_lote->estado != "Agotado"){

							$dif = $stock_art->stock - $stock_lote->stock;

							$res_stock = $articulo->modificarStock($reg->idarticulo,$dif);

							$res_estado = $lote->modificarEstado("Devuelto",$reg->idlote);

							if (!$res_estado || !$res_stock) 

								$sw = false;

						}

					}

		 			echo $rspta && $sw && $rspta2 ? "Ingreso anulado" : "Ingreso no se puede anular";

				break;						



				case 'mostrar':

					$rspta=$ingreso->mostrar($idingreso);

		 			//Codificar el resultado utilizando json

		 			echo json_encode($rspta);

				break;



				case 'listarDetalle':

					//Recibimos el idingreso

					$id = $_GET['id'];



					$rspta = $ingreso->listarDetalle($id);
					
					
					$data= Array();



					$i = 1;



					$total = 0;

			 		while ($reg=$rspta->fetch_object()){
			 		    
			 		    $vencimiento = "";
			 		    
			 		    if($reg->vencimiento == "" || $reg->vencimiento == "0000-00-00" || $reg->vencimiento == NULL || $reg->vencimiento == "0/0")
			 		        $vencimiento = "n/a";
			 		    else
			 		        $vencimiento = $reg->vencimiento;

			 			$data[] = array(

			 				"0"=>$i,

			 				"1"=>$reg->nombre,

			 				"2"=>$reg->cantidad,

			 				"3"=>$reg->precio_compra,

			 				"4"=>$reg->precio_venta,

			 				"5"=>$reg->cod_lote,
			 				
			 				"6"=>$vencimiento,
			 				
			 				"7"=>$reg->precio_compra*$reg->cantidad

						);

						$i++;

						

						$total = $total + ($reg->precio_compra*$reg->cantidad);

			 		}
			 		
			 		$data[] = array(

						"0"=>"<strong>TOTAL</strong>",

					   	"1"=>"",

						"2"=>"",

						"3"=>"",

						"4"=>"",

						"5"=>"",
						
						"6"=>"",

						"7"=>'<h4 id="total">Bs. ' . $total . '</h4>'

				   );



			 		$results = array(

			 			"aaData"=>$data);

			 		echo json_encode($results);
					
				break;



				case 'listar':

					$rspta = $ingreso->listar();

		 			//Vamos a declarar un array

		 			$data = Array();

		 			while ($reg = $rspta->fetch_object()){

			 			$data[] = array(

							"0"=>'<button class="btn btn-warning" onclick="mostrar('.$reg->idingreso.')"><i class="fa fa-eye"></i></button>'.

							'<a target="_blank" href="../reportes/exIngreso.php?id='.$reg->idingreso.'"> <button class="btn btn-info"><i class="fa fa-file"></i></button></a>',

		 					/*"0"=>(($reg->estado == 'Aceptado')?'<button class="btn btn-warning" onclick="mostrar('.$reg->idingreso.')"><i class="fa fa-eye"></i></button>'.

			 					' <button class="btn btn-danger" onclick="anular('.$reg->idingreso.')"><i class="fa fa-close"></i></button>':

		 						'<button class="btn btn-warning" onclick="mostrar('.$reg->idingreso.')"><i class="fa fa-eye"></i></button>').

		 						'<a target="_blank" href="../reportes/exIngreso.php?id='.$reg->idingreso.'"> <button class="btn btn-info"><i class="fa fa-file"></i></button></a>',*/

		 					"1"=>$reg->fecha,

		 					"2"=>$reg->proveedor,

		 					"3"=>$reg->usuario,

		 					"4"=>$reg->tipo_comprobante,

		 					"5"=>$reg->num_comprobante,

		 					"6"=>$reg->total_compra,

		 					"7"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':

		 					'<span class="label bg-red">Anulado</span>'

		 				);

		 			}

		 			$results = array(

			 			"sEcho"=>1, //Información para el datatables

		 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

		 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

		 				"aaData"=>$data);

		 			echo json_encode($results);

				break;



				case 'selectProveedor':

					require_once "../modelos/Persona.php";

					$persona = new Persona();



					$rspta = $persona->listarP();



					echo '<option value='.'>-- Seleccione --</option>';

					while ($reg = $rspta->fetch_object())

						echo '<option value=' . $reg->idpersona . '>' . $reg->nombre . '</option>';	

				break;



				case 'selectArticulo':

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();



					$rspta = $articulo->listarArt();



					while ($reg = $rspta->fetch_object())

						echo '<option value=' . $reg->idarticulo . '>' . $reg->nombre_comercial . ' | ' . $reg->nombre_generico . ' | '. $reg->laboratorio .  '</option>';	

				break;



				case 'listarArticulos':

					require_once "../modelos/Articulo.php";

					

					$articulo = new Articulo();

					



					$rspta = $articulo->listarActivos();

		 			//Vamos a declarar un array

		 			$data = Array();

		 			$i = 0;

		 			while ($reg = $rspta->fetch_object()){

						

		 				$dato = "";

		 				//var_dump($reg);

		 				if ($reg->stock <= $reg->stock_minimo)

		 					$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";

		 				else 

			 				$dato = $reg->stock;

						$fecha_actual = date("Y-m-d");

		 				$data[] = array(

		 					"0"=>'<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre.'\',\''.$reg->idlaboratorio.'\',\''.$reg->precio_venta.'\',\''.$reg->cantidad.'\',\''.$reg->precio_compra.'\',\''.$fecha_actual.'\')"><span class="fa fa-plus"></span></button>',

		 					"1"=>$reg->cod_med,

		 					"2"=>$reg->nombre,

		 					"3"=>$reg->codigo,

		 					"4"=>$reg->categoria,

		 					"5"=>$reg->laboratorio,

		 					"6"=>$dato,

		 					"7"=>$reg->stock_minimo,

		 					"8"=>$reg->precio_venta,

		 					"9"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >"

		 				);

		 				$i++;

		 			}

		 			$results = array(

		 				"sEcho"=>1, //Información para el datatables

		 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

		 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

		 				"aaData"=>$data);

		 			echo json_encode($results);

				break;

				case "buscar_lotes":

					require_once "../modelos/Lote.php";

					$lote = new Lote();		

					$idarticulo = $_GET["idarticulo"];
					
					$cont = $_GET["cont"];

					$lotes = "<select class='form-control' name='idlote[]' id='idlote$cont' onclick='verif_lote(".$cont.")'><option value='nuevo' selected>--nuevo lote--</option>";

					$sql_listar_lotes = $lote->listar_lotes($idarticulo);

					$num_lotes = 0;

					while($rec = $sql_listar_lotes->fetch_object()){

						if($rec->estado == "Vencido" || $rec->estado == "Devuelto")
							$lotes .="<option value='".$rec->idlote."' disabled>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";
						else{
							if($rec->estado == "Agotado")
								$lotes .="<option value='".$rec->idlote."' style='font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";
							else{
								if($rec->vencimiento != "" && $rec->vencimiento != NULL && $rec->vencimiento != "0000-00-00"){
									date_default_timezone_set("America/La_Paz");
									$fecha_vencimiento = new DateTime($rec->vencimiento);
									$actual = date("Y-m-d");
									$fecha = new DateTime($actual);
									$res = $fecha->diff($fecha_vencimiento);
									$dias = $res->days;
									if($dias >= 180 && $res->invert == 0)//lote mayor a 180 dias
										$lotes .="<option value='".$rec->idlote."' style='color:green;font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";
									else{
										if ($dias > 90 && $dias < 180 && $res->invert == 0) //lote entre 90 a 180 dias
											$lotes .="<option value='".$rec->idlote."' style='color:orange;font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";
										else{
											if ($dias <= 90 && $res->invert == 0) //lote igual o menor a 90 dias
												$lotes .="<option value='".$rec->idlote."' style='color:red;font-weight:900'>".$rec->cod_lote." | " . $rec->vencimiento . " | " . $rec->estado . "</option>";
											else//lote vencido
												$lotes .="<option value='".$rec->idlote."' disabled>".$rec->cod_lote." - " . $rec->vencimiento . " - " . $rec->estado . "</option>";
										}
									}
								}else{//lote sin fecha de vencimiento
									$lotes .="<option value='".$rec->idlote."' style='color:blue;font-weight:900'>".$rec->cod_lote." - " . $rec->vencimiento . " - " . $rec->estado . "</option>";
								}
							}
						}
						$num_lotes++;
					}

					$lotes .= "</select>";

					echo $lotes;

				break;



				case "contar_lotes":

					require_once "../modelos/Lote.php";

					$lote = new Lote();		

					$idarticulo = $_GET["idarticulo"];

					$sql_listar_lotes = $lote->listar_lotes($idarticulo);

					echo $sql_listar_lotes->num_rows;

				break;

			}

		//Fin de las validaciones de acceso

		}

		else

	  		require 'noacceso.php';

	}

	ob_end_flush();