<?php
	date_default_timezone_set("America/La_Paz");
	ob_start();

	if (strlen(session_id()) < 1){
		session_start();//Validamos si existe o no la sesión
	}

	if (!isset($_SESSION["nombre"])){
		header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
	}else{
		//Validamos el acceso solo al usuario logueado y autorizado.
		if ($_SESSION['configuracion']==1 || $_SESSION['ventas']==1 ){

			require "../config/Conexion.php";

			$descripcion=isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";

			$nombrePuntoVenta=isset($_POST["nombrePuntoVenta"])? limpiarCadena($_POST["nombrePuntoVenta"]):"";

			$codigoTipoPuntoVenta=isset($_POST["codigoTipoPuntoVenta"])? limpiarCadena($_POST["codigoTipoPuntoVenta"]):"5";
			
			switch ($_GET["op"]){

				case 'listar':
					require_once "../xml/Operaciones.php";
					$oper = new Operaciones();
					$data = Array();
					$rspta=$oper->listarPuntosVenta();
					if(is_string($rspta)){//ingresa si es que no se tiene el CUIS de PV0 vigente
						
						$data[]=array(
							"0"=>'<button class="btn btn-success" type="button" title="Renovar/Generar CUIS" onclick="generar_cuis(0)"><i class="fa fa-refresh"></i> CUIS</button><br>',
							"1"=>"0",
							"2"=>$rspta,
							"3"=>$rspta,
							"4"=>$rspta,
							"5"=>$rspta,		
							"6"=>$rspta
						);
					}else{		
						$res = 	$rspta->res;
												
						for($i = 0; $i < count($res) - 1; $i++){
							//1. verificar que CUIS no es nulo y su fecha esta en vigencia
							//2. si el CUIS es nulo o está fuera de vigencia, se debe generar un botón que permita solicitar un nuevo CUIS
							//3. caso contrario, el botón no se genera (para evitar reintentar solicitar CUIS)
							//4. Un proceso parecido se debe hacer para el CUFD

							//variable botones agrupará los botones para generar cuis y cufd 
							$botones = "";
							if($res[$i]->codigo_cuis == null || $res[$i]->estado_cuis == 0 || $res[$i]->estado_cuis == null){
								$botones = '<button class="btn btn-success" type="button" title="Renovar/Generar CUIS" onclick="generar_cuis('.$res[$i]->codigoPuntoVenta.')"><i class="fa fa-refresh"></i> CUIS</button><br>';
							}else{
								$botones = '<button class="btn btn-primary" type="button" title="Renovar/Generar CUFD" onclick="generar_cufd('.$res[$i]->codigoPuntoVenta.')"><i class="fa fa-refresh"></i> CUFD</button><br>';
								//agregar el botón para cerrar operaciones de un PV
								if($i != 0 && $i != 1)
									$botones .= '<button class="btn btn-danger" type="button" title="Cierre de operaciones" onclick="cierre_operaciones('.$res[$i]->codigoPuntoVenta.')"><i class="fa fa-times"></i></button><br>';
							}
							
							
							$data[]=array(
								"0"=>$botones,
								"1"=>$res[$i]->codigoPuntoVenta,
								"2"=>$res[$i]->nombrePuntoVenta,
								"3"=>$res[$i]->tipoPuntoVenta,
								"4"=>$res[$i]->cuis,
								"5"=>$res[$i]->cufd,		
								"6"=>$res[$i]->estadoPuntoVenta ? '<span class="label bg-green">Activado</span>':'<span class="label bg-red">Desactivado</span>'
							);
						}
					}
					$results = array(
						"sEcho"=>1, //Información para el datatables
						"iTotalRecords"=>count($data), //enviamos el total registros al datatable
						"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
						"aaData"=>$data);
					echo json_encode($results);
					
				break;

				case 'listarTipoPuntosVenta':
					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();
					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaTipoPuntoVenta");
					$rspta = json_decode($res["sincronizarParametricaTipoPuntoVenta"])->RespuestaListaParametricas;
					$cad = "<option value=''>--Seleccione--</option>";
					if($rspta->transaccion){
						$listaCodigos = $rspta->listaCodigos;
						for($i = 0; $i < count($listaCodigos); $i++){
							if($listaCodigos[$i]->codigoClasificador == 5)
								$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'" selected>' . $listaCodigos[$i]->descripcion . '</option>';
							else
								$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'" disabled>' . $listaCodigos[$i]->descripcion . '</option>';
						}
					}
					echo $cad;
				break;

				case 'guardar':
					//1. se debe registrar con los servicios de SIN
					require_once "../xml/Operaciones.php";
					$operaciones = new Operaciones();
					$res = $operaciones->registroPuntoVenta($codigoTipoPuntoVenta,$descripcion,$nombrePuntoVenta);
					if($res != false){
						//2. si la transacción con los servicios de SIN es correcta, entonces se guarda en la BB.DD
						if($res->transaccion){
							require_once "../modelos/PuntoVenta.php";
							$pv = new PuntoVenta();
							$codigoPuntoVenta = $res->codigoPuntoVenta;
							$resdb = $pv->insertar($codigoPuntoVenta,$nombrePuntoVenta,$codigoTipoPuntoVenta);
							echo $resdb ? "Registro de punto de venta realizado correctamente":"Error al registrar punto de venta3";
						}else
							echo "Error al registrar punto de venta2";
					}else
						echo "Error al registrar punto de venta";
				break;

				case 'verificar_nombre_pv':
					$nombre_pv = $_GET["nombre_pv"];
					require_once "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$res = $pv->verificar_nombre_pv($nombre_pv);
					echo $res;
				break;

				case 'generar_cuis':
					//1. registrar el cuis nuevo con los servicios de SIN
					require_once "../xml/Codigos.php";
					$cod = new Codigos();
					$cod_pv = $_GET["cod_pv"];
					
					$rspta = $cod->generarCuis($cod_pv);
					//2. Si se registra correctamente en los servicios de SIN, se guarda en la BB.DD., asignando al PV el cuis creado
					if($rspta != false && !is_string($rspta)){
						require_once "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();
						$codigo_cuis = $rspta->codigo;
						$fecha_vigencia = $rspta->fechaVigencia;
						$res = $pv->registrar_cuis($codigo_cuis,$fecha_vigencia,$cod_pv);
						echo $res ? "1":"0";
						//echo $res ? "Registro de CUIS realizado correctamente":"ERROR EN REGISTRAR CUIS EN BB.DD.";
					}else{
					    var_dump($rspta);
						//echo $rspta."0";
						/*
						if($rspta == false)
							echo "ERROR EN GENERAR CUIS EN SERVICIOS DE SIN";
						else
							echo $rspta;
						*/
					}
				break;

				case 'generar_cufd':
					//1. registrar el cufd nuevo con los servicios de SIN
					require_once "../xml/Codigos.php";
					$cod = new Codigos();
					$cod_pv = $_GET["cod_pv"];
					$rspta = $cod->generarCufd($cod_pv);
					//2. Si se registra correctamente en los servicios de SIN, se guarda en la BB.DD., asignando al PV el cufd creado
					if($rspta != false && !is_string($rspta)){
						require_once "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();
						$codigo_cufd = $rspta->codigo;
						$codigo_control_cufd = $rspta->codigoControl;
						$direccion_cufd = $rspta->direccion;
						$fecha_vigencia_cufd = $rspta->fechaVigencia;
						$res = $pv->registrar_cufd($codigo_cufd,$codigo_control_cufd,$direccion_cufd,$fecha_vigencia_cufd,$cod_pv);
						echo $res ? "Registro de CUFD realizado correctamente":"ERROR EN REGISTRAR CUFD EN BB.DD.";
					}else{
						if($rspta == false)
							echo "ERROR EN GENERAR CUFD EN SERVICIOS DE SIN";
						else
							echo $rspta;
					}
				break;

				case 'generar_cufd2'://usado en ventas
					//1. registrar el cufd nuevo con los servicios de SIN
					require_once "../xml/Codigos.php";
					$cod = new Codigos();
					$cod_pv = $_GET["cod_pv"];
					$estado_pv = $_GET["estado_pv"];
					$rspta = $cod->generarCufd($cod_pv);
					//2. Si se registra correctamente en los servicios de SIN, se guarda en la BB.DD., asignando al PV el cufd creado
					if($rspta != false && !is_string($rspta)){
						//SE REGISTRA EL CUFD NUEVO EN LA BB.DD.
						require_once "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();
						$codigo_cufd = $rspta->codigo;
						$codigo_control_cufd = $rspta->codigoControl;
						$direccion_cufd = $rspta->direccion;
						$fecha_vigencia_cufd = $rspta->fechaVigencia;
						$res = $pv->registrar_cufd($codigo_cufd,$codigo_control_cufd,$direccion_cufd,$fecha_vigencia_cufd,$cod_pv);
						$act = $pv->actualizar_estado_pv($cod_pv,$estado_pv);
						if($res && $act)
							echo "1";
						else
							echo "0";
					}else
						echo "0";
				break;

				case 'cierre_operaciones':
					//1. registrar el cufd nuevo con los servicios de SIN
					require_once "../xml/Operaciones.php";
					$ope = new Operaciones();
					$cod_pv = $_GET["cod_pv"];
					$rspta = $ope->cerrar_operaciones($cod_pv);
					//2. Si se registra correctamente en los servicios de SIN, se guarda en la BB.DD., asignando al PV el cufd creado
					if($rspta != false && !is_string($rspta)){
						echo "Cierre de operaciones de punto de venta realizado correctamente";
					}else{
						if($rspta == false)
							echo "ERROR AL CERRAR DE OPERACIONES DE PUNTO DE VENTA";
						else
							echo $rspta;
					}
				break;			

				case 'select_puntos_venta':
					require "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$lista = $pv->listar2();
					$cad = "<option value='' selected> -- Seleccione --</option>";
					while($res_lista = $lista->fetch_object()){
						if($res_lista->estadoPuntoVenta == 0)
							$cad .= "<option value='$res_lista->id_punto_venta' disabled style='color:red;'>PV$res_lista->codigoPuntoVenta - $res_lista->nombrePuntoVenta</option>";
						else
							$cad .= "<option value='$res_lista->id_punto_venta'>PV$res_lista->codigoPuntoVenta - $res_lista->nombrePuntoVenta</option>";
					}
					echo $cad;
				break;

				case 'select_puntos_venta2':
					require "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$lista = $pv->listar2();
					$cad = "<option value='' selected> -- Seleccione --</option>";
					while($res_lista = $lista->fetch_object())
						$cad .= "<option value='$res_lista->id_punto_venta'>PV$res_lista->codigoPuntoVenta - $res_lista->nombrePuntoVenta</option>";
					
					echo $cad;
				break;

				case 'select_puntos_venta3':
					require "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$lista = $pv->listar3();
					$cad = '<select name="id_punto_venta_usr" id="id_punto_venta_usr" class="form-control" required><option selected value=""> -- Seleccione --</option>';
					while($res_lista = $lista->fetch_object())
						$cad .= "<option value='$res_lista->id_punto_venta'>PV$res_lista->codigoPuntoVenta - $res_lista->nombrePuntoVenta</option>";
					
					echo $cad."</select>";
				break;

				case 'contar_pv_disponibles':
					require_once "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$nro_pv_disponibles = $pv->contar_pv_disponibles();
					echo $nro_pv_disponibles["disponibles"];
				break;

			}
			//Fin de las validaciones de acceso
		}else
			require 'noacceso.php';
	}
	ob_end_flush();