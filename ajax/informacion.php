<?php 
	ob_start();
	if (strlen(session_id()) < 1)
		session_start();//Validamos si existe o no la sesión
	if (!isset($_SESSION["nombre"]))
	  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
	else{
		//Validamos el acceso solo al usuario logueado y autorizado.
		if ($_SESSION['configuracion'] == 1){
			require_once "../modelos/Informacion.php";
			$informacion = new Informacion();
			date_default_timezone_set("America/La_Paz");
			$telefono = isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
			$correo = isset($_POST["correo"])? limpiarCadena($_POST["correo"]):"";

			switch ($_GET["op"]){
				case 'editar':
					//var_dump($_POST);
					$rspta=$informacion->actualizar($telefono,$correo);
					echo $rspta ? "Información actualizada" : "No se actualizar la información";
				break;

				case 'mostrar':
					$rspta=$informacion->mostrar();
			 		//Codificar el resultado utilizando json
			 		echo json_encode($rspta);
				break;
		    }

		//Fin de las validaciones de acceso
	    }else
	  		require 'noacceso.php';
	}
	ob_end_flush();