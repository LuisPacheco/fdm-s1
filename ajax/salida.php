<?php 

	ob_start();

	if (strlen(session_id()) < 1)

		session_start();//Validamos si existe o no la sesión

	if (!isset($_SESSION["nombre"]))

	  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

	else{

		//Validamos el acceso solo al usuario logueado y autorizado.

		if ($_SESSION['salida'] == 1){

			require_once "../modelos/Salida.php";



			$salida = new Salida();

			date_default_timezone_set("America/La_Paz");

			$idsalida = isset($_POST["idsalida"])? limpiarCadena($_POST["idsalida"]):"";

			$sucursal = isset($_POST["sucursal"])? limpiarCadena($_POST["sucursal"]):"";

			$fecha_hora=isset($_POST["fecha_hora"])? date("Y") . "-" . date("m") . "-" . date("d") . " " . date("H") . ":" . date("i") . ":" . date("s"):"";

			$num_comprobante=isset($_POST["num_comprobante"])? limpiarCadena($_POST["num_comprobante"]):"";

			$id = isset($_POST["id"])? limpiarCadena($_POST["id"]):"";

			$idusuario = $_SESSION["idusuario"];



			switch ($_GET["op"]){

				case 'guardaryeditar':

					if (empty($idsalida)){

						if(isset($_POST["idarticulo"]) && isset($_POST["cantidad"]) && isset($_POST["idlote"])){

							if(count($_POST["idarticulo"]) > 0 && count($_POST["cantidad"]) > 0 && count($_POST["idlote"]) > 0){

								$idarticulo = $_POST["idarticulo"];

								$cantidad = $_POST["cantidad"];

								$idlote = $_POST["idlote"];

								$rspta = $salida->insertar($sucursal,$idusuario,$num_comprobante,$fecha_hora,$idarticulo,$cantidad,$idlote);

								echo $rspta ? "salida de medicamentos registrada correctamente" : "No se pudieron registrar todos los datos de la salida o la actualización del lote";

							}else{

								$errores = "";

								if(count($_POST["idarticulo"]) <= 0)

									$errores .= "Artículo vacío<br>";

								if(count($_POST["cantidad"]) <= 0)

									$errores .= "Cantidad vacía<br>";

								if(count($_POST["idlote"]) <= 0)

									$errores .= "Lote vacío<br>";

								$errores .= "<b>No se efectuará la salida</b>";

								echo $errores;

							}

						}else{

							$errores = "";

							if(!isset($_POST["idarticulo"]))

								$errores .= "Error en registrar idarticulo<br>";

							if(!isset($_POST["cantidad"]))

								$errores .= "Error en registrar cantidad<br>";

							if(!isset($_POST["idlote"]))

								$errores .= "Error en registrar lote<br>";

							$errores .= "No se efectuará la salida";

							echo $errores;

						}	

					}else 
						echo "Error al efectuar la salida";

				break;



				case 'anular':

					$rspta=$salida->anular($idsalida);



					$detalles = $salida->listarDetalle($idsalida);

					$sw = true;



					require_once "../modelos/Lote.php";

					require_once "../modelos/Articulo.php";



					$lote = new Lote();

					$articulo = new Articulo();



					while ($reg = $detalles->fetch_object()){

						

						$res = $lote->getStock($reg->idlote);

						$lot = $res->fetch_object();

						$dif = $lot->stock + $reg->cantidad;

						$act = $lote->setStock($reg->idlote,$dif);

						if (!$act) 

							$sw = false;

						

						$sql_cant_articulo = $articulo->devolverStock($reg->idarticulo);

						$cant_articulo = $sql_cant_articulo->fetch_object();

						$stock_actual = $cant_articulo->stock + $reg->cantidad;

						$sql_modif_stock = $articulo->modificarStock($reg->idarticulo,$stock_actual);

						if (!$act) 

							$sw = false;

					}

			 		echo $rspta && $sw ? "salida anulada" : "salida no se puede anular";

				break;



				case 'mostrar':

					$rspta=$salida->mostrar($idsalida);

			 		//Codificar el resultado utilizando json

			 		echo json_encode($rspta);

				break;



				case 'listarDetalle':

					//Recibimos el idingreso

					$id=$_GET['id'];



					$rspta = $salida->listarDetalle($id);

					$total=0;



					$data= Array();



					$i = 1;



					$total = 0;

			 		while ($reg=$rspta->fetch_object()){

						$venc = "";

						 

						if($reg->vencimiento == "" || $reg->vencimiento == "0000-00-00" || $reg->vencimiento == NULL || $reg->vencimiento == "0/0")

						 	$venc = $reg->cod_lote . " | n/a ";

						else

							$venc = $reg->cod_lote . " | " . $reg->vencimiento;



			 			$data[] = array(

			 				"0"=>$i,

			 				"1"=>$reg->nombre,

			 				"2"=>$reg->cantidad,

			 				"3"=>$venc,

						);



						$i++;

			 		}

					 

					$data[] = array(

						"0"=>"",

					   	"1"=>"",

						"2"=>"",

						"3"=>"",

				   );

				   $results = array(

					"aaData"=>$data);

					echo json_encode($results);

				break;



				case 'listar':

					$sucursal = $_GET["sucursal"];

					$rspta=$salida->listar($sucursal);

			 		//Vamos a declarar un array

			 		$data= Array();



			 		while ($reg=$rspta->fetch_object()){

			 			

			 			$url='../reportes/comprobante_salida.php?id=';

			 			

			 			



			 			$opciones = "";



			 			if($reg->estado == 'Aceptado'){

			 					$opciones = '<button class="btn btn-warning" onclick="mostrar('.$reg->idsalida.')"><i class="fa fa-eye"></i></button>'.

			 					' <button class="btn btn-danger" onclick="anular('.$reg->idsalida.')"><i class="fa fa-close"></i></button>'.

			 					'<a target="_blank" href="'.$url.$reg->idsalida.'"> <button class="btn btn-info"><i class="fa fa-file"></i></button></a>';

			 			}else{

			 					$opciones = '<button class="btn btn-warning" onclick="mostrar('.$reg->idsalida.')"><i class="fa fa-eye"></i></button>'.

			 					'<a target="_blank" href="'.$url.$reg->idsalida.'"> <button class="btn btn-info"><i class="fa fa-file"></i></button></a>';

			 			}



			 			$data[]=array(

			 				"0"=>$opciones,

			 				"1"=>$reg->fecha,

			 				"2"=>$reg->usuario,

			 				"3"=>$reg->num_comprobante,

			 				"4"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':

			 				'<span class="label bg-red">Anulado</span>');

			 		}

			 		$results = array(

			 			"sEcho"=>1, //Información para el datatables

			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

			 			"aaData"=>$data);

			 		echo json_encode($results);

				break;



				case 'listarArticulosSalida':

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();



					$rspta=$articulo->listarActivosVenta();

			 		//Vamos a declarar un array

			 		$data= Array();



			 		while ($reg=$rspta->fetch_object()){

			 			$dato = "";

			 			if ($reg->stock <= $reg->stock_minimo)

			 				$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";

			 			else 

			 				$dato = $reg->stock;

			 			$data[]=array(

			 				"0"=>($reg->stock > 0) ? '<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre_comercial.'\',\''.$reg->stock.'\')"><span class="fa fa-plus"></span></button>':'<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre_comercial.'\',\''.$reg->precio_salida.'\',\''.$reg->stock.'\')" disabled><span class="fa fa-plus"></span></button>',

			 				"1"=>$reg->cod_med,

			 				"2"=>$reg->nombre_comercial,

			 				"3"=>$reg->categoria,

			 				"4"=>$reg->nombre_generico,

			 				"5"=>$reg->laboratorio,

			 				"6"=>$dato,

			 				"7"=>$reg->stock_minimo,

			 				"8"=>$reg->precio_salida,

			 				"9"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >"

			 				);

			 		}

			 		$results = array(

			 			"sEcho"=>1, //Información para el datatables

			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

			 			"aaData"=>$data);

			 		echo json_encode($results);

				break;



				case 'listarArticulosSalida2':

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();

					require_once "../modelos/Lote.php";

					$lote = new Lote();

					$rspta=$articulo->listarActivosVenta2();

			 		//Vamos a declarar un array

			 		$data= Array();

					$i = 1;

			 		while ($reg=$rspta->fetch_object()){

			 			$estado = "";

						if($reg->stock == 0)
							$estado = "Agotado";
						else{
							if($reg->vencimiento != "0000-00-00" && $reg->vencimiento != "" && $reg->vencimiento != NULL && $reg->vencimiento != "0/0"){

								date_default_timezone_set("America/La_Paz");

								$fecha_vencimiento = new DateTime($reg->vencimiento);

								$actual = date("Y-m-d");

								$fecha = new DateTime($actual);

								$res = $fecha->diff($fecha_vencimiento);

								$dias = $res->days;

								if($dias >= 180 && $res->invert == 0)
									$estado = "Activo (6+)";
								else{
									if ($dias > 90 && $dias < 180 && $res->invert == 0) 
										$estado = "Activo (3+)";
									else{
										if ($dias <= 90 && $res->invert == 0) 
											$estado = "Por vencerse (3-)";
										else
											$estado = "Vencido";
									}
								}
							}else
								$estado = "n/a";
						}

						$sqllote = $lote->mostrarEstado($reg->idlote);

						$estado_lote = $sqllote->fetch_object();

						if ($estado_lote->estado != $estado)

							$lote->modificarEstado($estado,$reg->idlote);			

					

						$fondo = "";

						

						//determinar el tipo de estado del lote

						switch ($estado) {

							case 'n/a':

								$fondo = '<span class="label bg-blue text-white">N/A</span>'; 

							break;



							case 'Activo (6+)':

								$fondo = '<span class="label bg-green text-white">Activo (6+)</span>'; 

							break;



							case 'Activo (3+)':

								$fondo = '<span class="label bg-yellow text-white">Activo (3+)</span>'; 

							break;



							case 'Por vencerse (3-)':

								$fondo = '<span class="label bg-red text-white">Por vencerse (3-)</span>'; 

							break;



							case 'Agotado':

								$fondo = '<span class="label bg-black text-white">Agotado</span>'; 

							break;



							case 'Vencido':

								$fondo = '<span class="label bg-red text-white">Lote Vencido</span>'; 

							break;

						}

						

						$dato = "";

						if ($reg->stock_total <= $reg->stock_minimo)

							$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";

						else 

							$dato = $reg->stock;



						$vencimiento = "";



						if($reg->vencimiento != "0000-00-00" && $reg->vencimiento != "" && $reg->vencimiento != NULL && $reg->vencimiento != "0/0"){

							$vencimiento = $reg->cod_lote . " | " . $reg->mes . "-" . $reg->anio ;

						}else{

							$vencimiento = $reg->cod_lote . " | N/A";

						}			

								

						$data[]=array(

							"0"=>($reg->stock > 0 && $estado != "Vencido") ? '<button id="btnAdd'.$i.'" type="button" class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre_comercial.'\',\''.$reg->stock.'\',\''.$reg->cod_lote.'\',\''.$reg->idlote.'\',\''.$vencimiento.'\',\''.$i.'\')"><span class="fa fa-plus"></span></button>':'<button id="btnAdd'.$i.'" class="btn btn-warning" disabled><span class="fa fa-plus"></span></button>',

							"1"=>$reg->cod_med,

							"2"=>$reg->nombre_comercial,

							"3"=>$reg->nombre_generico,

							"4"=>$dato,

							"5"=>$reg->stock_minimo,

							"6"=>$reg->precio_venta,

							"7"=>$reg->laboratorio,

							"8"=>$reg->categoria,

							"9"=>$reg->vencimiento != "0000-00-00" && $reg->vencimiento != "" && $reg->vencimiento != NULL && $reg->vencimiento != "0/0" ? $reg->vencimiento . " | " . $reg->cod_lote . "<br>" . $fondo: "2099-12-31 | ".$reg->cod_lote . "<br>" . $fondo 

						);

							 

							

						$i++;		 			

			 		}

			 		$results = array(

			 			"sEcho"=>1, //Información para el datatables

			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

			 			"aaData"=>$data);

			 		echo json_encode($results);

				break;



				case 'select_sucursales':

					echo "<select id='sucursal_select'>

							<option value='ambas' selected>Todas</option>

							<option value='irpavi2'>Irpavi Nueva</option>

							<option value='irpavi'>Irpavi Central</option>

							<option value='seguencoma'>Seguencoma</option>

						</select>";

				break;



		    }

		//Fin de las validaciones de acceso

	    }else

	  		require 'noacceso.php';

	}

	ob_end_flush();