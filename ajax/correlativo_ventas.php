<?php
	date_default_timezone_set("America/La_Paz");
	ob_start();

	if (strlen(session_id()) < 1){
		session_start();//Validamos si existe o no la sesión
	}

	if (!isset($_SESSION["nombre"])){
		header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
	}else{
		//Validamos el acceso solo al usuario logueado y autorizado.
		if ($_SESSION['configuracion']==1){

			require_once "../modelos/CorrelativoVentas.php";

			$cv = new CorrelativoVentas();

			$id_correlativo = isset($_POST["id_correlativo"])? limpiarCadena($_POST["id_correlativo"]):"";

			$descripcion = isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";

			$actual = isset($_POST["actual"])? limpiarCadena($_POST["actual"]):"";

			$max = isset($_POST["max"])? limpiarCadena($_POST["max"]):"";

			$fecha_vigencia = isset($_POST["fecha_vigencia"])? limpiarCadena($_POST["fecha_vigencia"]):"";


			switch ($_GET["op"]){

				case 'listar':
					
					$rspta=$cv->listar();

					//Vamos a declarar un array

					$data= Array();

					while ($reg=$rspta->fetch_object()){

						$opciones = "";
						$estado = ($reg->estado=='1')?'<span class="label bg-green">Activado</span>':'<span class="label bg-red">Desactivado</span>';

						if($reg->estado == 1){
							
							if($reg->actual == 1 && date("Y-m-d") <= $reg->fecha_vigencia) //si el correlativo no se tiene ventas y la fecha no sobrepasa a la de caducidad, se puede editar cierta información 
								$opciones = '<button class="btn btn-warning" onclick="mostrar(\''.$reg->id_correlativo.'\')" title="Más detalles" title="editar"><i class="fa fa-pencil"></i></button>';
							
						}else{
							if($reg->actual == 1 && date("Y-m-d") <= $reg->fecha_vigencia) //si el correlativo no se tiene ventas y la fecha no sobrepasa a la de caducidad, se puede activar 
								$opciones = '<button class="btn btn-primary" onclick="activar(\''."$reg->id_correlativo".'\')" title="Activar Correlativo"><i class="fa fa-check"></i></button>';
						}

						$data[]=array(

							"0"=>$opciones,

							"1"=>$reg->fecha_registro,

							"2"=>$reg->descripcion,

							"3"=>$reg->fecha_vigencia,

							"4"=>$reg->max,

							"5"=>$reg->actual,

							"6" => $estado,

						);

					}

					$results = array(

						"sEcho"=>1, //Información para el datatables

						"iTotalRecords"=>count($data), //enviamos el total registros al datatable

						"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

						"aaData"=>$data);

					echo json_encode($results);
				break;

				case 'guardaryeditar':

					if(empty($id_correlativo)){
						$res = $cv->insertar($descripcion,$max,date("Y-m-d"),$fecha_vigencia);
						echo $res ? "Correlativo registrado correctamente": "Hubo un error en el registro del correlativo";
					}else{
						$res = $cv->editar($id_correlativo,$descripcion,$max,$fecha_vigencia);
						echo $res ? "Correlativo editado correctamente": "Hubo un error en la edición del correlativo";
					}
				break;

				case 'activar':
					$id_corr = $_GET["id_correlativo"];
					$res = $cv->activar($id_corr);
					echo $res ? "Correlativo activado correctamente":"Hubo un problema en activar correlativo";
				break;

				case 'mostrar':
					$res = $cv->mostrar($id_correlativo);
					echo json_encode($res);
				break;

				case 'mostrar_activo':
					$res = $cv->mostrar_activo();
					echo json_encode($res);
				break;

				
			}
			//Fin de las validaciones de acceso
		}else
			require 'noacceso.php';
	}
	ob_end_flush();