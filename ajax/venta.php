<?php 

	ob_start();

	if (strlen(session_id()) < 1)

		session_start();//Validamos si existe o no la sesión

	if (!isset($_SESSION["nombre"]))

		header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

	else{

		//Validamos el acceso solo al usuario logueado y autorizado.

		if ($_SESSION['ventas'] == 1){

			require_once "../modelos/Venta.php";

			$venta = new Venta();

			date_default_timezone_set("America/La_Paz");

			$idventa = isset($_POST["idventa"])? limpiarCadena($_POST["idventa"]):"";

			$nro_factura = isset($_POST["num_venta"]) ? limpiarCadena($_POST["num_venta"]):NULL;

			$nro_factura_contingencia = isset($_POST["num_venta_contingencia"]) ? limpiarCadena($_POST["num_venta_contingencia"]) : NULL;

			$idcliente = isset($_POST["idcliente"])? limpiarCadena($_POST["idcliente"]):"";

			$idusuario = $_SESSION["idusuario"];

			$fecha_hora = isset($_POST["fecha_hora"])? date("Y") . "-" . date("m") . "-" . date("d") . " " . date("H") . ":" . date("i") . ":" . date("s") . "." . date("v"):"";

			$fecha_hora_registro = date("Y") . "-" . date("m") . "-" . date("d") . " " . date("H") . ":" . date("i") . ":" . date("s") . "." . date("v");

			$fecha = isset($_POST["fecha"])? $_POST["fecha"]:"";

			$hora = isset($_POST["hora"])? $_POST["hora"]:"";

			$metodo_pago = isset($_POST["metodo_pago"]) ? limpiarCadena($_POST["metodo_pago"]) : "";

			$otro_metodo_pago = isset($_POST["otro_metodo_pago"]) ? limpiarCadena($_POST["otro_metodo_pago"]) : "";

			$nro_tarjeta = isset($_POST["nro_tarjeta"]) ? limpiarCadena($_POST["nro_tarjeta"]) : "";

			$codigo_excepcion = isset($_POST["codigo_excepcion"]) ? limpiarCadena($_POST["codigo_excepcion"]) : 0;

			$cafc = isset($_POST["codigo_contingencia"]) ? limpiarCadena($_POST["codigo_contingencia"]) : "";

			$login = isset($_SESSION["login"]) ? limpiarCadena($_SESSION["login"]) : "";

			$id_punto_venta = isset($_SESSION["id_punto_venta"]) ? limpiarCadena($_SESSION["id_punto_venta"]) : "";

			$total_venta = isset($_POST["total_venta"])? limpiarCadena($_POST["total_venta"]):"";

			$id = isset($_POST["id"])? limpiarCadena($_POST["id"]):"";

			$monto = isset($_POST["monto"])? limpiarCadena($_POST["monto"]):"";

			$cambio = isset($_POST["cambio"])? limpiarCadena($_POST["cambio"]):"";

			$evento_actual = isset($_POST["evento_actual"]) ? limpiarCadena($_POST["evento_actual"]):"";

			$id_evento_significativo = isset($_POST["id_evento_significativo"]) ? limpiarCadena($_POST["id_evento_significativo"]):"";

			//para el detalle de la venta

			$idarticulo = isset($_POST["idarticulo"]) ? $_POST["idarticulo"] : [];

			$cantidad = isset($_POST["cantidad"]) ? $_POST["cantidad"] : [];

			$precio_venta = isset($_POST["precio_venta"]) ? $_POST["precio_venta"] : [];

			$descuento = isset($_POST["descuento"]) ? $_POST["descuento"] : [];

			$idlaboratorio = isset($_POST["idlaboratorio"]) ? $_POST["idlaboratorio"] : [];

			$idlote = isset($_POST["idlote"]) ? $_POST["idlote"] : [];

			$cod_med = isset($_POST["cod_med"]) ? $_POST["cod_med"] : [];

			$cod_actividad = isset($_POST["cod_actividad"]) ? $_POST["cod_actividad"] : [];

			$cod_producto_sin = isset($_POST["cod_producto_sin"]) ? $_POST["cod_producto_sin"] : [];

			$unidad_medida = isset($_POST["unidad_medida"]) ? $_POST["unidad_medida"] : [];

			$nombre_unidad_medida = isset($_POST["nombreUnidadMedida"]) ? $_POST["nombreUnidadMedida"] : [];

			$descripcion = isset($_POST["descripcion"]) ? $_POST["descripcion"] : [];

			//para anulación
			$motivo_anulacion = isset($_POST["motivo_anulacion"]) ? limpiarCadena($_POST["motivo_anulacion"]):"";

			$nom_archivo = isset($_POST["nombre_archivo"]) ? limpiarCadena($_POST["nombre_archivo"]):"";

			switch ($_GET["op"]){

				//GUARDA VENTA Y ENVÍA A IMPUESTOS SI NO SE ENCUENTRA EN MODO FUERA DE LÍNEA
				case 'guardaryeditar':					
					if (empty($idventa)){
						$respuesta = "";
						$estado = "0";
						//SE DEBE TENER ESTOS DATOS LLENADOS EN EL FORMULARIO DE VENTAS PARA CONTINUAR
						if(($nro_factura != "" || $nro_factura_contingencia != "") && $idcliente != "" && $login != "" && $id_punto_venta != "" && $metodo_pago != "" && $total_venta != "" && $codigo_excepcion != ""){							
							//obtener el codigo del PV actual
							require "../modelos/PuntoVenta.php";
							$pv = new PuntoVenta();
							$datos_pv = $pv->getPuntoVenta2($id_punto_venta);

							//obtener información de la sucursal
							require "../modelos/Informacion.php";
							$informacion = Informacion::mostrar2();

							$codigo_sucursal = $informacion["codigoSucursal"];
							$razon_social_emisor = $informacion["razonSocialEmisor"];
							$nit_emisor = $informacion["nit"];
							$municipio = $informacion["municipio"];
							$telefono = $informacion["telefono"];

							$codigo_ambiente = $informacion["codigoAmbiente"];
							//$codigo_emision = 1; 
							$codigo_sistema = $informacion["codigoSistema"];
							$codigo_modalidad = $informacion["codigoModalidad"];

							//obtener token
							$token = TOKEN;

							//verificar si la emisión es online u offline
							if($evento_actual == "" || $evento_actual == 0)
								$tipo_emision = 1;//online
							else
								$tipo_emision = 2;//offline

							//obtener CUIS activo
							$cuis_activo = $pv->buscar_cuis_activo($datos_pv["id_punto_venta"]);

							if($cuis_activo){
								//obtener CUFD vigente y dirección o el cufd de la contingencia si se tiene un evento activo
								$cufd = "";
								$direccion = "";
								$cod_control = "";
								if($evento_actual == 0 || $evento_actual == ""){
									$cufd_activo = $pv->buscar_cufd_activo($datos_pv["id_punto_venta"]);
									if($cufd_activo){
										$cufd = $cufd_activo["codigo_cufd"];
										$direccion = $cufd_activo["direccion_cufd"];
										$cod_control = $cufd_activo["codigo_control_cufd"];
									}else{
										$vector_respuesta = ["respuesta" => "CUFD Inactivo o vencido", "estado" => "0"];
										echo json_encode($vector_respuesta);
										return;
									}
								}else{
									//Se obtiene los datos del CUFD del evento activo
									$cufd = isset($_POST["cufd_evento"]) ? limpiarCadena($_POST["cufd_evento"]) : "";
									$direccion = $informacion["direccion"];
									$cod_control = isset($_POST["cod_control_evento"]) ? limpiarCadena($_POST["cod_control_evento"]) : "";
								}

								$hora_actual = "";
								if($fecha_hora != "")//si se emite online
									$hora_actual = date("Y") . "-" . date("m") . "-" . date("d") . "T" . date("H") . ":" . date("i") . ":" . date("s") . "." . date("v");
								else //si se emite offline, se concatena la fecha y hora obtenidas por separado
									$hora_actual = $fecha."T".$hora;

								//guardar los datos generales para recepción de ventas (solo se aplicará para facturas emitidas en línea)
								$vector_datos_generales = [
									"codigoAmbiente" => $codigo_ambiente,
									"codigoDocumentoSector" => 1,
									"codigoEmision" => 1,  //debe emitirse online
									"codigoModalidad" => $codigo_modalidad,
									"codigoPuntoVenta" => $datos_pv["codigoPuntoVenta"],
									"codigoSistema" => $codigo_sistema,
									"codigoSucursal" => $codigo_sucursal,
									"cuis" => $cuis_activo["codigo_cuis"],
									"nit" => $nit_emisor,
									"tipoFacturaDocumento" => 1,//FACTURA CON DERECHO A CREDITO FISCAL
									"fechaEnvio" => $hora_actual,
									"token" => TOKEN
								];
							
								//validar cliente nuevo o existente
								$nombre_razon_social = "";
								$codigo_tipo_documento = "";
								$nro_documento = "";
								$complemento = "";
								$email = "";
								$codigo_cliente = "";
								$telefono_cliente = "";

								//se recuperan los datos de la BB.DD. en base al idcliente (SE TIENE QUE HACER CAMBIOS)
								require_once "../modelos/Persona.php";
								$persona = new Persona();
						
								//se obtienen los datos que se envían del formulario, una vez se valide desde SIN se guarda al cliente
								$nombre_razon_social = isset($_POST["nombre_cliente"]) ? (limpiarCadena($_POST["nombre_cliente"])) : "";
								$codigo_tipo_documento = isset($_POST["documento_identidad"]) ? limpiarCadena($_POST["documento_identidad"]) : "";
								$nro_documento = isset($_POST["nro_cliente"]) ? limpiarCadena($_POST["nro_cliente"]) : "";
								$complemento = isset($_POST["complemento"]) ? strtoupper(limpiarCadena($_POST["complemento"])) : "";
								$email = isset($_POST["email"]) ? limpiarCadena($_POST["email"]) : "";
								$telefono_cliente = isset($_POST["telefono"]) ? limpiarCadena($_POST["telefono"]) : "";
								$codigo_cliente = $nro_documento;

								//VERIFICAR SI SE TIENE FACTURA DEL CORRELATIVO O FACTURA POR CONTINGENCIA
								$factura = "";
								$corr_activo = $venta->mostrar_correlativo_vigente();
								if($nro_factura != NULL && $nro_factura != "0"){
									$nro_factura = $corr_activo["actual"];
									$factura = $nro_factura;
									$nro_factura_contingencia = NULL;
								}else{
									if($nro_factura_contingencia != NULL && $nro_factura_contingencia != "0"){
										$factura = $nro_factura_contingencia;
										$nro_factura = NULL;
									}
								}

								//SE VERIFICA QUE LOS DATOS DEL CLIENTE NO ESTÉN VACIOS PARA CONTINUAR
								if($nombre_razon_social != "" && $codigo_tipo_documento != "" && $nro_documento != ""){

									//sincronizar CATALOGOS
									require "../xml/SincronizacionDatos.php";
									$sinc = new SincronizacionDatos();
									//generar leyenda (aleatoria)
									$leyenda = "";
									$res_leyendas = $sinc->obtenerCatalogoX("sincronizarListaLeyendasFactura");
									$lista_leyendas = json_decode($res_leyendas["sincronizarListaLeyendasFactura"])->RespuestaListaParametricasLeyendas;

									if($lista_leyendas->transaccion){
										$nro_rand =  rand(0,count($lista_leyendas->listaLeyendas) - 1);
										$leyenda = $lista_leyendas->listaLeyendas[$nro_rand]->descripcionLeyenda;
									}else
										$leyenda = "Ley N° 453: Está prohibido importar, distribuir o comercializar productos expirados o prontos a expirar.";
									

									//generar CUF
									$caracteres_quitar = array("T",".",":","-");
									$nueva_hora_fecha = str_replace($caracteres_quitar, "", $hora_actual);

									$cuf = $venta->generar_cuf($nit_emisor,$nueva_hora_fecha,$codigo_sucursal,$codigo_modalidad,$tipo_emision,1,1,$factura,$datos_pv["codigoPuntoVenta"],$cod_control);

									//Ofuscar nro tarjeta si hubiese
									$cad_tarjeta_ofus = null;
									if($nro_tarjeta != null && $nro_tarjeta != ""){
										
										for($i = 0; $i < strlen($nro_tarjeta); $i++){
											if($i >= 4 && $i <= 11)
												$cad_tarjeta_ofus .= "0";
											else
												$cad_tarjeta_ofus .= $nro_tarjeta[$i];
										}
									}

									$nombre_razon_social = str_replace("\\","",$nombre_razon_social);

									//FORMAR VECTOR DE CABECERA
									$vector_cabecera = [
										"nitEmisor" => $nit_emisor,
										"razonSocialEmisor" => $razon_social_emisor,
										"municipio" => $municipio,
										"telefono" => $telefono,
										"numeroFactura" => $factura,
										"cuf" => $cuf,
										"cufd" => $cufd,
										"codigoSucursal" => $codigo_sucursal,
										"direccion" => $direccion,
										"codigoPuntoVenta" => $datos_pv["codigoPuntoVenta"],
										"fechaEmision" => $hora_actual,
										"nombreRazonSocial" => $nombre_razon_social,
										"codigoTipoDocumentoIdentidad" => $codigo_tipo_documento,
										"numeroDocumento" => $nro_documento,
										"complemento" => $complemento,
										"codigoCliente" => $codigo_cliente,
										"codigoMetodoPago" => $metodo_pago,
										"numeroTarjeta" => $cad_tarjeta_ofus,
										"montoTotal" => $total_venta,
										"montoTotalSujetoIva" => $total_venta,
										"codigoMoneda" => 1, //en un futuro obtener de catálogo
										"tipoCambio" => 1, //averiguar como este valor puede cambiar si no fuera Bs.
										"montoTotalMoneda" => $total_venta, //se calcula en base al tipo de moneda
										"montoGiftCard " => null,
										"descuentoAdicional" => "0.00", //No se tendrá descuentos globales (de momento)
										"codigoExcepcion" => (int)$codigo_excepcion,
										"cafc" => $cafc,
										"leyenda" => $leyenda,
										"usuario" => $login,
										"codigoDocumentoSector" =>1 //en un futuro obtener de catálogo COMPRA VENTA
									];

									//FORMAR EL VECTOR DE DETALLES
									$vector_detalles = [];

									//VALIDAR QUE SE TENGA AL MENOS UN ARTÍCULO EN EL DETALLE
									if(count($idarticulo) > 0 && count($cantidad) > 0 && count($precio_venta) > 0 && count($descuento) > 0 && count($cod_med) > 0 && count($cod_actividad) > 0 && count($cod_producto_sin) && count($unidad_medida) && count($descripcion)){
										for($i = 0; $i < count($idarticulo) ; $i++){

											$vector_detalle = [
												"actividadEconomica" => $cod_actividad[$i],
												"codigoProductoSin" => $cod_producto_sin[$i],
												"codigoProducto" => $cod_med[$i],
												"descripcion" => $descripcion[$i],
												"cantidad" => $cantidad[$i],
												"unidadMedida" => $unidad_medida[$i],
												"precioUnitario" => $precio_venta[$i],
												"montoDescuento" => $descuento[$i],
												"subTotal" => (($precio_venta[$i] * $cantidad[$i]) - $descuento[$i]),
												"numeroSerie" => "",
												"numeroImei" => ""
											];

											array_push($vector_detalles,$vector_detalle);
											$vector_detalle = [];
										}

										require "../xml/crear_xml/CrearXML.php";
										$crear_xml = new CrearXML();

										//FUNCIÓN PARA GENERAR Y RECEPCIÓN DE FACTURA
										$res_crear_xml = $crear_xml->generar_xml_factura($vector_cabecera,$vector_detalles,$vector_datos_generales,$tipo_emision,$nueva_hora_fecha,$evento_actual,$id_evento_significativo);

										if(is_object($res_crear_xml) || $res_crear_xml){//SE VERIFICA SI SE TIENE CREADA DE FORMA CORRECTA DE LA FACTURA EN LÍNEA O FUERA DE LÍNEA
											//Se generan los nombres de los archivos en formato .xml y .png para el qr
											$ruta = "";
											if($evento_actual == 5 || $evento_actual == 6 || $evento_actual == 7)
												$nombre_archivo = $nueva_hora_fecha.$factura."CTG".$nro_documento.$complemento;
											else
												$nombre_archivo = $nueva_hora_fecha.$factura.$nro_documento.$complemento;
											
											$nombre_xml = $nombre_archivo.".xml";
											$nombre_qr = $nombre_archivo.'.png';

											$carpeta_contingencia = "";
											//SE VERIFICA SI LA VENTA TIENE UN EVENTO Y ASÍ OBTENER O NO SU PAQUETE
											if($evento_actual != 0 && $evento_actual != ""){
												require_once "../modelos/EventoSignificativo.php";
												$eve_sig = new EventoSignificativo();
												$res_pqt = $eve_sig->obtener_ultimo_paquete_es($id_evento_significativo);
												if($res_pqt)
													$carpeta_contingencia = $res_pqt["nombre_paquete"];
											}

											//para crear QR's
											require_once "../phpqrcode/qrlib.php";
										
											if(is_object($res_crear_xml)){//SI SE EMITIÓ LA FACTURA EN LÍNEA
												if($res_crear_xml->transaccion){
													//YA SE ENVIÓ A IMPUESTOS EXITOSAMENTE
													if($res_crear_xml->codigoEstado == 908)
														$respuesta = "<i class='fa fa-check-circle' aria-hidden='true'></i> Factura aceptada por Impuestos<br>";
													else{
														//se verifica las posibles observaciones que se den (quizá nunca pase por aquí)
														if(is_array($res_crear_xml->mensajesList)){
															for($j = 0; $j < count($res_crear_xml->mensajesList); $j++)
																$respuesta .= "AAA<i class='fa fa-exclamation-circle' aria-hidden='true'></i>	".($j+1). ". (Error ".$res_crear_xml->mensajesList[$j]->codigo .") ".$res_crear_xml->mensajesList[$j]->descripcion . "<br>"; 
														}else
															$respuesta = "AAA<i class='fa fa-exclamation-circle' aria-hidden='true'></i> (Error ".$res_crear_xml->mensajesList->codigo .") ".$res_crear_xml->mensajesList->descripcion . "<br>";
													}

													//GENERAR QR para representación gráfica
													if($evento_actual != 0 && $evento_actual != ""){//para offline 1,2,3 y 4
														$ruta = "../xml_qr/paquetes_contingencia/$carpeta_contingencia/";
														$ruta_qr = $ruta.$nombre_qr;
													}else{//para online
														$ruta = "../xml_qr/";
														$ruta_qr = $ruta.$nombre_qr;
													}

													$nuevo_cliente = $idcliente;
													if($idcliente == "nuevo"){
														//SE REGISTRA AL NUEVO CLIENTE
														$crear_cliente = $persona->insertar2("Cliente",$nombre_razon_social,$codigo_tipo_documento,$nro_documento,$complemento,$telefono_cliente,$email);
														$nuevo_cliente = $persona->obtener_ultimo_cliente_registrado()["idpersona"];
													}else{
														//EDITAR DATOS DE CLIENTE EXISTENTE
														$editar_cliente = $persona->editar($idcliente,"Cliente",$nombre_razon_social,$codigo_tipo_documento,$nro_documento,$complemento,"",$telefono_cliente,$email);
													}

												
													//SE GUARDA REGISTRO DE VENTA EN LA BB.DD.
													$rspta = $venta->insertar($nro_factura,$nro_factura_contingencia,$nuevo_cliente,$idusuario,$metodo_pago,$nro_tarjeta,$cad_tarjeta_ofus,$hora_actual,date("Y-m-d H:i:s.v"),$codigo_excepcion,$cafc,$id_punto_venta,$cufd,$cuf,$leyenda,$total_venta,$monto,$cambio,$res_crear_xml->codigoRecepcion,$nombre_archivo,$ruta,$tipo_emision,$id_evento_significativo,"Aceptado",$otro_metodo_pago,
																				$idarticulo,$cantidad,$precio_venta,$descuento,$idlote,$cod_med,$cod_actividad,$cod_producto_sin,$unidad_medida,$nombre_unidad_medida,$descripcion);

													if($rspta){	//SE VERIFICA SI SE GUARDÓ CORRECTAMENTE EL REGISTRO DE LA VENTA Y SUS DETALLES
														//SE GENERA QR
														$cad_archivo_qr = "https://pilotosiat.impuestos.gob.bo/consulta/QR?nit=".$nit_emisor."&cuf=".$cuf."&numero=".$factura."&t=1";
														QRcode::png($cad_archivo_qr,$nombre_qr);
														rename($nombre_qr,$ruta_qr);

														//OBTENER EL CORRELATIVO ACTIVO
														$corr_activo = $venta->mostrar_correlativo_vigente();
														if($corr_activo && $nro_factura != ""){
															//FINALMENTE SE ACTUALIZA EL CORRELATIVO
															$nro_factura++;
															$act_corr = $venta->actualizar_correlativo($nro_factura,$corr_activo["id_correlativo"]);
															if($act_corr){
																$respuesta .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Venta registrada en la Base de Datos<br>";
																$estado = "1";

																//GENERAR PDF EN LA CARPETA
																require "../reportes/exFactura.php";
																generar_pdf($nombre_archivo,1);


																//ENVIAR POR CORREO SI TUVIESE EL CLIENTE
																if($email != ""){
																	//OBTENER ÚLTIMA VENTA
																	$ultima_venta = $venta->obtener_ultima_venta_cliente($nuevo_cliente);
																	
																	$id_venta_actual = $ultima_venta["idventa"];
																	$resp_correo = $venta->enviar_correo($id_venta_actual,$email);
																	if($resp_correo)
																		$respuesta .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Correo enviado correctamente";
																	else
																		$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo enviar el correo";
																}
															}else
																$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Hubo un problema en actualizar el correlativo de factura";
														}else
															$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo obtener el correlativo actual de facturas";
													}else
														$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo registrar en la Base de Datos";
													
												}else{//HUBO UN PROBLEMA EN ENVIAR A IMPUESTOS
													//MOSTRAR MENSAJES
													if(is_array($res_crear_xml->mensajesList)){
														for($j = 0; $j < count($res_crear_xml->mensajesList); $j++)
															$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i>" . ($j+1). ". (Error ".$res_crear_xml->mensajesList[$j]->codigo .") ".$res_crear_xml->mensajesList[$j]->descripcion . "<br>"; 
													}else
														$respuesta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> (Error ".$res_crear_xml->mensajesList->codigo .") ".$res_crear_xml->mensajesList->descripcion;

													//SE DEBE BORRAR TAMBIÉN EL ARCHIVO XML DE LA CARPETA xml_qr
													unlink("../xml_qr/".$nombre_xml);
												}

												unlink($nombre_xml);//se borra el archivo XML generado en la carpeta ajax

											}else{//se genera el xml aunque es fuera de línea

												$respuesta = "<i class='fa fa-check-circle' aria-hidden='true'></i> Factura generada FUERA DE LÍNEA<br>";
												//GENERAR QR para representación gráfica en caso de ser OFFLINE
												$ruta = "../xml_qr/paquetes_contingencia/$carpeta_contingencia/";
												$ruta_qr = $ruta.$nombre_qr;

								

												$nuevo_cliente = $idcliente;
												if($idcliente == "nuevo"){
													//SE REGISTRA AL NUEVO CLIENTE
													$crear_cliente = $persona->insertar2("Cliente",$nombre_razon_social,$codigo_tipo_documento,$nro_documento,$complemento,$telefono_cliente,$email);
													$nuevo_cliente = $persona->obtener_ultimo_cliente_registrado()["idpersona"];
												}else{
													//EDITAR DATOS DE CLIENTE EXISTENTE
													$editar_cliente = $persona->editar($idcliente,"Cliente",$nombre_razon_social,$codigo_tipo_documento,$nro_documento,$complemento,"",$telefono_cliente,$email);
												}
											
												$rspta = $venta->insertar($nro_factura,$nro_factura_contingencia,$nuevo_cliente,$idusuario,$metodo_pago,$nro_tarjeta,$cad_tarjeta_ofus,$hora_actual,date("Y-m-d H:i:s.v"),$codigo_excepcion,$cafc,$id_punto_venta,$cufd,$cuf,$leyenda,$total_venta,$monto,$cambio,NULL,$nombre_archivo,$ruta,$tipo_emision,$id_evento_significativo,"Pendiente",$otro_metodo_pago,
																				$idarticulo,$cantidad,$precio_venta,$descuento,$idlote,$cod_med,$cod_actividad,$cod_producto_sin,$unidad_medida,$nombre_unidad_medida,$descripcion);

												if($rspta){
													//SE GENERA EL QR
													$cad_archivo_qr = "https://pilotosiat.impuestos.gob.bo/consulta/QR?nit=".$nit_emisor."&cuf=".$cuf."&numero=".$factura."&t=1";
													QRcode::png($cad_archivo_qr,$nombre_qr);
													rename($nombre_qr,$ruta_qr);//SE MUEVE ARCHIVO QR A RUTA DEFINIDA EN $ruta_qr

													//GENERAR PDF EN LA CARPETA
													require "../reportes/exFactura.php";
													generar_pdf($nombre_archivo,1);

													//OBTENER EL CORRELATIVO ACTIVO SI ES EVENTO 1, 2, 3 o 4
													if($evento_actual == "1" || $evento_actual == "2" || $evento_actual == "3" || $evento_actual == "4"){
														$corr_activo = $venta->mostrar_correlativo_vigente();
														if($corr_activo && $nro_factura != ""){
															//FINALMENTE SE ACTUALIZA EL CORRELATIVO
															$nro_factura++;
															$act_corr = $venta->actualizar_correlativo($nro_factura,$corr_activo["id_correlativo"]);
															if($act_corr){
																$respuesta .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Venta registrada en la Base de Datos";
																$estado = "1";

																if($evento_actual == "2"){
																	//ENVIAR POR CORREO SI TUVIESE EL CLIENTE SI ES EVENTO 2
																	if($email != ""){
																		//OBTENER ÚLTIMA VENTA
																		$ultima_venta = $venta->obtener_ultima_venta_cliente($nuevo_cliente);
																		
																		$id_venta_actual = $ultima_venta["idventa"];
																		$resp_correo = $venta->enviar_correo($id_venta_actual,$email);
																		if($resp_correo)
																			$respuesta .= "<br><i class='fa fa-check-circle' aria-hidden='true'></i> Correo enviado correctamente";
																		else
																			$respuesta .= "<br><i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo enviar el correo";
																	}
																}

															}else
																$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Hubo un problema en actualizar el correlativo de factura";
														}else
															$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo obtener el correlativo actual de facturas";
													}else{
														$respuesta .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Venta registrada en la Base de Datos";
														$estado = "1";

														//ENVIAR POR CORREO SI TUVIESE EL CLIENTE SI ES EVENTO 2
														if($email != ""){
															//OBTENER ÚLTIMA VENTA
															$ultima_venta = $venta->obtener_ultima_venta_cliente($nuevo_cliente);
															
															$id_venta_actual = $ultima_venta["idventa"];
															$resp_correo = $venta->enviar_correo($id_venta_actual,$email);
															if($resp_correo)
																$respuesta .= "<br><i class='fa fa-check-circle' aria-hidden='true'></i> Correo enviado correctamente";
															else
																$respuesta .= "<br><i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo enviar el correo";
														}
													}
												}else
													$respuesta .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo registrar en la Base de Datos";
											}
										}else
											$respuesta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Hubo un problema INTERNO en generar la factura, revise los datos llenados";
									}else
										$respuesta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error en registrar los detalles del producto";
								}else
									$respuesta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error en registrar cliente u obtener datos de cliente registrado";
							}else
								$respuesta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> CUIS no activo o vencido";
						}else
							$respuesta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error en datos generales de la factura";
						
						$vector_respuesta = ["respuesta" => $respuesta, "estado" => $estado];
						echo json_encode($vector_respuesta);
					}
					
				break;

				//EDITAR VENTA
				case 'editar_venta':
					//var_dump($_POST);
					//return;

					if(!empty($_POST["idventa_editar"])){
						//en caso de editar una venta anterior (ventas observadas)
						//SE DEBERÍA BORRAR EL ARCHIVO XML ANTERIOR, AL IGUAL QUE EL QR Y EL PDF
						//EN BASE A LOS DATOS EDITADOS, GENERAR ESTOS ARCHIVOS EN LA MISMA CARPETA (PAQUETE)
						//SOLO SE PODRÁN MODIFICAR CIERTOS DATOS GENERALES DE LA VENTA, MAS NO LOS DETALLES

						$mensaje = "";
						$estado = "0";
						//obtener la venta a editar
						$venta_editar = $venta->mostrar($_POST["idventa_editar"]);
						if($venta_editar){
							
							$id_venta_editar = $_POST["idventa_editar"];
							try{
								$sw_borrado = false;
								//BORRAR LOS ARCHIVOS QUE SE TENÍAN ANTES (PDF,XML, Y PNG)

								if(file_exists($venta_editar["ruta"].$venta_editar["nombre_archivo"].".pdf")){
									unlink($venta_editar["ruta"].$venta_editar["nombre_archivo"].".pdf");
									$sw_borrado = true;
								}
								
								if(file_exists($venta_editar["ruta"].$venta_editar["nombre_archivo"].".png")){
									unlink($venta_editar["ruta"].$venta_editar["nombre_archivo"].".png");
									$sw_borrado = true;
								}

								if(file_exists($venta_editar["ruta"].$venta_editar["nombre_archivo"].".xml")){
									unlink($venta_editar["ruta"].$venta_editar["nombre_archivo"].".xml");
									$sw_borrado = true;
								}

								//obtener el codigo del PV actual
								require "../modelos/PuntoVenta.php";
								$pv = new PuntoVenta();
								$datos_pv = $pv->getPuntoVenta2($id_punto_venta);

								//obtener información de la sucursal
								require "../modelos/Informacion.php";
								$informacion = Informacion::mostrar2();

								$codigo_sucursal = $informacion["codigoSucursal"];
								$razon_social_emisor = $informacion["razonSocialEmisor"];
								$nit_emisor = $informacion["nit"];
								$municipio = $informacion["municipio"];
								$telefono = $informacion["telefono"];
								$direccion = $informacion["direccion"];
								$codigo_modalidad = $informacion["codigoModalidad"];

								//obtener token
								$token = TOKEN;

								//tipo de emisión
								$tipo_emision = 2;//siempre se editará una venta offline

								//RECUPERAR DATOS DE VENTA EDITADA
								$evento_editar = isset($_POST["evento_editar"])? limpiarCadena($_POST["evento_editar"]): "";
								$id_evento_significativo_editar = isset($_POST["id_evento_significativo_editar"])? limpiarCadena($_POST["id_evento_significativo_editar"]): "";
								$cufd_editar = isset($_POST["cufd_editar"])? limpiarCadena($_POST["cufd_editar"]): "";
								$cod_control_editar = isset($_POST["cod_control_editar"])? limpiarCadena($_POST["cod_control_editar"]): "";
								$num_venta_contingencia_editar = isset($_POST["num_venta_contingencia_editar"])? limpiarCadena($_POST["num_venta_contingencia_editar"]): "";
								$num_venta_editar = isset($_POST["num_venta_editar"])? limpiarCadena($_POST["num_venta_editar"]): "";
								$fecha_editar = isset($_POST["fecha_editar"])? limpiarCadena($_POST["fecha_editar"]): "";
								$hora_editar = isset($_POST["hora_editar"])? limpiarCadena($_POST["hora_editar"]): "";

								//en cliente se valida si es nuevo primero
								$id_cliente_editar = isset($_POST["idcliente_editar"])? limpiarCadena($_POST["idcliente_editar"]): "";
								require_once "../modelos/Persona.php";
								$persona = new Persona();
								
								//se obtienen los datos que se envían del formulario, una vez se valide desde SIN se guarda al cliente
								$nombre_razon_social = isset($_POST["nombre_cliente_editar"]) ? (limpiarCadena($_POST["nombre_cliente_editar"])) : "";
								$codigo_tipo_documento = isset($_POST["documento_identidad_editar"]) ? limpiarCadena($_POST["documento_identidad_editar"]) : "";
								$nro_documento = isset($_POST["nro_cliente_editar"]) ? limpiarCadena($_POST["nro_cliente_editar"]) : "";
								$complemento = isset($_POST["complemento_editar"]) ? strtoupper(limpiarCadena($_POST["complemento_editar"])) : "";
								$email = isset($_POST["email_editar"]) ? limpiarCadena($_POST["email_editar"]) : "";
								$telefono_editar = isset($_POST["telefono_editar"]) ? limpiarCadena($_POST["telefono_editar"]) : "";
								$codigo_cliente = $nro_documento;
							
								//fehca de emisión
								$fecha_emision = $fecha_editar."T".$hora_editar;

								//factura
								$factura = "";
								if($num_venta_editar != "" && $num_venta_editar != "0")
									$factura = $num_venta_editar;
								else{
									if($num_venta_contingencia_editar != "" && $num_venta_contingencia_editar != "0")
										$factura = $num_venta_contingencia_editar;
								}

								//cod excepcion y cafc
								$codigo_excepcion_editar = isset($_POST["codigo_excepcion_editar"]) ? limpiarCadena($_POST["codigo_excepcion_editar"]) : ""; 
								$cafc_editar = isset($_POST["codigo_contingencia_editar"]) ? limpiarCadena($_POST["codigo_contingencia_editar"]) : ""; 

								//formar nuevo CUF
								$caracteres_quitar = array("T",".",":","-");
								$nueva_hora_fecha = str_replace($caracteres_quitar, "", $fecha_emision);

								$cuf = $venta->generar_cuf($nit_emisor,$nueva_hora_fecha,$codigo_sucursal,$codigo_modalidad,$tipo_emision,1,1,$factura,$datos_pv["codigoPuntoVenta"],$cod_control_editar);


								//FORMAR VECTOR DE CABECERA
								$vector_cabecera = [
									"nitEmisor" => $nit_emisor,
									"razonSocialEmisor" => $razon_social_emisor,
									"municipio" => $municipio,
									"telefono" => $telefono,
									"numeroFactura" => $factura,
									"cuf" => $cuf,
									"cufd" => $cufd_editar,
									"codigoSucursal" => $codigo_sucursal,
									"direccion" => $direccion,
									"codigoPuntoVenta" => $datos_pv["codigoPuntoVenta"],
									"fechaEmision" => $fecha_emision,
									"nombreRazonSocial" => $nombre_razon_social,
									"codigoTipoDocumentoIdentidad" => $codigo_tipo_documento,
									"numeroDocumento" => $nro_documento,
									"complemento" => $complemento,
									"codigoCliente" => $codigo_cliente,
									"codigoMetodoPago" => $venta_editar["metodo_pago"],
									"numeroTarjeta" => $venta_editar["nro_tarjeta_ofus"],
									"montoTotal" => $venta_editar["total_venta"],
									"montoTotalSujetoIva" => $venta_editar["total_venta"],
									"codigoMoneda" => 1, 
									"tipoCambio" => 1, 
									"montoTotalMoneda" => $venta_editar["total_venta"],
									"montoGiftCard " => null,
									"descuentoAdicional" => 0,
									"codigoExcepcion" => (int)$codigo_excepcion_editar,
									"cafc" => $cafc_editar,
									"leyenda" => $venta_editar["leyenda"],
									"usuario" => $login,
									"codigoDocumentoSector" =>1 
								];

								//Obtener detalles de venta
								$det_venta = $venta->listarDetalle($_POST["idventa_editar"]);

								//FORMAR EL VECTOR DE DETALLES
								$vector_detalles = [];

								//VALIDAR QUE SE TENGA AL MENOS UN ARTÍCULO EN EL DETALLE
								while($dv = $det_venta->fetch_object()){
									$vector_detalle = [
										"actividadEconomica" => $dv->codigoActividad,
										"codigoProductoSin" => $dv->codigoProductoSin,
										"codigoProducto" => $dv->cod_med,
										"descripcion" => $dv->nombre,
										"cantidad" => $dv->cantidad,
										"unidadMedida" => $dv->unidadMedida,
										"precioUnitario" => $dv->precio_venta,
										"montoDescuento" => $dv->descuento,
										"subTotal" => (($dv->precio_venta * $dv->cantidad) - $dv->descuento),
										"numeroSerie" => "",
										"numeroImei" => ""
									];

									array_push($vector_detalles,$vector_detalle);
									$vector_detalle = [];
								}

								require "../xml/crear_xml/CrearXML.php";
								$crear_xml = new CrearXML();

								//FUNCIÓN PARA GENERAR Y RECEPCIÓN DE FACTURA
								$res_crear_xml = $crear_xml->generar_xml_factura($vector_cabecera,$vector_detalles,[],$tipo_emision,$nueva_hora_fecha,$evento_editar,$id_evento_significativo_editar,$sw_borrado);

								if($res_crear_xml){

									if($evento_editar == 5 || $evento_editar == 6 || $evento_editar == 7)
										$nombre_archivo = $nueva_hora_fecha.$factura."CTG".$nro_documento.$complemento;
									else
										$nombre_archivo = $nueva_hora_fecha.$factura.$nro_documento.$complemento;
									//$nombre_archivo = $venta_editar["nombre_archivo"];

									$nombre_xml = $nombre_archivo.".xml";
									$nombre_qr = $nombre_archivo.'.png';

									$carpeta_contingencia = "";
									//SE VERIFICA SI LA VENTA TIENE UN EVENTO Y ASÍ OBTENER O NO SU PAQUETE
									
									require_once "../modelos/EventoSignificativo.php";
									$eve_sig = new EventoSignificativo();
									$res_pqt = $eve_sig->obtener_ultimo_paquete_es($id_evento_significativo_editar);
									if($res_pqt)
										$carpeta_contingencia = $res_pqt["nombre_paquete"];
									

									//GENERAR QR para representación gráfica de la venta editada
									$ruta = "../xml_qr/paquetes_contingencia/$carpeta_contingencia/";
									$ruta_qr = $ruta.$nombre_qr;

									$nuevo_cliente = $id_cliente_editar;
									if($id_cliente_editar == "nuevo"){
										//SE REGISTRA AL NUEVO CLIENTE
										$crear_cliente = $persona->insertar2("Cliente",$nombre_razon_social,$codigo_tipo_documento,$nro_documento,$complemento,$telefono_editar,$email);
										$nuevo_cliente = $persona->obtener_ultimo_cliente_registrado()["idpersona"];
									}else{
										//SE MODIFICA INFORMACIÓN DEL CLIENTE
										$editar_cliente = $persona->editar($id_cliente_editar,"Cliente",$nombre_razon_social,$codigo_tipo_documento,$nro_documento,$complemento,"",$telefono_editar,$email);
									}
								
									//ACTUALIZAR VENTA
									//var_dump($nuevo_cliente);
									$rspta = $venta->actualizar($id_venta_editar,$idusuario,$cufd_editar,$id_evento_significativo_editar,$num_venta_editar,$num_venta_contingencia_editar,$fecha_editar." ".$hora_editar,date("Y-m-d H:i:s.v"),$nuevo_cliente,$codigo_excepcion_editar,$cafc_editar,$id_punto_venta,$cuf,$nombre_archivo);
										
									if($rspta){
										//para crear QR's
										require_once "../phpqrcode/qrlib.php";
										//SE GENERA EL QR
										$cad_archivo_qr = "https://pilotosiat.impuestos.gob.bo/consulta/QR?nit=".$nit_emisor."&cuf=".$cuf."&numero=".$factura."&t=1";
										QRcode::png($cad_archivo_qr,$nombre_qr);
										rename($nombre_qr,$ruta_qr);//SE MUEVE ARCHIVO QR A RUTA DEFINIDA EN $ruta_qr

										//GENERAR PDF EN LA CARPETA
										require "../reportes/exFactura.php";
										generar_pdf($nombre_archivo,1);
										
										$mensaje = "<i class='fa fa-check-circle' aria-hidden='true'></i> XML generado correctamente";
										$mensaje .= "<br><i class='fa fa-check-circle' aria-hidden='true'></i> Venta actualizada en la Base de Datos";
										$estado = "1";
										
									}else
										$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo registrar la actualización en la Base de Datos";							
										
								}else
									$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error en generar XML de venta";
								
							}catch(Exception $e){
								$mensaje = $e->getMessage();
							}
						}else
							$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo encontrar la venta para editar";
					}else
						$mensaje = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error en guardar venta";
						
					echo json_encode(["mensaje"=>$mensaje,"estado"=>$estado]);
				break;

				//LISTADO DE LAS VENTAS DE LOS ÚLTIMOS 5 DÍAS
				case 'listar':
                    date_default_timezone_set("America/La_Paz");
					require_once "../modelos/CorrelativoCaja.php";
					require_once "../modelos/PuntoVenta.php";

					$corr = new CorrelativoCaja();
					$pv = new PuntoVenta();

					require_once "../modelos/Usuario.php";

					$usuario = new Usuario();

					$sqlContarCorr = $corr->contar_correlativos_fecha();

					$registrosHoy = $sqlContarCorr->fetch_object();

					if($registrosHoy->total == 0){

						$res = $corr->cerrar_cajas_dias_anteriores();

						$enUsoCero = $pv->setColumna2("enUso","0");

						$sqlCodVendedores = $usuario->selectCodVendedores();

						$id_vendedores = Array();

						while($reg =  $sqlCodVendedores->fetch_object())

							array_push($id_vendedores,$reg->idusuario);

						$resul = $corr->crearRegistrosHoy($id_vendedores);

					}



					$rspta=$venta->listar();

			 		//Vamos a declarar un array

			 		$data= Array();



			 		while ($reg=$rspta->fetch_object()){

						$url = "../reportes/exFactura.php?id=";

						$url_generar_pdf = "&gen_pdf=0";

						//SI ES FUERA DE LÍNEA SE DEBE CAMBIAR LA RUTA
						$url_xml = $reg->ruta;

			 			$opciones = "";
						
			 			if($reg->estado == 'Aceptado'){//FACTURA ACEPTADA POR IMPUESTOS

							$opciones = '<button class="btn btn-warning" onclick="mostrar(\''.$reg->idventa.'\',\'0\')" title="Más detalles"><i class="fa fa-eye"></i></button>'.

							' <button class="btn btn-danger" onclick="anular(\''."$reg->nombre_archivo".'\')" title="Anular Venta"><i class="fa fa-close"></i></button>'.

							'<a target="_blank" href="'.$url.$reg->nombre_archivo.$url_generar_pdf.'"> <button class="btn btn-info" title="Reporte factura"><i class="fa fa-file-pdf-o"></i></button></a>'.

							'<a target="_blank" href="'.$url_xml.$reg->nombre_archivo.'.xml"> <button class="btn btn-success" title="Generar XML Factura"><i class="fa fa-code"></i></button></a>';

							if($reg->email != "")
								$opciones .= '<button class="btn btn-primary" onclick="enviar_correo(\''."$reg->email".'\',\''.$reg->idventa.'\')" title="Enviar factura por correo"><i class="fa fa-at"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>';

							$opciones .= '<button class="btn btn-secondary" onclick="verificar_estado(\''.$reg->cuf.'\')" title="Verificar estado factura"><i class="fa fa-search"></i></button>';
							

			 			}else{

							if($reg->estado == "Pendiente"){//FACTURA GENERADA FUERA DE LÍNEA PENDIENTE DE VALIDACIÓN

								$opciones = '<button class="btn btn-warning" onclick="mostrar(\''.$reg->idventa.'\',\'0\')" title="Más detalles"><i class="fa fa-eye"></i></button>'.

			 					'<a target="_blank" href="'.$url.$reg->nombre_archivo.$url_generar_pdf.'"> <button class="btn btn-info" title="Reporte factura"><i class="fa fa-file-pdf-o"></i></button></a>'.

								'<a target="_blank" href="'.$url_xml.$reg->nombre_archivo.'.xml"> <button class="btn btn-success" title="Generar XML Factura"><i class="fa fa-code"></i></button></a>';

								if($reg->email != "")
									$opciones .= '<button class="btn btn-primary" onclick="enviar_correo(\''."$reg->email".'\',\''.$reg->idventa.'\')" title="Enviar factura por correo"><i class="fa fa-at"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>';


							}else{
								if($reg->estado == "Anulado"){ //FACTURAS ANULADAS EN IMPUESTOS
									$opciones = '<button class="btn btn-warning" onclick="mostrar(\''.$reg->idventa.'\',\'0\')" title="Más detalles"><i class="fa fa-eye"></i></button>'.

									'<a target="_blank" href="'.$url.$reg->nombre_archivo.$url_generar_pdf.'"> <button class="btn btn-info" title="Reporte factura"><i class="fa fa-file-pdf-o"></i></button></a>'.

									'<a target="_blank" href="'.$url_xml.$reg->nombre_archivo.'.xml"> <button class="btn btn-success" title="Generar XML Factura"><i class="fa fa-code"></i></button></a>';

									if($reg->email != "")
										$opciones .= '<button class="btn btn-primary" onclick="enviar_correo(\''."$reg->email".'\',\''.$reg->idventa.'\')" title="Notificar anulación"><i class="fa fa-at"></i> <i class="fa fa-times" aria-hidden="true"></i></button>';

									$opciones .= '<button class="btn btn-secondary" onclick="verificar_estado(\''.$reg->cuf.'\')" title="Verificar estado factura"><i class="fa fa-search"></i></button>';
								}else{//FACTURAS QUE UNA VEZ FUERON VALIDADAS, REQUIEREN SER REVISADAS Y MODIFICADAS
									$opciones = '<button class="btn btn-danger" onclick="mostrar(\''.$reg->idventa.'\',\'1\')" title="Corregir venta"><i class="fa fa-pencil"></i></button>'.

									'<a target="_blank" href="'.$url.$reg->nombre_archivo.$url_generar_pdf.'"> <button class="btn btn-info" title="Reporte factura"><i class="fa fa-file-pdf-o"></i></button></a>'.

									'<a target="_blank" href="'.$url_xml.$reg->nombre_archivo.'.xml"> <button class="btn btn-success" title="Generar XML Factura"><i class="fa fa-code"></i></button></a>';

									$opciones .= '<button class="btn btn-secondary" onclick="verificar_estado(\''.$reg->cuf.'\')" title="Verificar estado factura"><i class="fa fa-search"></i></button>';

								}
							}

			 			}


						$met_pago = "";

						//AGREGAR TRANSFERENCIA BANCARIA
						switch($reg->metodo_pago){
							case "1":
								$met_pago = "Efectivo";
							break;

							case "2":
								$met_pago = "Tarjeta";
							break;

							case "7":
								$met_pago = "Transferencia Bancaria";
							break;

							case "10":
								$met_pago = "Mixto";
							break;

							default:
								$met_pago = "Otro";
							break;
						}


						$est_venta = "";
						if($reg->estado == "Aceptado"){
							$est_venta = '<span class="label bg-green">Aceptado</span>';
						}else{
							if($reg->estado == "Pendiente"){
								$est_venta = '<span class="label bg-yellow">Pendiente</span>';
							}else{
								if($reg->estado == "Anulado")
									$est_venta = '<span class="label bg-red">Anulado</span>';
								else
									$est_venta = '<span class="label bg-black">Pendiente Corrección</span>';
							}
						}

						$num_factura = "";
						if($reg->nro_venta != "" && $reg->nro_venta != "0")
							$num_factura = $reg->nro_venta;
						else
							$num_factura = $reg->nro_venta_contingencia;
						
						$cafc = "";
						if($reg->cafc != "" && $reg->cafc != null)
							$cafc = $reg->cafc;
						else
							$cafc = "N/A";

			 			$data[]=array(

			 				"0"=>$opciones,

							"1"=>$reg->fecha_hora_registro,

			 				"2"=>$reg->fecha,

							"3"=>$num_factura,

			 				"4"=>$reg->cliente,

							"5"=>$reg->num_documento,

			 				"6"=>$reg->usuario,

			 				"7"=>$met_pago,

			 				"8"=>$reg->total_venta,

							"9"=>$cafc,

			 				"10"=>$est_venta,
						);

			 		}

			 		$results = array(

			 			"sEcho"=>1, //Información para el datatables

			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

			 			"aaData"=>$data);

			 		echo json_encode($results);

				break;

				//ANULACIÓN DE FACTURA
				case 'anular':
					//obtener el codigo del PV
					require "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$datos_pv = $pv->getPuntoVenta2($id_punto_venta);

					//obtener información de la sucursal
					require "../modelos/Informacion.php";

					$informacion = Informacion::mostrar2();

					$codigo_sucursal = $informacion["codigoSucursal"];
					$nit_emisor = $informacion["nit"];

					$codigo_ambiente = $informacion["codigoAmbiente"];
					$codigo_emision = 1;
					$codigo_sistema = $informacion["codigoSistema"];
					$codigo_modalidad = $informacion["codigoModalidad"];

					//obtener token
					$token = TOKEN;

					//obtener CUIS activo
					$cuis_activo = $pv->buscar_cuis_activo($datos_pv["id_punto_venta"]);

					if($cuis_activo){
						//obtener CUFD y dirección
						$cufd_activo = $pv->buscar_cufd_activo($datos_pv["id_punto_venta"]);

						if($cufd_activo){
							//sincronizar fecha
							require "../xml/SincronizacionDatos.php";
							$sinc = new SincronizacionDatos();
							$hora_actual = $sinc->obtenerFechaHora($token,$codigo_ambiente,$codigo_sistema,$nit_emisor,$codigo_modalidad,$codigo_sucursal,$cuis_activo["codigo_cuis"],$datos_pv["codigoPuntoVenta"]);

							//obtener venta a anular
							$venta_anular = $venta->mostrar2($nom_archivo);

							//guardar los datos realizar la anulación
							$vector_datos = [
								"codigoAmbiente" => $codigo_ambiente,
								"codigoEmision" => 1,  //siempre debe ser online
								"codigoSistema" => $codigo_sistema,
								"codigoSucursal" => $codigo_sucursal,
								"codigoMotivo" => $motivo_anulacion,
								"codigoModalidad" => $codigo_modalidad,
								"cuis" => $cuis_activo["codigo_cuis"],
								"codigoPuntoVenta" => $datos_pv["codigoPuntoVenta"],
								"cuf" => $venta_anular["cuf"],
								"tipoFacturaDocumento" => 1,
								"nit" => $nit_emisor,
								"codigoDocumentoSector" => 1,
								"cufd" =>$cufd_activo["codigo_cufd"],
								"token" => TOKEN
							];

							try{
								require "../xml/crear_xml/CrearXML.php";
								$crear_xml = new CrearXML();
								$res_anular_factura = $crear_xml->anular_factura($vector_datos);

								if(is_object($res_anular_factura)){
									if($res_anular_factura->transaccion){
										if($res_anular_factura->codigoEstado == 905){ //confirmación de anulación
											echo "<i class='fa fa-check-circle' aria-hidden='true'></i>  Factura anulada correctamente por Impuestos<br>";
											
											//registrar la anulación en la BB.DD.

											$rspta=$venta->anular($venta_anular["idventa"]);

											$detalles = $venta->listarDetalle($venta_anular["idventa"]);

											$sw = true;

											require_once "../modelos/Lote.php";

											require_once "../modelos/Articulo.php";

											$lote = new Lote();

											$articulo = new Articulo();

											while ($reg = $detalles->fetch_object()){

												$res = $lote->getStock($reg->idlote);

												$lot = $res->fetch_object();

												$dif = $lot->stock + $reg->cantidad;

												$act = $lote->setStock($reg->idlote,$dif);

												if (!$act) 
													$sw = false;

												$sql_cant_articulo = $articulo->devolverStock($reg->idarticulo);

												$cant_articulo = $sql_cant_articulo->fetch_object();

												$stock_actual = $cant_articulo->stock + $reg->cantidad;

												$sql_modif_stock = $articulo->modificarStock($reg->idarticulo,$stock_actual);

												if (!$act) 
													$sw = false;

											}

											echo $rspta && $sw ? "<i class='fa fa-check-circle' aria-hidden='true'></i>  Registro de anulación realizada en base de datos<br>" : "<i class='fa fa-times' aria-hidden='true'></i>  Venta no se puede anular en la base de datos<br>";

											//RECUPERAR INFORMACIÓN DEL CLIENTE DE LA FACTURA
											//se recuperan los datos de la BB.DD. en base al idcliente (SE TIENE QUE HACER CAMBIOS)
											require_once "../modelos/Persona.php";
											$persona = new Persona();
											$cliente = $persona->mostrar($venta_anular["idcliente"]);
											
											//ENVIAR POR CORREO LA CONFIRMACIÓN DE LA ANULACIÓN SI EL CLIENTE TIENE CORREO REGISTRADO
											//echo $cliente["email"];
											//return;
											if($cliente["email"] != ""){
												//OBTENER ÚLTIMA VENTA
												$ultima_venta = $venta->obtener_ultima_venta_cliente($cliente["idpersona"]);
												
												$id_venta_actual = $ultima_venta["idventa"];
												$resp_correo = $venta->enviar_correo($id_venta_actual,$cliente["email"]);
												if($resp_correo)
													echo "<i class='fa fa-check-circle' aria-hidden='true'></i> Correo enviado correctamente";
												else
													echo "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo enviar el correo";
											}
										}else{
											//considerar la opción de que haya más de un mensaje de parte de impuestos
											echo "<i class='fa fa-times' aria-hidden='true'></i> (".$res_anular_factura->mensajesList->codigo . "): " .$res_anular_factura->mensajesList->descripcion;
										}
									}else
										echo "<i class='fa fa-times' aria-hidden='true'></i> (".$res_anular_factura->mensajesList->codigo . "): " .$res_anular_factura->mensajesList->descripcion;
								}else
									echo "Hubo un problema en anular la factura, intente más tarde";
							}catch(Exception $e){
								echo $e->getMessage();
							}
						}else
							echo "No se puede anular, no tiene un CUFD activo";
					}else
						echo "No se puede anular, no tiene un CUIS activo";
				break;

				//MOSTRAR DATOS GENERALES DE UNA VENTA
				case 'mostrar':

					$rspta=$venta->mostrar($idventa);

					if(isset($_POST["editar"])){
						if($_POST["editar"] == "0")
							echo json_encode(["venta" => $rspta, "evento" => []]);
						else{
							//si se llama para editar la venta, se debe invocar al evento significativo y todas sus propiedades
							require "../modelos/EventoSignificativo.php";
							$es = new EventoSignificativo();
							$evento_venta = $es->obtener_ultimo_evento2($rspta["id_evento_significativo"]);
							echo json_encode(["venta" => $rspta, "evento" => $evento_venta]);
						}
					}else{
						//Codificar el resultado utilizando json
						echo json_encode(["venta" => $rspta, "evento" => []]);
					}
				break;

				//LISTAR EL DETALLE DE ARTÍCULOS VENDIDOS EN UNA VENTA
				case 'listarDetalle':

					//Recibimos el idingreso
					$id=$_GET['id'];

					$rspta = $venta->listarDetalle($id);

					$data= Array();

					$i = 1;

					$total = 0;

			 		while ($reg=$rspta->fetch_object()){

						$venc = "";

						if($reg->vencimiento == "" || $reg->vencimiento == "0000-00-00" || $reg->vencimiento == NULL || $reg->vencimiento == "0/0")
						 	$venc = $reg->cod_lote . " | n/a ";
						else
							$venc = $reg->cod_lote . " | " . $reg->vencimiento;

			 			$data[] = array(

			 				"0"=>$reg->iddetalle_venta,

			 				"1"=>"<strong>".$reg->nombre."</strong><br><small>Código: " . $reg->cod_med . "</small><br><small>Unidad Medida: " . $reg->nombreUnidadMedida . "</small>",

			 				"2"=>$reg->cantidad,

			 				"3"=>$reg->precio_venta,

			 				"4"=>$venc,

			 				"5"=>$reg->descuento,

			 				"6"=>$reg->subtotal

						);

						$i++;


						$total = $total + ($reg->precio_venta * $reg->cantidad - $reg->descuento);

			 		}

					$data[] = array(

						"0"=>"<strong>TOTAL</strong>",

					   	"1"=>"",

						"2"=>"",

						"3"=>"",

						"4"=>"",

						"5"=>"",

						"6"=>'<h4 id="total">Bs. ' . number_format((float)$total, 2, '.', '') . '</h4>'

				   );

				   $results = array(

					"aaData"=>$data);

					echo json_encode($results);

				break;

				case 'listarDetalleConsultas':
					$id=$_GET['id'];
					$rspta = $venta->listarDetalle($id);
			 		$data= Array();
					$i = 1;
					$total = 0;
			 		while ($reg=$rspta->fetch_object()){
			 			$data[] = array(
			 				"0"=>$i,
			 				"1"=>$reg->nombre,
			 				"2"=>$reg->cantidad,
			 				"3"=>$reg->precio_venta,
			 				"4"=>$reg->descuento,
			 				"5"=>round($reg->subtotal,2)
						);
						$i++;
						$total = $total + ($reg->subtotal);
			 		}
			 		$results = array(
			 			"aaData"=>$data);
			 		echo json_encode($results);
				break;

				//OBTENER TODOS LOS CLIENTES REGISTRADOS
				case 'selectCliente':
					require_once "../modelos/Persona.php";

					$persona = new Persona();

					$rspta = $persona->listarC();

					$data = Array();

					while($res = $rspta->fetch_object()){
						$data[]=array(

							"0"=>$res->idpersona,

						   	"1"=>$res->nombre,

							"2"=>$res->num_documento,

						   	"3"=>$res->email,

					   );
					}

					echo json_encode(["datos"=>$data]);

					/*

					echo "<option value='' selected>--Seleccione--</option><option value='nuevo'>NUEVO CLIENTE</option>";
					while ($reg = $rspta->fetch_object())
						echo '<option value=' . $reg->idpersona . '>' . $reg->nombre . ' - ' . $reg->num_documento . ' - ' . $reg->email . '</option>';
					*/
				break;

				case 'listarArticulosVenta':

					require_once "../modelos/Articulo.php";

					$articulo=new Articulo();



					$rspta=$articulo->listarActivosVenta();

			 		//Vamos a declarar un array

			 		$data= Array();



			 		while ($reg=$rspta->fetch_object()){

			 			$dato = "";

			 			if ($reg->stock <= $reg->stock_minimo)

			 				$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";

			 			else 

			 				$dato = $reg->stock;

			 			$data[]=array(

			 				"0"=>($reg->stock > 0) ? '<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre_comercial.'\',\''.$reg->precio_venta.'\',\''.$reg->stock.'\')"><span class="fa fa-plus"></span></button>':'<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre_comercial.'\',\''.$reg->precio_venta.'\',\''.$reg->stock.'\')" disabled><span class="fa fa-plus"></span></button>',

			 				"1"=>$reg->cod_med,

			 				"2"=>$reg->nombre_comercial,

			 				"3"=>$reg->categoria,

			 				"4"=>$reg->nombre_generico,

			 				"5"=>$reg->laboratorio,

			 				"6"=>$dato,

			 				"7"=>$reg->stock_minimo,

			 				"8"=>$reg->precio_venta,

			 				"9"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >"

			 				);

			 		}

			 		$results = array(

			 			"sEcho"=>1, //Información para el datatables

			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

			 			"aaData"=>$data);

			 		echo json_encode($results);

				break;

				case 'listarArticulosVenta2':

					require_once "../modelos/Articulo.php";

					$articulo = new Articulo();

					require_once "../modelos/Lote.php";

					$lote = new Lote();

					$rspta=$articulo->listarActivosVenta2();

			 		//Vamos a declarar un array

			 		$data= Array();

					$i =1;

			 		while ($reg=$rspta->fetch_object()){

			 			$estado = "";

						if($reg->stock == 0)

							$estado = "Agotado";

						else{

							if($reg->vencimiento != "0000-00-00" && $reg->vencimiento != "" && $reg->vencimiento != NULL && $reg->vencimiento != "0/0"){

								date_default_timezone_set("America/La_Paz");

								$fecha_vencimiento = new DateTime($reg->vencimiento);

								$actual = date("Y-m-d");

								$fecha = new DateTime($actual);

								$res = $fecha->diff($fecha_vencimiento);

								$dias = $res->days;

								if($dias >= 180 && $res->invert == 0)

									$estado = "Activo (6+)";

								else{

									if ($dias > 90 && $dias < 180 && $res->invert == 0) 

										$estado = "Activo (3+)";

									else{

										if ($dias <= 90 && $res->invert == 0) 

											$estado = "Por vencerse (3-)";

										else

											$estado = "Vencido";

									}

								}

							}else

								$estado = "n/a";

						}



						$sqllote = $lote->mostrarEstado($reg->idlote);



						$estado_lote = $sqllote->fetch_object();



						if ($estado_lote->estado != $estado)

							$lote->modificarEstado($estado,$reg->idlote);

						

						$fondo = "";

						$fondo_por_vencer1 = "";
						$fondo_por_vencer2 = "";
						

						//determinar el tipo de estado del lote

						switch ($estado) {

							case 'n/a':

								$fondo = '<span class="label bg-blue text-white">N/A</span>'; 

							break;



							case 'Activo (6+)':

								$fondo = '<span class="label bg-green text-white">Activo (6+)</span>'; 

							break;



							case 'Activo (3+)':

								$fondo = '<span class="label bg-yellow text-white">Activo (3+)</span>'; 

							break;



							case 'Por vencerse (3-)':

								$fondo = '<span class="label bg-red text-white">Por vencerse (3-)</span>'; 
								$fondo_por_vencer1 = '<div style="background-color:#faaba5;">';
								$fondo_por_vencer2 = '</div>';
							break;



							case 'Agotado':

								$fondo = '<span class="label bg-black text-white">Agotado</span>'; 

							break;



							case 'Vencido':

								$fondo = '<span class="label bg-red text-white">Lote vencido</span>'; 

							break;

						}

						

						$dato = "";

						if ($reg->stock_total <= $reg->stock_minimo)

							$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";

						else 

							$dato = $reg->stock;

						

						$data[]=array(

							"0"=>($reg->stock > 0 && $estado != "Vencido") ? '<button id="btnAdd'.$i.'" type="button" class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre_comercial.'\',\''.$reg->precio_venta.'\',\''.$reg->stock.'\',\''.$reg->idlaboratorio.'\',\''.$reg->cod_lote.'\',\''.$reg->idlote.'\',\''.$i.'\',\''.$reg->cod_med.'\',\''.$reg->codigoActividad.'\',\''.$reg->codigoProductoSin.'\',\''.$reg->unidadMedida.'\',\''.$reg->nombreUnidadMedida.'\')"><span class="fa fa-plus"></span></button>':'<button id="btnAdd'.$i.'" class="btn btn-warning" disabled><span class="fa fa-plus"></span></button>',

							"1"=>$fondo_por_vencer1.$reg->cod_med.$fondo_por_vencer2,

							"2"=>$fondo_por_vencer1.$reg->nombre_comercial.$fondo_por_vencer2,

							"3"=>$fondo_por_vencer1.$reg->nombre_generico.$fondo_por_vencer2,

							"4"=>$fondo_por_vencer1.$dato.$fondo_por_vencer2,

							"5"=>$fondo_por_vencer1.$reg->stock_minimo.$fondo_por_vencer2,

							"6"=>$fondo_por_vencer1.$reg->precio_venta.$fondo_por_vencer2,

							"7"=>$fondo_por_vencer1.$reg->laboratorio.$fondo_por_vencer2,

							"8"=>$fondo_por_vencer1.$reg->categoria.$fondo_por_vencer2,

							"9"=>$reg->vencimiento != "0000-00-00" && $reg->vencimiento != "" && $reg->vencimiento != NULL && $reg->vencimiento != "0/0" ? $reg->vencimiento . " | " . $reg->cod_lote . "<br>" . $fondo: "2099-12-31 | ".$reg->cod_lote . "<br>" . $fondo 

						);

							 

							

						$i++;		 			

			 		}

			 		$results = array(

			 			"sEcho"=>1, //Información para el datatables

			 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable

			 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

			 			"aaData"=>$data);

			 		echo json_encode($results);

				break;

				case "verificar_usuario_apertura_caja":
				    date_default_timezone_set("America/La_Paz");

					require_once "../modelos/CorrelativoCaja.php";

					$corr = new CorrelativoCaja();



					require_once "../modelos/Usuario.php";

					$usuario = new Usuario();



					$sqlContarCorr = $corr->verificar_estado_caja_usuario($_SESSION["idusuario"]);



					$registrosHoy = $sqlContarCorr->fetch_object();



					echo $registrosHoy->estado;

				break;

				case "aperturar_caja":
                    date_default_timezone_set("America/La_Paz");
					

					if($_POST["id_punto_venta_usr"] != NULL && $_POST["id_punto_venta_usr"] != "" && !empty($_POST["id_punto_venta_usr"]) && $_POST["turno"] != NULL && $_POST["turno"] != "" && !empty($_POST["turno"])){
						//ACTUALIZAR CORRELATIVO DE CAJA
						require_once "../modelos/CorrelativoCaja.php";
						$corr = new CorrelativoCaja();

						$rspta = $corr->aperturar_caja($_SESSION["idusuario"],$_POST["observaciones_inicio"],$_POST["monto_inicial"],$_POST["turno"]);

						if($rspta){

							//ACTUALIZAR PV DE USUARIO
							require_once "../modelos/Usuario.php";
							$usr = new Usuario();
							if($_POST["id_punto_venta_usr"] != NULL && $_POST["id_punto_venta_usr"] != "" && !empty($_POST["id_punto_venta_usr"])){
								$rspta2 = $usr->setColumna("id_punto_venta",$_POST["id_punto_venta_usr"],$_SESSION["idusuario"]);

								if($rspta2){
									$_SESSION["id_punto_venta"] = $_POST["id_punto_venta_usr"];

									//GENERAR NUEVO CUFD PARA LA CAJA (PV) ABIERTA
									require_once "../xml/Codigos.php";
									$cod = new Codigos();
									$rspta4 = $cod->generarCufd3($_POST["id_punto_venta_usr"]);

									//2. Si se registra correctamente en los servicios de SIN, se guarda en la BB.DD., asignando al PV el cufd creado
									if($rspta4 != false && !is_string($rspta4)){
										//ACTUALIZAR ESTADO DE USO DE PV
										require_once "../modelos/PuntoVenta.php";
										$pv = new PuntoVenta();
										$rspta3 = $pv->setColumna("enUso","1",$_POST["id_punto_venta_usr"]);
										
										$codigo_cufd = $rspta4->codigo;
										$codigo_control_cufd = $rspta4->codigoControl;
										$direccion_cufd = $rspta4->direccion;
										$fecha_vigencia_cufd = $rspta4->fechaVigencia;

										//OBTENER PV PARA REGISTRAR CUFD
										$mostrar_pv = $pv->getPuntoVenta2($_POST["id_punto_venta_usr"]);

										$res = $pv->registrar_cufd($codigo_cufd,$codigo_control_cufd,$direccion_cufd,$fecha_vigencia_cufd,$mostrar_pv["codigoPuntoVenta"]);
										if($res)
											echo json_encode(["mensaje"=>"si"]);
										else
											echo json_encode(["mensaje"=>"no5"]);
									}else
										echo json_encode(["mensaje"=>"no4"]);
								}else
									echo json_encode(["mensaje"=>"no3"]);
							}else
								echo json_encode(["mensaje"=>"no2"]);
						}else
							echo json_encode(["mensaje"=>"no1"]);
					}else
						echo json_encode(["mensaje"=>"no2"]);

				break;				

				case "cerrar_caja":

                    date_default_timezone_set("America/La_Paz");
					
					require_once "../modelos/CorrelativoCaja.php";

					$corr = new CorrelativoCaja();

					//CIERRE DE CAJA
					$rspta = $corr->cerrar_caja($_SESSION["idusuario"],$_POST["observaciones_fin"]);

					//ANULAR PUNTO DE VENTA PARA VENDEDOR(A)
					$id_usr = $_SESSION["idusuario"];
					$id_pv = $_SESSION["id_punto_venta"];
					require_once "../modelos/Usuario.php";
					$usr = new Usuario();
					$rspta2 = $usr->setColumna("id_punto_venta",NULL,$id_usr);
					$_SESSION["id_punto_venta"] = null;

					//ESTABLECER PUNTO DE VENTA COMO "DISPONIBLE" (enUso = 0)
					require_once "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					$rspta3 = $pv->setColumna("enUso","0",$id_pv);

					echo $rspta && $rspta2 && $rspta3 ? "Cierre de caja realizado satisfactoriamente" : "Hubo un problema al cerrar la caja";

				break;

				case "obtener_monto_inicial":
				    date_default_timezone_set("America/La_Paz");

					require_once "../modelos/CorrelativoCaja.php";

					$corr = new CorrelativoCaja();

					$rspta=$corr->obtener_monto_inicial($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->monto_inicial;

				break;

				case "obtener_efectivo":

					$rspta=$venta->obtener_efectivo($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;

				case "obtener_pagos_tarjeta":

					$rspta=$venta->obtener_pagos_tarjeta($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;

				case "obtener_pagos_transferencia":

					$rspta=$venta->obtener_pagos_transferencia($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;


				case "obtener_efectivo_anulado":

					$rspta=$venta->obtener_efectivo_anulado($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;

				case "obtener_tarjeta_anulado":

					$rspta=$venta->obtener_tarjeta_anulado($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;

				case "obtener_transferencia_anulado":

					$rspta=$venta->obtener_transferencia_anulado($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;

				case "obtener_descuentos":

					$rspta=$venta->obtener_descuentos($_GET["idusuario"],$_GET["fecha"]);

					$data= Array();

					$reg = $rspta->fetch_object();

					echo $reg->total;

				break;

				case "listar_cajas":
                    date_default_timezone_set("America/La_Paz");
					require_once "../modelos/CorrelativoCaja.php";

					$corr = new CorrelativoCaja();



					$fecha=$_REQUEST["fecha"];



					$rspta=$corr->listar_cajas($fecha);

					//Vamos a declarar un array

					$data= Array();

					$descuento = "";

					while ($reg=$rspta->fetch_object()){

						$estado = "";

						if($reg->estado == "Sin Abrir")

							$estado = '<span class="label bg-blue">Sin Abrir</span>';

						if($reg->estado == "Caja Abierta")

							$estado = '<span class="label bg-green">Abierta</span>';

						if($reg->estado == "Caja Cerrada")

							$estado = '<span class="label bg-red">Cerrada</span>';

						$data[]=array(

							"0"=>'<button class="btn btn-warning" onclick="mostrar_detalles_caja('.$reg->id.',\''.$reg->idusuario.'\',\''.$fecha.'\')"><i class="fa fa-eye"></i></button>',

							"1"=>$reg->fecha,

							"2"=>$reg->login,

							"3"=>$reg->hora_apertura,

							"4"=>$reg->hora_cierre,

							"5"=>$reg->turno,

							"6"=>$estado

						);

					}

					$results = array(

						"sEcho"=>1, //Información para el datatables

						"iTotalRecords"=>count($data), //enviamos el total registros al datatable

						"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

						"aaData"=>$data);

					echo json_encode($results);

				break;

				case "mostrar_detalles_caja":
				    date_default_timezone_set("America/La_Paz");

					require_once "../modelos/CorrelativoCaja.php";

					$corr = new CorrelativoCaja();

					$rspta=$corr->mostrar_detalles_caja($_GET["id"]);

			 		//Codificar el resultado utilizando json

			 		echo json_encode($rspta);

				break;

				case 'obtenerDocumentoIdentidad':
					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();
					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaTipoDocumentoIdentidad");
					$rspta = json_decode($res["sincronizarParametricaTipoDocumentoIdentidad"])->RespuestaListaParametricas;
					$cad = "<option value='' selected>--Seleccione--</option>";
					if($rspta->transaccion){
						$listaCodigos = $rspta->listaCodigos;
						for($i = 0; $i < count($listaCodigos); $i++){
							$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'">' . $listaCodigos[$i]->descripcion . '</option>';
						}
					}
					echo $cad;
				break;

				case 'obtenerMetodosPago':
					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();
					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaTipoMetodoPago");
					$rspta = json_decode($res["sincronizarParametricaTipoMetodoPago"])->RespuestaListaParametricas;
					
					$data = Array();

					if(($rspta->transaccion)){
						$listaCodigos = $rspta->listaCodigos;
						for($i = 0; $i < 18; $i++){
							if($i == 0 || $i == 1 || $i == 6 || $i == 9 || $i == 12 || $i == 17){
								$data[]=array(
									"cod"=>$listaCodigos[$i]->codigoClasificador,
		
									"desc"=>$listaCodigos[$i]->descripcion
							   	);
							}
						}
					}

					echo json_encode(["datos"=>$data]);
				
				break;

				case 'obtenerModalidad':
					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();
					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaEventosSignificativos");
					$rspta = json_decode($res["sincronizarParametricaEventosSignificativos"])->RespuestaListaParametricas;
					$cad = "<option value='0' selected>EN LÍNEA</option>";
					if($rspta->transaccion){
						$listaCodigos = $rspta->listaCodigos;
						for($i = 0; $i < count($listaCodigos); $i++){
							if($i <= 3){
								if($i == 2 || $i == 3)
									$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'" disabled>' . $listaCodigos[$i]->descripcion . '</option>';
								else
									$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'">' . $listaCodigos[$i]->descripcion . '</option>';
							}
						}
					}
					echo $cad;
				break;

				//MOTIVOS DE ANULACIÓN
				case 'obtenerMotivosAnulacion':
					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();
					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaMotivoAnulacion");
					$rspta = json_decode($res["sincronizarParametricaMotivoAnulacion"])->RespuestaListaParametricas;
					$cad = "<select class='form-control' id='motivo_anulacion'><option value='' selected>--Seleccione--</option>";
					if($rspta->transaccion){
						$listaCodigos = $rspta->listaCodigos;
						for($i = 0; $i < count($listaCodigos); $i++){
							if($listaCodigos[$i]->codigoClasificador == 1 || $listaCodigos[$i]->codigoClasificador == 3)
								$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'">' . $listaCodigos[$i]->descripcion . '</option>';
							else
								$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'" disabled>' . $listaCodigos[$i]->descripcion . '</option>';
						}
					}
					$cad .= "</select>";
					echo $cad;
				break;

				//VALIDAR PARA QUE EL EVENTO SE REGISTRE AUTOMÁTICAMENTE
				case 'verificar_comunicacion':
					require "../modelos/Informacion.php";
					//obtener token
					$token = TOKEN;

					$res_verif = GestionToken::verificar_comunicacion($token);

					
					if(!$res_verif)
						$_SESSION["modo_sistema"] = 2;
					
					echo($res_verif);
					
				break;

				case 'verificar_comunicacion2':
					require "../modelos/Informacion.php";
					//obtener token
					$token = TOKEN;

					$res_verif = GestionToken::verificar_comunicacion2($token);

					echo($res_verif);
					
				break;

				//VALIDAR PARA QUE SE REGISTRE EL EVENTO AUTOMÁTICAMENTE
				case 'verificar_conexion_internet':
					require "../modelos/Informacion.php";
					$res_verif_internet = GestionToken::verificar_conexion_internet();
					
					if(!$res_verif_internet){
						$_SESSION["modo_sistema"] = 1;
					}
					echo($res_verif_internet);
				break;

				case 'verificar_conexion_internet2':
					require "../modelos/Informacion.php";
					$res_verif_internet = GestionToken::verificar_conexion_internet2();
					if($res_verif_internet)
						echo true;
					else
						echo false;
				break;

				//CAMBIO A FUERA DE LÍNEA EN LA INTERFAZ DE VENTA
				case 'cambiar_modo_offline':

					$modo = $_GET["modo"];// lo que se selecciona o se tiene como evento activo
					$reg_es = $_GET["reg_es"]; //si es una acción que se seleccionó DE LA LISTA DE MODALIDADES 1 = seleccionado, 0 = no seleccionado
					$_SESSION["modo_sistema"] = $modo;

					require_once "../xml/SincronizacionDatos.php";
					$sinc_datos = new SincronizacionDatos();

					$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaEventosSignificativos");
					$rspta = json_decode($res["sincronizarParametricaEventosSignificativos"])->RespuestaListaParametricas;//obtención eventos 

					require_once "../modelos/EventoSignificativo.php";
					$eve_sig = new EventoSignificativo();
					
					$cafc="";

					$estado = "0";
					$mensaje = "";

					if($modo != "0" && $modo != ""){//EXISTE ALGUN EVENTO SELECCIONADO (fuera de línea del 1 al 7)

						//obtener el CUFD ACTUAL
						require "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();

						//obtener el CUFD activo cuando se de la contingencia
						$datos_cufd = $pv->buscar_cufd_activo($_SESSION["id_punto_venta"]);
						$id_cufd_activo = NULL;
						if($datos_cufd)
							$id_cufd_activo = $datos_cufd["id_cufd"];

						//ELEMENTOS A MOSTRAR CUANDO SE REGISTRE EL EVENTO (1 al 4)
						$cad = "<option value='0'>VOLVER MODALIDAD EN LÍNEA</option>";
						$nombre_evento = "";
						$hora_registro = "";
						$id_evento_actual = "";
						
						$cufd_evento = "";
						$cod_control_evento = "";
					

						if($rspta->transaccion){ //si se obtuvo bien los mensajes de eventos
							$listaCodigos = $rspta->listaCodigos;
							for($i = 0; $i < count($listaCodigos); $i++){ 
								if($i == $modo - 1){ //se obtiene el evento seleccionado para añadirlo al select
									$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'" selected>' . $listaCodigos[$i]->descripcion . '</option>';
									$nombre_evento = $listaCodigos[$i]->descripcion; //se obtiene el nombre del evento seleccionado

									//registrar evento significativo (solo si es que se usa el select)
									if($reg_es == 1){

										$fecha =  date("Y-m-d");
										$hora = date("H:i:s.v");
										
										$hora_actual = $fecha."T".$hora;

										//se registra el evento con la hora de inicio y finalización iguales (después al finalizar el evento se cambia)
										$res_reg = $eve_sig->insertar($listaCodigos[$i]->codigoClasificador,$listaCodigos[$i]->descripcion,$hora_actual,$hora_actual,date("Y-m-d H:i:s.v"),$_SESSION["id_punto_venta"],$id_cufd_activo,NULL);

										if($res_reg){//Si se inserta correctamente...
											$hora_registro = $hora_actual;//fecha_hora del evento
											
											//recuperar evento creado recientemente para el PV actual
											$res_evento_actual = $eve_sig->obtener_eventos_activos($_SESSION["id_punto_venta"]);

											if($res_evento_actual){//se obtiene el id del evento significativo recientemente creado, y luego se obtiene el CUFD del evento
												$id_evento_actual = $res_evento_actual["id_evento_significativo"];
												$cufd_evt = $pv->mostrar_cufd($res_evento_actual["id_cufd"]);
												if($cufd_evt){
													$cufd_evento = $cufd_evt["codigo_cufd"];
													$cod_control_evento = $cufd_evt["codigo_control_cufd"];
													//crear registro de paquete para evento significativo
													
													//crear carpeta 1 para guardar XML's
													$caracteres_quitar = array("T",".",":","-");
													$nueva_hora_fecha = str_replace($caracteres_quitar, "", $hora_registro);

													try{
														$nombre_carpeta = $nueva_hora_fecha."_".$_SESSION["id_punto_venta"]."_".$res_evento_actual["codigo_evento"]."_1";
														$ruta_carpeta = "../xml_qr/paquetes_contingencia/".$nombre_carpeta;
														if (!file_exists($ruta_carpeta)){
															mkdir($ruta_carpeta, 0777, true);
															$crear_pqt = $eve_sig->crear_paquete($nombre_carpeta,$res_evento_actual["id_evento_significativo"]);
															if($crear_pqt)
																$estado = "1";
														}	
													}catch(Exception $e){

													}
												}
											}
										}
									}else{
										//en caso que no se haya seleccionado ningún evento, Recuperar ultimo evento creado  para el PV actual si hubiese
										$res_evento_actual = $eve_sig->obtener_eventos_activos($_SESSION["id_punto_venta"]);
										if($res_evento_actual){//se obtiene el id del evento significativo recientemente creado
											$id_evento_actual = $res_evento_actual["id_evento_significativo"];
											$cufd_evt = $pv->mostrar_cufd($res_evento_actual["id_cufd"]);
											$cafc = $res_evento_actual["cafc_evento"];
											if($cufd_evt){
												$cufd_evento = $cufd_evt["codigo_cufd"];
												$cod_control_evento = $cufd_evt["codigo_control_cufd"];
												$estado = "1";
											}
										}else{//INGRESA AQUÍ EN CASO DE QUE SE APRETE EL BOTÓN DE CONEXIÓN DE SISTEMA
											$estado = "1";
											$nombre_evento = "GUAU";
										}
									}
									break;
								}
							}
						}

						if($estado == "1")
							$resp = ["select" => $cad, "mensaje" => $nombre_evento, "hora_registro" => $hora_registro, "id_evento_creado" => $id_evento_actual, "cufd" => $cufd_evento,"codigo_control"=>$cod_control_evento,"cafc"=>$cafc,"estado" => $estado];
						else
							$resp = ["select" => $cad, "mensaje" => "No se pudo registrar el evento, intente más tarde","cufd" => "","codigo_control"=>"","cafc"=>"", "estado" => $estado];

						echo json_encode($resp);

					}else{
						//NO SE TIENE NINGÚN EVENTO SIGNIFICATIVO ACTIVO
						/*
							1. VERIFICAR SI SE TIENE FACTURAS GENERADAS EN EL PAQUETE DURANTE EL EVENTO
							 	1.1. SI SE TIENE FACTURAS GENERADAS REGISTRAR EL EVENTO EN "SIN" (MODIFICAR LA HORA DE FINALIZACIÓN PARA EVENTOS 1 Y 2 ANTES DE REGISTRAR). 
									PARA EVENTOS 5, 6, 7 SIMPLEMENTE REGISTRAR EL EVENTO EN IMPUESTOS TAL COMO SE TIENE.
								1.2. POR CASO CONTRARIO NO SE REGISTRA EL EVENTO EN "SIN", SOLO SE HACE UN REGISTRO INTERNO (NO SEGUIR DESDE PASO 2)
								1.3. PARA EL EVENTO 1 SE ENVIAN LAS FACTURAS POR CORREO LUEGO DE PONER EL SISTEMA "EN LÍNEA", PARA EL RESTO DE EVENTOS SE ENVÍA LA FACTURA POR CORREO
									DE FORMA INSTANTANEA.

							2.	UNA VEZ REGISTRADO EL EVENTO...
								2.1. SI SE REGISTRÓ CORRECTAMENTE CONTINUAR CON EL ENVÍO DE PAQUETE
								2.2. SI EXISTE ALGÚN PERCANCE VOLVER A EDITAR INFORMACIÓN DEL EVENTO PARA INTENTAR ENVIAR NUEVAMENTE(NO SEGUIR DESDE PASO 3)

							3.  ENVÍO DE PAQUETE...
								3.1. SI SE ENVÍA EL PAQUETE CORRECTAMENTE DEBE EMITIRSE EL MENSAJE "PENDIENTE DE REVISIÓN"
								3.3. POR CASO CONTRARIO SE MANDARÁ UN MENSAJE INDICANDO QUE NO SE PUDO ENVIAR EL MENSAJE (NO SEGUIR DESDE PASO 4)

							4. VALIDACIÓN DE PAQUETE...
								4.1. SI NO EXISTEN OBSERVACIONES EN LA VALIDACIÓN, SE ACEPTA EL PAQUETE Y SE ESTABLECE LA MODALIDAD "EN LÍNEA"
								4.2. SI EXISTE ALGUNA OBSERVACIÓN SE DEBE PROCEDER A CORREGIR LA(S) FACTURA(S) MAL EMITIDA(S). MANTENER EL SISTEMA "FUERA DE LÍNEA"
									CON SU RESPECTIVO EVENTO
							
						*/

						//Solo se ingresa a esta parte si se tiene algún evento previamente establecido
						$cad = "<option value='0'>EN LÍNEA</option>";
		
						if($rspta->transaccion){
							//LISTA DE CONTINGENCIAS HASTA LA 4TA
							$listaCodigos = $rspta->listaCodigos;
							for($i = 0; $i < count($listaCodigos); $i++){
								if($i <= 3){
									if($i == 0 || $i == 1)
										$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'">' . $listaCodigos[$i]->descripcion . '</option>';
									else
										$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'" disabled>' . $listaCodigos[$i]->descripcion . '</option>';
								}
							}

							//SE TIENE QUE TERMINAR DE REGISTRAR EL EVENTO (HORA FINALIZACIÓN) EN LA BASE DE DATOS SI $reg_es == 1
							if($reg_es == 1){
								$res_evento_actual = $eve_sig->obtener_eventos_activos($_SESSION["id_punto_venta"]);
								if($res_evento_actual){

									$id_evento_actual = $res_evento_actual["id_evento_significativo"];
									if($res_evento_actual["codigo_evento"] != 5 && $res_evento_actual["codigo_evento"] != 6 && $res_evento_actual["codigo_evento"] != 7)//PARA LOS EVENTOS 1 y 2
										$res_act = $eve_sig->setColumna("fecha_fin_evento",date("Y-m-d")."T".date("H:i:s.v"),$id_evento_actual);
									//$res_act = $eve_sig->setColumna("estado_evento","2",$id_evento_actual);

									//PASO 1.
									//OBTENER EL PAQUETE DEL EVENTO SIGNIFICATIVO
									$paquete = $eve_sig->obtener_ultimo_paquete_es($res_evento_actual["id_evento_significativo"]);

									if($paquete){//SI EL PAQUETE FUE HALLADO
										//PASO 1.1.
										//SI EL NRO DE FACTURAS ES MAYOR A 0 SIGNIFICA QUE SE PUEDE REGISTRAR EL EVENTO Y ENVIAR EL PAQUETE
										if($paquete["actual"] != 0){
											//OBTENER EL PUNTO DE VENTA DEL EVENTO
											require_once "../modelos/PuntoVenta.php";
											$pv = new PuntoVenta();

											$pv_actual = $pv->getPuntoVenta2($_SESSION["id_punto_venta"]);
											if($pv_actual){
												//OBTENER EL NOMBRE DE LA CARPETA DONDE SE ENCUENTRAN LOS XML's
												$nombre_carpeta = $paquete["nombre_paquete"];
												$ruta = "../xml_qr/paquetes_contingencia/".$nombre_carpeta;
												//GUARDAR EN UN ARRAY EL NOMBRE DE LOS ARCHIVOS XML VERIFICANDO PRIMERO SI LA RUTA ES VÁLIDA
												$array_nombres_xml = [];
												try{
													// Abrir la carpeta que se pasa como parámetro
													if(!is_dir($ruta)){
														//INGRESA SI NO SE ENCUENTRA LA RUTA DEL PAQUETE (NUNCA DEBERÍA INGRESAR AQUÍ YA QUE AL CREAR EL PAQUETE SE DEBE CREAR LA CARPETA EN EL SERVIDOR)
														$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> No se pudo encontrar el paquete";
													}else{
														$dir = opendir($ruta);
														// Leer todos los ficheros de la carpeta
														while ($elemento = readdir($dir)){
															// Tratar los elementos . y .. que tiene la carpeta
															if( $elemento != "." && $elemento != ".."){
																// Si NO es una carpeta
																if( !is_dir($ruta.$elemento) ){
																	// obtener el nombre del archivo si tiene extensión .xml
																	if(strpos($elemento,".xml"))
																		array_push($array_nombres_xml,$elemento);
																} 
															}
														}
														
														//ORDENA ARRAY DE NOMBRES EN BASE AL ORDEN DE CREACIÓN (NOMBRE)
														sort($array_nombres_xml);



														//OBTENER EL CUFD Y CÓDIGO DE CONTROL DEL EVENTO
														$obj_cufd_evento = $pv->mostrar_cufd($res_evento_actual["id_cufd"]);




														if($obj_cufd_evento){
															$cufd_evento = $obj_cufd_evento["codigo_cufd"];
															$cod_control_evento = $obj_cufd_evento["codigo_control_cufd"];
				
															//OBTENER INFORMACIÓN GENERAL DEL SISTEMA PARA ENVIAR  
															require_once "../modelos/Informacion.php";
															$cuis_actual = $pv->buscar_cuis_activo($pv_actual["id_punto_venta"]);

															$token = TOKEN;


															
															$cod_ambiente = Informacion::mostrar2()["codigoAmbiente"];
															$cod_modalidad = Informacion::mostrar2()["codigoModalidad"];
															$cod_sistema = Informacion::mostrar2()["codigoSistema"];
															$cod_sucursal = Informacion::mostrar2()["codigoSucursal"];
															$nit = Informacion::mostrar2()["nit"];
															$cuis = $cuis_actual["codigo_cuis"];
															$cod_punto_venta = $pv_actual["codigoPuntoVenta"];
				
															$cod_doc_sector = 1;//FACTURA COMPRA VENTA
															$cod_emision = 2;//OFFLINE
															$tipo_factura = 1;//FACTURA CON DERECHO A CREDITO FISCAL
															$cafc_evento = $res_evento_actual["cafc_evento"];
															$cant_facturas = count($array_nombres_xml);//cantidad de facturas en el array
															$cod_evento = $res_evento_actual["codigo_evento"];//DEL 1 AL 7
				


															//LLAMAR A SERVICIO PARA GENERAR UN NUEVO CUFD
															require_once "../xml/Codigos.php";
															$codigos = new Codigos();

															//GENERAR CUFD
															$obj_nuevo_cufd = $codigos->generarCufd2($cod_punto_venta,$cod_ambiente,$cod_modalidad,$cod_sistema,$cod_sucursal,$cuis,$nit,$token);

															if(is_object($obj_nuevo_cufd)){
																//SI LA TRANSACCION ES CORRECTA, SE ALMACENA EL NUEVO CUFD EN LA BB.DD, Y SE LO UTILIZA PARA LAS ACCIONES SIGUIENTES
																if($obj_nuevo_cufd->transaccion){
																	$cufd_actual = $obj_nuevo_cufd->codigo;
																	$cod_control_actual = $obj_nuevo_cufd->codigoControl;
																	$direccion_actual = $obj_nuevo_cufd->direccion;
																	$fecha_vigencia_actual = $obj_nuevo_cufd->fechaVigencia;

																	//GUARDADO EN LA BB.DD.
																	$res_cufd_nuevo = $pv->registrar_cufd($cufd_actual,$cod_control_actual,$direccion_actual,$fecha_vigencia_actual,$cod_punto_venta);

																	if($res_cufd_nuevo){

																		$res_evento_actual2 = $eve_sig->obtener_eventos_activos($_SESSION["id_punto_venta"]);

																		//REGISTRAR EVENTO EN IMPUESTOS
																		$descripcion_evento = $res_evento_actual2["descripcion_evento"];
																		$fecha_inicio_evento = $res_evento_actual2["fecha_inicio_evento"];
																		$fecha_fin_evento = $res_evento_actual2["fecha_fin_evento"];

																		$res_act2 = $eve_sig->setColumna("estado_evento","2",$id_evento_actual);


																		/*
																		if($cod_evento == "1" || $cod_evento == "2" || $cod_evento == "3" || $cod_evento == "4"){
																			$fecha_fin_evento = date("Y-m-d")."T".date("H:i:s.v");
																		}else
																			$fecha_fin_evento = $res_evento_actual["fecha_fin_evento"];
																		*/
				


																		//PASO 2.
																		require_once "../xml/Operaciones.php";
																		$ope = new Operaciones();
																		$res_registro_evento = $ope->registrar_evento_significativo($token,$cod_ambiente,$cod_evento,$cod_punto_venta,$cod_sistema,$cod_sucursal,$cufd_actual,$cufd_evento,$cuis,$descripcion_evento,$fecha_inicio_evento,$fecha_fin_evento,$nit);


																		//PASO 2.1. 
																		if(is_object($res_registro_evento)){



																			if($res_registro_evento->transaccion){

																				
																				
																				$cod_recepcion_es = $res_registro_evento->codigoRecepcionEventoSignificativo;


																				//MENSAJE INDICANDO QUE EL EVENTO FUE ENVIADO CORRECTAMENTE
																				$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Evento registrado correctamente. Código de recepción: $cod_recepcion_es<br>";

																				//UNA VEZ SE TENGA EL CÓDIGO DE RECEPCIÓN SE INVOCA AL MÉTODO PARA ENVIAR EL PAQUETE
																				$vector_datos = [
																					"codigoAmbiente"=>$cod_ambiente,
																					"codigoDocumentoSector"=>$cod_doc_sector,
																					"codigoEmision"=>$cod_emision,
																					"codigoModalidad"=>$cod_modalidad,
																					"codigoPuntoVenta"=>$cod_punto_venta,
																					"codigoSistema"=>$cod_sistema,
																					"codigoSucursal"=>$cod_sucursal,
																					"cufd"=>$cufd_actual,
																					"cuis"=>$cuis,
																					"nit"=>$nit,
																					"tipoFacturaDocumento"=>$tipo_factura,
																					"cafc"=>$cafc_evento,
																					"cantidadFacturas"=>$cant_facturas,
																					"codigoEvento"=>$cod_evento,
																					"codigoRecepcion"=>$cod_recepcion_es
																				];
																			
																				sleep(2);

																				
																				//PASO 3.
																				//FECHA Y HORA ENVÍO PAQUETE
																				$fecha_hora = date("Y-m-d")."T".date("H:i:s.v");
																				require "../xml/crear_xml/CrearXML.php";
																				$recep_pqt = new CrearXML();
																				$res_recep_pqt = $recep_pqt->recepcion_paquete_facturas($token,$vector_datos,$array_nombres_xml,$ruta,$nombre_carpeta,$fecha_hora);
																				


																				
																				if(is_object($res_recep_pqt)){
																					$res = $res_recep_pqt->RespuestaServicioFacturacion;
																					if($res->transaccion){



																						//PASO 3.1.
																						if(count($array_nombres_xml) != 0){
																							//EL CÓDIGO DE RECEPCIÓN DEL PAQUETE Y DEL EVENTO DEBEN GUARDARSE EN LA BASE DE DATOS
																							$cod_recepcion = $res->codigoRecepcion;
																							$res_actl_pqt = $eve_sig->setColumnaPaquete("codigo_recepcion_paquete",$cod_recepcion,$paquete["id_paquete"]);
																							$res_actl_pqt_est = $eve_sig->setColumnaPaquete("estado_paquete",2,$paquete["id_paquete"]);//PENDIENTE
																							$res_actl_pqt_intento = $eve_sig->setColumnaPaquete("intento",1,$paquete["id_paquete"]); //SE ACTUALIZA EL INTENTO EN EL CUAL SE ENVÍA TODO EL PAQUETE
																							$res_actl_es = $eve_sig->setColumna("codigo_recepcion_evento_significativo",$cod_recepcion_es,$res_evento_actual["id_evento_significativo"]);
																							$res_actl_es_est = $eve_sig->setColumna("estado_evento",3,$res_evento_actual["id_evento_significativo"]);//PARA VALIDACIÓN
																							if($res_actl_pqt && $res_actl_es && $res_actl_pqt_est && $res_actl_es_est){

																								//PASO 4.
																								if($res->codigoEstado == "901"){
																									//MENSAJE INDICANDO QUE EL PAQUETE FUE ENVIADO CORRECTAMENTE SE ENCUENTRA EN REVISIÓN
																									$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> (".$res->codigoDescripcion.") Paquete de facturas enviado y pendiente de revisión en impuestos<br>";

																									//REALIZACIÓN DE VALIDACIÓN DE PAQUETE

																									$paquete2 = $eve_sig->obtener_ultimo_paquete_es($res_evento_actual["id_evento_significativo"]);
																									$codigo_recepcion_paquete = $paquete2["codigo_recepcion_paquete"];


																									//UNA VEZ SE TENGA EL CÓDIGO DE RECEPCIÓN SE INVOCA AL MÉTODO PARA VALIDAR EL PAQUETE
																									$vector_datos = [
																										"codigoAmbiente"=>$cod_ambiente,
																										"codigoDocumentoSector"=>$cod_doc_sector,
																										"codigoEmision"=>$cod_emision,
																										"codigoModalidad"=>$cod_modalidad,
																										"codigoPuntoVenta"=>$cod_punto_venta,
																										"codigoSistema"=>$cod_sistema,
																										"codigoSucursal"=>$cod_sucursal,
																										"cufd"=>$cufd_actual,
																										"cuis"=>$cuis,
																										"nit"=>$nit,
																										"tipoFacturaDocumento"=>$tipo_factura,
																										"codigoRecepcion"=>$codigo_recepcion_paquete
																									];
																								
																									sleep(2);

																									//FECHA Y HORA ENVÍO PAQUETE PARA VALIDACIÓN
																									$fecha_hora = date("Y-m-d")."T".date("H:i:s.v");
																	
																									$res_val_pqt = $recep_pqt->validar_recepcion_paquete_facturas($token,$vector_datos,$fecha_hora);
																	
																									if(is_object($res_val_pqt)){

																										$rpqt = $res_val_pqt->RespuestaServicioFacturacion;
																										if($rpqt->transaccion){
																											require_once "../modelos/Venta.php";
																											$venta = new Venta();
																											//OBTENER TODAS LAS VENTAS EN BASE AL NOMBRE DEL PAQUETE
																											$ventas_paquete = $venta->mostrar3($nombre_carpeta);
																											
																											/*
																												CÓDIGOS PARA EVENTO
																													0 = GENERADO
																													1 = FINALIZADO
																													2 = PEDIENTE REGISTRO EVENTO Y ENVÍO PAQUETE
																													3 = PENDIENTE VALIDACIÓN
																													4 = PENDIENTE REENVÍO PAQUETE
																											
																												CÓDIGOS PAQUETE
																													0 = CREADO
																													1 = FINALIZADO
																													2 = PENDIENTE
																											
																												SI EL CÓDIGO DE ESTADO ES 908... 
																													1. TODAS LAS VENTAS SE PONEN COMO ESTADO ACEPTADO Y SE PONE EL CÓDIGO DE RECEPCIÓN EN LA VENTA
																													2. EL ESTADO DEL PAQUETE Y DEL EVENTO SE CAMBIAN A 1 EN AMBOS CASOS
																													3. SE ENVIAN POR CORREO (SI ES QUE TIENEN) A TODOS LOS CLIENTES QUE TIENEN VENTAS EN EL "ES" 1
																											
																												SI EL CÓDIGO DE ESTADO ES 904 (OBSERVADO)...
																													1. SE VERIFICA EN BASE A LA LISTA DE OBSERVACIONES DE CADA ARCHIVO Y SE MANTIENE COMO PENDIENTE DE ARREGLO
																													2. LOS QUE NO TIENEN OBSERVACIONES SE CAMBIAN A ACEPTADO Y SE PONE EL CÓDIGO DE RECEPCIÓN EN LA VENTA
																													3. ES ESTADO DEL PAQUETE SE MANTIENE EN 2, Y EL ESTADO DEL EVENTO CAMBIA A 4 (REENVIAR)
																													4. SOLO SE ENVÍA POR CORREO (SI ES QUE TIENEN) A AQUELLOS CLIENTES CUYA VENTA SE ENCUENTRE CORRECTAMENTE VALIDADA
																											*/

																											if($rpqt->codigoEstado == 908){
																												$sw = true;
																												while($vq = $ventas_paquete->fetch_object()){
																													$act_cr = $venta->setColumnaVenta("codigo_recepcion",$rpqt->codigoRecepcion,$vq->idventa);
																													$act_estado = $venta->setColumnaVenta("estado","Aceptado",$vq->idventa);
																													//OBTENER EL CORREO DEL CLIENTE (SI TUVIESE) EN CASO QUE EL CODIGO DE EVENTO SEA 1 (SIN INTERNET)
																	
																													if($cod_evento == "1"){
																														$num_venta = "";
																														if($vq->nro_venta != "" && $vq->nro_venta != "0")
																															$num_venta = $vq->nro_venta;
																														else{
																															if($vq->nro_venta_contingencia != "" && $vq->nro_venta_contingencia != "0")
																																$num_venta = $vq->nro_venta_contingencia;
																														}
																		
																														if($vq->email != ""){
																															//OBTENER ÚLTIMA VENTA													
																															$resp_correo = $venta->enviar_correo($vq->idventa,$vq->email);
																															if($resp_correo)
																																$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Venta Nro. $num_venta: Correo enviado correctamente<br>";
																															else
																																$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Venta Nro. $num_venta: No se pudo enviar el correo<br>";
																														}
																													}

																													if(!$act_cr || !$act_estado){
																														$sw = false;
																														$mensaje .= "Venta " . $vq->idventa . " no se pudo actualizar<br>";
																													}
																												}

																												if($sw){
																													$act_estado_es = $eve_sig->setColumna("estado_evento",1,$res_evento_actual["id_evento_significativo"]);
																													$act_estado_pqt = $eve_sig->setColumnaPaquete("estado_paquete",1,$paquete["id_paquete"]);
																													if(!$act_estado_es || !$act_estado_pqt)
																														$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Estado de evento y/o paquete no se actualizaron correctamente<br>";
																													else{
																														$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Validación de paquete realizada correctamente";
																														$estado = 1;
																													}
																												}
																											}else{
																												if($rpqt->codigoEstado == 904){
																													$mensaje .= "<br>*** Hubo errores en algunas de las facturas validadas del paquete que debe corregir ***<br>";
																													
																													$array_errores = $rpqt->mensajesList;
																													
																												
																													$sw = true;
																													$i = 0;
																													$sw1 = true; //para ver estado del paquete
																													//RECORRER TODAS LAS VENTAS DEL PAQUETE
																													while($vq = $ventas_paquete->fetch_object()){
																														$sw = true;//si sw se mantiene true quiere decir que la venta es correcta
																	


																														/*
																														//Verificar si es venta normal o de contingencia
																														$nro_venta = "";
																														if($vq->nro_venta != "" && $vq->nro_venta != 0 && $vq->nro_venta != null)
																															$nro_venta = $vq->nro_venta;
																														else
																															$nro_venta = $vq->nro_venta_contingencia;
																														*/





																														if($cod_evento == "1"){
																															$num_venta = "";
																															if($vq->nro_venta != "" && $vq->nro_venta != "0")
																																$num_venta = $vq->nro_venta;
																															else{
																																if($vq->nro_venta_contingencia != "" && $vq->nro_venta_contingencia != "0")
																																	$num_venta = $vq->nro_venta_contingencia;
																															}
																			
																															if($vq->email != ""){
																																//OBTENER ÚLTIMA VENTA													
																																$resp_correo = $venta->enviar_correo($vq->idventa,$vq->email);
																																if($resp_correo)
																																	$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Venta Nro. $num_venta: Correo enviado correctamente<br>";
																																else
																																	$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Venta Nro. $num_venta: No se pudo enviar el correo<br>";
																															}
																														}










																														$mensaje_venta = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Errores factura Nro. " . $num_venta . ": <br>";//mensaje a concatenar en caso de hallar error(es) para la venta
																														//se recorre el array de errores por cada venta para verificar si tiene errores
																														$k = 1; //nro de errores por factura
																														//si se tiene más de una observación se lo considera como array
																														if(is_array($array_errores)){
																															for($j = 0; $j < count($array_errores); $j++){
																																//si existe coincidencia con el numeroArchivo
																																if($array_errores[$j]->numeroArchivo == $i){
																																	$sw = false;
																																	$mensaje_venta .= $k . ". (Error " .  $array_errores[$j]->codigo . ") " . $array_errores[$j]->descripcion . "<br>";
																																	$k++;
																																}
																															}
																														}else{
																															if($array_errores->numeroArchivo == $i){
																																$mensaje_venta .= "(Error " .  $array_errores->codigo . ") " . $array_errores->descripcion . "<br>";
																																$sw = false;
																															}
																														}
																	
																														if(!$sw){
																															$mensaje .= $mensaje_venta;
																															//actualizar venta poniendo código de recepción y cambiando estado a "Pendiente Correccion"
																															$act_cr = $venta->setColumnaVenta("codigo_recepcion",$rpqt->codigoRecepcion,$vq->idventa);
																															$act_estado = $venta->setColumnaVenta("estado","Pendiente Correccion",$vq->idventa);
																															if(!$act_cr || !$act_estado){
																																$sw1 = false;
																																$mensaje .= "Venta " . $nro_venta . " no se pudo actualizar<br>";
																															}
																														}else{
																															//si la venta no tiene errores, igual se actualiza poniendo código de recepción y cambiando esta a "Aceptado"
																															$act_cr = $venta->setColumnaVenta("codigo_recepcion",$rpqt->codigoRecepcion,$vq->idventa);
																															$act_estado = $venta->setColumnaVenta("estado","Aceptado",$vq->idventa);
																															if(!$act_cr || !$act_estado){
																																$sw1 = false;
																																$mensaje .= "Venta " . $nro_venta . " no se pudo actualizar<br>";
																															}
																														}
																														$mensaje_venta = "";
																	
																														$i++;
																													}
																	
																													if($sw1){
																														$act_estado_es = $eve_sig->setColumna("estado_evento",4,$res_evento_actual["id_evento_significativo"]);
																														$act_estado_pqt = $eve_sig->setColumnaPaquete("estado_paquete",2,$paquete["id_paquete"]);
																														if(!$act_estado_es || !$act_estado_pqt)
																															$mensaje .= "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Estado de evento y/o paquete no se actualizaron correctamente<br>";
																													}
																													
																												}else
																													$mensaje .= "<br><i class='fa fa-times' aria-hidden='true'></i> Hubo un problema en la verificación de SIN";
																											}
																								
																										}else{
																											
																											$mensaje .= "-- VALIDACIÓN DE PAQUETE --<br>";
																											if(is_array($rpqt->mensajesList)){
																												for($j = 0; $j < count($rpqt->mensajesList); $j++)
																													$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> ".($j+1). ". (Error ".$rpqt->mensajesList[$j]->codigo .") ".$rpqt->mensajesList[$j]->descripcion . "<br>"; 
																											}else
																												$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> (Error ".$rpqt->mensajesList->codigo .") ".$rpqt->mensajesList->descripcion;
																										}
																										
																									}else  //PASO 4.2.
																										$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error en validar paquete a SIN";	




























																									
																								}else{ 
																									//NO TENDRIÁ QUE INGRESAR AQUÍ
																									$mensaje .= "-- ENVIÓ DE PAQUETE --<br><i class='fa fa-times' aria-hidden='true'></i> Hubo un problema en el envío del paquete a SIN";
																								}



																							}else
																								$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al guardar algunos de los códigos de recepción en la BASE DE DATOS";
									

																						}else	//PASO 3.2.
																							$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error, no se encontraron facturas para enviar";



																					}else{
																						//PASO 3.2.
																						//SE MUESTRA LOS POSIBLES ERRORES UNA VEZ ENVIADO EL PAQUETE
																						$mensaje .= "-- ERROR EN ENVIÓ DE PAQUETE --<br>";
																						if(is_array($res->mensajesList)){
																							for($j = 0; $j < count($res->mensajesList); $j++)
																								$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> ".($j+1). ". (Error ".$res->mensajesList[$j]->codigo .") ".$res->mensajesList[$j]->descripcion . "<br>"; 
																						}else
																							$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> (Error ".$res->mensajesList->codigo .") ".$res->mensajesList->descripcion;
																					}



																				}else	//PASO 3.2.
																					$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error en enviar paquete a SIN";
																				



																			}else{
																				//MENSAJE CON CÓDIGOS DE IMPUESTOS INDICANDO EL PORQUE NO SE REGISTRÓ EL EVENTO
																				$mensaje .= "-- ERROR EN REGISTRO DE EVENTO --<br>";
																				if(is_array($res_registro_evento->mensajesList)){
																					for($j = 0; $j < count($res_registro_evento->mensajesList); $j++)
																						$mensaje .= "".($j+1). ". <i class='fa fa-times' aria-hidden='true'></i> (Error ".$res_registro_evento->mensajesList[$j]->codigo .") ".$res_registro_evento->mensajesList[$j]->descripcion . "<br>"; 
																				}else
																					$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> (Error ".$res_registro_evento->mensajesList->codigo .") ".$res_registro_evento->mensajesList->descripcion;
																			}







																		}else	//PASO 2.2.
																			$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al registrar el Evento Significativo en Impuestos!!";
																	}else
																		$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al registrar el nuevo CUFD en la Base de Datos";	
																}else
																	$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al obtener el nuevo CUFD";
															}else
																$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al obtener el nuevo CUFD";
														}else{
															//EN TEORÍA NUNCA TENDRÍA QUE INGRESAR A ESTE SECTOR YA QUE DEBE EXISTIR UN CUFD PARA HACER CUALQUIER VENTA
															$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al obtener CUFD actual";
														}
													}
												}catch(Exception $e){
													$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al REGISTRAR EVENTO Y/O ENVIAR PAQUETES";
												}
											}else{
												//EN TEORÍA NUNCA TENDRÍA QUE INGRESAR A ESTE SECTOR YA QUE EL PUNTO DE VENTA DEBE ENCONTRARSE PRESENTE AL HACER VENTAS
												$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al encontrar PUNTO DE VENTA, intente más tarde";
											}
										}else{
											//PASO 1.2.
											//NO SE TIENEN FACTURAS EMITIDAS, POR TANTO NO SE REGISTRA EL EVENTO EN IMPUESTOS, SOLO EN LA BB.DD.
											$estado = "1";
											$mensaje .= "<i class='fa fa-check-circle' aria-hidden='true'></i> Evento registrado internamente, ninguna factura almacenada!";
											$ces = $eve_sig->setColumna("estado_evento","1",$id_evento_actual);//cambia el estado a finalizado
										}
									}else{ //EN TEORÍA NUNCA TENDRÍA QUE INGRESAR A ESTE SECTOR YA QUE AL CREAR UN "ES", TAMBIÉN SE GENERA EL PAQUETE CORRESPONDIENTE, Y ESTE DEBE SER HALLADO AL PASAR AL MODO "EN LÍNEA"
										$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error al HALLAR EL PAQUETE";
										$ces = $eve_sig->setColumna("estado_evento","1",$id_evento_actual);//cambia el estado a finalizado
									}
								}else
									$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error, no se encontró ningún evento significativo VIGENTE";			
							}else{
								$mensaje .= "<i class='fa fa-times' aria-hidden='true'></i> Error, se cambió a modo EN LÍNEA sin seleccionar previamente";
								//NO TENDRÍA QUE CAMBIARSE AUTOMÁTICAMENTE SI SE INGRESÓ A ALGÚN EVENTO SIGNIFICAITVO
								//$estado = "1";
							}
						}


						
						
						$resp = ["select" => $cad, "mensaje" =>$mensaje, "estado" => $estado,"cafc"=>""];
						
						echo json_encode($resp);
					}

				break;




				
				/*
				//Solo se ingresa a esta parte si se tiene algún evento previamente establecido
				$cad = "<option value='0'>EN LÍNEA</option>";
				//$id_evento_actual = "";
				$estado = "0";

				if($rspta->transaccion){
					$listaCodigos = $rspta->listaCodigos;
					for($i = 0; $i < count($listaCodigos); $i++){
						if($i <= 3)
							$cad .= '<option value="'.$listaCodigos[$i]->codigoClasificador.'">' . $listaCodigos[$i]->descripcion . '</option>';
					}

					//SE TIENE QUE TERMINAR DE REGISTRAR EL EVENTO (HORA FINALIZACIÓN) EN LA BASE DE DATOS SI $reg_es == 1
					if($reg_es == 1){
						$res_evento_actual = $eve_sig->obtener_eventos_activos($_SESSION["id_punto_venta"]);
						if($res_evento_actual){
							$id_evento_actual = $res_evento_actual["id_evento_significativo"];
							if($res_evento_actual["codigo_evento"] != 5 && $res_evento_actual["codigo_evento"] != 6 && $res_evento_actual["codigo_evento"] != 7)
								$res_act = $eve_sig->setColumna("fecha_fin_evento",date("Y-m-d")."T".date("H:i:s.v"),$id_evento_actual);
							$res_act = $eve_sig->setColumna("estado_evento","2",$id_evento_actual);
							if($res_act){
								$cafc = "";
								$estado = "1";
								//$resp = ["select" => $cad, "mensaje" => "Evento registrado correctamente", "estado" => $estado];
							}else{
								$estado = "0";
							}
						}else{
							$estado = "0";
						}
						
					}else{
						//NO TENDRÍA QUE CAMBIARSE AUTOMÁTICAMENTE SI SE INGRESÓ A ALGÚN EVENTO SIGNIFICAITVO
						//$estado = "1";
					}
				}
				*/





				//OBTENER UN NUEVO CUIS EN LA INTERFAZ DE VENTA
				case 'generar_cuis':
					//1. registrar el cuis nuevo con los servicios de SIN
					require_once "../xml/Codigos.php";
					$cod = new Codigos();
					$cod_pv = $_GET["cod_pv"];
					$rspta = $cod->generarCuis($cod_pv);
					//2. Si se registra correctamente en los servicios de SIN, se guarda en la BB.DD., asignando al PV el cuis creado
					if($rspta != false && !is_string($rspta)){
						require_once "../modelos/PuntoVenta.php";
						$pv = new PuntoVenta();
						$codigo_cuis = $rspta->codigo;
						$fecha_vigencia = $rspta->fechaVigencia;
						$res = $pv->registrar_cuis($codigo_cuis,$fecha_vigencia,$cod_pv);
						echo $res ? "1":"0";
						//echo $res ? "Registro de CUIS realizado correctamente":"ERROR EN REGISTRAR CUIS EN BB.DD.";
					}else{
						echo "0";
						/*
						if($rspta == false)
							echo "ERROR EN GENERAR CUIS EN SERVICIOS DE SIN";
						else
							echo $rspta;
						*/
					}
				break;

				//VERIFICAR SI NIT ES VÁLIDO O NO
				case 'verificar_nit':
					$mensaje = "";
					$estado = "0";

					$dato_nit = $_GET["dato_nit"];
					
					//obtener información de la sucursal
					require "../modelos/Informacion.php";
					$informacion = Informacion::mostrar2();

					$codigo_ambiente = $informacion["codigoAmbiente"];
					$codigo_modalidad = $informacion["codigoModalidad"];
					$codigo_sistema = $informacion["codigoSistema"];
					$codigo_sucursal = $informacion["codigoSucursal"];
					$nit = $informacion["nit"];

					//CUIS actual
					require "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();
					
					$cuis_activo = $pv->buscar_cuis_activo($_SESSION["id_punto_venta"]);

					if($cuis_activo){
						//obtener token
						$token = TOKEN;

						if($token){
							$vector_datos = [
								"codigoAmbiente" => $codigo_ambiente,
								"codigoModalidad" => $codigo_modalidad,
								"codigoSistema" => $codigo_sistema,
								"codigoSucursal" => $codigo_sucursal,
								"cuis" => $cuis_activo["codigo_cuis"],
								"nit" => $nit,
								"nitParaVerificacion" => $dato_nit
							];

							require_once "../xml/Codigos.php";
							$cod = new Codigos();

							$res_verif = $cod->verificar_nit($token,$vector_datos);

							if(is_object($res_verif)){
								
								if(!$res_verif->RespuestaVerificarNit->transaccion){
									//se verifica las posibles observaciones que se den (quizá nunca pase por aquí)
									if(is_array($res_verif->RespuestaVerificarNit->mensajesList)){
										for($j = 0; $j < count($res_verif->RespuestaVerificarNit->mensajesList); $j++)
											$mensaje .= ($j+1). ". (".$res_verif->RespuestaVerificarNit->mensajesList[$j]->codigo .") ".$res_verif->RespuestaVerificarNit->mensajesList[$j]->descripcion . "<br>"; 
									}else
										$mensaje = "(".$res_verif->RespuestaVerificarNit->mensajesList->codigo .") ".$res_verif->RespuestaVerificarNit->mensajesList->descripcion;
								}else{
									if(is_array($res_verif->RespuestaVerificarNit->mensajesList)){
										for($j = 0; $j < count($res_verif->RespuestaVerificarNit->mensajesList); $j++)
											$mensaje .= ($j+1). ". (".$res_verif->RespuestaVerificarNit->mensajesList[$j]->codigo .") ".$res_verif->RespuestaVerificarNit->mensajesList[$j]->descripcion . "<br>"; 
									}else{
										if($res_verif->RespuestaVerificarNit->mensajesList->codigo != 986)
											$mensaje = "(".$res_verif->RespuestaVerificarNit->mensajesList->codigo .") ".$res_verif->RespuestaVerificarNit->mensajesList->descripcion;
										else
											$estado = "1";
									}
									
									//$estado = "1";
								}
							}else
								$mensaje = "Hubo un problema al buscar el nit";						
						}else
							$mensaje = "No se tiene Token Activo";
					}else
						$mensaje = "No se tiene un CUIS ACTIVO";
					
					echo json_encode(["mensaje" => $mensaje, "estado" => $estado]);
				break;

				//OBTENER CORRELATIVO VENTAS
				case 'obtener_correlativo_actual':
					$corr = $venta->mostrar_correlativo_vigente();
					echo json_encode($corr);
				break;

				//ENVIAR POR CORREO
				case 'enviar_correo':
					$id_venta = $_GET["id_venta"];
					$correo = $_GET["correo"];
					$resp_correo = $venta->enviar_correo($id_venta,$correo);
					if($resp_correo)
						echo "<i class='fa fa-check-circle' aria-hidden='true'></i> Correo enviado correctamente";
					else
						echo "<br><i class='fa fa-exclamation-circle' aria-hidden='true'></i> No se pudo enviar el correo";
				break;

				//VERIFICAR ESTADO FACTURA
				case 'verificar_estado_factura':
					$cuf = $_GET["cuf"];
					$mensaje = "";
					$estado = "0";

					//OBTENER PUNTO DE VENTA
					require_once "../modelos/PuntoVenta.php";
					$pv = new PuntoVenta();

					//OBTENER INFORMACIÓN GENERAL DEL SISTEMA PARA ENVIAR  
					require_once "../modelos/Informacion.php";
					$cuis_actual = $pv->buscar_cuis_activo($_SESSION["id_punto_venta"]);
					$cufd_actual = $pv->buscar_cufd_activo($_SESSION["id_punto_venta"]);
					$pv_actual = $pv->getPuntoVenta2($_SESSION["id_punto_venta"]);

					$token = TOKEN;

					if($cuf != "" && $cuis_actual && $cufd_actual && $token && $pv_actual){
						$cod_ambiente = Informacion::mostrar2()["codigoAmbiente"];
						$cod_doc_sector = 1; //FACTURA COMPRA VENTA
						$cod_emision = 1; //ONLINE
						$cod_modalidad = Informacion::mostrar2()["codigoModalidad"];
						$cod_punto_venta = $pv_actual["codigoPuntoVenta"];
						$cod_sistema = Informacion::mostrar2()["codigoSistema"];
						$cod_sucursal = Informacion::mostrar2()["codigoSucursal"];
						$cufd = $cufd_actual["codigo_cufd"];
						$cuis = $cuis_actual["codigo_cuis"];
						$nit = Informacion::mostrar2()["nit"];
						$tipo_factura = 1;//FACTURA CON DERECHO A CRÉDITO FISCAL
						
						$vector_datos = [
							"codigoAmbiente"=>$cod_ambiente,
							"codigoDocumentoSector"=>$cod_doc_sector,
							"codigoEmision"=>$cod_emision,
							"codigoModalidad"=>$cod_modalidad,
							"codigoPuntoVenta"=>$cod_punto_venta,
							"codigoSistema"=>$cod_sistema,
							"codigoSucursal"=>$cod_sucursal,
							"cufd"=>$cufd,
							"cuis"=>$cuis,
							"nit"=>$nit,
							"tipoFacturaDocumento"=>$tipo_factura,
							"cuf"=>$cuf
						];
			
						require "../xml/crear_xml/CrearXML.php";
						$recep_verif = new CrearXML();
						$res_verif = $recep_verif->verificar_estado_factura($token,$vector_datos);
						if(is_object($res_verif)){
							if($res_verif->RespuestaServicioFacturacion->transaccion){
								$mensaje = "(".$res_verif->RespuestaServicioFacturacion->codigoEstado.") Factura " . $res_verif->RespuestaServicioFacturacion->codigoDescripcion;
								
							}else{
								
								if(is_array($res_verif->RespuestaServicioFacturacion->mensajesList)){
									for($j = 0; $j < count($res_verif->RespuestaServicioFacturacion->mensajesList); $j++)
										$mensaje .= ($j+1). ". (Error ".$res_verif->RespuestaServicioFacturacion->mensajesList[$j]->codigo .") ".$res_verif->RespuestaServicioFacturacion->mensajesList[$j]->descripcion . "<br>"; 
								}else
									$mensaje .= "(Error ".$res_verif->RespuestaServicioFacturacion->mensajesList->codigo .") ".$res_verif->RespuestaServicioFacturacion->mensajesList->descripcion;
									
							}
						}else
							$mensaje = "Error de verificación por parte de SIN";
									
					}else{
						$mensaje = "Error en verifiación verifique si el CUF es correcto y si el cuis y/o cufd están vigentes";
						//echo "Error en verifiación verifique si el CUF es correcto y si el cuis y/o cufd están vigentes";
					}
					//echo json_encode(["mensaje" => $mensaje, "estado" => $estado]);
					echo $mensaje;
				break;

				//MOSTRAR DATOS DE CLIENTE
				case 'mostrar_cliente':
					require_once "../modelos/Persona.php";
					$persona = new Persona();
					$res = $persona->mostrar($_GET["cliente"]);
					echo json_encode($res);
				break;

				case 'contar_nro_ventas':
					$cant = $venta->contar_nro_ventas();
					echo $cant["cant_ventas"];
				break;
		    }

		//Fin de las validaciones de acceso

	    }else

	  		require 'noacceso.php';

	}

	ob_end_flush();