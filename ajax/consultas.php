<?php 
date_default_timezone_set("America/La_Paz");
ob_start();

if (strlen(session_id()) < 1){

	session_start();//Validamos si existe o no la sesión

}

if (!isset($_SESSION["nombre"]))

{

  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.

}

else

{

//Validamos el acceso solo al usuario logueado y autorizado.

if ($_SESSION['consultav']==1 || $_SESSION['consultac']==1){
    
    date_default_timezone_set("America/La_Paz");

	require_once "../modelos/Consultas.php";



	$consulta=new Consultas();



	switch ($_GET["op"]){

		case 'comprasfecha':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->comprasfecha($fecha_inicio,$fecha_fin);

	 		//Vamos a declarar un array

 			$data= Array();

			 

 			while ($reg=$rspta->fetch_object()){

 				$data[]=array(

 					"0"=>$reg->fecha,

 					"1"=>$reg->usuario,

 					"2"=>$reg->proveedor,

	 				"3"=>$reg->tipo_comprobante,

 					"4"=>$reg->num_comprobante,

 					"5"=>$reg->total_compra,

 					"6"=>$reg->impuesto,

	 				"7"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':

 					'<span class="label bg-red">Anulado</span>'

 					);

 			}

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);



		break;



		case 'monto_comprasfecha':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->monto_comprasfecha($fecha_inicio,$fecha_fin);

			$data= Array();

 			$reg = $rspta->fetch_object();

 			$data[] = array("0"=>$reg->total);

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'anul_comprasfecha':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->anul_comprasfecha($fecha_inicio,$fecha_fin);

			$data= Array();

 			$reg = $rspta->fetch_object();

 			$data[] = array("0"=>$reg->total);

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'ventasfechacliente':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];

			$idcliente=$_REQUEST["idcliente"];



			$rspta=$consulta->ventasfechacliente($fecha_inicio,$fecha_fin,$idcliente);

 			//Vamos a declarar un array

 			$data= Array();



 			while ($reg=$rspta->fetch_object()){

 				$data[]=array(

	 				"0"=>$reg->fecha,

 					"1"=>$reg->usuario,

 					"2"=>$reg->cliente,

 					"3"=>$reg->tipo_comprobante,

 					"4"=>$reg->num_comprobante,

	 				"5"=>$reg->tiene_tarjeta,

 					"6"=>$reg->total_venta,

 					"7"=>$reg->impuesto,

 					"8"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':

 					'<span class="label bg-red">Anulado</span>'

 					);

 			}

 			$results = array(

 				"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'ventasfechaclientes':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];

			$rspta=$consulta->ventasfechaclientes($fecha_inicio,$fecha_fin);

 			//Vamos a declarar un array

 			$data= Array();

			$descuento = "";

			$url_generar_pdf = "&gen_pdf=0";

			$url='../reportes/exFactura.php?id=';

 			while ($reg=$rspta->fetch_object()){

				$sqlDesc = $consulta->hayDescuento($reg->idventa);

				$resp = $sqlDesc->fetch_object();

				if($resp->descuento != NULL && $resp->descuento != 0)

					$descuento = "<strong style='color:green'>D</strong>";

				else

					$descuento = "<strong style='color:red'>SD</strong>";

				$opciones = '';

				$url_xml = $reg->ruta;

				//LAS VENTAS SOLO DEBERIAN PODERSE ANULAR HASTA 5 DIAS DESPUÉS DE SU EMISIÓN

				if($reg->estado == 'Aceptado'){

					$opciones = '<br><a target="_blank" href="'.$url.$reg->nombre_archivo.$url_generar_pdf.'"> <button class="btn btn-info" title="Reporte factura"><i class="fa fa-file-pdf-o"></i></button></a>'.

							'<br><a target="_blank" href="'.$url_xml.$reg->nombre_archivo.'.xml"> <button class="btn btn-success" title="Generar XML Factura"><i class="fa fa-code"></i></button></a>';

					if($reg->email != "")
						$opciones .= '<br><button class="btn btn-primary" onclick="enviar_correo(\''."$reg->email".'\',\''.$reg->idventa.'\')" title="Enviar factura por correo"><i class="fa fa-at"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>';

					$opciones .= '<br><button class="btn btn-secondary" onclick="verificar_estado(\''.$reg->cuf.'\')" title="Verificar estado factura"><i class="fa fa-search"></i></button>';
							
                    //'<button class="btn btn-danger" onclick="anular(\''."$reg->nombre_archivo".'\')" title="Anular Venta"><i class="fa fa-close"></i></button>'. ANULAR FACTURA
				}else{
					$opciones = '<a target="_blank" href="'.$url.$reg->nombre_archivo.$url_generar_pdf.'"> <button class="btn btn-info" title="Reporte factura"><i class="fa fa-file-pdf-o"></i></button></a>'.

					'<br><a target="_blank" href="'.$url_xml.$reg->nombre_archivo.'.xml"> <button class="btn btn-success" title="Generar XML Factura"><i class="fa fa-code"></i></button></a>';

					if($reg->email != "")
						$opciones .= '<br><button class="btn btn-primary" onclick="enviar_correo(\''."$reg->email".'\',\''.$reg->idventa.'\')" title="Notificar anulación"><i class="fa fa-at"></i> <i class="fa fa-times" aria-hidden="true"></i></button>';

					$opciones .= '<br><button class="btn btn-secondary" onclick="verificar_estado(\''.$reg->cuf.'\')" title="Verificar estado factura"><i class="fa fa-search"></i></button>';
				}

				$met_pago = "";

				//AGREGAR TRANSFERENCIA BANCARIA
				switch($reg->metodo_pago){
					case "1":
						$met_pago = "Efectivo";
					break;

					case "2":
						$met_pago = "Tarjeta";
					break;

					case "7":
						$met_pago = "Transferencia Bancaria";
					break;

					case "10":
						$met_pago = "Mixto";
					break;

					default:
						$met_pago = "Otro";
					break;
				}

				$est_venta = "";
				if($reg->estado == "Aceptado"){
					$est_venta = '<span class="label bg-green">Aceptado</span>';
				}else{
					if($reg->estado == "Pendiente"){
						$est_venta = '<span class="label bg-yellow">Pendiente</span>';
					}else{
						if($reg->estado == "Anulado")
							$est_venta = '<span class="label bg-red">Anulado</span>';
						else
							$est_venta = '<span class="label bg-black">Pendiente Corrección</span>';
					}
				}

				$num_factura = "";
				if($reg->nro_venta != "" && $reg->nro_venta != "0")
					$num_factura = $reg->nro_venta;
				else
					$num_factura = $reg->nro_venta_contingencia;
				
				$cafc = "";
				if($reg->cafc != "" && $reg->cafc != null)
					$cafc = $reg->cafc;
				else
					$cafc = "N/A";


				$data[]=array(

					"0"=>$opciones . "<br>" . $descuento,

				   	"1"=>$reg->fecha_hora_registro,

					"2"=>$reg->fecha,

				   	"3"=>$num_factura,

					"4"=>$reg->cliente,

				   	"5"=>$reg->num_documento,

					"6"=>$reg->usuario,

					"7"=>$met_pago,

					"8"=>$reg->total_venta,

				   	"9"=>$cafc,

					"10"=>$est_venta,
			   	);

	 		}

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'ventasfechaclientes_ct_acp':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->ventasfechaclientes_ct_acp($fecha_inicio,$fecha_fin);

			$data= Array();

 			$reg = $rspta->fetch_object();

 			$data[] = array("0"=>$reg->total);

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'ventasfechaclientes_st_acp':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->ventasfechaclientes_st_acp($fecha_inicio,$fecha_fin);

			$data= Array();

 			$reg = $rspta->fetch_object();

 			$data[] = array("0"=>$reg->total);

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'ventasfechaclientes_nul':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->ventasfechaclientes_anulado($fecha_inicio,$fecha_fin);

			$data= Array();

 			$reg = $rspta->fetch_object();

 			$data[] = array("0"=>$reg->total);

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'sumar_descuentos':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->sumar_descuentos($fecha_inicio,$fecha_fin);

			$data= Array();

 			$reg = $rspta->fetch_object();

 			$data[] = array("0"=>$reg->total);

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;



		case 'productos_vendidos':
            date_default_timezone_set("America/La_Paz");
			$fecha_inicio=$_REQUEST["fecha_inicio"];

			$fecha_fin=$_REQUEST["fecha_fin"];



			$rspta=$consulta->productos_vendidos($fecha_inicio,$fecha_fin);

 			//Vamos a declarar un array

 			$data= Array();

 			while ($reg=$rspta->fetch_object()){

	 			$data[]=array(

					"0"=>$reg->cod_med,

 					"1"=>$reg->descripcion,

					 "2"=>$reg->medida,

	 				"3"=>$reg->unidad,

	 				"4"=>$reg->categoria . " - " . $reg->subcategoria,

					"5"=>$reg->marca,

 					"6"=>round($reg->cantidad,2),

 					"7"=>round($reg->cant,2)

 					

 					);

	 		}

 			$results = array(

	 			"sEcho"=>1, //Información para el datatables

 				"iTotalRecords"=>count($data), //enviamos el total registros al datatable

 				"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

 				"aaData"=>$data);

 			echo json_encode($results);

		break;

	}

//Fin de las validaciones de acceso

}else

  require 'noacceso.php';



}

ob_end_flush();

?>

