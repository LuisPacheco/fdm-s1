<?php 
    date_default_timezone_set("America/La_Paz");
	ob_start();

	if (strlen(session_id()) < 1)

		session_start();//Validamos si existe o no la sesión

	if (!isset($_SESSION["nombre"]))
	  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
	else{
		//Validamos el acceso solo al usuario logueado y autorizado.

		if ($_SESSION['almacen']==1){	

			require_once "../modelos/Articulo.php";

			$articulo=new Articulo();

			$idarticulo=isset($_POST["idarticulo"])? limpiarCadena($_POST["idarticulo"]):"";

			$idcategoria=isset($_POST["idcategoria"])? limpiarCadena($_POST["idcategoria"]):"";

			$codigo=isset($_POST["codigo"])? limpiarCadena($_POST["codigo"]):"";

			$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";

			$stock=isset($_POST["stock"])? limpiarCadena($_POST["stock"]):"";

			$stock_minimo=isset($_POST["stock_minimo"])? limpiarCadena($_POST["stock_minimo"]):"";

			$descripcion=isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";

			$imagen=isset($_POST["imagen"])? limpiarCadena($_POST["imagen"]):"";

			$idlaboratorio=isset($_POST["idlaboratorio"])? limpiarCadena($_POST["idlaboratorio"]):"";

			$cod_med = isset($_POST["cod_med"])? limpiarCadena($_POST["cod_med"]):"";

			$precio_venta = isset($_POST["precio_venta"])? limpiarCadena($_POST["precio_venta"]):"";

			$unidadMedida = isset($_POST["unidadMedida"])? limpiarCadena($_POST["unidadMedida"]):"";

			$nombreUnidadMedida = isset($_POST["nombreUnidadMedida"])? limpiarCadena($_POST["nombreUnidadMedida"]):"";

			$codigoProductoSin = isset($_POST["codigoProductoSin"])? limpiarCadena($_POST["codigoProductoSin"]):"";

			$otraUnidadMedida = isset($_POST["otraUnidadMedida"])? limpiarCadena($_POST["otraUnidadMedida"]):"";

			if(isset($_POST["opcion"])){

				switch ($_POST["opcion"]){
					case "contar_meds_por_lab":
						$idlab=isset($_POST["idlab"])? $_POST["idlab"]:0;
						$res = $articulo->buscar_lab_corr($idlab);
						echo $res;
					break;

					case "verif_cod_med":
						$res = $articulo->verif_cod_med($cod_med,$idarticulo);
						echo $res;
					break;
				}
			}

			if(isset($_GET["op"])){
				switch ($_GET["op"]){

					case 'guardaryeditar':

						if (!is_uploaded_file($_FILES['imagen']['tmp_name'])){

							if ($_POST["imagenactual"] == "") 

								$imagen = "medicamento.png";

							else

								$imagen=$_POST["imagenactual"];		

						}else{

							if(!file_exists($_FILES['imagen']['tmp_name']))

								$imagen = "medicamento.png";

							else{

								$ext = explode(".", $_FILES["imagen"]["name"]);

								if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png"){

									$imagen = round(microtime(true)) . '.' . end($ext);

									move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/articulos/" . $imagen);

								}

							}

						}

						if (empty($idarticulo)){
							echo "NO SE PUEDEN REGISTRAR NUEVOS ARTÍCULOS EN LA SUCURSAL";
						}else{
							//Actualizando columnas de stock y stock mínimo
							$act_stock = $articulo->setColumna("stock",$stock,$idarticulo);
							$act_stock_minimo = $articulo->setColumna("stock_minimo",$stock_minimo,$idarticulo);
							echo $act_stock && $act_stock_minimo ? "Artículo actualizado" : "Artículo no se pudo actualizar";
						}

					break;



					case 'desactivar':

						$rspta=$articulo->desactivar($idarticulo);

						echo $rspta ? "Artículo Desactivado" : "Artículo no se puede desactivar";

					break;



					case 'activar':

						$rspta=$articulo->activar($idarticulo);

						echo $rspta ? "Artículo activado" : "Artículo no se puede activar";

					break;



					case 'mostrar':

						$rspta=$articulo->mostrar($idarticulo);

						//Codificar el resultado utilizando json

						echo json_encode($rspta);

					break;



					case 'listar':

						$rspta=$articulo->listar();

						//Vamos a declarar un array

						$data= Array();



						while ($reg=$rspta->fetch_object()){

							$dato = "";

							if ($reg->stock <= $reg->stock_minimo)

								$dato = "<strong> <p style='color:#FF0000'>". $reg->stock  ."</p></strong>";

							else 

								$dato = $reg->stock;

							$data[]=array(

								"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulo.')"><i class="fa fa-pencil"></i></button>'.

									' <button class="btn btn-danger" onclick="desactivar('.$reg->idarticulo.')"><i class="fa fa-close"></i></button>':

									'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulo.')"><i class="fa fa-pencil"></i></button>'.

									' <button class="btn btn-primary" onclick="activar('.$reg->idarticulo.')"><i class="fa fa-check"></i></button>',

								"1"=>$reg->cod_med,

								"2"=>$reg->nombre_comercial,

								"3"=>$reg->nombre_generico,

								"4"=>$reg->categoria,

								"5"=>$reg->laboratorio,

								"6"=>$dato,

								"7"=>$reg->stock_minimo,

								"8"=>$reg->precio_venta,

								"9"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >",

								"10"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':

								'<span class="label bg-red">Desactivado</span>'

								);

						}

						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>count($data), //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

							"aaData"=>$data);

						echo json_encode($results);



					break;



					case "selectCategoria":

						require_once "../modelos/Categoria.php";

						$categoria = new Categoria();



						$rspta = $categoria->select();

						echo '<option value='.'>--Seleccione--</option>';

						while ($reg = $rspta->fetch_object())

								{

									echo '<option value=' . $reg->idcategoria . '>' . $reg->nombre . '</option>';

								}

					break;



					case "selectLaboratorio":

						require_once "../modelos/Laboratorio.php";

						$laboratorio = new Laboratorio();



						$rspta = $laboratorio->select();

						echo '<option value='.'>--Seleccione--</option>';

						while ($reg = $rspta->fetch_object())

							echo '<option value=' . $reg->idlaboratorio . '>' . $reg->nombre . '</option>';

						

					break;

					case "selectListaProductosSin":
						$rspta = $articulo->selectListaProductosSin();
						echo($rspta);
					break;

					case "selectUnidadMedida":

						$rspta = $articulo->selectUnidadMedida();

						echo($rspta);
					break;

					

					case "selectArticuloLab":

						require_once "../modelos/Articulo.php";

						$articulo = new Articulo();



						$rspta = $articulo->selectArtLab($idlaboratorio);

						$cad = '<select id="idarticulo" name="idarticulo" class="form-control selectpicker" data-live-search="true" required>';

						while ($reg = $rspta->fetch_object()){

							if($reg->idarticulo == $idarticulo)

								$cad .= '<option value=' . $reg->idarticulo . ' selected>' . $reg->cod_med . " - " . $reg->nombre . '</option>';

							else

								$cad .= '<option value=' . $reg->idarticulo . '>' . $reg->cod_med . " - " . $reg->nombre . '</option>';						

						}

						$cad .= '</select>';

						echo $cad;

					break;

					

					case "listar_venta_articulo":

						$idarticulo = $_GET["idarticulo"];

						$lista_venta = $articulo->venta_articulo($idarticulo);

						$data= Array();



						while ($reg=$lista_venta->fetch_object()){

							

							$data[]=array(

								"0"=>$reg->login,

								"1"=>$reg->cantidad,

								"2"=>$reg->precio_venta,

								"3"=>$reg->descuento,

								"4"=>$reg->fecha_hora,

								"5"=>$reg->cod_lote,

								"6"=>$reg->vencimiento,

								"7"=>$reg->estado,

								);

						}

						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>count($data), //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

							"aaData"=>$data);

						echo json_encode($results);

					break;



					case "listar_entradas_articulo":

						$idarticulo = $_GET["idarticulo"];

						$lista_entrada = $articulo->entrada_articulo($idarticulo);

						$data = Array();



						while ($reg=$lista_entrada->fetch_object()){

							

							$data[]=array(

								"0"=>$reg->login,

								"1"=>$reg->cantidad,

								"2"=>$reg->fecha_hora,

								"3"=>$reg->cod_lote,

								"4"=>$reg->vencimiento,

								"5"=>$reg->sucursal,

								"6"=>$reg->estado,

								);

						}

						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>count($data), //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

							"aaData"=>$data);

						echo json_encode($results);

					break;



					case "listar_salidas_articulo":

						$idarticulo = $_GET["idarticulo"];

						$lista_salida = $articulo->salida_articulo($idarticulo);

						$data = Array();



						while ($reg=$lista_salida->fetch_object()){

							

							$data[]=array(

								"0"=>$reg->login,

								"1"=>$reg->cantidad,

								"2"=>$reg->fecha_hora,

								"3"=>$reg->cod_lote,

								"4"=>$reg->vencimiento,

								"5"=>$reg->sucursal,

								"6"=>$reg->estado,

								);

						}

						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>count($data), //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

							"aaData"=>$data);

						echo json_encode($results);

					break;



					case "listar_ingresos_articulo":

						$idarticulo = $_GET["idarticulo"];

						$lista_ingreso = $articulo->ingreso_articulo($idarticulo);

						$data = Array();



						while ($reg=$lista_ingreso->fetch_object()){

							

							$data[]=array(

								"0"=>$reg->login,

								"1"=>$reg->cantidad,

								"2"=>$reg->fecha_hora,

								"3"=>$reg->cod_lote,

								"4"=>$reg->vencimiento,

								"5"=>$reg->proveedor,

								"6"=>$reg->estado,

								);

						}

						$results = array(

							"sEcho"=>1, //Información para el datatables

							"iTotalRecords"=>count($data), //enviamos el total registros al datatable

							"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar

							"aaData"=>$data);

						echo json_encode($results);

					break;

					

				}
			}

			//Fin de las validaciones de acceso

		}else

		  require 'noacceso.php';

	}

	ob_end_flush();