<?php 
    date_default_timezone_set("America/La_Paz");
    //Incluímos inicialmente la conexión a la base de datos

    Class CrearXML{

        //Implementamos nuestro constructor
        public function __construct(){

        }

        public function generar_xml_factura($vector_cabecera,$vector_detalles,$vector_datos_generales,$tipo_emision,$nueva_hora_fecha,$evento_actual,$id_evento_significativo,$sw_borrado = false){
            try{
                $dom = new DOMDocument(); $dom->encoding = 'utf-8'; $dom->xmlVersion = '1.0'; $dom->formatOutput = true;

                //para el caso de ser OFFLINE considerar el evento actual que se tiene activo
                //fecha_hora+nro_factura+nro_documento_cliente+complemento
                $xml_file_name = "";
                if($evento_actual == 5 || $evento_actual == 6 || $evento_actual == 7)
                    $xml_file_name = $nueva_hora_fecha.$vector_cabecera["numeroFactura"]."CTG".$vector_cabecera["numeroDocumento"].$vector_cabecera["complemento"].'.xml';
                else
                    $xml_file_name = $nueva_hora_fecha.$vector_cabecera["numeroFactura"].$vector_cabecera["numeroDocumento"].$vector_cabecera["complemento"].'.xml';
                
                $root = $dom->createElement('facturaComputarizadaCompraVenta');

                $factura_attr1 = new DOMAttr("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
                $root->setAttributeNode($factura_attr1);
                $factura_attr = new DOMAttr("xsi:noNamespaceSchemaLocation","factura_computarizada_compra_venta.xsd");
                $root->setAttributeNode($factura_attr);

                //CABECERA DE FACTURA
                $factura_node = $dom->createElement('cabecera');

                $child_node_nit = $dom->createElement('nitEmisor', $vector_cabecera["nitEmisor"]); $factura_node->appendChild($child_node_nit);
                
                $child_node_razon_social_emisor = $dom->createElement('razonSocialEmisor', $vector_cabecera["razonSocialEmisor"]); $factura_node->appendChild($child_node_razon_social_emisor);

                $child_node_municipio = $dom->createElement('municipio', mb_strtoupper($vector_cabecera["municipio"])); $factura_node->appendChild($child_node_municipio);

                $child_node_telefono = $dom->createElement('telefono', $vector_cabecera["telefono"]); $factura_node->appendChild($child_node_telefono);

                $child_node_nro_factura = $dom->createElement('numeroFactura', $vector_cabecera["numeroFactura"]); $factura_node->appendChild($child_node_nro_factura);

                $child_node_cuf = $dom->createElement('cuf', $vector_cabecera["cuf"]); $factura_node->appendChild($child_node_cuf);

                $child_node_cufd = $dom->createElement('cufd', $vector_cabecera["cufd"]); $factura_node->appendChild($child_node_cufd);

                $child_node_codigo_sucursal = $dom->createElement('codigoSucursal', $vector_cabecera["codigoSucursal"]); $factura_node->appendChild($child_node_codigo_sucursal);

                $child_node_direccion = $dom->createElement('direccion', $vector_cabecera["direccion"]); $factura_node->appendChild($child_node_direccion);

                $child_node_cod_pv = $dom->createElement('codigoPuntoVenta', $vector_cabecera["codigoPuntoVenta"]); $factura_node->appendChild($child_node_cod_pv);

                $child_node_fecha_emision = $dom->createElement('fechaEmision', $vector_cabecera["fechaEmision"]); $factura_node->appendChild($child_node_fecha_emision);

                $child_node_nombre_razon_social = $dom->createElement('nombreRazonSocial', $vector_cabecera["nombreRazonSocial"]); $factura_node->appendChild($child_node_nombre_razon_social);

                $child_node_documento_identidad = $dom->createElement('codigoTipoDocumentoIdentidad', $vector_cabecera["codigoTipoDocumentoIdentidad"]); $factura_node->appendChild($child_node_documento_identidad);

                $child_node_nro_documento = $dom->createElement('numeroDocumento', $vector_cabecera["numeroDocumento"]); $factura_node->appendChild($child_node_nro_documento);

                if($vector_cabecera["complemento"] != null && $vector_cabecera["complemento"] != "")
                    $child_node_complemento = $dom->createElement('complemento', $vector_cabecera["complemento"]);
                else{
                    $child_node_complemento = $dom->createElement('complemento');
                    $child_attribute = new DOMAttr("xsi:nil","true");
                    $child_node_complemento->setAttributeNode($child_attribute);
                }
                $factura_node->appendChild($child_node_complemento);

                $child_node_cod_cliente = $dom->createElement('codigoCliente', $vector_cabecera["codigoCliente"]); $factura_node->appendChild($child_node_cod_cliente);

                $child_node_cod_metodo_pago = $dom->createElement('codigoMetodoPago', $vector_cabecera["codigoMetodoPago"]); $factura_node->appendChild($child_node_cod_metodo_pago);

                if($vector_cabecera["numeroTarjeta"] != null && $vector_cabecera["numeroTarjeta"] != "")
                    $child_node_nro_tarjeta = $dom->createElement('numeroTarjeta', $vector_cabecera["numeroTarjeta"]);
                else{
                    $child_node_nro_tarjeta = $dom->createElement('numeroTarjeta');
                    $child_attribute = new DOMAttr("xsi:nil","true");
                    $child_node_nro_tarjeta->setAttributeNode($child_attribute);
                }
                $factura_node->appendChild($child_node_nro_tarjeta);


                $child_node_monto_total = $dom->createElement('montoTotal', $vector_cabecera["montoTotal"]); $factura_node->appendChild($child_node_monto_total);

                $child_node_monto_total_iva = $dom->createElement('montoTotalSujetoIva', $vector_cabecera["montoTotalSujetoIva"]); $factura_node->appendChild($child_node_monto_total_iva);

                $child_node_codigo_moneda = $dom->createElement('codigoMoneda', $vector_cabecera["codigoMoneda"]); $factura_node->appendChild($child_node_codigo_moneda);

                $child_node_tipo_cambio = $dom->createElement('tipoCambio', $vector_cabecera["tipoCambio"]); $factura_node->appendChild($child_node_tipo_cambio);

                $child_node_monto_total_moneda = $dom->createElement('montoTotalMoneda', $vector_cabecera["montoTotalMoneda"]); $factura_node->appendChild($child_node_monto_total_moneda);

                $child_node_gift_card = $dom->createElement('montoGiftCard');
                $child_attribute = new DOMAttr("xsi:nil","true");
                $child_node_gift_card->setAttributeNode($child_attribute);
                $factura_node->appendChild($child_node_gift_card);

                $child_node_descuento_adicional = $dom->createElement('descuentoAdicional', $vector_cabecera["descuentoAdicional"]); $factura_node->appendChild($child_node_descuento_adicional);

                $child_node_codigo_excepcion = $dom->createElement('codigoExcepcion', $vector_cabecera["codigoExcepcion"]); $factura_node->appendChild($child_node_codigo_excepcion);

                if($vector_cabecera["cafc"] != null && $vector_cabecera["cafc"] != ""){
                    $child_node_cafc = $dom->createElement('cafc', $vector_cabecera["cafc"]);
                }else{
                    $child_node_cafc = $dom->createElement('cafc');
                    $child_attribute = new DOMAttr("xsi:nil","true");
                    $child_node_cafc->setAttributeNode($child_attribute);
                }
                $factura_node->appendChild($child_node_cafc);

                $child_node_leyenda = $dom->createElement('leyenda', $vector_cabecera["leyenda"]); $factura_node->appendChild($child_node_leyenda);

                $child_node_usuario = $dom->createElement('usuario', $vector_cabecera["usuario"]); $factura_node->appendChild($child_node_usuario);

                $child_node_cod_doc_sector = $dom->createElement('codigoDocumentoSector', $vector_cabecera["codigoDocumentoSector"]); $factura_node->appendChild($child_node_cod_doc_sector);

                $root->appendChild($factura_node);

                //AGREGAR DETALLES DE VENTA
                if(count($vector_detalles) > 0){

                    for($i = 0; $i < count($vector_detalles); $i++){
                        $detalle_node = $dom->createElement('detalle');

                        $child_node_cod_actividad = $dom->createElement('actividadEconomica', $vector_detalles[$i]["actividadEconomica"]); $detalle_node->appendChild($child_node_cod_actividad);

                        $child_node_cod_prod_sin = $dom->createElement('codigoProductoSin', $vector_detalles[$i]["codigoProductoSin"]); $detalle_node->appendChild($child_node_cod_prod_sin);

                        $child_node_cod_prod = $dom->createElement('codigoProducto', $vector_detalles[$i]["codigoProducto"]); $detalle_node->appendChild($child_node_cod_prod);

                        $child_node_descripcion = $dom->createElement('descripcion', str_replace("\\","",limpiarCadena($vector_detalles[$i]["descripcion"]))); $detalle_node->appendChild($child_node_descripcion);

                        //$nombre_razon_social = str_replace("\\","",$nombre_razon_social);

                        $child_node_cantidad= $dom->createElement('cantidad', $vector_detalles[$i]["cantidad"]); $detalle_node->appendChild($child_node_cantidad);

                        $child_node_unidad_medida = $dom->createElement('unidadMedida', $vector_detalles[$i]["unidadMedida"]); $detalle_node->appendChild($child_node_unidad_medida);

                        $child_node_precio_unitario = $dom->createElement('precioUnitario', $vector_detalles[$i]["precioUnitario"]); $detalle_node->appendChild($child_node_precio_unitario);

                        $child_node_descuento = $dom->createElement('montoDescuento', number_format($vector_detalles[$i]["montoDescuento"],2,".","")); $detalle_node->appendChild($child_node_descuento);

                        $child_node_subtotal = $dom->createElement('subTotal', number_format($vector_detalles[$i]["subTotal"],2,".","")); $detalle_node->appendChild($child_node_subtotal);

                        $child_node_serie = $dom->createElement('numeroSerie');
                        $child_attribute = new DOMAttr("xsi:nil","true");
                        $child_node_serie->setAttributeNode($child_attribute); $detalle_node->appendChild($child_node_serie);

                        $child_node_imei = $dom->createElement('numeroImei');
                        $child_attribute = new DOMAttr("xsi:nil","true");
                        $child_node_imei->setAttributeNode($child_attribute); $detalle_node->appendChild($child_node_imei);

                        $root->appendChild($detalle_node);
                    }

                    $dom->appendChild($root);
                    
                    $dom->save($xml_file_name);

                    /*
                        SI EL EVENTO ACTUAL ES 5, 6 o 7:
                            1. RECUPERAR EL ÚLTIMA PAQUETE DE LA BB.DD. PARA EL EVENTO SIGNIFICATIVO
                            2. VERIFICAR SI EL PAQUETE TIENE MENOS DE 500 ARCHIVOS XML
                            3. SI ES MENOR A 500 SE AGREGA EL XML CREADO EN EL PAQUETE
                            4. SI ES MAYOR A 500 SE DEBE CREAR OTRO PAQUETE CON EL CORRELATIVO SIGUIENTE Y SE GUARDA EL XML EN EL PAQUETE CREADO (REVISAR)
                    */
                    

                    require_once "../modelos/EventoSignificativo.php";
                    $es = new EventoSignificativo();

                    $ruta_xml = "";

                    if($evento_actual != 0 && $evento_actual != ""){ //SI SE TIENE ALGÚN EVENTO ACTIVO
                        $paquete_actual = $es->obtener_ultimo_paquete_es($id_evento_significativo);//RECUPERAR LA INF DEL PAQUETE (CARPETA)
                        if($paquete_actual){//SI EXISTE PAQUETE PARA EVENTO
                            $nro_actual = $paquete_actual["actual"];
                            if((int)$nro_actual < 500){
                                if(!$sw_borrado)//SI SE ESTÁ BORRANDO UN XML POR CORRECCIÓN NO SE AUMENTA EL VALOR ACTUAL DE ELEMENTOS EN PAQUETE
                                    $nro_actual++;
                                $ruta_xml = "../xml_qr/paquetes_contingencia/".$paquete_actual["nombre_paquete"]."/".$xml_file_name;
                                $actual_pqt = $es->modificar_paquete($paquete_actual["id_paquete"],$paquete_actual["nombre_paquete"],$nro_actual,$id_evento_significativo);
                                if(!$actual_pqt)
                                    return false;
                            }else{//DE MOMENTO NO TOMAR EN CUENTA CREAR DOS PAQUETES POR EVENTO, SOLO UNO
                                /*
                                //OBTENER EL NÚMERO DE LA ÚLTIMA CARPETA
                                $pos_cad_np = stripos($paquete_actual["nombre_paquete"],"_",-1);
                                $nro = "";
                                for($i = 20; $i < strlen($paquete_actual["nombre_paquete"]); $i++)
                                    $nro .= $paquete_actual["nombre_paquete"][$i];
                                
                                $nro = (int)$nro + 1;

                                $nombre_carpeta = $nueva_hora_fecha."_".$evento_actual."_".$nro;

                                $ruta_carpeta = "../xml_qr/paquetes_contingencia/".$nombre_carpeta;
                                if (!file_exists($ruta_carpeta)){
                                    mkdir($ruta_carpeta, 0777, true);
                                    $crear_pqt = $es->crear_paquete($nombre_carpeta,$id_evento_significativo);
                                    if($crear_pqt){
                                        //SI SE CREÓ CORRECTAMENTE EL REGISTRO DEL PAQUETE
                                        $paquete_actual = $es->obtener_ultimo_paquete_es($id_evento_significativo);
                                        if($paquete_actual){
                                            $nro_actual = (int)$paquete_actual["actual"];
                                            
                                            $nro_actual++;
                                            $ruta_xml = "../xml_qr/paquetes_contingencia/".$paquete_actual["nombre_paquete"]."/".$xml_file_name;
                                            $actual_pqt = $es->modificar_paquete($paquete_actual["id_paquete"],$paquete_actual["nombre_paquete"],$nro_actual,$id_evento_significativo);
                                            if(!$actual_pqt)
                                                return false;
                                        }else
                                            return false;
                                    }else
                                        return false;
                                }else
                                    return false;
                                */

                                return false;
                            }
                        }else
                            return false;
                    }else
                        $ruta_xml = "../xml_qr/".$xml_file_name;

                    //SE MUEVE EL ARCHIVO XML CREADO EN LA CARPETA AJAX A LA RUTA ESTABLECIDA EN $ruta_xml
                    rename($xml_file_name,$ruta_xml);

                    //VALIDACIÓN CONTRA XSD
                    $ruta_xsd = "../xml/archivos_xml/factura_computarizada_compra_venta.xsd";

                    require "ValidarXML.php";

                    $validar_xml = new ValidacionXSD();

                    if(!$validar_xml->validar($ruta_xml,$ruta_xsd)){//SI LA VALICACIÓN TIENE ALGÚN ERROR SE HARÁ NOTAR AQUÍ
                        //print $validar_xml->mostrarError();
                        return false;
                    }else{
                        //comprimir el archivo xml con GZIP si el tipo de emisión es online
                        if($tipo_emision == 1){//TIPO DE EMISIÓN ONLINE

                            if( empty($ruta_xml) ){
                                throw new Exception('The invoice buffer is empty');
                                return false;
                            }

                            file_put_contents($xml_file_name, $ruta_xml);
                            $archivo = gzcompress(file_get_contents($ruta_xml), 9, FORCE_GZIP);
		                    $hashArchivo = hash('sha256', $archivo, !true);
                          
                            $cufd = $vector_cabecera["cufd"];
                            $token = $vector_datos_generales["token"];

                            $respuesta = false;
                            
                            $opts = array(
                                'http' => array(
                                    'header' => "apikey: TokenApi " . $token,
                                )
                            );
                        
                            $context = stream_context_create($opts);
                        
                            $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                                'stream_context' => $context,
                            ]);
                        
                            $request_param = array("SolicitudServicioRecepcionFactura" => array(
                                "codigoAmbiente" => $vector_datos_generales["codigoAmbiente"],
                                "codigoDocumentoSector" => $vector_datos_generales["codigoDocumentoSector"],
                                "codigoEmision" => $vector_datos_generales["codigoEmision"],
                                "codigoModalidad" => $vector_datos_generales["codigoModalidad"],
                                "codigoPuntoVenta" => $vector_datos_generales["codigoPuntoVenta"],
                                "codigoSistema" => $vector_datos_generales["codigoSistema"],
                                "codigoSucursal" => $vector_datos_generales["codigoSucursal"],
                                "cufd" => $cufd,
                                "cuis" => $vector_datos_generales["cuis"],
                                "nit" => $vector_datos_generales["nit"],
                                "tipoFacturaDocumento"=>$vector_datos_generales["tipoFacturaDocumento"],
                                "archivo"=>$archivo,
                                "fechaEnvio"=>$vector_datos_generales["fechaEnvio"],
                                "hashArchivo"=>$hashArchivo
                            ));
                            $responce_param = $client->__soapCall('recepcionFactura', ["parameters" => $request_param]);
                            $respuesta = $responce_param->RespuestaServicioFacturacion;
                            return $respuesta;
                        }else//SI ES OFFLINE NO SE ENVÍA A IMPUESTOS, SOLO SE GENERA EL XML EN LA CARPETA DEL EVENTO
                            return true;
                    }
                }else
                    return false;
            }catch(Exception $e){
                echo $e->getMessage();
                return false;
            }
            return true;
        }

        public function anular_factura($vector_datos){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $vector_datos["token"],
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudServicioAnulacionFactura" => array(
                    "codigoAmbiente" => $vector_datos["codigoAmbiente"],
                    "codigoEmision" => $vector_datos["codigoEmision"],
                    "codigoSistema" => $vector_datos["codigoSistema"],
                    "codigoSucursal" => $vector_datos["codigoSucursal"],
                    "codigoMotivo"=>$vector_datos["codigoMotivo"],
                    "codigoModalidad" => $vector_datos["codigoModalidad"],
                    "cuis" => $vector_datos["cuis"],
                    "codigoPuntoVenta" => $vector_datos["codigoPuntoVenta"],
                    "cuf"=>$vector_datos["cuf"],
                    "tipoFacturaDocumento"=>$vector_datos["tipoFacturaDocumento"],
                    "nit" => $vector_datos["nit"],
                    "codigoDocumentoSector" => $vector_datos["codigoDocumentoSector"],
                    "cufd" => $vector_datos["cufd"]
                ));
                $responce_param = $client->__soapCall('anulacionFactura', ["parameters" => $request_param]);
                $respuesta = $responce_param->RespuestaServicioFacturacion;
                return $respuesta;
            }catch(Exception $e){
                echo $e->getMessage();
                return false;
            }
        }

        public function verificar_comunicacion($token){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("" => array());
                $responce_param = $client->__soapCall('verificarComunicacion', ["parameters" => $request_param]);
                $respuesta = $responce_param;
                return $respuesta->return->transaccion;
            }catch(Exception $e){
                return false;
            }
        }

        public function recepcion_paquete_facturas($token,$vector_datos,$vector_nombres_facturas,$ruta_paquete,$nombre_carpeta,$fecha_hora,$intento = 0){
            try{
                //REALIZANDO LA COMPRENSIÓN DE LAS FACTURAS XML Y GUARDARLAS EN UN ARCHIVO .tar
                $archivo = "";
                $hashArchivo = "";
                if($intento == 0)
                    $package = $nombre_carpeta.".tar";
                else
                    $package = $nombre_carpeta."-".$intento.".tar";

                $tar = new \PharData($package);

                for($i = 0; $i < count($vector_nombres_facturas); $i++){
                    $arch_xml = file_get_contents($ruta_paquete."/".$vector_nombres_facturas[$i]);
                    $localname = $vector_nombres_facturas[$i];
                    $tar->addFromString($localname, $arch_xml);
                }

                if(count($vector_nombres_facturas) != 0){
                    $archivo = gzcompress(file_get_contents($package), 9, FORCE_GZIP);
                    $hashArchivo = hash('sha256', $archivo, !true);
                    rename($package,$ruta_paquete."/".$package);
                }

                //PREPARACIÓN PARA ENVIAR PAQUETE
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);

                $cafc = $vector_datos["cafc"];
                $codigo_evento = $vector_datos["codigoRecepcion"];
                $cantidad_facturas = $vector_datos["cantidadFacturas"];
                
                $request_param = array("SolicitudServicioRecepcionPaquete" => array(
                    "codigoAmbiente" => $vector_datos["codigoAmbiente"],
                    "codigoDocumentoSector"=>$vector_datos["codigoDocumentoSector"],
                    "codigoEmision" => $vector_datos["codigoEmision"],
                    "codigoModalidad" => $vector_datos["codigoModalidad"],
                    "codigoPuntoVenta" => $vector_datos["codigoPuntoVenta"],
                    "codigoSistema" => $vector_datos["codigoSistema"],
                    "codigoSucursal" => $vector_datos["codigoSucursal"],
                    "cufd"=>$vector_datos["cufd"],
                    "cuis" => $vector_datos["cuis"],
                    "nit" => $vector_datos["nit"],
                    "tipoFacturaDocumento"=>$vector_datos["tipoFacturaDocumento"],
                    "archivo"=>$archivo,
                    "fechaEnvio"=>$fecha_hora,
                    "hashArchivo"=>$hashArchivo,
                    "cafc"=>$cafc,
                    "cantidadFacturas"=>$cantidad_facturas,
                    "codigoEvento" => $codigo_evento,
                ));
                
                $responce_param = $client->__soapCall('recepcionPaqueteFactura', ["parameters" => $request_param]);
                $respuesta = $responce_param;
            }catch(Exception $e){
                $respuesta = false;
            }
            return $respuesta;
        }

        public function validar_recepcion_paquete_facturas($token,$vector_datos,$fecha_hora){
            try{
                
                //PREPARACIÓN PARA ENVIAR PAQUETE
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);

                $codigo_recepcion = $vector_datos["codigoRecepcion"];
                
                $request_param = array("SolicitudServicioValidacionRecepcionPaquete" => array(
                    "codigoAmbiente" => $vector_datos["codigoAmbiente"],
                    "codigoDocumentoSector"=>$vector_datos["codigoDocumentoSector"],
                    "codigoEmision" => $vector_datos["codigoEmision"],
                    "codigoModalidad" => $vector_datos["codigoModalidad"],
                    "codigoPuntoVenta" => $vector_datos["codigoPuntoVenta"],
                    "codigoSistema" => $vector_datos["codigoSistema"],
                    "codigoSucursal" => $vector_datos["codigoSucursal"],
                    "cufd"=>$vector_datos["cufd"],
                    "cuis" => $vector_datos["cuis"],
                    "nit" => $vector_datos["nit"],
                    "tipoFacturaDocumento"=>$vector_datos["tipoFacturaDocumento"],
                    "codigoRecepcion" => $vector_datos["codigoRecepcion"],
                ));
                
                $responce_param = $client->__soapCall('validacionRecepcionPaqueteFactura', ["parameters" => $request_param]);
                $respuesta = $responce_param;
            }catch(Exception $e){
                $respuesta = false;
            }
            return $respuesta;
        
        }

        public function verificar_estado_factura($token,$vector_datos){
            try{
                
                //PREPARACIÓN PARA VERIFICAR ESTADO
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);
                
                $request_param = array("SolicitudServicioVerificacionEstadoFactura" => array(
                    "codigoAmbiente" => $vector_datos["codigoAmbiente"],
                    "codigoDocumentoSector"=>$vector_datos["codigoDocumentoSector"],
                    "codigoEmision" => $vector_datos["codigoEmision"],
                    "codigoModalidad" => $vector_datos["codigoModalidad"],
                    "codigoPuntoVenta" => $vector_datos["codigoPuntoVenta"],
                    "codigoSistema" => $vector_datos["codigoSistema"],
                    "codigoSucursal" => $vector_datos["codigoSucursal"],
                    "cufd"=>$vector_datos["cufd"],
                    "cuis" => $vector_datos["cuis"],
                    "nit" => $vector_datos["nit"],
                    "tipoFacturaDocumento"=>$vector_datos["tipoFacturaDocumento"],
                    "cuf" => $vector_datos["cuf"],
                ));
                
                $responce_param = $client->__soapCall('verificacionEstadoFactura', ["parameters" => $request_param]);
                $respuesta = $responce_param;
            }catch(Exception $e){
                $respuesta = false;
            }
            return $respuesta;
        }
    }