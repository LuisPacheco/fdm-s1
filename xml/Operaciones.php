<?php 
    require "../config/Conexion.php";
    //require "../modelos/GestionToken.php";
    
    date_default_timezone_set("America/La_Paz");

    class Operaciones{

        public function __construct(){
    	}

        public function listarPuntosVenta(){
            require "../modelos/Informacion.php";
            require "../modelos/PuntoVenta.php";
            try{
                $pv = new PuntoVenta();
                $datosPV = $pv->getPuntoVenta(0); //AQUÍ IGUAL DEBE IR EL PV0
                $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);

                $cad_cuis = "";
                if(!$get_cuis_pv) //cuis inválido
                    $cad_cuis = "ddd";
                else
                    $cad_cuis = $get_cuis_pv["codigo_cuis"];

                $sw = true;
                $fecha_actual = date("Y-m-d H:i:s");

                //actualizar estado de CUIS en base a la fecha de vigencia
                $lista_cuis = $pv->listar_cuis_activos();
                while($cuis_act = $lista_cuis->fetch_object()){
                    if($fecha_actual >= $cuis_act->fecha_hora || $cuis_act->fecha_hora == null ){
                        $act = $pv->actualizar_cuis_estado(0,$cuis_act->id_cuis);
                        if(!$act)
                            $sw = false;
                    }
                }

                //actualizar estado de cufd
                $lista_cufd = $pv->listar_cufd_activos();
                while($cufd_act = $lista_cufd->fetch_object()){
                    if($fecha_actual >= $cufd_act->fecha_hora || $cufd_act->fecha_hora == null || !$get_cuis_pv){
                        $act = $pv->actualizar_cufd_estado(0,$cufd_act->id_cufd);
                        if(!$act)
                            $sw = false;
                    }
                }
            
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . TOKEN,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(OPERACIONES, [
                    'stream_context' => $context,
                ]);

                
            
                $request_param = array("SolicitudConsultaPuntoVenta" => array(
                    "codigoAmbiente" => Informacion::mostrar2()["codigoAmbiente"],
                    "codigoSistema" => Informacion::mostrar2()["codigoSistema"],
                    "codigoSucursal" => Informacion::mostrar2()["codigoSucursal"], //valor que va cambiar cuando se registren sucursales
                    "cuis" => $cad_cuis,
                    "nit" => Informacion::mostrar2()["nit"]
                ));

                $responce_param = $client->__soapCall('consultaPuntoVenta', ["parameters" => $request_param]);
                $res_sin = $responce_param->RespuestaConsultaPuntoVenta;
                
                if($res_sin->transaccion){
                    $res_SIN_listaPV = $responce_param->RespuestaConsultaPuntoVenta->listaPuntosVentas;
                    //var_dump($res_SIN_listaPV);
                    //return;
                    //OBTENER EL LISTADO DE PUNTOS DE VENTAS EN LA BB.DD
                    $lista_pv_bbdd = $pv->listar2();
                    //$res = array();
                    $i = 0;
                    $cad = '{"res":[';
                       
                    while($rspta = $lista_pv_bbdd->fetch_object()){
                        //buscar cuis que esté activo(si existe)
                        $res_cuis = $pv->buscar_cuis_activo($rspta->id_punto_venta);
                        $codigo_cuis = null;
                        $fecha_hora_cuis = null;
                        $estado_cuis = null;

                        if($res_cuis && $res_cuis != null){
                            $codigo_cuis = $res_cuis["codigo_cuis"];
                            $fecha_hora_cuis = $res_cuis["fecha_hora"];
                            $estado_cuis = $res_cuis["estado_cuis"];
                        }

                        //buscar cufd que esté activo (si existe)
                        $res_cufd = $pv->buscar_cufd_activo($rspta->id_punto_venta);
                        $codigo_cufd = null;
                        $codigo_control_cufd = null;
                        $fecha_hora_cufd = null;
                        $estado_cufd = null;

                        if($res_cufd && $res_cufd != null){
                            $codigo_cufd = $res_cufd["codigo_cufd"];
                            $codigo_control_cufd = $res_cufd["codigo_control_cufd"];
                            $fecha_hora_cufd = $res_cufd["fecha_hora"];
                            $estado_cufd = $res_cufd["estado_cufd"];
                        }

                        
                        $sw_est = "";
                        if($i == 0){
                            //verificar si cuis y cufd existen o no para actualizar estado PV
                            if(($codigo_cufd != null || $codigo_cufd != 0) && ($codigo_cuis != null || $codigo_cuis != 0)){
                                $res_act = $pv->actualizar_estado_pv($rspta->codigoPuntoVenta,1);
                                $sw_est = true;
                            }else{
                                $res_act = $pv->actualizar_estado_pv($rspta->codigoPuntoVenta,0);
                                $sw_est = false;
                            }
                            $cad .= '{"id_punto_venta":"'.$rspta->id_punto_venta.'",
                                    "codigoPuntoVenta":"'.$rspta->codigoPuntoVenta.'",
                                    "nombrePuntoVenta":"'.$rspta->nombrePuntoVenta.'",
                                    "tipoPuntoVenta":"N/A",
                                    "estadoPuntoVenta":"'.$sw_est.'",

                                    "cuis":"<strong>'.$codigo_cuis.' </strong><br>' . $fecha_hora_cuis .'<br>",
                                    "codigo_cuis":"'.$codigo_cuis.'",
                                    "estado_cuis":"'.$estado_cuis.'",

                                    "cufd":"<strong>'.$codigo_cufd.' </strong><br>' . $codigo_control_cufd .'<br>'.$fecha_hora_cufd.'",
                                    "codigo_cufd":"'.$codigo_cufd.'",
                                    "codigo_control_cufd":"'.$codigo_control_cufd.'",
                                    "estado_cufd":"'.$estado_cufd.'"},';
                        }else{
                            if(is_object($res_SIN_listaPV)){
                                //verificar si cuis y cufd existen o no para actualizar estado PV
                                if(($codigo_cufd != null || $codigo_cufd != 0) && ($codigo_cuis != null || $codigo_cuis != 0)){
                                    $res_act = $pv->actualizar_estado_pv($res_SIN_listaPV->codigoPuntoVenta,1);
                                    $sw_est = true;
                                }else{
                                    $res_act = $pv->actualizar_estado_pv($res_SIN_listaPV->codigoPuntoVenta,0);
                                    $sw_est = false;
                                }
                                $cad .= '{"id_punto_venta":"'.$rspta->id_punto_venta.'",
                                    "codigoPuntoVenta":"'.$res_SIN_listaPV->codigoPuntoVenta.'",
                                    "nombrePuntoVenta":"'.$res_SIN_listaPV->nombrePuntoVenta.'",
                                    "tipoPuntoVenta":"'.$res_SIN_listaPV->tipoPuntoVenta.'",
                                    "estadoPuntoVenta":"'.$sw_est.'",

                                    "cuis":"<strong>'.$codigo_cuis.' </strong><br>' . $fecha_hora_cuis .'<br>",
                                    "codigo_cuis":"'.$codigo_cuis.'",
                                    "estado_cuis":"'.$estado_cuis.'",
                                
                                    "cufd":"<strong>'.$codigo_cufd.' </strong><br>' . $codigo_control_cufd .'<br>'.$fecha_hora_cufd.'",
                                    "codigo_cufd":"'.$codigo_cufd.'",
                                    "codigo_control_cufd":"'.$codigo_control_cufd.'",
                                    "estado_cufd":"'.$estado_cufd.'"},';
                            }else{
                                //verificar si cuis y cufd existen o no para actualizar estado PV
                                if(($codigo_cufd != null || $codigo_cufd != 0) && ($codigo_cuis != null || $codigo_cuis != 0)){
                                    $res_act = $pv->actualizar_estado_pv($res_SIN_listaPV[$i-1]->codigoPuntoVenta,1);
                                    $sw_est = true;
                                }else{
                                    $res_act = $pv->actualizar_estado_pv($res_SIN_listaPV[$i-1]->codigoPuntoVenta,0);
                                    $sw_est = false;
                                }
                                $cad .= '{"id_punto_venta":"'.$rspta->id_punto_venta.'",
                                    "codigoPuntoVenta":"'.$res_SIN_listaPV[$i-1]->codigoPuntoVenta.'",
                                    "nombrePuntoVenta":"'.$res_SIN_listaPV[$i-1]->nombrePuntoVenta.'",
                                    "tipoPuntoVenta":"'.$res_SIN_listaPV[$i-1]->tipoPuntoVenta.'",
                                    "estadoPuntoVenta":"'.$sw_est.'",

                                    "cuis":"<strong>'.$codigo_cuis.' </strong><br>' . $fecha_hora_cuis .'<br>",
                                    "codigo_cuis":"'.$codigo_cuis.'",
                                    "estado_cuis":"'.$estado_cuis.'",
                                
                                    "cufd":"<strong>'.$codigo_cufd.' </strong><br>' . $codigo_control_cufd .'<br>'.$fecha_hora_cufd.'",
                                    "codigo_cufd":"'.$codigo_cufd.'",
                                    "codigo_control_cufd":"'.$codigo_control_cufd.'",
                                    "estado_cufd":"'.$estado_cufd.'"},';
                            }
                        }
                        $i++;
                    }
                    $cad .= '{}]}';
                    //return $cad;
                    return json_decode($cad);
                }else{
                    //return false;
                    //Tendría que invalidarse el CUIS del PV0
                    if($get_cuis_pv){
                        $invalidar_cuis = $pv->actualizar_cuis_estado(0,$get_cuis_pv["id_cuis"]);

                        //actualizar estado de cufd
                        $lista_cufd = $pv->listar_cufd_activos();
                        while($cufd_act = $lista_cufd->fetch_object()){
                            $act = $pv->actualizar_cufd_estado(0,$cufd_act->id_cufd);
                            if(!$act)
                                $sw = false; 
                        }
                    }

                    //LISTA DE MENSAJES DONDE ESTAN LOS ERRORES
                    $lista_mensajes_sin = $res_sin->mensajesList->codigo;
                    //obtener los códigos de servicios               
                    require "SincronizacionDatos.php";
                    $sinc_datos = new SincronizacionDatos();
                    $lista_mensajes = $sinc_datos->obtenerCatalogoX("sincronizarListaMensajesServicios");
                    $res_lista_mensajes = $lista_mensajes["sincronizarListaMensajesServicios"];
                    $decode_lista_mensajes_servicios = json_decode($res_lista_mensajes);
                    $lista_mensajes = $decode_lista_mensajes_servicios->RespuestaListaParametricas->listaCodigos;
                    $res = "";
                    for($i = 0; $i < count($lista_mensajes); $i++){
                        if($lista_mensajes[$i]->codigoClasificador == $lista_mensajes_sin){
                            $res = $lista_mensajes[$i]->descripcion;
                            break;
                        }
                    }
                    return $res;               
                    
                }
            }catch(Exception $e){
                return false;
            }
        }

        public function registroPuntoVenta($codigoTipoPuntoVenta,$descripcion,$nombrePuntoVenta){
            require "../modelos/Informacion.php";
            //Obtener el CUIS del PV0 de la sucursal
            require "../modelos/PuntoVenta.php";
            $pv = new PuntoVenta();
            $datosPV = $pv->getPuntoVenta(0); //PV0, en este caso si debe estar 0
            $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);

            $respuesta = false;
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . TOKEN,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(OPERACIONES, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudRegistroPuntoVenta" => array(
                    "codigoAmbiente" => Informacion::mostrar2()["codigoAmbiente"],
                    "codigoModalidad" => Informacion::mostrar2()["codigoModalidad"],
                    "codigoSistema" => Informacion::mostrar2()["codigoSistema"],
                    "codigoSucursal" => Informacion::mostrar2()["codigoSucursal"],
                    "codigoTipoPuntoVenta" => $codigoTipoPuntoVenta,
                    "cuis" => $get_cuis_pv["codigo_cuis"],
                    "descripcion"=>$descripcion,
                    "nit" => Informacion::mostrar2()["nit"],
                    "nombrePuntoVenta"=>$nombrePuntoVenta
                ));
                $responce_param = $client->__soapCall('registroPuntoVenta', ["parameters" => $request_param]);
                $respuesta = $responce_param->RespuestaRegistroPuntoVenta;
            }catch(Exception $e){
                return $false;
            }
            return $respuesta;
        }

        public function cerrar_operaciones($cod_pv){
            require "../modelos/Informacion.php";
            //Obtener el CUIS del PV 
            require "../modelos/PuntoVenta.php";
            $pv = new PuntoVenta();
            $datosPV = $pv->getPuntoVenta($cod_pv); //cod_pv

            $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);
            $get_cufd_pv = $pv->buscar_cufd_activo($datosPV["id_punto_venta"]);//puede que no tenga registros si solo se generó CUIS

            $respuesta = false;
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . TOKEN,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(OPERACIONES, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudOperaciones" => array(
                    "codigoAmbiente" => Informacion::mostrar2()["codigoAmbiente"],
                    "codigoModalidad" => Informacion::mostrar2()["codigoModalidad"],
                    "codigoPuntoVenta" => $datosPV["codigoPuntoVenta"],
                    "codigoSistema" => Informacion::mostrar2()["codigoSistema"],
                    "codigoSucursal" => Informacion::mostrar2()["codigoSucursal"],
                    "cuis" => $get_cuis_pv["codigo_cuis"],
                    "nit" => Informacion::mostrar2()["nit"]
                ));

                $responce_param = $client->__soapCall('cierreOperacionesSistema', ["parameters" => $request_param]);
                $respuesta = $responce_param->RespuestaCierreSistemas;

                if($respuesta->transaccion){//si se ejecuta el servicio de cierre de servicio, se debe poner en 0 el estado del PV
                    $res_est = $pv->actualizar_estado_pv($datosPV["codigoPuntoVenta"],0);
                    $res_est = $pv->actualizar_cuis_estado(0,$get_cuis_pv["id_cuis"]);
                    if($get_cufd_pv != null && $get_cufd_pv != false)
                        $res_est = $pv->actualizar_cufd_estado(0,$get_cufd_pv["id_cufd"]);
                    return $res_est;
                }else{
                    //LISTA DE MENSAJES DONDE ESTAN LOS ERRORES
                    $lista_mensajes_sin = $respuesta->mensajesList->codigo;
                    //obtener los códigos de servicios               
                    require "SincronizacionDatos.php";
                    $sinc_datos = new SincronizacionDatos();
                    $lista_mensajes = $sinc_datos->obtenerCatalogoX("sincronizarListaMensajesServicios");
                    $res_lista_mensajes = $lista_mensajes["sincronizarListaMensajesServicios"];
                    $decode_lista_mensajes_servicios = json_decode($res_lista_mensajes);
                    $lista_mensajes = $decode_lista_mensajes_servicios->RespuestaListaParametricas->listaCodigos;
                    $res = "";
                    for($i = 0; $i < count($lista_mensajes); $i++){
                        if($lista_mensajes[$i]->codigoClasificador == $lista_mensajes_sin){
                            $res = $lista_mensajes[$i]->descripcion;
                            break;
                        }
                    }
                    return $res;     
                }
            }catch(Exception $e){
                return $false;
            }
            return $respuesta;
        }

        public function registrar_evento_significativo($token,$cod_ambiente,$cod_motivo_evento,$cod_punto_venta,$cod_sistema,$cod_sucursal,$cufd,$cufd_evento,$cuis,$descripcion,$fecha_hora_inicio,$fecha_hora_fin,$nit){
            $respuesta = "";
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(OPERACIONES, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudEventoSignificativo" => array(
                    "codigoAmbiente" => $cod_ambiente,
                    "codigoMotivoEvento" => $cod_motivo_evento,
                    "codigoPuntoVenta" => $cod_punto_venta,
                    "codigoSistema" => $cod_sistema,
                    "codigoSucursal" => $cod_sucursal,
                    "cufd" => $cufd,
                    "cufdEvento" => $cufd_evento,
                    "cuis"=>$cuis,
                    "descripcion"=>$descripcion,
                    "fechaHoraFinEvento" => $fecha_hora_fin,
                    "fechaHoraInicioEvento" => $fecha_hora_inicio,
                    "nit" => $nit
                ));
                $responce_param = $client->__soapCall('registroEventoSignificativo', ["parameters" => $request_param]);
                $respuesta = $responce_param->RespuestaListaEventos;
            }catch(Exception $e){
                $respuesta = false;
            }
            return $respuesta;
        }
    }