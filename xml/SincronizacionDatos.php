<?php 
    require "../config/Conexion.php";
    //require "../modelos/GestionToken.php";
    


    class SincronizacionDatos{

        public function __construct(){
    	}

        //obtener la fecha y hora
        public function obtenerFechaHora($token,$cod_ambiente,$cod_sistema,$nit,$cod_modalidad,$cod_sucursal,$cuis,$codigo_pv){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(SINC_DATOS, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudSincronizacion" => array(
                    "codigoAmbiente" => $cod_ambiente,
                    "codigoSistema" => $cod_sistema,
                    "nit" => $nit,
                    "codigoModalidad" => $cod_modalidad,
                    "codigoSucursal" => $cod_sucursal,
                    "cuis" => $cuis,
                    "codigoPuntoVenta" => $codigo_pv));
                $responce_param = $client->__soapCall('sincronizarFechaHora', ["parameters" => $request_param]);

                return $responce_param->RespuestaFechaHora->fechaHora;

            }catch(Exception $e){
                return false;
            }
            return true;
        }

        //listar sicronizaciones diarias
        public function listar(){
            $sql = "SELECT sd.* 
			FROM sincronizacion_datos sd 
			ORDER BY sd.fecha_hora_sincronizacion desc";
		    return ejecutarConsulta($sql);		
        }

        //verificar si hay sincronización para fecha actual
        public function hay_sincronizacion(){
            $sql = "SELECT sd.fecha_sincronizacion,sd.id_sincronizacion 
			        FROM sincronizacion_datos sd";
            return ejecutarConsulta($sql);
        }

        //generar sincronización del día
        public function generar_sinc($id_punto_venta){
            require "../modelos/Informacion.php";
            $array_sinc_datos = array(
                0 => "sincronizarActividades",//la descripción de la actividad comercial
                1 => "sincronizarFechaHora",//la hora actual, ver en que momento usar
                2 => "sincronizarListaActividadesDocumentoSector",//lista de documentos en base a la actividad, entiendo que debe ser FCV(3)
                3 => "sincronizarListaLeyendasFactura",//conjunto de mensajes que irían (1 al azar) al final de cada factura
                4 => "sincronizarListaMensajesServicios",//codigos para los servicios que se realice (analizar y averiguar)
                5 => "sincronizarListaProductosServicios",//asociar como corresponda a los productos de la farmacia (preferible usar del 0 al 7, averiguar y analizar)
                6 => "sincronizarParametricaEventosSignificativos",//motivos para no poder realizar facturas o acceder al sistema
                7 => "sincronizarParametricaMotivoAnulacion",//motivos para anulación de factura
                8 => "sincronizarParametricaPaisOrigen",//listado de todos los paises (Bolivia es 22)
                9 => "sincronizarParametricaTipoDocumentoIdentidad", //documentos de identificación para registrar transacción
                10 => "sincronizarParametricaTipoDocumentoSector", // siempre será 1 (Factura compra venta)
                11 => "sincronizarParametricaTipoEmision",//1 = en línea, 2 = fuera de línea, 3 = masivo ?? averiguar
                12 => "sincronizarParametricaTipoHabitacion",//?? nunca se usaría
                13 => "sincronizarParametricaTipoMetodoPago",//efectivo, tarjeta y la combinación de ambas
                14 => "sincronizarParametricaTipoMoneda", //1 = Boliviano, 2 = Dólar EEUU
                15 => "sincronizarParametricaTipoPuntoVenta", //supongo que 5 (punto de venta cajeros)
                16 => "sincronizarParametricaTiposFactura", //siempre será 1 derecho a crédito fiscal
                17 => "sincronizarParametricaUnidadMedida" //lista grandes de unidades de medida, ver los adecuados para la farmacia
            );                    

            try{
                $opts = array('http' => array('header' => "apikey: TokenApi " . TOKEN));//el token delegado se cambia despues de un año
                $context = stream_context_create($opts);
                $client = new SoapClient(SINC_DATOS, ['stream_context' => $context,]);

                $inf = Informacion::mostrar2();
                require "../modelos/PuntoVenta.php";
                $pv = new PuntoVenta();

                $datosPV = $pv->getPuntoVenta2($id_punto_venta);//AQUI DEBE IR EL PV de la sesión actual

                $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);

                $request_param = array("SolicitudSincronizacion" => array(
                    "codigoAmbiente" => $inf["codigoAmbiente"],
                    "codigoSistema" => $inf["codigoSistema"],
                    "nit" => $inf["nit"],
                    "codigoModalidad" => $inf["codigoModalidad"],
                    "codigoSucursal" => $inf["codigoSucursal"], //valor que va cambiar cuando se registren sucursales
                    "cuis" => $get_cuis_pv["codigo_cuis"], //valor que va cambiar por punto de venta
                    "codigoPuntoVenta" => $datosPV["codigoPuntoVenta"])); //valor que va cambiar
                
                for($i = 0; $i < count($array_sinc_datos); $i++){
                    $responce_param = $client->__soapCall($array_sinc_datos[$i], ["parameters" => $request_param]);
                    //var_dump(array_keys((array)$responce_param)["0"]);
                    //return false;
                    if(is_object($responce_param)){
                        $nombre = array_keys((array)$responce_param)["0"];
                        if($responce_param->$nombre->transaccion){
                            $respuesta = '{';
                            if($i != 1){ //1 = sincronización de la hora (indice 1) que se hará aparte
                                $claves_1 = array_keys((array)$responce_param);
                                $valores_1 = array_values((array)$responce_param);
                                for($j = 0; $j < count($claves_1); $j++){
                                    $respuesta .= '"'. $claves_1[$j] .'":{';
                                    $claves_2 = array_keys((array)$valores_1[$j]); //titulos de "transaccion" y "lista..."
                                    $valores_2 = array_values((array)$valores_1[$j]);
                                    
                                    for($k = 0; $k < count($claves_2); $k++){//de longitud 2
                                        $respuesta .= '"' . $claves_2[$k] .'":';
                                        if(is_array($valores_2[$k])){//indice 0 no es array, indice 1 si es array
                                            $claves_3 = array_keys($valores_2[$k]);
                                            $valores_3 = array_values($valores_2[$k]);
                                            $respuesta .= '[';
                                            for($l = 0 ; $l < count($claves_3);$l++){
                                                $respuesta .= '{';
                                                $claves_4 = array_keys((array)$valores_3[$l]);
                                                $valores_4 = array_values((array)$valores_3[$l]);
                                                for($m = 0; $m < count($claves_4); $m++){
                                                    if($i != 5){
                                                        if($m != count($claves_4) - 1){
                                                            $respuesta .= '"'.$claves_4[$m].'":"'.$valores_4[$m].'",';
                                                        }else{
                                                            $respuesta .= '"'.$claves_4[$m].'":"'.$valores_4[$m].'"';
                                                        }
                                                    }else{
                                                        if($l < 8){//ya que a antes del índice 8 se tiene la nomenclatura "nandina"
                                                            if($m != count($claves_4) - 1 ){
                                                                if($m != count($claves_4) - 2){
                                                                    $respuesta .= '"'.$claves_4[$m].'":"'.$valores_4[$m].'",';
                                                                }else{
                                                                    $respuesta .= '"'.$claves_4[$m].'":"'.$valores_4[$m].'"';
                                                                }
                                                            }
                                                        }else{
                                                            if($m != count($claves_4) - 1){
                                                                $respuesta .= '"'.$claves_4[$m].'":"'.$valores_4[$m].'",';
                                                            }else{
                                                                $respuesta .= '"'.$claves_4[$m].'":"'.$valores_4[$m].'"';
                                                            }
                                                        }
                                                    }
                                                }
                                                if($l != count($claves_3) - 1)
                                                    $respuesta .= '},';
                                                else
                                                    $respuesta .= '}';
                                            }
                                            if($k != count($claves_2) - 1)
                                                $respuesta .='],';
                                            else
                                                $respuesta .= ']';
                                        }else{
                                            if($i != 0){ //el código de la transacción (que debería ser 1)
                                                if($k != count($claves_2) - 1)
                                                    $respuesta .= '"' . $valores_2[$k] . '",';
                                                else
                                                    $respuesta .= '"' . $valores_2[$k] . '"';
                                            }else{ //el caso especial es para la actividad económica
                                                if($k == 0)
                                                    $respuesta .= '"' . $valores_2[$k] . '",';
                                                else{
                                                    $respuesta .= '{';
                                                    $claves_act = array_keys((array)$valores_2[$k]);
                                                    $valores_act = array_values((array)$valores_2[$k]);
                
                                                    for($l = 0; $l < count($claves_act); $l++){
                                                        if($l != count($claves_act) - 1){
                                                            $respuesta .= '"'.$claves_act[$l].'":"' . $valores_act[$l] . '",';
                                                        }else{
                                                            $respuesta .= '"'.$claves_act[$l].'":"' . $valores_act[$l] . '"';
                                                        }
                                                    }
                                                    $respuesta .= '}';
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    $respuesta .= '}';
                                }
                            }else{
                                $respuesta = '{"RespuestaFechaHora":{"transaccion":"' . $responce_param->RespuestaFechaHora->transaccion . '","fechaHora":"' . $responce_param->RespuestaFechaHora->fechaHora . '"}';
                            }
                            $respuesta .= '}';

                            if($i == 0){
                                $fecha = date("Y-m-d");
                                $ahora = date("Y-m-d H:i:s");
                                $sql = "INSERT INTO sincronizacion_datos(fecha_sincronizacion,fecha_hora_sincronizacion) VALUES('$fecha','$ahora')";
                    
                                $res = ejecutarConsulta($sql);
                    
                            }
                            
                            $sql = "UPDATE sincronizacion_datos SET $array_sinc_datos[$i] = '$respuesta' WHERE fecha_sincronizacion = '$fecha'";

                            $res = ejecutarConsulta($sql);
                            if(!$res)
                                return false;
                            //return $res;
                        }else
                            return false;
                    }else
                        return false;   
                    
                }
                
            }catch(Exception $e){
                return $e->getMessage();
            }

            return $res;
                
        }

        //sincronizar hora
        public function sincronizar_hora($id_punto_venta){
            require "../modelos/Informacion.php";
            try{
                $opts = array('http' => array('header' => "apikey: TokenApi " . TOKEN));//el token delegado se cambia despues de un año
                $context = stream_context_create($opts);
                $client = new SoapClient(SINC_DATOS, ['stream_context' => $context,]);

                $inf = Informacion::mostrar2();
                require "../modelos/PuntoVenta.php";
                $pv = new PuntoVenta();

                $datosPV = $pv->getPuntoVenta2($id_punto_venta);//AQUI DEBE IR EL PV de la sesión actual

                $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);

                $request_param = array("SolicitudSincronizacion" => array(
                    "codigoAmbiente" => $inf["codigoAmbiente"],
                    "codigoSistema" => $inf["codigoSistema"],
                    "nit" => $inf["nit"],
                    "codigoModalidad" => $inf["codigoModalidad"],
                    "codigoSucursal" => $inf["codigoSucursal"], //valor que va cambiar cuando se registren sucursales
                    "cuis" => $get_cuis_pv["codigo_cuis"], //valor que va cambiar por punto de venta
                    "codigoPuntoVenta" => $datosPV["codigoPuntoVenta"])); //valor que va cambiar
                
                $responce_param = $client->__soapCall("sincronizarFechaHora", ["parameters" => $request_param]);

                $respuesta_fecha_hora = $responce_param->RespuestaFechaHora;
                    
                $respuesta = '{"RespuestaFechaHora":{"transaccion":"' . $respuesta_fecha_hora->transaccion . '","fechaHora":"' . $respuesta_fecha_hora->fechaHora . '"}}';

                $fecha = date("Y-m-d");                    
                
                $sql = "UPDATE sincronizacion_datos SET sincronizarFechaHora = '$respuesta' WHERE fecha_sincronizacion = '$fecha'";

                return ejecutarConsulta($sql);
                
            }catch(Exception $e){
                return $e->getMessage();
            }
        }

        //actualizar sincronizacion
        public function actualizar_sincronizacion($id_punto_venta){
            $fecha = date("Y-m-d");
            $sql = "DELETE FROM sincronizacion_datos WHERE fecha_sincronizacion = '$fecha'";
            if(ejecutarConsulta($sql))
                return $this->generar_sinc($id_punto_venta);
            else
                return false;
        }

        //HACER LLAMADAS PARA OBTENER CATÁLOGOS
        public function obtenerCatalogoX($columna){
            if($this->hay_sincronizacion()){
                $sql = "SELECT $columna FROM sincronizacion_datos ORDER BY fecha_sincronizacion DESC LIMIT 1";
                return ejecutarConsultaSimpleFila($sql);
            }else
                return false;
        }

    }