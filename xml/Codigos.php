<?php 
    require "../config/Conexion.php";
    //require "../modelos/GestionToken.php";
    
    date_default_timezone_set("America/La_Paz");

    class Codigos{

        public function __construct(){
    	}

        public function generarCuis($cod_pv){
            require "../modelos/Informacion.php";
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . TOKEN,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(CODIGOS, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudCuis" => array(
                    "codigoAmbiente" => Informacion::mostrar2()["codigoAmbiente"],
                    "codigoModalidad" => Informacion::mostrar2()["codigoModalidad"],
                    "codigoPuntoVenta" => $cod_pv,
                    "codigoSistema" => Informacion::mostrar2()["codigoSistema"],
                    "codigoSucursal" => Informacion::mostrar2()["codigoSucursal"],
                    "nit" => Informacion::mostrar2()["nit"]
                ));

                $responce_param = $client->__soapCall('cuis', ["parameters" => $request_param]);
                $res_cuis = $responce_param->RespuestaCuis;
                
                if($res_cuis->transaccion)
                    return $res_cuis;
                else{
                    //LISTA DE MENSAJES DONDE ESTAN LOS ERRORES
                    $lista_mensajes_sin = $res_cuis->mensajesList->codigo;
                    //obtener los códigos de servicios               
                    require "SincronizacionDatos.php";
                    $sinc_datos = new SincronizacionDatos();
                    $lista_mensajes = $sinc_datos->obtenerCatalogoX("sincronizarListaMensajesServicios");
                    $res_lista_mensajes = $lista_mensajes["sincronizarListaMensajesServicios"];
                    $decode_lista_mensajes_servicios = json_decode($res_lista_mensajes);
                    $lista_mensajes = $decode_lista_mensajes_servicios->RespuestaListaParametricas->listaCodigos;
                    $res = "";
                    for($i = 0; $i < count($lista_mensajes); $i++){
                        if($lista_mensajes[$i]->codigoClasificador == $lista_mensajes_sin){
                            $res = $lista_mensajes[$i]->descripcion;
                            break;
                        }
                    }
                    return $res;               
                }
            }catch(Exception $e){
                return false;
            }
        }

        public function generarCufd($cod_pv){
            require "../modelos/Informacion.php";
            require "../modelos/PuntoVenta.php";
            $pv = new PuntoVenta();
            //obtener CUIS del pv (se debe tener CUIS primeramente)
            $datosPV = $pv->getPuntoVenta($cod_pv);
            $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);

            if($datosPV != NULL && $get_cuis_pv["codigo_cuis"] != null){//si el PV no tiene CUIS no se puede continuar
                try{
                    $opts = array(
                        'http' => array(
                            'header' => "apikey: TokenApi " . TOKEN,
                        )
                    );
                
                    $context = stream_context_create($opts);
                
                    $client = new SoapClient(CODIGOS, [
                        'stream_context' => $context,
                    ]);
                
                    $request_param = array("SolicitudCufd" => array(
                        "codigoAmbiente" => Informacion::mostrar2()["codigoAmbiente"],
                        "codigoModalidad" => Informacion::mostrar2()["codigoModalidad"],
                        "codigoPuntoVenta" => $cod_pv,
                        "codigoSistema" => Informacion::mostrar2()["codigoSistema"],
                        "codigoSucursal" => Informacion::mostrar2()["codigoSucursal"],
                        "cuis"=>$get_cuis_pv["codigo_cuis"],
                        "nit" => Informacion::mostrar2()["nit"]
                    ));

                    $responce_param = $client->__soapCall('cufd', ["parameters" => $request_param]);
                    $res_cufd = $responce_param->RespuestaCufd;
                    
                    if($res_cufd->transaccion)
                        return $res_cufd;
                    else{
                        //LISTA DE MENSAJES DONDE ESTAN LOS ERRORES
                        $lista_mensajes_sin = $res_cufd->mensajesList->codigo;
                        //obtener los códigos de servicios               
                        require "SincronizacionDatos.php";
                        $sinc_datos = new SincronizacionDatos();
                        $lista_mensajes = $sinc_datos->obtenerCatalogoX("sincronizarListaMensajesServicios");
                        $res_lista_mensajes = $lista_mensajes["sincronizarListaMensajesServicios"];
                        $decode_lista_mensajes_servicios = json_decode($res_lista_mensajes);
                        $lista_mensajes = $decode_lista_mensajes_servicios->RespuestaListaParametricas->listaCodigos;
                        $res = "";
                        for($i = 0; $i < count($lista_mensajes); $i++){
                            if($lista_mensajes[$i]->codigoClasificador == $lista_mensajes_sin){
                                $res = $lista_mensajes[$i]->descripcion;
                                break;
                            }
                        }
                        return $res;               
                    }
                }catch(Exception $e){
                    return false;
                }
            }else
                return false;
        }

        public function generarCufd2($cod_pv,$cod_ambiente,$cod_modalidad,$cod_sistema,$cod_sucursal,$cod_cuis,$nit,$token){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(CODIGOS, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudCufd" => array(
                    "codigoAmbiente" => $cod_ambiente,
                    "codigoModalidad" => $cod_modalidad,
                    "codigoPuntoVenta" => $cod_pv,
                    "codigoSistema" => $cod_sistema,
                    "codigoSucursal" => $cod_sucursal,
                    "cuis"=>$cod_cuis,
                    "nit" => $nit
                ));

                $responce_param = $client->__soapCall('cufd', ["parameters" => $request_param]);
                $res_cufd = $responce_param->RespuestaCufd;
                
                if($res_cufd->transaccion)
                    return $res_cufd;
                else
                    return $res_cufd->mensajesList->descripcion;     
            }catch(Exception $e){
                return false;
            }
        }

        //GENERAR CUFD POR EL id_punto_venta
        public function generarCufd3($id_punto_venta){
            require "../modelos/Informacion.php";
            require "../modelos/PuntoVenta.php";
            $pv = new PuntoVenta();
            //obtener CUIS del pv (se debe tener CUIS primeramente)
            $datosPV = $pv->getPuntoVenta2($id_punto_venta);
            $get_cuis_pv = $pv->buscar_cuis_activo($datosPV["id_punto_venta"]);

            if($datosPV != NULL && $get_cuis_pv["codigo_cuis"] != null){//si el PV no tiene CUIS no se puede continuar
                try{
                    $opts = array(
                        'http' => array(
                            'header' => "apikey: TokenApi " . TOKEN,
                        )
                    );
                
                    $context = stream_context_create($opts);
                
                    $client = new SoapClient(CODIGOS, [
                        'stream_context' => $context,
                    ]);
                
                    $request_param = array("SolicitudCufd" => array(
                        "codigoAmbiente" => Informacion::mostrar2()["codigoAmbiente"],
                        "codigoModalidad" => Informacion::mostrar2()["codigoModalidad"],
                        "codigoPuntoVenta" => $datosPV["codigoPuntoVenta"],
                        "codigoSistema" => Informacion::mostrar2()["codigoSistema"],
                        "codigoSucursal" => Informacion::mostrar2()["codigoSucursal"],
                        "cuis"=>$get_cuis_pv["codigo_cuis"],
                        "nit" => Informacion::mostrar2()["nit"]
                    ));

                    $responce_param = $client->__soapCall('cufd', ["parameters" => $request_param]);
                    $res_cufd = $responce_param->RespuestaCufd;
                    
                    if($res_cufd->transaccion)
                        return $res_cufd;
                    else{
                        //LISTA DE MENSAJES DONDE ESTAN LOS ERRORES
                        $lista_mensajes_sin = $res_cufd->mensajesList->codigo;
                        //obtener los códigos de servicios               
                        require "SincronizacionDatos.php";
                        $sinc_datos = new SincronizacionDatos();
                        $lista_mensajes = $sinc_datos->obtenerCatalogoX("sincronizarListaMensajesServicios");
                        $res_lista_mensajes = $lista_mensajes["sincronizarListaMensajesServicios"];
                        $decode_lista_mensajes_servicios = json_decode($res_lista_mensajes);
                        $lista_mensajes = $decode_lista_mensajes_servicios->RespuestaListaParametricas->listaCodigos;
                        $res = "";
                        for($i = 0; $i < count($lista_mensajes); $i++){
                            if($lista_mensajes[$i]->codigoClasificador == $lista_mensajes_sin){
                                $res = $lista_mensajes[$i]->descripcion;
                                break;
                            }
                        }
                        return $res;               
                    }
                }catch(Exception $e){
                    return false;
                }
            }else
                return false;
        }

        public function verificar_nit($token,$vector_datos){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(CODIGOS, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("SolicitudVerificarNit" => array(
                    "codigoAmbiente" => $vector_datos["codigoAmbiente"],
                    "codigoModalidad" => $vector_datos["codigoModalidad"],
                    "codigoSistema" => $vector_datos["codigoSistema"],
                    "codigoSucursal" => $vector_datos["codigoSucursal"],
                    "cuis"=>$vector_datos["cuis"],
                    "nit" => $vector_datos["nit"],
                    "nitParaVerificacion" => $vector_datos["nitParaVerificacion"]
                ));

                $responce_param = $client->__soapCall('verificarNit', ["parameters" => $request_param]);
                $res = $responce_param;
                
            }catch(Exception $e){
                $res = false;
            }

            return $res;
        }
    }