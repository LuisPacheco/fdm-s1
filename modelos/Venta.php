<?php
	//Import PHPMailer classes into the global namespace
    //These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception; 

	date_default_timezone_set("America/La_Paz");
	//Incluímos inicialmente la conexión a la base de datos

	require "../config/Conexion.php";



	Class Venta{

		//Implementamos nuestro constructor

		public function __construct(){
		}

		//Contar nro de ventas
		public function contar_nro_ventas(){
			$sql = "SELECT COUNT(nro_venta) AS cant_ventas 
					FROM venta
					";
			return ejecutarConsultaSimpleFila($sql);
		}


		//Implementamos un método para insertar registros
		public function insertar($nro_venta,$nro_venta_contingencia,$idcliente,$idusuario,$metodo_pago,$nro_tarjeta,$nro_tarjeta_ofus,$fecha_hora,$fecha_hora_registro,$codigo_excepcion,$cafc,$id_punto_venta,$cufd,$cuf,$leyenda,$total_venta,$monto,$cambio,$codigo_recepcion,$nombre_archivo,$ruta,$tipo_emision,$id_evento_significativo,$estado,$otro_metodo_pago,
								$idarticulo,$cantidad,$precio_venta,$descuento,$idlote,$cod_med,$cod_actividad,$cod_producto_sin,$unidad_medida,$nombre_unidad_medida,$descripcion){
			try{
				date_default_timezone_set("America/La_Paz");
				$evento_sig = "";
				if($id_evento_significativo == "" || $id_evento_significativo == null)
					$evento_sig = "0";
				else
					$evento_sig = $id_evento_significativo;
				

				$sql="INSERT INTO venta (nro_venta,nro_venta_contingencia,idcliente,idusuario,metodo_pago,nro_tarjeta,nro_tarjeta_ofus,fecha_hora,fecha_hora_registro,codigo_excepcion,cafc,id_punto_venta,codigo_cufd,cuf,leyenda,total_venta,monto,cambio,codigo_recepcion,nombre_archivo,ruta,tipo_emision,id_evento_significativo,estado,otro_metodo_pago)
						VALUES ('$nro_venta','$nro_venta_contingencia','$idcliente','$idusuario','$metodo_pago','$nro_tarjeta','$nro_tarjeta_ofus','$fecha_hora','$fecha_hora_registro','$codigo_excepcion','$cafc','$id_punto_venta','$cufd','$cuf','$leyenda','$total_venta','$monto','$cambio','$codigo_recepcion','$nombre_archivo','$ruta','$tipo_emision','$evento_sig','$estado','$otro_metodo_pago')";

				$idventanew=ejecutarConsulta_retornarID($sql);

				$num_elementos=0;

				$sw1=true;

				require_once "../modelos/Articulo.php";

				require_once "../modelos/Lote.php";

				$articulo = new Articulo();

				$lote = new Lote();



				while ($num_elementos < count($precio_venta)){

					$sql_detalle = "INSERT INTO detalle_venta(idventa, idarticulo,cantidad,precio_venta,descuento,
																idlote,cod_med,codigoActividad,codigoProductoSin,unidadMedida,
																nombreUnidadMedida,descripcion) 

									VALUES ('$idventanew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_venta[$num_elementos]','$descuento[$num_elementos]',
											'$idlote[$num_elementos]','$cod_med[$num_elementos]','$cod_actividad[$num_elementos]','$cod_producto_sin[$num_elementos]','$unidad_medida[$num_elementos]',
											'$nombre_unidad_medida[$num_elementos]','$descripcion[$num_elementos]')";

					$sql_cant_articulo = $articulo->devolverStock($idarticulo[$num_elementos]);

					$cant_articulo = $sql_cant_articulo->fetch_object();

					$stock_actual = $cant_articulo->stock - $cantidad[$num_elementos];

					$sql_modif_stock = $articulo->modificarStock($idarticulo[$num_elementos],$stock_actual);

					if(!$sql_modif_stock)

						$sw1 = false;



					$sql_cant_lote = $lote->getStock($idlote[$num_elementos]);

					$cant_lote = $sql_cant_lote->fetch_object();

					$stock_lote_actual = $cant_lote->stock - $cantidad[$num_elementos];

					$sql_modif_stock_lote = $lote->setStock($idlote[$num_elementos],$stock_lote_actual);

					if(!$sql_modif_stock_lote)

						$sw1 = false;



					ejecutarConsulta($sql_detalle) or $sw1 = false;

					$num_elementos=$num_elementos + 1;

				}

				return $sw1 ;
			} catch (Exception $e) {
				return $e->getMessage();
			}

		}

		//ACTUALIZAR VENTA
		public function actualizar($id_venta,$id_usuario,$cufd,$id_evento_significativo,$num_venta,$num_venta_contingencia,$fecha_hora,$fecha_hora_registro,$id_cliente,$codigo_excepcion,$cafc,$id_punto_venta,$cuf,$nombre_archivo){
			$sql = "UPDATE venta SET
					nro_venta = '$num_venta',nro_venta_contingencia = '$num_venta_contingencia',
					idcliente = '$id_cliente', idusuario = '$id_usuario',
					fecha_hora = '$fecha_hora', fecha_hora_registro = '$fecha_hora_registro',
					codigo_excepcion = '$codigo_excepcion',cafc = '$cafc',
					id_punto_venta = '$id_punto_venta', codigo_cufd = '$cufd',
					cuf = '$cuf', id_evento_significativo = '$id_evento_significativo',
					nombre_archivo = '$nombre_archivo'
					WHERE idventa = '$id_venta'";
			return ejecutarConsulta($sql);
		}
		

		//Implementamos un método para anular la venta
		public function anular($idventa){
			date_default_timezone_set("America/La_Paz");
			$sql="UPDATE venta SET estado='Anulado' WHERE idventa='$idventa'";

			return ejecutarConsulta($sql);
		}





		//Implementar un método para mostrar los datos de un registro a modificar
		public function mostrar($idventa){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT v.*,DATE(v.fecha_hora) as fecha,p.nombre as cliente,p.num_documento,p.email,u.idusuario,u.nombre as usuario
					FROM venta v 
					INNER JOIN persona p ON v.idcliente=p.idpersona 
					INNER JOIN usuario u ON v.idusuario=u.idusuario 
					WHERE v.idventa='$idventa'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function mostrar2($nombre_archivo){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT v.*,DATE(v.fecha_hora) as fecha,p.nombre as cliente,u.idusuario,u.nombre as usuario
				FROM venta v 
				INNER JOIN persona p ON v.idcliente=p.idpersona 
				INNER JOIN usuario u ON v.idusuario=u.idusuario 
				WHERE v.nombre_archivo='$nombre_archivo'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function mostrar3($nombre_paquete){//buscar si el archivo tiene parte del nombre de la ruta
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT v.idventa,DATE(v.fecha_hora) as fecha,v.idcliente,p.nombre as cliente,p.email,p.num_documento,u.idusuario,u.nombre as usuario,v.total_venta,v.estado,v.monto,v.cambio,v.cuf,v.nombre_archivo,v.ruta,v.fecha_hora,v.nro_venta,v.nro_venta_contingencia
				FROM venta v 
				INNER JOIN persona p ON v.idcliente=p.idpersona 
				INNER JOIN usuario u ON v.idusuario=u.idusuario 
				WHERE v.ruta LIKE '%$nombre_paquete%'
				ORDER BY v.fecha_hora";
			return ejecutarConsulta($sql);
		}

		public function listarDetalle($idventa){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT dv.iddetalle_venta,dv.idventa,dv.idarticulo,dv.cantidad,dv.precio_venta,dv.descuento,(dv.cantidad * dv.precio_venta - dv.descuento) as subtotal, lo.cod_lote, lo.idlote,lo.vencimiento,a.nombre,a.idarticulo,a.cod_med,a.codigoActividad,a.codigoProductoSin,a.unidadMedida,a.nombreUnidadMedida
				FROM detalle_venta dv 
				inner join articulo a on dv.idarticulo=a.idarticulo
				inner join lote lo ON dv.idlote = lo.idlote 
				where dv.idventa='$idventa'
				GROUP by lo.idlote";
			return ejecutarConsulta($sql);
		}

		//Implementar un método para listar los registros
		public function listar(){
			
			$hoy = date("Y-m-d");
			$sql="SELECT v.idventa,v.nro_venta,v.nro_venta_contingencia,v.fecha_hora as fecha,v.ruta,v.fecha_hora_registro,v.idcliente,p.nombre as cliente,p.email,p.num_documento,u.idusuario,u.nombre as usuario,v.metodo_pago,v.codigo_cufd,v.total_venta,v.estado,v.nombre_archivo,v.cuf,v.cafc

				FROM venta v 

				INNER JOIN persona p ON v.idcliente=p.idpersona 

				INNER JOIN usuario u ON v.idusuario=u.idusuario

				where v.fecha_hora between date_sub(now(),INTERVAL 5 DAY) and now()

				ORDER by v.idventa desc";

			return ejecutarConsulta($sql);		

		}

		public function ventacabecera($nombre_archivo){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT v.idventa,v.idcliente,p.nombre as cliente,v.ruta,p.direccion,p.tipo_documento,p.num_documento,p.complemento,p.email,p.telefono,v.idusuario,u.nombre as usuario,u.login AS vendedor,v.nro_venta,v.nro_venta_contingencia,DAY(v.fecha_hora) as dia,MONTH(v.fecha_hora) as mes,YEAR(v.fecha_hora) as anio,HOUR(v.fecha_hora) as hora,MINUTE(v.fecha_hora) as minuto, SECOND(v.fecha_hora) as segundo,v.total_venta, pv.codigoPuntoVenta,v.cuf,v.leyenda,v.nombre_archivo,v.tipo_emision

				FROM venta v 

				INNER JOIN persona p ON v.idcliente=p.idpersona 

				INNER JOIN usuario u ON v.idusuario=u.idusuario 

				INNER JOIN punto_venta pv ON pv.id_punto_venta = v.id_punto_venta

				WHERE v.nombre_archivo='$nombre_archivo'";

						return ejecutarConsultaSimpleFila($sql);

		}

		public function ventadetalle($idventa){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT d.*,(d.cantidad*d.precio_venta-d.descuento) as subtotal 
				FROM detalle_venta d 
				WHERE d.idventa='$idventa'";
			return ejecutarConsulta($sql);
		}

		public function obtener_efectivo($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(v.total_venta) as total

					FROM venta v 

					WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Aceptado' AND metodo_pago = '1' AND idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		public function obtener_pagos_tarjeta($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(v.total_venta) as total

					FROM venta v 

					WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Aceptado' AND metodo_pago = '2' AND idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		public function obtener_pagos_transferencia($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(v.total_venta) as total

					FROM venta v 

					WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Aceptado' AND metodo_pago = '7' AND idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		public function obtener_efectivo_anulado($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(v.total_venta) as total

					FROM venta v 

					WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Anulado' AND metodo_pago = '1' AND idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		public function obtener_tarjeta_anulado($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(v.total_venta) as total

					FROM venta v 

					WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Anulado' AND metodo_pago = '2' AND idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		public function obtener_transferencia_anulado($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(v.total_venta) as total

					FROM venta v 

					WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Anulado' AND metodo_pago = '7' AND idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		public function obtener_descuentos($idusuario,$fecha){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(dv.descuento) as total
					FROM venta v

					INNER JOIN detalle_venta dv ON dv.idventa = v.idventa 

					WHERE DATE(v.fecha_hora)='$fecha' AND v.estado = 'Aceptado' AND v.idusuario = '$idusuario'";

			return ejecutarConsulta($sql);

		}

		//GENERAR CUF DE LA VENTA
		public function generar_cuf($nit_emisor,$fecha_hora,$cod_sucursal,$modalidad,$tipo_emision,$tipo_factura,$tipo_doc,$nro_factura,$pv,$cod_autorizacion_cufd){

			//para nit debe completarse 13 caracteres
			$nit_emisor = $this->rellenar_ceros($nit_emisor,13);

			//para fecha debe completarse 17 caracteres
			$fecha_hora = $this->rellenar_ceros($fecha_hora,17);

			//para sucursal debe completarse 4 caracteres
			$cod_sucursal = $this->rellenar_ceros($cod_sucursal,4);

			//para documento sector debe completarse 2 caracteres
			$tipo_doc = $this->rellenar_ceros($tipo_doc,2);

			//para el nro de factura debe completarse 10 caracteres
			$nro_factura = $this->rellenar_ceros($nro_factura,10);
			
			//para el punto de venta debe completarse 4 caracteres
			$pv = $this->rellenar_ceros($pv,4);

			$cad = $nit_emisor.$fecha_hora.$cod_sucursal.$modalidad.$tipo_emision.$tipo_factura.$tipo_doc.$nro_factura.$pv;

			$verificador 	= $this->calculaDigitoMod11($cad, 1, 9, false);
		
			$b16_str 		= strtoupper( $this->bcdechex( $cad . $verificador ) );
			return $b16_str . $cod_autorizacion_cufd;
		}

		public function calculaDigitoMod11(string $cadena, int $numDig, int $limMult, bool $x10){
			$cadenaSrc = $cadena;
			
			$mult = $suma = $i = $n = $dig = 0;
			
			if (!$x10) $numDig = 1;
			
			for($n = 1; $n <= $numDig; $n++){
				$suma = 0;
				$mult = 2;
				for($i = strlen($cadena) - 1; $i >= 0; $i--){
					$cadestr = $cadena[$i];
					$intNum = (int)($cadestr);
					$suma += ($mult * $intNum);
					if(++$mult > $limMult) $mult = 2;
				}
				if ($x10){
					$dig = (($suma * 10) % 11) % 10;
				}else{
					$dig = $suma % 11;
				}
				if ($dig == 10){
					$cadena .= "1";
				}
				if ($dig == 11){
					$cadena .= "0";
				}
				if ($dig < 10) {
					$cadena .= $dig;
				}
			}
			
			$modulo = substr($cadena, strlen($cadena) - $numDig, strlen($cadena));
		
			
			return $modulo;
		}


		public function bcdechex($dec) {
			$hex = '';
			do {
				$last = bcmod($dec, 16);
				$hex = dechex($last).$hex;
				$dec = bcdiv(bcsub($dec, $last), 16);
			} while($dec>0);
			return $hex;
		}

		public function rellenar_ceros($cad,$lim){
			while(strlen($cad) < $lim)
				$cad = "0".$cad;
			return $cad;
		}

		//ACTUALIZAR UNA COLUMNA DE LA VENTA
		public function setColumnaVenta($columna,$valor,$id_venta){
			$sql = "UPDATE venta SET $columna = '$valor' WHERE idventa = '$id_venta'";
			return ejecutarConsulta($sql);
		}

		public function mostrar_correlativo_vigente(){
			$sql = "SELECT * FROM correlativo_ventas WHERE estado = 1 LIMIT 1";
			return ejecutarConsultaSimpleFila($sql);
		}		

		public function actualizar_correlativo($valor,$id_correlativo_actual){
			$sql = "UPDATE correlativo_ventas SET actual = '$valor' WHERE id_correlativo = '$id_correlativo_actual'";
			return ejecutarConsulta($sql);
		}

		public function enviar_correo($id_venta,$correo){
			
			//Load Composer's autoloader
			require '../vendor/autoload.php';

			//Create an instance; passing `true` enables exceptions
			$mail = new PHPMailer();

			//importar datos de informacion
			require_once "Informacion.php";
			$correo_farmacia = Informacion::mostrar2()["correo"];
			$telefono_farmacia = Informacion::mostrar2()["telefono"];

			//obtener venta 
			$venta = $this->mostrar($id_venta);
			
			
			try {
				//Server settings
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->CharSet = 'UTF-8';

				$mail->Host = 'smtp.hostinger.com';  //gmail SMTP server
				$mail->Port = 587;                    //SMTP port
				$mail->SMTPAuth = true;
				$mail->SMTPSecure = "tls";

				$mail->Username = $correo_farmacia;   //email
				$mail->Password = 'FarmaDiv2022%';   //la contraseña debe cambiarse en caso de que el correo actual sufra cambios


				//sender information
				$mail->setFrom($correo_farmacia, 'Farmacia Divina Misericordia');
				//$mail->addReplyTo($correo_farmacia, 'Farmacia Divina Misericordia');


				//receiver email address and name
				$mail->addAddress($correo, $venta["cliente"]); 

				//adjuntos de PDF y XML
				$ruta = $venta["ruta"];
				$archivo = $venta["nombre_archivo"];
				$extension_pdf = ".pdf";
				$extension_xml = ".xml";

				$mail->addAttachment($ruta.$archivo.$extension_xml);

				//NO ENVIAR REPRESENTACIÓN GRÁFICA SI LA FACTURA SE ENCUENTRA ANULADA
				if($venta["estado"] != "Anulado")
					$mail->addAttachment($ruta.$archivo.$extension_pdf);				       

				// Add cc or bcc   
				// $mail->addCC('email@mail.com');  
				// $mail->addBCC('user@mail.com');  

				$txt_sucursal = "";
				$cod_sucursal = Informacion::mostrar2()["codigoSucursal"];
				switch($cod_sucursal){
					case "0":
						$txt_sucursal = "Irpavi Casa Matriz";
					break;

					case "1":
						$txt_sucursal = "Mallasa sucursal " . $cod_sucursal;
					break;

					case "2":
						$txt_sucursal = "Seguencoma sucursal " . $cod_sucursal;
					break;

					case "3":
						$txt_sucursal = "Irpavi sucursal " . $cod_sucursal;
					break;
				}

				$nro_factura = "";
				if($venta["nro_venta"] != 0 && $venta["nro_venta"] != ""){
					$nro_factura = $venta["nro_venta"];
				}else{
					if($venta["nro_venta_contingencia"] != 0 && $venta["nro_venta_contingencia"] != ""){
						$nro_factura = $venta["nro_venta_contingencia"];
					}
				}

				//EN BASE AL ESTADO DE LA VENTA (ACEPTADO O ANULADO MANDAR UN MENSAJE DISTINTO)

				$subject = "";

				$cuerpo_mensaje = "";

				if($venta["estado"] == "Aceptado" || $venta["estado"] == "Pendiente"){
					$subject = 'Factura emitida Nro.' . $nro_factura;
					$cuerpo_mensaje = utf8_encode("Estimado Cliente,<br>

					En el presente correo se adjunta su factura electro&#769;nica por el periodo actual en formato PDF y archivo XML, Factura Nro. <strong>". $nro_factura ."</strong> con fecha <strong>".$venta["fecha"]."</strong> por compras en la farmacia de ".$txt_sucursal .".

					<br>CUF: <strong>" . $venta['cuf'] . "</strong>

					<br>Monto Venta: <strong> Bs. ". $venta['total_venta'] . "</strong>
					
					<br>Conforme a nueva normativa de facturacio&#769;n electro&#769;nica y computarizada en li&#769;nea (RND-102100000011) establecida por Impuestos Nacionales, ahora recibira&#769; sus facturas vi&#769;a correo electro&#769;nico.
					
					<br>Saludos cordiales
					
					<br>Gracias por elegirnos.");
				}else{
					if($venta["estado"] == "Anulado"){
						$subject = 'Factura anulada Nro.' . $nro_factura;
						$cuerpo_mensaje = utf8_encode("Estimado Cliente,<br>

						En el presente correo se le notifica que la factura electro&#769;nica Nro. <strong>". $nro_factura ."</strong>con fecha <strong>".$venta["fecha"]."</strong> por compras en la farmacia de ".$txt_sucursal ." se encuentra ANULADA.

						<br>CUF: <strong>" . $venta['cuf'] . "</strong>

						<br>Monto Venta: <strong> Bs. ". $venta['total_venta'] . "</strong>

						<br>Haga click <strong><a href='https://pilotosiat.impuestos.gob.bo/consulta/QR?nit=".Informacion::mostrar2()['nit']."&cuf=".$venta['cuf']."&numero=".$nro_factura."&t=1'>aqui&#769;</a></strong> para obtener la representacio&#769;n gra&#769;fica de la factura anulada.
						
						<br>Si se trata de un error, responda a este correo o comuni&#769;quese con el nu&#769;mero de celular $telefono_farmacia.
						
						<br>Saludos cordiales.");
					}
				}

				$mail->Subject = $subject;
				
				$mail->msgHTML($cuerpo_mensaje);
				
				// Send mail   
				if (!$mail->send()) {
					//echo 'Email not sent an error was encountered: ' . $mail->ErrorInfo;
					return false;
				} else {
					return true;
				}

				$mail->smtpClose();

				
			} catch (Exception $e) {
				//echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
				return false;
			}
		}

		//OBTENER ÚLTIMA VENTA
		public function obtener_ultima_venta_cliente($idcliente){
			$sql = "SELECT * FROM venta WHERE idcliente = '$idcliente' ORDER BY idventa DESC LIMIT 1";
			return ejecutarConsultaSimpleFila($sql);
		}


	}
