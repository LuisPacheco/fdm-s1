<?php 
	//Incluímos inicialmente la conexión a la base de datos
	date_default_timezone_set("America/La_Paz");
	require "../config/Conexion.php";

	Class Consultas{

		//Implementamos nuestro constructor

		public function __construct(){
		}

		public function comprasfecha($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE(i.fecha_hora) as fecha,u.nombre as usuario, p.nombre as proveedor,i.tipo_comprobante,i.num_comprobante,i.total_compra,i.impuesto,i.estado FROM ingreso i INNER JOIN persona p ON i.idproveedor=p.idpersona INNER JOIN usuario u ON i.idusuario=u.idusuario WHERE DATE(i.fecha_hora)>='$fecha_inicio' AND DATE(i.fecha_hora)<='$fecha_fin'";

			return ejecutarConsulta($sql);		

		}

		public function monto_comprasfecha($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT SUM(i.total_compra) as total

					FROM ingreso i 

					INNER JOIN persona p ON i.idproveedor=p.idpersona 

					INNER JOIN usuario u ON i.idusuario=u.idusuario 

					WHERE DATE(i.fecha_hora)>='$fecha_inicio' AND DATE(i.fecha_hora)<='$fecha_fin' AND estado = 'Aceptado'";

			return ejecutarConsulta($sql);		

		}

		public function anul_comprasfecha($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT SUM(i.total_compra) as total

					FROM ingreso i 

					INNER JOIN persona p ON i.idproveedor=p.idpersona 

					INNER JOIN usuario u ON i.idusuario=u.idusuario 

					WHERE DATE(i.fecha_hora)>='$fecha_inicio' AND DATE(i.fecha_hora)<='$fecha_fin' AND estado = 'Anulado'";

			return ejecutarConsulta($sql);		

		}

		public function ventasfechacliente($fecha_inicio,$fecha_fin,$idcliente){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT v.fecha_hora as fecha,u.nombre as usuario, p.nombre as cliente,v.tipo_comprobante,v.num_comprobante,v.total_venta,v.impuesto,v.estado,v.tiene_tarjeta 

			FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario 

			WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin' AND v.idcliente='$idcliente'";

			return ejecutarConsulta($sql);		

		}

		public function hayDescuento($idventa){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT SUM(descuento) AS descuento

				FROM detalle_venta 

				WHERE idventa = '$idventa'";

			return ejecutarConsulta($sql);

		}

		public function ventasfechaclientes($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT v.idventa,v.fecha_hora as fecha,v.fecha_hora_registro, u.nombre as usuario, p.nombre as cliente,p.email,v.nro_venta,v.nro_venta_contingencia,v.total_venta,v.estado,v.nro_tarjeta,v.cuf,v.cafc,v.nombre_archivo,v.ruta,p.num_documento,v.metodo_pago

					FROM venta v 

					INNER JOIN persona p ON v.idcliente=p.idpersona 

					INNER JOIN usuario u ON v.idusuario=u.idusuario 

					WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin';";

			return ejecutarConsulta($sql);		

		}

		public function ventasfechaclientes_ct_acp($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT SUM(v.total_venta) as total

					FROM venta v 

					INNER JOIN persona p ON v.idcliente=p.idpersona 

					INNER JOIN usuario u ON v.idusuario=u.idusuario 

					WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin' AND estado = 'Aceptado' AND tiene_tarjeta = 'Si' ";

			return ejecutarConsulta($sql);		

		}

		public function ventasfechaclientes_st_acp($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT SUM(v.total_venta) as total

					FROM venta v 

					INNER JOIN persona p ON v.idcliente=p.idpersona 

					INNER JOIN usuario u ON v.idusuario=u.idusuario 

					WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin' AND estado = 'Aceptado' AND tiene_tarjeta = 'No' ";

			return ejecutarConsulta($sql);		

		}

		public function ventasfechaclientes_anulado($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT SUM(v.total_venta) as total

					FROM venta v 

					INNER JOIN persona p ON v.idcliente=p.idpersona 

					INNER JOIN usuario u ON v.idusuario=u.idusuario 

					WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin' AND estado = 'Anulado'";

			return ejecutarConsulta($sql);		

		}

		public function sumar_descuentos($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql = "SELECT SUM(dv.descuento) as total

					FROM detalle_venta dv

					INNER JOIN venta v ON v.idventa = dv.idventa

					WHERE DATE(v.fecha_hora) >= '$fecha_inicio' AND DATE(v.fecha_hora) <= '$fecha_fin' AND v.estado = 'Aceptado'";

			return ejecutarConsulta($sql);

		}

		public function totalcomprahoy(){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");
			$sql="SELECT IFNULL(SUM(total_compra),0) as total_compra 
				FROM ingreso 
				WHERE DATE(fecha_hora) = '$hoy' AND estado = 'Aceptado'";
			return ejecutarConsulta($sql);
		}

		public function totalventahoy(){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");
			$sql="SELECT IFNULL(SUM(total_venta),0) as total_venta 
				FROM venta 
				WHERE DATE(fecha_hora) = '$hoy'";
			return ejecutarConsulta($sql);

		}

		public function totalventahoy2($turno){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");

			$sql = "SELECT IFNULL(SUM(ve.total_venta),0) as total_venta 
					FROM venta AS ve
					INNER JOIN usuario AS usr ON usr.idusuario = ve.idusuario
					INNER JOIN correlativo_caja AS cc ON cc.idusuario = usr.idusuario
					WHERE DATE(ve.fecha_hora) = '$hoy' AND cc.turno = '$turno' AND DATE(cc.fecha) = '$hoy' AND ve.estado = 'Aceptado';";

			return ejecutarConsultaSimpleFila($sql);

		}

		public function total_ventas_met_pago($metodo_pago){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");
			$sql="SELECT IFNULL(SUM(total_venta),0) as total_venta_mp
				FROM venta 
				WHERE DATE(fecha_hora) = '$hoy' AND metodo_pago = '$metodo_pago' AND estado = 'Aceptado'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function totalventahoy_ct(){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");
			$sql="SELECT IFNULL(SUM(total_venta),0) as total_venta_ct 
				FROM venta 
				WHERE DATE(fecha_hora) = '$hoy' AND metodo_pago = '2' AND estado = 'Aceptado'";
			return ejecutarConsulta($sql);

		}

		public function totalventahoy_st(){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");
			$sql="SELECT IFNULL(SUM(total_venta),0) as total_venta_st 
			FROM venta 
			WHERE DATE(fecha_hora) = '2022-11-10' AND metodo_pago = '1'  AND estado = 'Aceptado'";

			return ejecutarConsulta($sql);

		}

		public function totalventamaniana(){
			date_default_timezone_set("America/La_Paz");
			$hoy = date("Y-m-d");
			$sql="SELECT IFNULL(SUM(total_venta),0) as total_venta 
				FROM venta 
				WHERE DATE(fecha_hora) = '$hoy'";
			return ejecutarConsulta($sql);

		}

		public function comprasultimos_10dias(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT CONCAT(DAY(fecha_hora),'-',MONTH(fecha_hora)) as fecha,SUM(total_compra) as total 

				FROM ingreso 

				WHERE estado = 'Aceptado'

				GROUP by fecha_hora 

				ORDER BY fecha_hora 

				DESC limit 0,10";

			return ejecutarConsulta($sql);

		}

		public function comprasultimos_10meses(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%M') as fecha,SUM(total_compra) as total 

				FROM ingreso 

				WHERE estado = 'Aceptado'

				GROUP by MONTH(fecha_hora) 

				ORDER BY fecha_hora 

				DESC limit 0,10";

			return ejecutarConsulta($sql);

		}

		public function ventasultimos_12meses(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%M') as fecha,SUM(total_venta) as total 

				FROM venta 

				GROUP by MONTH(fecha_hora) 

				ORDER BY fecha_hora 

				DESC limit 0,10";

			return ejecutarConsulta($sql);

		}

		public function ventasultimos_12meses_st(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%M') as fecha,SUM(total_venta) as total 

				FROM venta 

				WHERE tiene_tarjeta = 'No'

				GROUP by MONTH(fecha_hora) 

				ORDER BY fecha_hora 

				DESC limit 0,10";

			return ejecutarConsulta($sql);

		}

		public function ventasultimos_12meses_ct(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%M') as fecha,SUM(total_venta) as total 

				FROM venta 

				WHERE tiene_tarjeta = 'Si'

				GROUP by MONTH(fecha_hora) 

				ORDER BY fecha_hora 

				DESC limit 0,10";

			return ejecutarConsulta($sql);

		}

		public function ventasultimos_sem_ct(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%d-%m-%Y') as fecha, SUM(total_venta)as total 

				FROM venta 

				WHERE fecha_hora <= NOW() AND fecha_hora >= date_add(NOW(), INTERVAL -7 DAY) AND tiene_tarjeta = 'Si' AND estado = 'Aceptado'

				GROUP BY DATE_FORMAT(fecha_hora,'%d-%m-%Y')

				ORDER BY DATE_FORMAT(fecha_hora,'%d-%m-%Y')

				DESC limit 0,7";

			return ejecutarConsulta($sql);

		}

		public function ventasultimos_sem_st(){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%d-%m-%Y') as fecha, SUM(total_venta)as total 

				FROM venta 

				WHERE fecha_hora <= NOW() AND fecha_hora >= date_add(NOW(), INTERVAL -7 DAY) AND tiene_tarjeta = 'No' AND estado = 'Aceptado'

				GROUP BY DATE_FORMAT(fecha_hora,'%d-%m-%Y')

				ORDER BY DATE_FORMAT(fecha_hora,'%d-%m-%Y')

				DESC limit 0,7";

			return ejecutarConsulta($sql);

		}

		public function ventas_ultimos_sem_mp($met_pago){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT DATE_FORMAT(fecha_hora,'%d-%m-%Y') as fecha, SUM(total_venta)as total 

				FROM venta 

				WHERE fecha_hora <= NOW() AND fecha_hora >= date_add(NOW(), INTERVAL -7 DAY) AND metodo_pago = '$met_pago' AND estado = 'Aceptado'

				GROUP BY DATE_FORMAT(fecha_hora,'%d-%m-%Y')

				ORDER BY DATE_FORMAT(fecha_hora,'%d-%m-%Y')

				DESC limit 0,7";

			return ejecutarConsulta($sql);
		}

		public function productos_vendidos($fecha_inicio,$fecha_fin){
			date_default_timezone_set("America/La_Paz");
			$sql="SELECT pr.idarticulo, pr.cod_med,pr.codigo,pr.nombre,pr.unidad,pr.medida,pr.idcategoria,pr.idsubcategoria,pr.idmarca,pr.cantidad, ca.nombre AS categoria, sc.nombre AS subcategoria, ma.nombre AS marca,(dv.cantidad) AS cant

			FROM articulo pr

			INNER JOIN detalle_venta dv ON dv.idproducto = pr.idproducto

			INNER JOIN venta v ON v.idventa = dv.idventa

			INNER JOIN categoria ca ON pr.idcategoria = ca.idcategoria

			INNER JOIN subcategoria sc ON pr.idsubcategoria = sc.idsubcategoria

			INNER JOIN marca ma ON ma.idmarca = pr.idmarca

			WHERE v.estado = 'Aceptado' and DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin'

			GROUP BY pr.idproducto

			UNION 

			SELECT pr.idproducto,pr.cod_producto,pr.descripcion,pr.unidad,pr.medida,pr.idcategoria,pr.idsubcategoria,pr.idmarca,pr.cantidad, ca.nombre AS categoria, sc.nombre AS subcategoria, ma.nombre AS marca,CONCAT('0') AS cant

			FROM producto pr

			INNER JOIN categoria ca ON pr.idcategoria = ca.idcategoria

					INNER JOIN subcategoria sc ON pr.idsubcategoria = sc.idsubcategoria

					INNER JOIN marca ma ON ma.idmarca = pr.idmarca

			WHERE pr.idproducto NOT IN(SELECT DISTINCT dv.idproducto

										FROM detalle_venta dv

										WHERE dv.idproducto IS NOT NULL)";

			return ejecutarConsulta($sql);

		}

	}