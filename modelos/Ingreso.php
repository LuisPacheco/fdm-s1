<?php 
date_default_timezone_set("America/La_Paz");

//Incluímos inicialmente la conexión a la base de datos

require "../config/Conexion.php";



Class Ingreso{

	//Implementamos nuestro constructor

	public function __construct(){

	}



	//Implementamos un método para insertar registros

	public function insertar($idproveedor,$idusuario,$tipo_comprobante,$num_comprobante,$fecha_hora,$impuesto,$total_compra,$idarticulo,$cantidad,$precio_compra,$precio_venta,$cod_lote,$vencimiento,$idlote){
	    
	    $hoy = date("Y-m-d");
	    
		$sql="INSERT INTO ingreso (idproveedor,idusuario,tipo_comprobante,num_comprobante,fecha_hora,impuesto,total_compra,estado)

			  VALUES ('$idproveedor','$idusuario','$tipo_comprobante','$num_comprobante','$fecha_hora','$impuesto','$total_compra','Aceptado')";

		//return ejecutarConsulta($sql);

		$idingresonew = ejecutarConsulta_retornarID($sql);



		$num_elementos = 0;

		$sw = true;

		$sw2 = true;

		$sw3 = true;

		require_once "../modelos/Lote.php";

		$lote = new Lote();



		require_once "../modelos/Articulo.php";

		$articulo = new Articulo();



		while ($num_elementos < count($idarticulo)){

			//VERIFICAR SI ES NUEVO LOTE

			if($idlote[$num_elementos] == "nuevo"){

				//VERIFICA ESTADO DE LA FECHA DE VENCIMIENTO DEL LOTE QUE SE ESTÁ INGRESANDO

				$estado = "";

				if($cantidad[$num_elementos] == 0)

					$estado = "Agotado";

				else{

					if($vencimiento[$num_elementos] != "" && $vencimiento[$num_elementos] != NULL){

						date_default_timezone_set("America/La_Paz");

						$fecha_vencimiento = new DateTime($vencimiento[$num_elementos]);

						$actual = date("Y-m-d");

						$fecha = new DateTime($actual);

						$res = $fecha->diff($fecha_vencimiento);

						$dias = $res->days;

						if($dias >= 180 && $res->invert == 0)

							$estado = "Activo (6+)";

						else{

							if ($dias > 90 && $dias < 180 && $res->invert == 0) 

								$estado = "Activo (3+)";

							else{

								if ($dias <= 90 && $res->invert == 0) 

									$estado = "Por vencerse (3-)";

								else

									$estado = "Vencido";

							}

						}

					}else

						$estado = "n/a";

				}

				//VERIFICA SI LA FECHA DE VENCIMIENTO INTRODUCIDA TIENE O NO CONTENIDO PARA LUEGO INSERTAR EN LA TABLA LOTE

				if($vencimiento[$num_elementos] == "" || $vencimiento[$num_elementos] == NULL){

					$sql_insert_lote = "INSERT INTO lote (cod_lote,vencimiento,fecha_registro,stock,estado) 

										VALUES('$cod_lote[$num_elementos]',NULL, '$hoy','$cantidad[$num_elementos]','$estado')";

				}else{

					$sql_insert_lote = "INSERT INTO lote (cod_lote,vencimiento,fecha_registro,stock,estado) 

												VALUES('$cod_lote[$num_elementos]','$vencimiento[$num_elementos]', '$hoy','$cantidad[$num_elementos]','$estado')";

				}

				$idlote_nuevo = ejecutarConsulta_retornarID($sql_insert_lote);

				$sql_detalle = "INSERT INTO detalle_ingreso(idingreso, idarticulo,cantidad,precio_compra,precio_venta,idlote) 

										VALUES ('$idingresonew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_compra[$num_elementos]','$precio_venta[$num_elementos]','$idlote_nuevo')";

				$rspta = ejecutarConsulta($sql_detalle);



			}else{//SI EL LOTE YA EXISTE

				$sql_stock_lote = $lote->getStock($idlote[$num_elementos]);

				$stock_lote = $sql_stock_lote->fetch_object();

				$nuevo_stock = $stock_lote->stock + $cantidad[$num_elementos];

				$rspta2 = $lote->setStock($idlote[$num_elementos],$nuevo_stock);

				$sql_detalle = "INSERT INTO detalle_ingreso(idingreso, idarticulo,cantidad,precio_compra,precio_venta,idlote) 	

								VALUES ('$idingresonew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_compra[$num_elementos]','$precio_venta[$num_elementos]','$idlote[$num_elementos]')";

				$rspta = ejecutarConsulta($sql_detalle);



				if(!$rspta2)

					$sw2 = false;

			}

			if (!$rspta)

					$sw = false;



			if($rspta){

				$sql_stock_art = $articulo->devolverStock($idarticulo[$num_elementos]);

				$stock_art = $sql_stock_art->fetch_object();

				$stock_actual = $stock_art->stock + $cantidad[$num_elementos];

				$actualizar_stock = $articulo->modificarStock($idarticulo[$num_elementos],$stock_actual);

				if(!$actualizar_stock)

					$sw3 = false;

			}

			$num_elementos = $num_elementos + 1;

		}



		$num_elementos = 0;

		$sw1 = true;



		

		while ($num_elementos < count($idarticulo)){

			$pre_ven = $articulo->getPrecioVenta($idarticulo[$num_elementos]);

			$precio = $pre_ven->fetch_object();

			if ($precio->precio_venta != $precio_venta[$num_elementos]) {

				$res = $articulo->modificarPrecioVenta($idarticulo[$num_elementos], $precio_venta[$num_elementos]);

				if (!$res)

					$sw1 = false;

			}

			$num_elementos++;

		}



		return $sw && $sw1 && $sw2 && $sw3;

	}



	

	//Implementamos un método para anular categorías

	public function anular($idingreso){

		$sql="UPDATE ingreso 

			SET estado='Anulado' 

			WHERE idingreso='$idingreso'";

		return ejecutarConsulta($sql);

	}





	//Implementar un método para mostrar los datos de un registro a modificar

	public function mostrar($idingreso){

		$sql="SELECT i.idingreso,DATE(i.fecha_hora) as fecha,i.idproveedor,p.nombre as proveedor,u.idusuario,u.nombre as usuario,i.tipo_comprobante,i.num_comprobante,i.total_compra,i.impuesto,i.estado 

			FROM ingreso i 

			INNER JOIN persona p ON i.idproveedor=p.idpersona 

			INNER JOIN usuario u ON i.idusuario=u.idusuario 

			WHERE i.idingreso='$idingreso'";

		return ejecutarConsultaSimpleFila($sql);

	}



	public function listarDetalle($idingreso){

		$sql = 'SELECT a.nombre, di.cantidad, di.precio_compra, di.precio_venta, lo.cod_lote, CONCAT(MONTH(lo.vencimiento), "/", YEAR(lo.vencimiento)) as vencimiento,di.idarticulo,lo.idlote,lo.estado

				FROM detalle_ingreso di 

				inner join articulo a on di.idarticulo=a.idarticulo

				inner join lote lo ON di.idlote = lo.idlote

				where di.idingreso =  ' . $idingreso;

		return ejecutarConsulta($sql);

	}



	//Implementar un método para listar los registros

	public function listar(){

		$sql = "SELECT i.idingreso,i.fecha_hora as fecha, i.idproveedor, p.nombre as proveedor, u.idusuario, u.nombre as usuario, i.tipo_comprobante,i.num_comprobante, i.total_compra, i.impuesto, i.estado 

			FROM ingreso i 

			INNER JOIN persona p 

			ON i.idproveedor = p.idpersona 

			INNER JOIN usuario u 

			ON i.idusuario = u.idusuario 

			ORDER BY i.idingreso desc";

		return ejecutarConsulta($sql);		

	}

	

	public function ingresocabecera($idingreso){

		$sql="SELECT i.idingreso,i.idproveedor,p.nombre as proveedor,p.direccion,p.tipo_documento,p.num_documento,p.email,p.telefono,i.idusuario,u.nombre as usuario,i.tipo_comprobante,i.num_comprobante,i.fecha_hora as fecha,i.impuesto,i.total_compra,DAY(i.fecha_hora) as dia,MONTH(i.fecha_hora) as mes,YEAR(i.fecha_hora) as anio,HOUR(i.fecha_hora) as hora,MINUTE(i.fecha_hora) as minuto, SECOND(i.fecha_hora) as segundo 
    		FROM ingreso i 
    		INNER JOIN persona p ON i.idproveedor=p.idpersona 
    		INNER JOIN usuario u ON i.idusuario=u.idusuario 
    		WHERE i.idingreso='$idingreso'";

		return ejecutarConsulta($sql);

	}



	public function ingresodetalle($idingreso){

		$sql="SELECT a.descripcion as articulo,a.nombre,d.cantidad,d.precio_compra,d.precio_venta,(d.cantidad*d.precio_compra) as subtotal, lo.cod_lote,lo.vencimiento

			FROM detalle_ingreso d 

            INNER JOIN lote lo ON lo.idlote = d.idlote

			INNER JOIN articulo a ON d.idarticulo=a.idarticulo 

			WHERE d.idingreso='$idingreso'";

		return ejecutarConsulta($sql);

	}

}



?>