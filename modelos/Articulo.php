<?php 

//Incluímos inicialmente la conexión a la base de datos

require "../config/Conexion.php";



Class Articulo{

	//Implementamos nuestro constructor

	public function __construct()	{



	}



	//Implementamos un método para insertar registros

	public function insertar($idcategoria,$codigo,$nombre,$stock,$stock_minimo,$descripcion,$imagen,$idlaboratorio,$cod_med,$precio_venta,$codigoProductoSin,$unidadMedida,$otraUnidadMedida,$nombreUnidadMedida){

		$sql="INSERT INTO articulo (idcategoria,codigo,nombre,idlaboratorio,stock,stock_minimo,descripcion,imagen,condicion,cod_med,precio_venta,codigoProductoSin,unidadMedida,otraUnidadMedida,nombreUnidadMedida)

		VALUES ('$idcategoria','$codigo','$nombre','$idlaboratorio','$stock','$stock_minimo','$descripcion','$imagen','1','$cod_med','$precio_venta','$codigoProductoSin','$unidadMedida','$otraUnidadMedida','$nombreUnidadMedida')";

		return ejecutarConsulta($sql);

	}



	public function selectArtLab($labo){

		$sql="SELECT a.* FROM articulo a where a.idlaboratorio = '$labo'";


		return ejecutarConsulta($sql);	

	}



	//Implementamos un método para editar registros

	public function editar($idarticulo,$idcategoria,$codigo,$nombre,$stock,$stock_minimo,$descripcion,$imagen,$idlaboratorio,$cod_med,$precio_venta,$codigoProductoSin,$unidadMedida,$otraUnidadMedida,$nombreUnidadMedida){

		if ($cod_med == "") 
			$cod_med = NULL;
		

		if ($precio_venta == "") 
			$precio_venta = NULL;
		

		

		$sql="UPDATE articulo 
			  SET idcategoria='$idcategoria',codigo='$codigo',nombre='$nombre',idlaboratorio='$idlaboratorio',stock='$stock',stock_minimo='$stock_minimo',descripcion='$descripcion',imagen='$imagen',cod_med = '$cod_med', precio_venta = '$precio_venta', codigoProductoSin = '$codigoProductoSin', unidadMedida = '$unidadMedida', otraUnidadMedida = '$otraUnidadMedida', nombreUnidadMedida = '$nombreUnidadMedida'
			  WHERE idarticulo='$idarticulo'";
		return ejecutarConsulta($sql);
	}

	//ACTUALIZAR UNA COLUMNA
	public function setColumna($columna,$dato,$idarticulo){
		$sql="UPDATE articulo 
			  SET $columna = '$dato'
			  WHERE idarticulo='$idarticulo'";
		return ejecutarConsulta($sql);
	}


	//Implementamos un método para desactivar registros

	public function desactivar($idarticulo){

		$sql="UPDATE articulo SET condicion='0' WHERE idarticulo='$idarticulo'";

		return ejecutarConsulta($sql);

	}



	//Implementamos un método para activar registros

	public function activar($idarticulo)	{

		$sql="UPDATE articulo SET condicion='1' WHERE idarticulo='$idarticulo'";

		return ejecutarConsulta($sql);

	}



	//Implementar un método para mostrar los datos de un registro a modificar

	public function mostrar($idarticulo){

		$sql="SELECT * FROM articulo WHERE idarticulo='$idarticulo'";

		return ejecutarConsultaSimpleFila($sql);

	}



	public function mostrar2($idarticulo){

		$sql="SELECT * FROM articulo WHERE idarticulo='$idarticulo'";

		return ejecutarConsulta($sql);

	}



	//Implementar un método para listar los registros

	public function listar(){

		$sql="SELECT a.idarticulo,a.cod_med,a.idcategoria,c.nombre as categoria,a.codigo AS nombre_generico,a.nombre AS nombre_comercial ,l.nombre as laboratorio, a.stock,a.stock_minimo,a.descripcion,a.imagen,a.condicion,a.precio_venta

			FROM articulo a 

			INNER JOIN categoria c ON a.idcategoria = c.idcategoria

			INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio
			
			WHERE a.condicion != 2";

		return ejecutarConsulta($sql);		

	}



	public function listarArt()	{

		$sql="SELECT a.idarticulo,a.cod_med,a.idcategoria,c.nombre as categoria,a.codigo AS nombre_generico,a.nombre AS nombre_comercial ,l.nombre as laboratorio, a.stock,a.stock_minimo,a.descripcion,a.imagen,a.condicion,a.precio_venta

			FROM articulo a 

			INNER JOIN categoria c ON a.idcategoria = c.idcategoria

			INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio";

		return ejecutarConsulta($sql);		

	}



	//Implementar un método para listar los registros activos

	public function listarActivos(){

		$sql = 

		   "SELECT 

				a.cod_med,

				a.idarticulo,

				a.idcategoria,

				c.nombre as categoria,

				a.codigo,

				a.nombre,

				l.nombre as laboratorio,

				a.stock,

				a.stock_minimo,

				a.descripcion,

				a.imagen,

				a.condicion, 

				a.precio_venta, 

				l.idlaboratorio,

				(SELECT precio_compra FROM detalle_ingreso WHERE idarticulo = a.idarticulo order by iddetalle_ingreso desc limit 0,1) as precio_compra, 

				(SELECT cantidad FROM detalle_ingreso WHERE idarticulo = a.idarticulo order by iddetalle_ingreso desc limit 0,1) as cantidad

				,

				(SELECT lo.cod_lote FROM lote AS lo INNER JOIN 	detalle_ingreso di ON di.idlote = lo.idlote WHERE di.idarticulo = a.idarticulo ORDER BY lo.fecha_registro desc LIMIT 0,1) AS lote,

				(SELECT lo.vencimiento FROM lote AS lo INNER JOIN detalle_ingreso di ON di.idlote = lo.idlote WHERE di.idarticulo = a.idarticulo ORDER BY lo.fecha_registro desc LIMIT 0,1) AS vencimiento

			FROM articulo a 

			INNER JOIN categoria c ON a.idcategoria=c.idcategoria 

			INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio

			WHERE a.condicion='1'";

			return ejecutarConsulta($sql);		

	}



	public function listarActivosVenta2(){

		$sql = "(SELECT DISTINCT a.cod_med,a.codigoActividad,a.codigoProductoSin,a.unidadMedida,a.nombreUnidadMedida, a.idarticulo, a.idcategoria, a.codigo as nombre_generico,a.nombre as nombre_comercial, a.stock_minimo, a.precio_venta,a.descripcion, a.imagen,a.condicion, a.stock as stock_total,c.nombre as categoria,l.idlaboratorio, l.nombre as laboratorio,lo.idlote, lo.cod_lote, lo.vencimiento, lo.estado, lo.stock, MONTH(lo.vencimiento) as mes, YEAR(lo.vencimiento) as anio

		FROM articulo a 

		INNER JOIN categoria c ON a.idcategoria=c.idcategoria 

		INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio

		LEFT JOIN detalle_ingreso di ON di.idarticulo = a.idarticulo

		INNER JOIN lote lo ON di.idlote = lo.idlote

		WHERE a.condicion='1' and lo.estado != 'Devuelto' and lo.visibilidad = '1' and lo.estado != 'borrado')

UNION 

(SELECT distinct a.cod_med,a.codigoActividad,a.codigoProductoSin,a.unidadMedida,a.nombreUnidadMedida, a.idarticulo, a.idcategoria, a.codigo as nombre_generico,a.nombre as nombre_comercial, a.stock_minimo, a.precio_venta,a.descripcion, a.imagen,a.condicion, a.stock as stock_total,c.nombre as categoria,l.idlaboratorio, l.nombre as laboratorio,lo.idlote, lo.cod_lote, lo.vencimiento, lo.estado, lo.stock, MONTH(lo.vencimiento) as mes, YEAR(lo.vencimiento) as anio

		FROM articulo a 

		INNER JOIN categoria c ON a.idcategoria=c.idcategoria 

		INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio

		LEFT JOIN detalle_entrada de ON de.idarticulo = a.idarticulo

		INNER JOIN lote lo ON de.idlote = lo.idlote

		WHERE a.condicion='1' and lo.estado != 'Devuelto' and lo.visibilidad = '1' and lo.estado != 'borrado')  
ORDER BY vencimiento";

			return ejecutarConsulta($sql);	

	}



/*

SELECT a.cod_med, a.idarticulo, a.idcategoria, a.codigo as nombre_generico,a.nombre as nombre_comercial, a.stock_minimo, a.precio_venta,a.descripcion, a.imagen,a.condicion, a.stock as stock_total,c.nombre as categoria,l.idlaboratorio, l.nombre as laboratorio,lo.idlote, lo.cod_lote, lo.vencimiento, lo.estado, lo.stock, MONTH(lo.vencimiento) as mes, YEAR(lo.vencimiento) as anio

FROM articulo a 

INNER JOIN categoria c ON a.idcategoria=c.idcategoria 

INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio

INNER JOIN detalle_ingreso di ON di.idarticulo = a.idarticulo

INNER JOIN lote lo ON di.idlote = lo.idlote

WHERE a.condicion='1' and lo.estado != 'Devuelto' and lo.estado != 'Vencido' and lo.visibilidad = '1'

GROUP BY a.idarticulo, lo.stock

*/



	//Implementar un método para listar los registros activos, su último precio y el stock (vamos a unir con el último registro de la tabla detalle_ingreso)

	public function listarActivosVenta(){

		$sql="SELECT a.cod_med,a.idarticulo,a.idcategoria,c.nombre as categoria,a.codigo as nombre_generico,a.nombre as nombre_comercial,l.nombre as laboratorio,a.stock,a.stock_minimo,a.precio_venta,a.descripcion,a.imagen,a.condicion 

			FROM articulo a 

			INNER JOIN categoria c ON a.idcategoria=c.idcategoria 

			INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio

			WHERE a.condicion='1'";

		return ejecutarConsulta($sql);		

	}



	/*

	SELECT a.cod_med, a.idarticulo, a.idcategoria, c.nombre as categoria, a.codigo as nombre_generico,

	a.nombre as nombre_comercial, l.nombre as laboratorio, lo.stock, a.stock_minimo, a.precio_venta,

	a.descripcion, a.imagen,a.condicion, lo.cod_lote, lo.vencimiento, lo.estado,lo.idlote

	FROM articulo a 

	INNER JOIN categoria c ON a.idcategoria=c.idcategoria 

	INNER JOIN laboratorio l on a.idlaboratorio = l.idlaboratorio

	INNER JOIN lote lo ON a.idarticulo = lo.idarticulo

	WHERE a.condicion='1' and lo.estado != "Devuelto" and lo.estado != "Agotado" and lo.estado != "Vencido"

	GROUP BY lo.idarticulo, lo.stock

	*/



	public function devolverStock($idarticulo){

		$sql = "SELECT stock,idarticulo 

				FROM articulo 

				WHERE idarticulo = '$idarticulo'";

		return ejecutarConsulta($sql);

	}



	public function modificarStock($idarticulo,$newStock){

		$sql = "UPDATE articulo SET stock = '$newStock' WHERE idarticulo = '$idarticulo'";

		return ejecutarConsulta($sql);

	}



	public function getPrecioVenta($idarticulo){

		$sql = "SELECT precio_venta FROM articulo WHERE idarticulo = '$idarticulo'";

		return ejecutarConsulta($sql);

	}



	public function modificarPrecioVenta($idarticulo, $newPrecioVenta){

		$sql = "UPDATE articulo SET precio_venta = '$newPrecioVenta' WHERE idarticulo = '$idarticulo'";

		return ejecutarConsulta($sql);

	}

	

	public function venta_articulo($idarticulo){

		$sql = "SELECT art.idarticulo, art.nombre,dv.cantidad,dv.precio_venta,dv.descuento, ve.fecha_hora, lo.cod_lote, lo.vencimiento,ve.estado, us.login

				FROM articulo AS art

				INNER JOIN detalle_venta AS dv

				INNER JOIN venta AS ve ON ve.idventa = dv.idventa

				INNER JOIN lote AS lo ON lo.idlote = dv.idlote

				INNER JOIN usuario AS us ON us.idusuario = ve.idusuario

				WHERE dv.idarticulo = art.idarticulo AND art.idarticulo = '$idarticulo'

				ORDER BY ve.fecha_hora desc";

		return ejecutarConsulta($sql);

	}



	public function entrada_articulo($idarticulo){

		$sql = "SELECT art.idarticulo, art.nombre,de.cantidad, en.fecha_hora, lo.cod_lote, lo.vencimiento,en.estado, us.login,en.sucursal

				FROM articulo AS art

				INNER JOIN detalle_entrada AS de

				INNER JOIN entrada AS en ON en.identrada = de.identrada

				INNER JOIN lote AS lo ON lo.idlote = de.idlote

				INNER JOIN usuario AS us ON us.idusuario = en.idusuario

				WHERE de.idarticulo = art.idarticulo AND art.idarticulo = '$idarticulo'

				ORDER BY en.fecha_hora desc";

		return ejecutarConsulta($sql);

	}



	public function salida_articulo($idarticulo){

		$sql = "SELECT art.idarticulo, art.nombre,ds.cantidad, sa.fecha_hora, lo.cod_lote, lo.vencimiento,sa.estado, us.login,sa.sucursal

				FROM articulo AS art

				INNER JOIN detalle_salida AS ds

				INNER JOIN salida AS sa ON sa.idsalida = ds.idsalida

				INNER JOIN lote AS lo ON lo.idlote = ds.idlote

				INNER JOIN usuario AS us ON us.idusuario = sa.idusuario

				WHERE ds.idarticulo = art.idarticulo AND art.idarticulo = '$idarticulo'

				ORDER BY sa.fecha_hora desc";

		return ejecutarConsulta($sql);

	}



	public function ingreso_articulo($idarticulo){

		$sql = "SELECT art.idarticulo, art.nombre,di.cantidad, ing.fecha_hora, lo.cod_lote, lo.vencimiento,ing.estado, us.login, pe.nombre AS proveedor

				FROM articulo AS art

				INNER JOIN detalle_ingreso AS di

				INNER JOIN ingreso AS ing ON ing.idingreso = di.idingreso

				INNER JOIN lote AS lo ON lo.idlote = di.idlote

				INNER JOIN usuario AS us ON us.idusuario = ing.idusuario

				INNER JOIN persona AS pe ON pe.idpersona = ing.idproveedor

				WHERE di.idarticulo = art.idarticulo AND art.idarticulo = '$idarticulo'

				ORDER BY ing.fecha_hora desc";

		return ejecutarConsulta($sql);

	}

	public function buscar_lab_corr($id){
		$sql = "SELECT COUNT(cod_med) AS cant_total
						FROM articulo
						WHERE cod_med LIKE ";
		$pref = "";
		$labs = "";
		switch ($id){
			
			//VERIFICAR SI EL ID PERTENECE A INTI (1,30,31,32,145,146,147)
			case 1: case 30: case 31: case 32: case 145: case 146: case 147:
				$pref = "INT";
				$labs = " AND (idlaboratorio = 1 OR idlaboratorio = 30 OR idlaboratorio = 31 OR idlaboratorio = 32 OR idlaboratorio = 145 OR idlaboratorio = 146 OR idlaboratorio = 147)";
			break;

			//VERIFICAR SI EL ID PERTENECE A SAE (16,18,19,20,21,22,23)
			case 16: case 18: case 19: case 20: case 21: case 22: case 23:
				$pref = "SAE";
				$labs = " AND (idlaboratorio = 16 OR idlaboratorio = 18 OR idlaboratorio = 19 OR idlaboratorio = 20 OR idlaboratorio = 21 OR idlaboratorio = 22 OR idlaboratorio = 23)";
			break;

			//VERIFICAR SI EL ID PERTENECE A COFAR (10,11)
			case 10: case 11:
				$pref = "COF";
				$labs = " AND (idlaboratorio = 10 OR idlaboratorio = 11)";
			break;

			//VERIFICAR SI EL ID PERTENECE A FARAON (70)
			case 70:
				$pref = "FRN";
				$labs = " AND idlaboratorio = 70";
			break;

			//VERIFICAR SI EL ID PERTENECE A BAGÓ (3)
			case 3:
				$pref = "BAG";
				$labs = " AND idlaboratorio = 3";
			break;

			//VERIFICAR SI EL ID PERTENECE A MEGALABS (33)
			case 33:
				$pref = "MEG";
				$labs = " AND idlaboratorio = 33";
			break;

			//VERIFICAR SI EL ID PERTENECE A DELTA (47)
			case 47:
				$pref = "DEL";
				$labs = " AND idlaboratorio = 47";
			break;

			//VERIFICAR SI EL ID PERTENECE A LAFAR (54)
			case 54:
				$pref = "LAF";
				$labs = " AND idlaboratorio = 54";
			break;

			//VERIFICAR SI EL ID NO PERTENECE A NINGUNO DE LOS ID's DESIGNADOS
			default:
				$pref = "FDM";
			break;
		}
		$sql .= "'".$pref."%'";
		
		
		$res = ejecutarConsultaSimpleFila($sql);
		$tot = $res["cant_total"];
		return $pref.($tot+1);
		
		
	}

	public function verif_cod_med($cod_med,$idarticulo){
		$sql = "SELECT COUNT(cod_med) AS cant_total
						FROM articulo
						WHERE cod_med = '$cod_med'";
		$res = ejecutarConsultaSimpleFila($sql);
		$tot = $res["cant_total"];
		if($tot != 0){//si el código se repite, se verifica si es el mismo de la id del artículo
			if($idarticulo == NULL || $idarticulo == "")
				return $tot;
			else{
				$sql1 = "SELECT COUNT(cod_med) AS cant_total
						FROM articulo
						WHERE cod_med = '$cod_med'
						AND idarticulo = '$idarticulo'";
				$res1 = ejecutarConsultaSimpleFila($sql1);
				$tot1 = $res1["cant_total"];
				if($tot1 == 1)
					return 0;
				else
					return 1;
			}
		}else   //si el código no se repite
			return 0;
	}

	public function selectListaProductosSin(){
		require_once "../xml/SincronizacionDatos.php";
		$sinc_datos = new SincronizacionDatos();
		$res = $sinc_datos->obtenerCatalogoX("sincronizarListaProductosServicios");
		if($res)
			return $res["sincronizarListaProductosServicios"];
		else
			return false;
	}

	public function selectUnidadMedida(){
		require_once "../xml/SincronizacionDatos.php";
		$sinc_datos = new SincronizacionDatos();
		$res = $sinc_datos->obtenerCatalogoX("sincronizarParametricaUnidadMedida");
		if($res)
			return $res["sincronizarParametricaUnidadMedida"];
		else
			return false;
	}


}