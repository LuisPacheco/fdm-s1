<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Salida{
	//Implementamos nuestro constructor
	public function __construct(){

	}

	//Implementamos un método para insertar registros
	public function insertar($sucursal,$idusuario,$num_comprobante,$fecha_hora,$idarticulo,$cantidad,$idlote){
		$sql="INSERT INTO salida (sucursal,idusuario,num_comprobante,fecha_hora,estado)
				 VALUES ('$sucursal','$idusuario','$num_comprobante','$fecha_hora','Aceptado')";
		//return ejecutarConsulta($sql);
		$idsalidanew=ejecutarConsulta_retornarID($sql);

		$num_elementos=0;
		$sw1=true;

		require_once "../modelos/Articulo.php";
		require_once "../modelos/Lote.php";

		$articulo = new Articulo();
		$lote = new Lote();

		while ($num_elementos < count($idarticulo)){
			$sql_detalle = "INSERT INTO detalle_salida(idsalida, idarticulo,cantidad,idlote) 
								VALUES ('$idsalidanew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$idlote[$num_elementos]')";
			
			$sql_cant_articulo = $articulo->devolverStock($idarticulo[$num_elementos]);
			$cant_articulo = $sql_cant_articulo->fetch_object();
			$stock_actual = $cant_articulo->stock - $cantidad[$num_elementos];
			$sql_modif_stock = $articulo->modificarStock($idarticulo[$num_elementos],$stock_actual);
			if(!$sql_modif_stock)
				$sw1 = false;

			$sql_cant_lote = $lote->getStock($idlote[$num_elementos]);
			$cant_lote = $sql_cant_lote->fetch_object();
			$stock_lote_actual = $cant_lote->stock - $cantidad[$num_elementos];
			$sql_modif_stock_lote = $lote->setStock($idlote[$num_elementos],$stock_lote_actual);
			if(!$sql_modif_stock_lote)
				$sw1 = false;
				
			ejecutarConsulta($sql_detalle) or $sw1 = false;
			$num_elementos=$num_elementos + 1;
		}

		return $sw1 ;
	}

	
	//Implementamos un método para anular la salida
	public function anular($idsalida){
		$sql="UPDATE salida SET estado='Anulado' WHERE idsalida='$idsalida'";
		return ejecutarConsulta($sql);
	}


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsalida){
		$sql="SELECT v.idsalida,DATE(v.fecha_hora) as fecha,u.idusuario,u.nombre as usuario,v.num_comprobante,v.estado,v.sucursal
			FROM salida v 
			INNER JOIN usuario u ON v.idusuario=u.idusuario 
			WHERE v.idsalida='$idsalida'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listarDetalle($idsalida){
		$sql="SELECT dv.idsalida,dv.idarticulo,a.nombre,dv.cantidad, lo.cod_lote, lo.idlote,lo.vencimiento 
			FROM detalle_salida dv 
			inner join articulo a on dv.idarticulo=a.idarticulo
			inner join lote lo ON dv.idlote = lo.idlote 
			where dv.idsalida='$idsalida'
			GROUP by lo.idlote";
		return ejecutarConsulta($sql);
	}

	
	//Implementar un método para listar los registros
	public function listar($sucursal){
		if($sucursal == "ambas"){
			$sql="SELECT v.idsalida,v.fecha_hora as fecha,u.idusuario,u.nombre as usuario,v.num_comprobante,v.estado 
		       FROM salida v 
			   INNER JOIN usuario u ON v.idusuario=u.idusuario 
			   ORDER by v.idsalida desc";
		}else{
			$sql="SELECT v.idsalida,v.fecha_hora as fecha,u.idusuario,u.nombre as usuario,v.num_comprobante,v.estado 
		       FROM salida v 
			   INNER JOIN usuario u ON v.idusuario=u.idusuario 
			   WHERE sucursal = '$sucursal'
			   ORDER by v.idsalida desc";
		}
		
		return ejecutarConsulta($sql);		
	}

	public function listarSucursalX($x){
		$sql="SELECT v.idsalida,v.fecha_hora as fecha,u.idusuario,u.nombre as usuario,v.num_comprobante,v.estado 
		       FROM salida v 
			   INNER JOIN usuario u ON v.idusuario=u.idusuario 
			   WHERE v.sucursal = '$x'
			   ORDER by v.idsalida desc";
		return ejecutarConsulta($sql);		
	}

	public function salidacabecera($idsalida){
		$sql="SELECT v.idsalida,v.idusuario,u.nombre as usuario,v.num_comprobante,DAY(v.fecha_hora) as dia,MONTH(v.fecha_hora) as mes,YEAR(v.fecha_hora) as anio,HOUR(v.fecha_hora) as hora,MINUTE(v.fecha_hora) as minuto, SECOND(v.fecha_hora) as segundo,v.sucursal,v.fecha_hora
			FROM salida v 
			INNER JOIN usuario u ON v.idusuario=u.idusuario 
			WHERE v.idsalida='$idsalida'";
					return ejecutarConsulta($sql);
	}

	public function salidadetalle($idsalida){
		$sql="SELECT a.codigo as articulo,a.nombre,d.cantidad,lo.cod_lote,lo.vencimiento,a.cod_med,la.nombre as laboratorio
		      FROM detalle_salida d 
			  INNER JOIN articulo a ON d.idarticulo=a.idarticulo 
			  INNER JOIN lote lo ON lo.idlote = d.idlote
			  INNER JOIN laboratorio la ON la.idlaboratorio = a.idlaboratorio
			  WHERE d.idsalida='$idsalida'";
		return ejecutarConsulta($sql);
	}

	public function obtener_efectivo($idusuario,$fecha){
		$sql = "SELECT SUM(v.total_salida) as total
				FROM salida v 
				WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Aceptado' AND tiene_tarjeta = 'No' AND idusuario = '$idusuario'";
		return ejecutarConsulta($sql);
	}

	public function obtener_pagos_tarjeta($idusuario,$fecha){
		$sql = "SELECT SUM(v.total_salida) as total
				FROM salida v 
				WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Aceptado' AND tiene_tarjeta = 'Si' AND idusuario = '$idusuario'";
		return ejecutarConsulta($sql);
	}

	public function obtener_efectivo_anulado($idusuario,$fecha){
		$sql = "SELECT SUM(v.total_salida) as total
				FROM salida v 
				WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Anulado' AND tiene_tarjeta = 'No' AND idusuario = '$idusuario'";
		return ejecutarConsulta($sql);
	}

	public function obtener_tarjeta_anulado($idusuario,$fecha){
		$sql = "SELECT SUM(v.total_salida) as total
				FROM salida v 
				WHERE DATE(v.fecha_hora)='$fecha' AND estado = 'Anulado' AND tiene_tarjeta = 'Si' AND idusuario = '$idusuario'";
		return ejecutarConsulta($sql);
	}

	public function obtener_descuentos($idusuario,$fecha){
		$sql = "SELECT SUM(dv.descuento) as total
				FROM salida v
				INNER JOIN detalle_salida dv ON dv.idsalida = v.idsalida 
				WHERE DATE(v.fecha_hora)='$fecha' AND v.estado = 'Aceptado' AND v.idusuario = '$idusuario'";
		return ejecutarConsulta($sql);
	}


	
}