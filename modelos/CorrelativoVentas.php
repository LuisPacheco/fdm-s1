<?php 
    date_default_timezone_set("America/La_Paz");
    //Incluímos inicialmente la conexión a la base de datos

    require "../config/Conexion.php";

    Class CorrelativoVentas{

        //Implementamos nuestro constructor

        public function __construct(){
        }

        //Implementar un método para listar los registros

        public function listar(){
            $sql="SELECT cv.* 
                FROM correlativo_ventas AS cv;";
            return ejecutarConsulta($sql);		

        }

        //registrar de correlativo de venta
        public function insertar($descripcion,$max,$fecha_registro,$fecha_vigencia){
            $sql = "INSERT INTO correlativo_ventas (descripcion,max,fecha_registro,fecha_vigencia)
                    VALUES ('$descripcion','$max','$fecha_registro','$fecha_vigencia')";
            return ejecutarConsulta($sql);
        }

        //editar un correlativo de venta
        public function editar($id_correlativo,$descripcion,$max,$fecha_vigencia){
            $sql = "UPDATE correlativo_ventas SET descripcion = '$descripcion',max = '$max',fecha_vigencia = '$fecha_vigencia' WHERE id_correlativo = '$id_correlativo'";
            return ejecutarConsulta($sql);
        }

        //activar correlativo y desactivar el resto
        public function activar($id_corr){
            //desactivar todos los correlativos
            $sql1 = "UPDATE correlativo_ventas SET estado = 0";
            $res1 = ejecutarConsulta($sql1);
            if($res1){
                //luego se activa el correlativo con el $id_corr de parámetro
                $sql2 = "UPDATE correlativo_ventas SET estado = 1 WHERE id_correlativo = '$id_corr'";
                return ejecutarConsulta($sql2);
            }else
                return false;
        }

         //mostrar correlativo x
        public function mostrar($id_correlativo){
            $sql="SELECT cv.*
                FROM correlativo_ventas cv 

                WHERE cv.id_correlativo='$id_correlativo'";

            return ejecutarConsultaSimpleFila($sql);
        }

         //mostrar correlativo activo
         public function mostrar_activo(){
            $sql="SELECT cv.*
                FROM correlativo_ventas cv 

                WHERE cv.estado='1'";

            return ejecutarConsultaSimpleFila($sql);
        }

    }
