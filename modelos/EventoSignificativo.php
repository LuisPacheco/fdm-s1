<?php 
    date_default_timezone_set("America/La_Paz");
    //Incluímos inicialmente la conexión a la base de datos

    require "../config/Conexion.php";

    Class EventoSignificativo{

        //Implementamos nuestro constructor

        public function __construct(){
        }

        //Implementar un método para listar los registros

        public function listar($pv,$fecha_ev){
            $sql="SELECT es.*, pa.codigo_recepcion_paquete,pa.estado_paquete,pa.actual
                FROM evento_significativo AS es 
                INNER JOIN paquete AS pa on pa.id_evento_significativo = es.id_evento_significativo
                WHERE date(es.fecha_registro_evento) = '$fecha_ev' AND es.id_punto_venta = '$pv';";
            return ejecutarConsulta($sql);		
        }

        //registrar evento significativo en la base de datos
        public function insertar($codigo_evento,$descripcion_evento,$fecha_inicio_evento,$fecha_fin_evento,$fecha_registro_evento,$id_punto_venta,$id_cufd,$cafc){
            //estados 0 = iniciado, 1 = terminado
            $sql = "INSERT INTO evento_significativo (codigo_evento,descripcion_evento,cafc_evento,fecha_inicio_evento,fecha_fin_evento,fecha_registro_evento,id_punto_venta,id_cufd)
                    VALUES ('$codigo_evento','$descripcion_evento','$cafc','$fecha_inicio_evento','$fecha_fin_evento','$fecha_registro_evento','$id_punto_venta','$id_cufd')";
            return ejecutarConsulta($sql);
        }

        //obtener eventos activos para pv (evento activo tiene estado = 0)
        public function obtener_eventos_activos($id_pv){
            $sql = "SELECT es.* 
                    FROM evento_significativo AS es
                    WHERE es.estado_evento = '0' AND es.id_punto_venta = '$id_pv'
                    LIMIT 1";
            return ejecutarConsultaSimpleFila($sql);
        }

        //obtener eventos activos para pv (evento activo tiene estado = 0)
        public function obtener_eventos_activos2($id_pv,$estado){
            $sql = "SELECT es.* 
                    FROM evento_significativo AS es
                    WHERE es.estado_evento = '$estado' AND es.id_punto_venta = '$id_pv'
                    LIMIT 1";
            return ejecutarConsultaSimpleFila($sql);
        }

        //obtener último evento creado
        public function obtener_ultimo_evento(){
            $sql = "SELECT es.*
                    FROM evento_significativo AS es
                    ORDER BY es.id_evento_significativo DESC
                    LIMIT 1";
            return ejecutarConsultaSimpleFila($sql);
        }

         //obtener último evento creado
         public function obtener_ultimo_evento2($id_es){
            $sql = "SELECT es.*, c.codigo_cufd,c.codigo_control_cufd
                    FROM evento_significativo AS es
                    INNER JOIN cufd c ON c.id_cufd = es.id_cufd
                    WHERE id_evento_significativo = '$id_es'
                    ORDER BY es.id_evento_significativo DESC
                    LIMIT 1";
            return ejecutarConsultaSimpleFila($sql);
        }

        public function setColumna($columna,$valor,$id){
            $sql = "UPDATE evento_significativo 
                    SET $columna = '$valor'
                    WHERE id_evento_significativo = '$id'";
            return ejecutarConsulta($sql);
        }

        public function crear_paquete($nombre,$id_evento_significativo){
            $sql = "INSERT INTO paquete(nombre_paquete,id_evento_significativo) VALUES ('$nombre','$id_evento_significativo')";
            return ejecutarConsulta($sql);
        }

        public function obtener_ultimo_paquete_es($id_evento_significativo){
            $sql = "SELECT * FROM paquete WHERE id_evento_significativo = $id_evento_significativo ORDER BY id_paquete DESC LIMIT 1";
            return ejecutarConsultaSimpleFila($sql);
        }

        public function modificar_paquete($id_paquete,$nombre_paquete,$actual,$id_evento_significativo){
            $sql = "UPDATE paquete
                    SET nombre_paquete = '$nombre_paquete', actual = '$actual', id_evento_significativo = '$id_evento_significativo'
                    WHERE id_paquete = '$id_paquete'";
            return ejecutarConsulta($sql);
        }

        public function setColumnaPaquete($columna,$valor,$id){
            $sql = "UPDATE paquete SET $columna = '$valor' WHERE id_paquete = '$id'";
            return ejecutarConsulta($sql);
        }

        public function obtener_paquetes_es($id_evento_significativo){
            $sql = "SELECT * FROM paquete WHERE id_evento_significativo = $id_evento_significativo ORDER BY id_paquete";
            return ejecutarConsulta($sql);
        }

    }
