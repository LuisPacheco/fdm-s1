<?php 
//Incluímos inicialmente la conexión a la base de datos
date_default_timezone_set("America/La_Paz");
require "../config/Conexion.php";

Class CorrelativoCaja{
	//Implementamos nuestro constructor
	public function __construct(){
	}

	public function contar_correlativos_fecha(){
	    $hoy = date("Y-m-d");
		$sql="SELECT COUNT(*) as total 
			FROM correlativo_caja 
				where fecha = '$hoy'";
		return ejecutarConsulta($sql);		
	}

	public function crearRegistrosHoy($idusuario){
	    $hoy = date("Y-m-d");
		$nro_elementos = 0;
		$sw = true;
		while($nro_elementos < count($idusuario)){
			$sql = "INSERT INTO correlativo_caja (fecha,idusuario)
					VALUES ('$hoy','$idusuario[$nro_elementos]')";
			ejecutarConsulta($sql) or $sw = false;
			$nro_elementos++;
		}
		return $sw;
	}

	public function verificar_estado_caja_usuario($idusuario){
	    $hoy = date("Y-m-d");
		$sql = "SELECT estado 
				FROM correlativo_caja 
				WHERE idusuario = '$idusuario' and fecha = '$hoy'";
		return ejecutarConsulta($sql);		
	}

	public function aperturar_caja($idusuario,$obs_inicial,$monto_inicial,$turno){
	    if($turno != "" && $turno != NULL && $idusuario != "" && $idusuario != NULL){
			$hoy = date("Y-m-d H:i:s");
			$fecha = date("Y-m-d");
			$hora_actual = date("%H:%i:%s");
			if($monto_inicial == "" || $monto_inicial == null)
				$monto_inicial = 0;
			$sql = "UPDATE correlativo_caja 
					SET observaciones_inicio = '$obs_inicial', monto_inicial = '$monto_inicial', hora_apertura = DATE_FORMAT('$hoy', '$hora_actual'),turno = '$turno', estado = 'Caja Abierta' 
					WHERE idusuario = '$idusuario' AND fecha = '$fecha'";
			return ejecutarConsulta($sql);		
		}else
			return false;	
	}

	public function cerrar_caja($idusuario,$obs_final){
	    $hoy = date("Y-m-d H:i:s");
	    $fecha = date("Y-m-d");
	    $hora_actual = date("%H:%i:%s");
		$sql = "UPDATE correlativo_caja 
				SET observaciones_fin = '$obs_final', hora_cierre = DATE_FORMAT('$hoy', '$hora_actual'), estado = 'Caja Cerrada' 
				WHERE idusuario = '$idusuario' AND fecha = '$fecha'";
		return ejecutarConsulta($sql);		
	}

	public function cerrar_cajas_dias_anteriores(){
	    $hoy = date("Y-m-d");
		$sql = "UPDATE correlativo_caja 
				SET estado = 'Caja Cerrada'
				WHERE fecha != '$hoy'";
		return ejecutarConsulta($sql);		
	}

	public function obtener_monto_inicial($idusuario,$fecha){
		$sql = "SELECT monto_inicial 
				FROM correlativo_caja 
				WHERE idusuario = '$idusuario' AND date(fecha) = '$fecha'";
		return ejecutarConsulta($sql);
	}

	public function listar_cajas($fecha){
		$sql = "SELECT cj.id,cj.fecha,us.idusuario,us.nombre,us.login,cj.hora_apertura,cj.hora_cierre,cj.estado,cj.turno
				FROM correlativo_caja cj
				INNER JOIN usuario us ON us.idusuario = cj.idusuario
				WHERE cj.hora_apertura IS NOT NULL AND DATE(cj.fecha) = '$fecha' ";
		return ejecutarConsulta($sql);
	}

	public function mostrar_detalles_caja($id){
		$sql = "SELECT cj.id,cj.estado,cj.fecha,cj.monto_inicial,cj.observaciones_inicio,cj.observaciones_fin,cj.hora_apertura,cj.hora_cierre,cj.idusuario,us.login,us.nombre,cj.turno
				FROM correlativo_caja cj
				INNER JOIN usuario us ON us.idusuario = cj.idusuario
				WHERE cj.id = '$id'";
		return ejecutarConsultaSimpleFila($sql);
	}
}


?>