<?php 
	//Incluímos inicialmente la conexión a la base de datos
	require "../config/Conexion.php";

	Class GestionToken{
		//Implementamos nuestro constructor
		public function __construct(){
		}

		//Implementar un método para listar los registros
		public function listar(){
			$sql="SELECT * FROM registro_token ORDER BY fecha_caducidad_token DESC";
			return ejecutarConsulta($sql);		
		}

		//validar si token ingresado es correcto
		static public function validarToken($token_prueba){
			$wsdl = "https://pilotosiatservicios.impuestos.gob.bo/v2/FacturacionSincronizacion?wsdl";

			$opts = array(
				'http' => array(
					'header' => "apikey: TokenApi $token_prueba",
				)
			);

			$context = stream_context_create($opts);

			$client = new SoapClient($wsdl, [
				'stream_context' => $context,
			]);
			try{
				$responce_param = $client->__soapCall("verificarComunicacion", []);	
				return $responce_param->transaccion;
		
			}catch(Exception $e){
				return false;
			}
		}

		//Implementamos un método para insertar registros
		public function insertar($token,$token_hash,$fecha_registro,$fecha_caducidad){
			$sql="INSERT INTO registro_token (codigo_token,codigo_token_hash,fecha_registro_token,fecha_caducidad_token,estado_token)
			VALUES ('$token','$token_hash','$fecha_registro','$fecha_caducidad','1')";
			return ejecutarConsulta($sql);
		}

		//Implementamos un método para desactivar un token
		public function desactivar($id_token){
			$sql="UPDATE registro_token SET estado_token='0' WHERE id_token='$id_token'";
			return ejecutarConsulta($sql);
		}

		//Método para desactivar varios tokens
		public function desactivarTokens($fecha_caducidad){
			$sql="UPDATE registro_token SET estado_token='0' WHERE fecha_caducidad_token!='$fecha_caducidad'";
			return ejecutarConsulta($sql);
		}

		static public function getTokenInfo(){
			$sql = "SELECT * FROM registro_token WHERE estado_token = '1' LIMIT 1";
			return ejecutarConsultaSimpleFila($sql);
		}

		static public function verificar_comunicacion($token){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("" => array());
                $responce_param = $client->__soapCall('verificarComunicacion', ["parameters" => $request_param]);
                $respuesta = $responce_param;

				if(!$respuesta->return->transaccion)//SI NO EXISTE RESPUESTA, SIGNIFICA QUE NO SE PUEDE CONECTAR CON EL SIN Y POR TANTO SE ASIGNA EVENTO 2
					$_SESSION["modo_sistema"] = 2;
					//$_SESSION["modo_sistema"] = 0;
                return $respuesta->return->transaccion;
            }catch(Exception $e){
				$_SESSION["modo_sistema"] = 2;//SIN ACCESO A SIN
                return false;
            }
        }

		static public function verificar_comunicacion2($token){
            try{
                $opts = array(
                    'http' => array(
                        'header' => "apikey: TokenApi " . $token,
                    )
                );
            
                $context = stream_context_create($opts);
            
                $client = new SoapClient(FACTURA_COMPRA_VENTA, [
                    'stream_context' => $context,
                ]);
            
                $request_param = array("" => array());
                $responce_param = $client->__soapCall('verificarComunicacion', ["parameters" => $request_param]);
                $respuesta = $responce_param;

				if(is_object($respuesta))
                	return $respuesta->return->transaccion;
				else
					return false;
            }catch(Exception $e){
                return false;
            }
        }

		static public function verificar_conexion_internet(){
			$connected = @fsockopen("google.com", 80); 
			if ($connected){
				$is_conn = true; //Conectado
				fclose($connected);
			}else{
				$is_conn = false; //No conectado
			}

			if(!$is_conn)
				$_SESSION["modo_sistema"] = 1;//SIN INTERNET

			return $is_conn;
		}

		static public function verificar_conexion_internet2(){
			$connected = @fsockopen("google.com", 80); 
			if ($connected){
				$is_conn = true; //Conectado
				fclose($connected);
			}else
				$is_conn = false; //No conectado
			
			return $is_conn;
		}
	}