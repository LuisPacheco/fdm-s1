<?php 
	//Incluímos inicialmente la conexión a la base de datos
	require "../config/Conexion.php";
	date_default_timezone_set("America/La_Paz");

	Class PuntoVenta{

		//Implementamos nuestro constructor
		public function __construct(){
		}

		//Implementar un método para listar los registros
		public function listar(){
			$sql = 'SELECT punto_venta.*,cuis.*,cufd.*,DATE_FORMAT(cuis.fecha_vigencia_cuis,"%Y-%m-%d %T") AS fecha_hora, DATE_FORMAT(cufd.fecha_vigencia_cufd,"%Y-%m-%d %T") AS fecha_hora_cufd  
					FROM punto_venta 
					LEFT JOIN cuis ON cuis.id_punto_venta_cuis = punto_venta.id_punto_venta
					LEFT JOIN cufd ON cufd.id_punto_venta_cufd = punto_venta.id_punto_venta
					ORDER BY punto_venta.codigoPuntoVenta;';
			return ejecutarConsulta($sql);		
		}

		public function listar2(){
			$sql = 'SELECT punto_venta.* 
					FROM punto_venta 
					ORDER BY punto_venta.codigoPuntoVenta;';
			return ejecutarConsulta($sql);
		}

		public function listar3(){//PUNTOS DE VENTA EN USO
			$sql = 'SELECT punto_venta.* 
					FROM punto_venta
					WHERE enUso = "0" 
					ORDER BY punto_venta.codigoPuntoVenta;';
			return ejecutarConsulta($sql);
		}

		public function buscar_cuis_activo($id_pv){
			$sql = 'SELECT cuis.*,DATE_FORMAT(cuis.fecha_vigencia_cuis,"%Y-%m-%d %T") AS fecha_hora 
					FROM cuis 
					WHERE cuis.id_punto_venta_cuis = '.$id_pv.' AND estado_cuis = 1 
					ORDER BY fecha_hora DESC
					LIMIT 1;';
			return ejecutarConsultaSimpleFila($sql);
		}

		public function buscar_cufd_activo($id_pv){
			$sql = 'SELECT cufd.*,DATE_FORMAT(cufd.fecha_vigencia_cufd,"%Y-%m-%d %T") AS fecha_hora 
					FROM cufd 
					WHERE cufd.id_punto_venta_cufd = '.$id_pv.' AND estado_cufd = 1 
					ORDER BY fecha_hora DESC
					LIMIT 1;';
			return ejecutarConsultaSimpleFila($sql);
		}

		//Obtener los datos de un punto de venta por el código de PV
		public function getPuntoVenta($codPV){
			$sql = "SELECT *
					FROM punto_venta 
					WHERE codigoPuntoVenta = '$codPV';";
			return ejecutarConsultaSimpleFila($sql);
		}

		//Obtener los datos de un punto de venta por el id
		public function getPuntoVenta2($id_pv){
			$sql = "SELECT *
					FROM punto_venta 
					WHERE id_punto_venta = '$id_pv';";
			return ejecutarConsultaSimpleFila($sql);
		}

		//Implementamos un método para insertar registros
		public function insertar($codigoPuntoVenta,$nombrePuntoVenta,$tipoPuntoVenta ){
			$sql="INSERT INTO punto_venta (codigoPuntoVenta,nombrePuntoVenta,tipoPuntoVenta)
					VALUES ('$codigoPuntoVenta','$nombrePuntoVenta','$tipoPuntoVenta')";
			return ejecutarConsulta($sql);
		}

		//Verificar si el nombre del punto de venta es repetido o no
		public function verificar_nombre_pv($nombre_pv){
			$sql = "SELECT COUNT(nombrePuntoVenta ) AS cant_total
						FROM punto_venta
						WHERE nombrePuntoVenta  = '$nombre_pv'";
			$res = ejecutarConsultaSimpleFila($sql);
			$tot = $res["cant_total"];
			return $tot;
		}

		//listar los CUIS
		public function listar_cuis_activos(){
			$sql = "SELECT *,DATE_FORMAT(cuis.fecha_vigencia_cuis,'%Y-%m-%d %T') AS fecha_hora  FROM cuis WHERE estado_cuis = 1";
			return ejecutarConsulta($sql);
		}

		//actualizar estado CUIS 
		public function actualizar_cuis_estado($estado,$id_cuis){
			$sql = "UPDATE cuis SET estado_cuis = '$estado' WHERE id_cuis = '$id_cuis'";
			return ejecutarConsulta($sql);
		}

		//crear CUIS nuevo
		public function registrar_cuis($codigo_cuis,$fecha_vigencia,$codigo_pv){
			//obtener el pv que tenga el codigo_pv enviado como argumento
			$sql_pv = "SELECT * FROM punto_venta WHERE codigoPuntoVenta = '$codigo_pv';";
			$res_sql_pv = ejecutarConsultaSimpleFila($sql_pv);
			if($res_sql_pv != NULL && $res_sql_pv){
				//anular (poner 0) los CUIS existentes que correspondan al pv 
				$id_punto_venta = $res_sql_pv['id_punto_venta'];
				$sql1 = "UPDATE cuis SET estado_cuis = 0 WHERE id_punto_venta_cuis = '$id_punto_venta';";
				$res_sql1 = ejecutarConsulta($sql1);
				if($res_sql1 && $res_sql1 != NULL){
					//agregar el cuis nuevo asignándole al pv
					$sql2 = "INSERT INTO cuis (codigo_cuis,fecha_vigencia_cuis,estado_cuis,id_punto_venta_cuis) VALUES('$codigo_cuis','$fecha_vigencia','1','$id_punto_venta');";
					$res_sql2 = ejecutarConsulta($sql2);
					if($res_sql2 && $res_sql2 != NULL)
						return true;
					else
						return false;
				}else
					return false;
			}else
				return false;
		}


		//listar los CUFD activos
		public function listar_cufd_activos(){
			$sql = "SELECT *,DATE_FORMAT(cufd.fecha_vigencia_cufd,'%Y-%m-%d %T') AS fecha_hora  FROM cufd WHERE estado_cufd = 1";
			return ejecutarConsulta($sql);
		}

		//actualizar estado CUFD 
		public function actualizar_cufd_estado($estado,$id_cufd){
			$sql = "UPDATE cufd SET estado_cufd = '$estado' WHERE id_cufd = '$id_cufd'";
			return ejecutarConsulta($sql);
		}

		//crear CUFD nuevo en BBDD
		public function registrar_cufd($codigo_cufd,$codigo_control_cufd,$direccion_cufd,$fecha_vigencia_cufd,$codigo_pv){
			//obtener el pv que tenga el codigo_pv enviado como argumento
			$fecha = date("Y-m-d H:i:s");
			$sql_pv = "SELECT * 
						FROM punto_venta 
						WHERE codigoPuntoVenta = '$codigo_pv';";

			$res_sql_pv = ejecutarConsultaSimpleFila($sql_pv);

			if($res_sql_pv != NULL && $res_sql_pv){
				//anular (poner 0) los CUFD existentes que correspondan al pv 
				$id_punto_venta = $res_sql_pv['id_punto_venta'];
				$sql1 = "UPDATE cufd 
						SET estado_cufd = 0 
						WHERE id_punto_venta_cufd = '$id_punto_venta';";

				$res_sql1 = ejecutarConsulta($sql1);
				if($res_sql1 && $res_sql1 != NULL){
					//agregar el cufd nuevo asignándole al pv
					$sql2 = "INSERT INTO cufd (codigo_cufd,codigo_control_cufd,direccion_cufd,fecha_registro_cufd,fecha_vigencia_cufd,estado_cufd,id_punto_venta_cufd) 
							VALUES('$codigo_cufd','$codigo_control_cufd','$direccion_cufd','$fecha','$fecha_vigencia_cufd','1','$id_punto_venta');";
					$res_sql2 = ejecutarConsulta($sql2);

					if($res_sql2 && $res_sql2 != NULL)
						return true;
					else
						return false;
						
				}else
					return false;
			}else
				return false;
		}

		//actualizar estado de PV
		public function actualizar_estado_pv($cod_pv,$estado){
			$sql = "UPDATE punto_venta SET estadoPuntoVenta = '$estado' WHERE codigoPuntoVenta = '$cod_pv'";
			return ejecutarConsulta($sql);
		}

		//Contar nro de pv disponibles (enUso = 0)
		public function contar_pv_disponibles(){
			$sql = "SELECT COUNT(enUso) AS disponibles
					FROM punto_venta
					WHERE enUso = '0'";
			return ejecutarConsultaSimpleFila($sql);
		}


		//Implementar un método para mostrar el cufd
		public function mostrar_cufd($id_cufd){
			$sql="SELECT * 
				FROM cufd 
				WHERE id_cufd='$id_cufd'";

			return ejecutarConsultaSimpleFila($sql);

		}

		public function mostrar_cufd_fecha($fecha_inicio_es,$id_pv){
			$sql = "SELECT * 
					FROM cufd
					WHERE DATE_FORMAT(fecha_vigencia_cufd,'%Y-%m-%d %H:%i:%s') > '$fecha_inicio_es' AND DATE_FORMAT(fecha_registro_cufd,'%Y-%m-%d %H:%i:%s') < '$fecha_inicio_es'  AND id_punto_venta_cufd = '$id_pv'
					ORDER BY DATE_FORMAT(fecha_vigencia_cufd,'%Y-%m-%d %H:%i:%s') DESC
					LIMIT 1";

					//DESC EN VES DE ASC
			return ejecutarConsultaSimpleFila($sql);
		}

		public function setColumna($columna,$dato,$id_punto_venta){
			$sql="UPDATE punto_venta SET $columna = '$dato' WHERE id_punto_venta='$id_punto_venta'";
			return ejecutarConsulta($sql);
		}

		public function setColumna2($columna,$dato){
			$sql="UPDATE punto_venta SET $columna = '$dato'";
			return ejecutarConsulta($sql);
		}

	}