<?php 
date_default_timezone_set("America/La_Paz");

//Incluímos inicialmente la conexión a la base de datos

require "../config/Conexion.php";



Class Entrada{

	//Implementamos nuestro constructor

	public function __construct(){

	}



	//Implementamos un método para insertar registros

	public function insertar($idusuario,$num_comprobante,$sucursal,$fecha_hora,$idarticulo,$cantidad,$cod_lote,$vencimiento,$idlote){
	    
	    $hoy = date("Y-m-d");
		

		$sql="INSERT INTO entrada (idusuario,sucursal,num_comprobante,fecha_hora,estado)

				VALUES ('$idusuario','$sucursal','$num_comprobante','$fecha_hora','Aceptado')";

		//return ejecutarConsulta($sql);

		$identradanew = ejecutarConsulta_retornarID($sql);



		$num_elementos = 0;

		$sw = true;

		$sw2 = true;

		$sw3 = true;

		require_once "../modelos/Lote.php";

		$lote = new Lote();



		require_once "../modelos/Articulo.php";

		$articulo = new Articulo();



		while ($num_elementos < count($idarticulo)){

			//VERIFICAR SI ES NUEVO LOTE

			if($idlote[$num_elementos] == "nuevo"){

				//VERIFICA ESTADO DE LA FECHA DE VENCIMIENTO DEL LOTE QUE SE ESTÁ INGRESANDO

				$estado = "";

				if($cantidad[$num_elementos] == 0)

					$estado = "Agotado";

				else{

					if($vencimiento[$num_elementos] != "" && $vencimiento[$num_elementos] != NULL){

						date_default_timezone_set("America/La_Paz");

						$fecha_vencimiento = new DateTime($vencimiento[$num_elementos]);

						$actual = date("Y-m-d");

						$fecha = new DateTime($actual);

						$res = $fecha->diff($fecha_vencimiento);

						$dias = $res->days;

						if($dias >= 180 && $res->invert == 0)

							$estado = "Activo (6+)";

						else{

							if ($dias > 90 && $dias < 180 && $res->invert == 0) 

								$estado = "Activo (3+)";

							else{

								if ($dias <= 90 && $res->invert == 0) 

									$estado = "Por vencerse (3-)";

								else

									$estado = "Vencido";

							}

						}

					}else{

						$estado = "n/a";

					}

				}

				//VERIFICA SI LA FECHA DE VENCIMIENTO INTRODUCIDA TIENE O NO CONTENIDO PARA LUEGO INSERTAR EN LA TABLA LOTE

				if($vencimiento[$num_elementos] == "" || $vencimiento[$num_elementos] == NULL){

					$sql_insert_lote = "INSERT INTO lote (cod_lote,vencimiento,fecha_registro,stock,estado) 

										VALUES('$cod_lote[$num_elementos]',NULL, '$hoy','$cantidad[$num_elementos]','$estado')";

				}else{

					$sql_insert_lote = "INSERT INTO lote (cod_lote,vencimiento,fecha_registro,stock,estado) 

												VALUES('$cod_lote[$num_elementos]','$vencimiento[$num_elementos]', '$hoy','$cantidad[$num_elementos]','$estado')";

				}



				$idlote_nuevo = ejecutarConsulta_retornarID($sql_insert_lote);



				$sql_detalle = "INSERT INTO detalle_entrada(identrada, idarticulo,cantidad,idlote) 

										VALUES ('$identradanew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$idlote_nuevo')";

				$rspta = ejecutarConsulta($sql_detalle);

				

			}else{//SI EL LOTE YA EXISTE

				$sql_stock_lote = $lote->getStock($idlote[$num_elementos]);

				$stock_lote = $sql_stock_lote->fetch_object();

				$nuevo_stock = $stock_lote->stock + $cantidad[$num_elementos];

				$rspta2 = $lote->setStock($idlote[$num_elementos],$nuevo_stock);

				$sql_detalle = "INSERT INTO detalle_entrada(identrada, idarticulo,cantidad,idlote) 

										VALUES ('$identradanew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$idlote[$num_elementos]')";



				$rspta = ejecutarConsulta($sql_detalle);



				if(!$rspta2)

					$sw2 = false;



			}

			if (!$rspta)

				$sw = false;



			if($rspta){

				$sql_stock_art = $articulo->devolverStock($idarticulo[$num_elementos]);

				$stock_art = $sql_stock_art->fetch_object();

				$stock_actual = $stock_art->stock + $cantidad[$num_elementos];

				$actualizar_stock = $articulo->modificarStock($idarticulo[$num_elementos],$stock_actual);

				if(!$actualizar_stock)

					$sw3 = false;

			}

			

			$num_elementos = $num_elementos + 1;

		}



		return $sw && $sw2 && $sw3;

	}



	

	//Implementamos un método para anular categorías

	public function anular($identrada){

		$sql="UPDATE entrada SET estado='Anulado' WHERE identrada='$identrada'";

		return ejecutarConsulta($sql);

	}





	//Implementar un método para mostrar los datos de un registro a modificar

	public function mostrar($identrada){

		$sql="SELECT i.identrada,DATE(i.fecha_hora) as fecha,u.idusuario,u.nombre as usuario,i.num_comprobante,i.estado, i.sucursal

			FROM entrada i

			INNER JOIN usuario u ON i.idusuario=u.idusuario 

			WHERE i.identrada='$identrada'";

		return ejecutarConsultaSimpleFila($sql);

	}



	public function listarDetalle($identrada){

		$sql = 'SELECT a.nombre, di.cantidad, lo.cod_lote, CONCAT(MONTH(lo.vencimiento), "/", YEAR(lo.vencimiento)) as vencimiento,di.idarticulo,lo.idlote,lo.estado

				FROM detalle_entrada di 

				inner join articulo a on di.idarticulo=a.idarticulo

				inner join lote lo ON di.idlote = lo.idlote

				where di.identrada =  ' . $identrada;

		return ejecutarConsulta($sql);

	}



	//Implementar un método para listar los registros

	public function listar($sucursal){

		if($sucursal == "ambas"){

			$sql = "SELECT i.identrada,i.fecha_hora as fecha, u.idusuario, u.nombre as usuario,i.num_comprobante, i.estado 

			FROM entrada i 

			INNER JOIN usuario u ON i.idusuario = u.idusuario 

			ORDER BY i.identrada desc";

		}else{

			$sql = "SELECT i.identrada,i.fecha_hora as fecha, u.idusuario, u.nombre as usuario,i.num_comprobante, i.estado

			FROM entrada i 

			INNER JOIN usuario u ON i.idusuario = u.idusuario 

			WHERE sucursal = '$sucursal'

			ORDER BY i.identrada desc";

		}

		

		return ejecutarConsulta($sql);		

	}

	

	public function entradacabecera($identrada){

		$sql="SELECT i.identrada,i.idusuario,u.nombre as usuario,i.num_comprobante,i.fecha_hora as fecha, i.sucursal ,DAY(i.fecha_hora) as dia,MONTH(i.fecha_hora) as mes,YEAR(i.fecha_hora) as anio,HOUR(i.fecha_hora) as hora,MINUTE(i.fecha_hora) as minuto, SECOND(i.fecha_hora) as segundo

		 FROM entrada i

		 INNER JOIN usuario u ON i.idusuario=u.idusuario 

		 WHERE i.identrada='$identrada'";

		return ejecutarConsulta($sql);

	}



	public function entradadetalle($identrada){

		$sql="SELECT a.nombre as articulo,d.cantidad, lo.cod_lote,lo.vencimiento,a.cod_med,la.nombre as laboratorio

			FROM detalle_entrada d 

            INNER JOIN lote lo ON lo.idlote = d.idlote

			INNER JOIN articulo a ON d.idarticulo=a.idarticulo 

			INNER JOIN laboratorio la ON la.idlaboratorio = a.idlaboratorio

			WHERE d.identrada='$identrada'";

		return ejecutarConsulta($sql);

	}

}



?>